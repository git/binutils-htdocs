<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>ar cmdline (GNU Binary Utilities)</title>

<meta name="description" content="ar cmdline (GNU Binary Utilities)">
<meta name="keywords" content="ar cmdline (GNU Binary Utilities)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Binutils-Index.html" rel="index" title="Binutils Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="ar.html" rel="up" title="ar">
<link href="ar-scripts.html" rel="next" title="ar scripts">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="ar-cmdline">
<div class="nav-panel">
<p>
Next: <a href="ar-scripts.html" accesskey="n" rel="next">Controlling <code class="command">ar</code> with a Script</a>, Up: <a href="ar.html" accesskey="u" rel="up">ar</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Controlling-ar-on-the-Command-Line">1.1 Controlling <code class="command">ar</code> on the Command Line</h3>

<div class="example smallexample">
<pre class="example-preformatted">ar [<samp class="option">-X32_64</samp>] [<samp class="option">-</samp>]<var class="var">p</var>[<var class="var">mod</var>] [<samp class="option">--plugin</samp> <var class="var">name</var>] [<samp class="option">--target</samp> <var class="var">bfdname</var>] [<samp class="option">--output</samp> <var class="var">dirname</var>] [<samp class="option">--record-libdeps</samp> <var class="var">libdeps</var>] [<samp class="option">--thin</samp>] [<var class="var">relpos</var>] [<var class="var">count</var>] <var class="var">archive</var> [<var class="var">member</var>&hellip;]
</pre></div>

<a class="index-entry-id" id="index-Unix-compatibility_002c-ar"></a>
<p>When you use <code class="command">ar</code> in the Unix style, <code class="command">ar</code> insists on at least two
arguments to execute: one keyletter specifying the <em class="emph">operation</em>
(optionally accompanied by other keyletters specifying
<em class="emph">modifiers</em>), and the archive name to act on.
</p>
<p>Most operations can also accept further <var class="var">member</var> arguments,
specifying particular files to operate on.
</p>

<p><small class="sc">GNU</small> <code class="command">ar</code> allows you to mix the operation code <var class="var">p</var> and modifier
flags <var class="var">mod</var> in any order, within the first command-line argument.
</p>
<p>If you wish, you may begin the first command-line argument with a
dash.
</p>
<a class="index-entry-id" id="index-operations-on-archive"></a>
<p>The <var class="var">p</var> keyletter specifies what operation to execute; it may be
any of the following, but you must specify only one of them:
</p>
<dl class="table">
<dt id='index-deleting-from-archive'><span>&lsquo;<samp class="samp">d</samp>&rsquo;<a class="copiable-link" href='#index-deleting-from-archive'> &para;</a></span></dt>
<dd><p><em class="emph">Delete</em> modules from the archive.  Specify the names of modules to
be deleted as <var class="var">member</var>&hellip;; the archive is untouched if you
specify no files to delete.
</p>
<p>If you specify the &lsquo;<samp class="samp">v</samp>&rsquo; modifier, <code class="command">ar</code> lists each module
as it is deleted.
</p>
</dd>
<dt id='index-moving-in-archive'><span>&lsquo;<samp class="samp">m</samp>&rsquo;<a class="copiable-link" href='#index-moving-in-archive'> &para;</a></span></dt>
<dd><p>Use this operation to <em class="emph">move</em> members in an archive.
</p>
<p>The ordering of members in an archive can make a difference in how
programs are linked using the library, if a symbol is defined in more
than one member.
</p>
<p>If no modifiers are used with <code class="code">m</code>, any members you name in the
<var class="var">member</var> arguments are moved to the <em class="emph">end</em> of the archive;
you can use the &lsquo;<samp class="samp">a</samp>&rsquo;, &lsquo;<samp class="samp">b</samp>&rsquo;, or &lsquo;<samp class="samp">i</samp>&rsquo; modifiers to move them to a
specified place instead.
</p>
</dd>
<dt id='index-printing-from-archive'><span>&lsquo;<samp class="samp">p</samp>&rsquo;<a class="copiable-link" href='#index-printing-from-archive'> &para;</a></span></dt>
<dd><p><em class="emph">Print</em> the specified members of the archive, to the standard
output file.  If the &lsquo;<samp class="samp">v</samp>&rsquo; modifier is specified, show the member
name before copying its contents to standard output.
</p>
<p>If you specify no <var class="var">member</var> arguments, all the files in the archive are
printed.
</p>
</dd>
<dt id='index-quick-append-to-archive'><span>&lsquo;<samp class="samp">q</samp>&rsquo;<a class="copiable-link" href='#index-quick-append-to-archive'> &para;</a></span></dt>
<dd><p><em class="emph">Quick append</em>; Historically, add the files <var class="var">member</var>&hellip; to the end of
<var class="var">archive</var>, without checking for replacement.
</p>
<p>The modifiers &lsquo;<samp class="samp">a</samp>&rsquo;, &lsquo;<samp class="samp">b</samp>&rsquo;, and &lsquo;<samp class="samp">i</samp>&rsquo; do <em class="emph">not</em> affect this
operation; new members are always placed at the end of the archive.
</p>
<p>The modifier &lsquo;<samp class="samp">v</samp>&rsquo; makes <code class="command">ar</code> list each file as it is appended.
</p>
<p>Since the point of this operation is speed, implementations of
<code class="command">ar</code> have the option of not updating the archive&rsquo;s symbol
table if one exists.  Too many different systems however assume that
symbol tables are always up-to-date, so <small class="sc">GNU</small> <code class="command">ar</code> will
rebuild the table even with a quick append.
</p>
<p>Note - <small class="sc">GNU</small> <code class="command">ar</code> treats the command &lsquo;<samp class="samp">qs</samp>&rsquo; as a
synonym for &lsquo;<samp class="samp">r</samp>&rsquo; - replacing already existing files in the
archive and appending new ones at the end.
</p>
</dd>
<dt id='index-replacement-in-archive'><span>&lsquo;<samp class="samp">r</samp>&rsquo;<a class="copiable-link" href='#index-replacement-in-archive'> &para;</a></span></dt>
<dd><p>Insert the files <var class="var">member</var>&hellip; into <var class="var">archive</var> (with
<em class="emph">replacement</em>). This operation differs from &lsquo;<samp class="samp">q</samp>&rsquo; in that any
previously existing members are deleted if their names match those being
added.
</p>
<p>If one of the files named in <var class="var">member</var>&hellip; does not exist, <code class="command">ar</code>
displays an error message, and leaves undisturbed any existing members
of the archive matching that name.
</p>
<p>By default, new members are added at the end of the file; but you may
use one of the modifiers &lsquo;<samp class="samp">a</samp>&rsquo;, &lsquo;<samp class="samp">b</samp>&rsquo;, or &lsquo;<samp class="samp">i</samp>&rsquo; to request
placement relative to some existing member.
</p>
<p>The modifier &lsquo;<samp class="samp">v</samp>&rsquo; used with this operation elicits a line of
output for each file inserted, along with one of the letters &lsquo;<samp class="samp">a</samp>&rsquo; or
&lsquo;<samp class="samp">r</samp>&rsquo; to indicate whether the file was appended (no old member
deleted) or replaced.
</p>
</dd>
<dt id='index-ranlib'><span>&lsquo;<samp class="samp">s</samp>&rsquo;<a class="copiable-link" href='#index-ranlib'> &para;</a></span></dt>
<dd><p>Add an index to the archive, or update it if it already exists.  Note
this command is an exception to the rule that there can only be one
command letter, as it is possible to use it as either a command or a
modifier.  In either case it does the same thing.
</p>
</dd>
<dt id='index-contents-of-archive'><span>&lsquo;<samp class="samp">t</samp>&rsquo;<a class="copiable-link" href='#index-contents-of-archive'> &para;</a></span></dt>
<dd><p>Display a <em class="emph">table</em> listing the contents of <var class="var">archive</var>, or those
of the files listed in <var class="var">member</var>&hellip; that are present in the
archive.  Normally only the member name is shown, but if the modifier
&lsquo;<samp class="samp">O</samp>&rsquo; is specified, then the corresponding offset of the member is also
displayed.  Finally, in order to see the modes (permissions), timestamp,
owner, group, and size the &lsquo;<samp class="samp">v</samp>&rsquo; modifier should be included.
</p>
<p>If you do not specify a <var class="var">member</var>, all files in the archive
are listed.
</p>
<a class="index-entry-id" id="index-repeated-names-in-archive"></a>
<a class="index-entry-id" id="index-name-duplication-in-archive"></a>
<p>If there is more than one file with the same name (say, &lsquo;<samp class="samp">fie</samp>&rsquo;) in
an archive (say &lsquo;<samp class="samp">b.a</samp>&rsquo;), &lsquo;<samp class="samp">ar t b.a fie</samp>&rsquo; lists only the
first instance; to see them all, you must ask for a complete
listing&mdash;in our example, &lsquo;<samp class="samp">ar t b.a</samp>&rsquo;.
</p>
</dd>
<dt id='index-extract-from-archive'><span>&lsquo;<samp class="samp">x</samp>&rsquo;<a class="copiable-link" href='#index-extract-from-archive'> &para;</a></span></dt>
<dd><p><em class="emph">Extract</em> members (named <var class="var">member</var>) from the archive.  You can
use the &lsquo;<samp class="samp">v</samp>&rsquo; modifier with this operation, to request that
<code class="command">ar</code> list each name as it extracts it.
</p>
<p>If you do not specify a <var class="var">member</var>, all files in the archive
are extracted.
</p>
<p>Files cannot be extracted from a thin archive, and there are
restrictions on extracting from archives created with <samp class="option">P</samp>: The
paths must not be absolute, may not contain <code class="code">..</code>, and any
subdirectories in the paths must exist.  If it is desired to avoid
these restrictions then used the <samp class="option">--output</samp> option to specify
an output directory.
</p></dd>
</dl>

<p>A number of modifiers (<var class="var">mod</var>) may immediately follow the <var class="var">p</var>
keyletter, to specify variations on an operation&rsquo;s behavior:
</p>
<dl class="table">
<dt id='index-relative-placement-in-archive'><span>&lsquo;<samp class="samp">a</samp>&rsquo;<a class="copiable-link" href='#index-relative-placement-in-archive'> &para;</a></span></dt>
<dd><p>Add new files <em class="emph">after</em> an existing member of the
archive.  If you use the modifier &lsquo;<samp class="samp">a</samp>&rsquo;, the name of an existing archive
member must be present as the <var class="var">relpos</var> argument, before the
<var class="var">archive</var> specification.
</p>
</dd>
<dt>&lsquo;<samp class="samp">b</samp>&rsquo;</dt>
<dd><p>Add new files <em class="emph">before</em> an existing member of the
archive.  If you use the modifier &lsquo;<samp class="samp">b</samp>&rsquo;, the name of an existing archive
member must be present as the <var class="var">relpos</var> argument, before the
<var class="var">archive</var> specification.  (same as &lsquo;<samp class="samp">i</samp>&rsquo;).
</p>
</dd>
<dt id='index-creating-archives'><span>&lsquo;<samp class="samp">c</samp>&rsquo;<a class="copiable-link" href='#index-creating-archives'> &para;</a></span></dt>
<dd><p><em class="emph">Create</em> the archive.  The specified <var class="var">archive</var> is always
created if it did not exist, when you request an update.  But a warning is
issued unless you specify in advance that you expect to create it, by
using this modifier.
</p>
</dd>
<dt id='index-deterministic-archives'><span>&lsquo;<samp class="samp">D</samp>&rsquo;<a class="copiable-link" href='#index-deterministic-archives'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-_002d_002denable_002ddeterministic_002darchives"></a>
<p>Operate in <em class="emph">deterministic</em> mode.  When adding files and the archive
index use zero for UIDs, GIDs, timestamps, and use consistent file modes
for all files.  When this option is used, if <code class="command">ar</code> is used with
identical options and identical input files, multiple runs will create
identical output files regardless of the input files&rsquo; owners, groups,
file modes, or modification times.
</p>
<p>If <samp class="file">binutils</samp> was configured with
<samp class="option">--enable-deterministic-archives</samp>, then this mode is on by default.
It can be disabled with the &lsquo;<samp class="samp">U</samp>&rsquo; modifier, below.
</p>
</dd>
<dt>&lsquo;<samp class="samp">f</samp>&rsquo;</dt>
<dd><p>Truncate names in the archive.  <small class="sc">GNU</small> <code class="command">ar</code> will normally permit file
names of any length.  This will cause it to create archives which are
not compatible with the native <code class="command">ar</code> program on some systems.  If
this is a concern, the &lsquo;<samp class="samp">f</samp>&rsquo; modifier may be used to truncate file
names when putting them in the archive.
</p>
</dd>
<dt>&lsquo;<samp class="samp">i</samp>&rsquo;</dt>
<dd><p>Insert new files <em class="emph">before</em> an existing member of the
archive.  If you use the modifier &lsquo;<samp class="samp">i</samp>&rsquo;, the name of an existing archive
member must be present as the <var class="var">relpos</var> argument, before the
<var class="var">archive</var> specification.  (same as &lsquo;<samp class="samp">b</samp>&rsquo;).
</p>
</dd>
<dt>&lsquo;<samp class="samp">l</samp>&rsquo;</dt>
<dd><p>Specify dependencies of this library.  The dependencies must immediately
follow this option character, must use the same syntax as the linker
command line, and must be specified within a single argument.  I.e., if
multiple items are needed, they must be quoted to form a single command
line argument.  For example &lsquo;<samp class="samp">L &quot;-L/usr/local/lib -lmydep1 -lmydep2&quot;</samp>&rsquo;
</p>
</dd>
<dt>&lsquo;<samp class="samp">N</samp>&rsquo;</dt>
<dd><p>Uses the <var class="var">count</var> parameter.  This is used if there are multiple
entries in the archive with the same name.  Extract or delete instance
<var class="var">count</var> of the given name from the archive.
</p>
</dd>
<dt id='index-dates-in-archive'><span>&lsquo;<samp class="samp">o</samp>&rsquo;<a class="copiable-link" href='#index-dates-in-archive'> &para;</a></span></dt>
<dd><p>Preserve the <em class="emph">original</em> dates of members when extracting them.  If
you do not specify this modifier, files extracted from the archive
are stamped with the time of extraction.
</p>
</dd>
<dt id='index-offsets-of-files'><span>&lsquo;<samp class="samp">O</samp>&rsquo;<a class="copiable-link" href='#index-offsets-of-files'> &para;</a></span></dt>
<dd><p>Display member offsets inside the archive. Use together with the &lsquo;<samp class="samp">t</samp>&rsquo;
option.
</p>
</dd>
<dt>&lsquo;<samp class="samp">P</samp>&rsquo;</dt>
<dd><p>Use the full path name when matching or storing names in the archive.
Archives created with full path names are not POSIX compliant, and
thus may not work with tools other than up to date <small class="sc">GNU</small> tools.
Modifying such archives with <small class="sc">GNU</small> <code class="command">ar</code> without using
<samp class="option">P</samp> will remove the full path names unless the archive is a
thin archive.  Note that <samp class="option">P</samp> may be useful when adding files to
a thin archive since <samp class="option">r</samp> without <samp class="option">P</samp> ignores the path
when choosing which element to replace.  Thus
</p><div class="example smallexample">
<pre class="example-preformatted">ar rcST archive.a subdir/file1 subdir/file2 file1
</pre></div>
<p>will result in the first <code class="code">subdir/file1</code> being replaced with
<code class="code">file1</code> from the current directory.  Adding <samp class="option">P</samp> will
prevent this replacement.
</p>
</dd>
<dt id='index-writing-archive-index'><span>&lsquo;<samp class="samp">s</samp>&rsquo;<a class="copiable-link" href='#index-writing-archive-index'> &para;</a></span></dt>
<dd><p>Write an object-file index into the archive, or update an existing one,
even if no other change is made to the archive.  You may use this modifier
flag either with any operation, or alone.  Running &lsquo;<samp class="samp">ar s</samp>&rsquo; on an
archive is equivalent to running &lsquo;<samp class="samp">ranlib</samp>&rsquo; on it.
</p>
</dd>
<dt id='index-not-writing-archive-index'><span>&lsquo;<samp class="samp">S</samp>&rsquo;<a class="copiable-link" href='#index-not-writing-archive-index'> &para;</a></span></dt>
<dd><p>Do not generate an archive symbol table.  This can speed up building a
large library in several steps.  The resulting archive can not be used
with the linker.  In order to build a symbol table, you must omit the
&lsquo;<samp class="samp">S</samp>&rsquo; modifier on the last execution of &lsquo;<samp class="samp">ar</samp>&rsquo;, or you must run
&lsquo;<samp class="samp">ranlib</samp>&rsquo; on the archive.
</p>
</dd>
<dt>&lsquo;<samp class="samp">T</samp>&rsquo;</dt>
<dd><p>Deprecated alias for <samp class="option">--thin</samp>.  <samp class="option">T</samp> is not recommended because in
many ar implementations <samp class="option">T</samp> has a different meaning, as specified by
X/Open System Interface.
</p>
</dd>
<dt id='index-updating-an-archive'><span>&lsquo;<samp class="samp">u</samp>&rsquo;<a class="copiable-link" href='#index-updating-an-archive'> &para;</a></span></dt>
<dd><p>Normally, &lsquo;<samp class="samp">ar r</samp>&rsquo;&hellip; inserts all files
listed into the archive.  If you would like to insert <em class="emph">only</em> those
of the files you list that are newer than existing members of the same
names, use this modifier.  The &lsquo;<samp class="samp">u</samp>&rsquo; modifier is allowed only for the
operation &lsquo;<samp class="samp">r</samp>&rsquo; (replace).  In particular, the combination &lsquo;<samp class="samp">qu</samp>&rsquo; is
not allowed, since checking the timestamps would lose any speed
advantage from the operation &lsquo;<samp class="samp">q</samp>&rsquo;.
</p>
<p>Note - if an archive has been created in a deterministic manner, eg
via the use of the <samp class="option">D</samp> modifier, then replacement will always
happen and the <samp class="option">u</samp> modifier will be ineffective.
</p>
</dd>
<dt id='index-deterministic-archives-1'><span>&lsquo;<samp class="samp">U</samp>&rsquo;<a class="copiable-link" href='#index-deterministic-archives-1'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-_002d_002denable_002ddeterministic_002darchives-1"></a>
<p>Do <em class="emph">not</em> operate in <em class="emph">deterministic</em> mode.  This is the inverse
of the &lsquo;<samp class="samp">D</samp>&rsquo; modifier, above: added files and the archive index will
get their actual UID, GID, timestamp, and file mode values.
</p>
<p>This is the default unless <samp class="file">binutils</samp> was configured with
<samp class="option">--enable-deterministic-archives</samp>.
</p>
</dd>
<dt>&lsquo;<samp class="samp">v</samp>&rsquo;</dt>
<dd><p>This modifier requests the <em class="emph">verbose</em> version of an operation.  Many
operations display additional information, such as filenames processed,
when the modifier &lsquo;<samp class="samp">v</samp>&rsquo; is appended.
</p>
</dd>
<dt>&lsquo;<samp class="samp">V</samp>&rsquo;</dt>
<dd><p>This modifier shows the version number of <code class="command">ar</code>.
</p></dd>
</dl>

<p>The <code class="command">ar</code> program also supports some command-line options which
are neither modifiers nor actions, but which do change its behaviour
in specific ways:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">--help</samp>&rsquo;</dt>
<dd><p>Displays the list of command-line options supported by <code class="command">ar</code>
and then exits.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--version</samp>&rsquo;</dt>
<dd><p>Displays the version information of <code class="command">ar</code> and then exits.
</p>
</dd>
<dt>&lsquo;<samp class="samp">-X32_64</samp>&rsquo;</dt>
<dd><p><code class="command">ar</code> ignores an initial option spelled &lsquo;<samp class="samp">-X32_64</samp>&rsquo;, for
compatibility with AIX.  The behaviour produced by this option is the
default for <small class="sc">GNU</small> <code class="command">ar</code>.  <code class="command">ar</code> does not support any
of the other &lsquo;<samp class="samp">-X</samp>&rsquo; options; in particular, it does not support
<samp class="option">-X32</samp> which is the default for AIX <code class="command">ar</code>.
</p>
</dd>
<dt id='index-plugins'><span>&lsquo;<samp class="samp">--plugin <var class="var">name</var></samp>&rsquo;<a class="copiable-link" href='#index-plugins'> &para;</a></span></dt>
<dd><p>The optional command-line switch <samp class="option">--plugin <var class="var">name</var></samp> causes
<code class="command">ar</code> to load the plugin called <var class="var">name</var> which adds support
for more file formats, including object files with link-time
optimization information.
</p>
<p>This option is only available if the toolchain has been built with
plugin support enabled.
</p>
<p>If <samp class="option">--plugin</samp> is not provided, but plugin support has been
enabled then <code class="command">ar</code> iterates over the files in
<samp class="file">${libdir}/bfd-plugins</samp> in alphabetic order and the first
plugin that claims the object in question is used.
</p>
<p>Please note that this plugin search directory is <em class="emph">not</em> the one
used by <code class="command">ld</code>&rsquo;s <samp class="option">-plugin</samp> option.  In order to make
<code class="command">ar</code> use the  linker plugin it must be copied into the
<samp class="file">${libdir}/bfd-plugins</samp> directory.  For GCC based compilations
the linker plugin is called <samp class="file">liblto_plugin.so.0.0.0</samp>.  For Clang
based compilations it is called <samp class="file">LLVMgold.so</samp>.  The GCC plugin
is always backwards compatible with earlier versions, so it is
sufficient to just copy the newest one.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--target <var class="var">target</var></samp>&rsquo;</dt>
<dd><p>The optional command-line switch <samp class="option">--target <var class="var">bfdname</var></samp>
specifies that the archive members are in an object code format
different from your system&rsquo;s default format.  See
See <a class="xref" href="Target-Selection.html">Target Selection</a>, for more information.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--output <var class="var">dirname</var></samp>&rsquo;</dt>
<dd><p>The <samp class="option">--output</samp> option can be used to specify a path to a
directory into which archive members should be extracted.  If this
option is not specified then the current directory will be used.
</p>
<p>Note - although the presence of this option does imply a <samp class="option">x</samp> 
extraction operation that option must still be included on the command
line.
</p>
</dd>
<dt>&lsquo;<samp class="samp">--record-libdeps <var class="var">libdeps</var></samp>&rsquo;</dt>
<dd><p>The <samp class="option">--record-libdeps</samp> option is identical to the <samp class="option">l</samp> modifier,
just handled in long form.
</p>
</dd>
<dt id='index-creating-thin-archive'><span>&lsquo;<samp class="samp">--thin</samp>&rsquo;<a class="copiable-link" href='#index-creating-thin-archive'> &para;</a></span></dt>
<dd><p>Make the specified <var class="var">archive</var> a <em class="emph">thin</em> archive.  If it already
exists and is a regular archive, the existing members must be present
in the same directory as <var class="var">archive</var>.
</p>
</dd>
</dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="ar-scripts.html">Controlling <code class="command">ar</code> with a Script</a>, Up: <a href="ar.html">ar</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
