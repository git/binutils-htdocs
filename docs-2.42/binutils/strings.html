<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>strings (GNU Binary Utilities)</title>

<meta name="description" content="strings (GNU Binary Utilities)">
<meta name="keywords" content="strings (GNU Binary Utilities)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Binutils-Index.html" rel="index" title="Binutils Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="strip.html" rel="next" title="strip">
<link href="size.html" rel="prev" title="size">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="chapter-level-extent" id="strings">
<div class="nav-panel">
<p>
Next: <a href="strip.html" accesskey="n" rel="next">strip</a>, Previous: <a href="size.html" accesskey="p" rel="prev">size</a>, Up: <a href="index.html" accesskey="u" rel="up">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h2 class="chapter" id="strings-1">7 strings</h2>
<a class="index-entry-id" id="index-strings"></a>
<a class="index-entry-id" id="index-listings-strings"></a>
<a class="index-entry-id" id="index-printing-strings"></a>
<a class="index-entry-id" id="index-strings_002c-printing"></a>


<div class="example smallexample">
<pre class="example-preformatted">strings [<samp class="option">-afovV</samp>] [<samp class="option">-</samp><var class="var">min-len</var>]
        [<samp class="option">-n</samp> <var class="var">min-len</var>] [<samp class="option">--bytes=</samp><var class="var">min-len</var>]
        [<samp class="option">-t</samp> <var class="var">radix</var>] [<samp class="option">--radix=</samp><var class="var">radix</var>]
        [<samp class="option">-e</samp> <var class="var">encoding</var>] [<samp class="option">--encoding=</samp><var class="var">encoding</var>]
        [<samp class="option">-U</samp> <var class="var">method</var>] [<samp class="option">--unicode=</samp><var class="var">method</var>]
        [<samp class="option">-</samp>] [<samp class="option">--all</samp>] [<samp class="option">--print-file-name</samp>]
        [<samp class="option">-T</samp> <var class="var">bfdname</var>] [<samp class="option">--target=</samp><var class="var">bfdname</var>]
        [<samp class="option">-w</samp>] [<samp class="option">--include-all-whitespace</samp>]
        [<samp class="option">-s</samp>] [<samp class="option">--output-separator</samp> <var class="var">sep_string</var>]
        [<samp class="option">--help</samp>] [<samp class="option">--version</samp>] <var class="var">file</var>&hellip;
</pre></div>


<p>For each <var class="var">file</var> given, <small class="sc">GNU</small> <code class="command">strings</code> prints the
printable character sequences that are at least 4 characters long (or
the number given with the options below) and are followed by an
unprintable character.
</p>
<p>Depending upon how the strings program was configured it will default
to either displaying all the printable sequences that it can find in
each file, or only those sequences that are in loadable, initialized
data sections.  If the file type is unrecognizable, or if strings is
reading from stdin then it will always display all of the printable
sequences that it can find.
</p>
<p>For backwards compatibility any file that occurs after a command-line
option of just <samp class="option">-</samp> will also be scanned in full, regardless of
the presence of any <samp class="option">-d</samp> option.
</p>
<p><code class="command">strings</code> is mainly useful for determining the contents of
non-text files.
</p>


<dl class="table">
<dt><code class="env">-a</code></dt>
<dt><code class="env">--all</code></dt>
<dt><code class="env">-</code></dt>
<dd><p>Scan the whole file, regardless of what sections it contains or
whether those sections are loaded or initialized.  Normally this is
the default behaviour, but strings can be configured so that the
<samp class="option">-d</samp> is the default instead.
</p>
<p>The <samp class="option">-</samp> option is position dependent and forces strings to
perform full scans of any file that is mentioned after the <samp class="option">-</samp>
on the command line, even if the <samp class="option">-d</samp> option has been
specified.
</p>
</dd>
<dt><code class="env">-d</code></dt>
<dt><code class="env">--data</code></dt>
<dd><p>Only print strings from initialized, loaded data sections in the
file.  This may reduce the amount of garbage in the output, but it
also exposes the strings program to any security flaws that may be
present in the BFD library used to scan and load sections.  Strings
can be configured so that this option is the default behaviour.  In
such cases the <samp class="option">-a</samp> option can be used to avoid using the BFD
library and instead just print all of the strings found in the file.
</p>
</dd>
<dt><code class="env">-f</code></dt>
<dt><code class="env">--print-file-name</code></dt>
<dd><p>Print the name of the file before each string.
</p>
</dd>
<dt><code class="env">--help</code></dt>
<dd><p>Print a summary of the program usage on the standard output and exit.
</p>
</dd>
<dt><code class="env">-<var class="var">min-len</var></code></dt>
<dt><code class="env">-n <var class="var">min-len</var></code></dt>
<dt><code class="env">--bytes=<var class="var">min-len</var></code></dt>
<dd><p>Print sequences of displayable characters that are at least
<var class="var">min-len</var> characters long.  If not specified a default minimum
length of 4 is used.  The distinction between displayable and
non-displayable characters depends upon the setting of the 
<samp class="option">-e</samp> and <samp class="option">-U</samp> options.  Sequences are always terminated
at control characters such as new-line and carriage-return, but not
the tab character.
</p>
</dd>
<dt><code class="env">-o</code></dt>
<dd><p>Like &lsquo;<samp class="samp">-t o</samp>&rsquo;.  Some other versions of <code class="command">strings</code> have <samp class="option">-o</samp>
act like &lsquo;<samp class="samp">-t d</samp>&rsquo; instead.  Since we can not be compatible with both
ways, we simply chose one.
</p>
</dd>
<dt><code class="env">-t <var class="var">radix</var></code></dt>
<dt><code class="env">--radix=<var class="var">radix</var></code></dt>
<dd><p>Print the offset within the file before each string.  The single
character argument specifies the radix of the offset&mdash;&lsquo;<samp class="samp">o</samp>&rsquo; for
octal, &lsquo;<samp class="samp">x</samp>&rsquo; for hexadecimal, or &lsquo;<samp class="samp">d</samp>&rsquo; for decimal.
</p>
</dd>
<dt><code class="env">-e <var class="var">encoding</var></code></dt>
<dt><code class="env">--encoding=<var class="var">encoding</var></code></dt>
<dd><p>Select the character encoding of the strings that are to be found.
Possible values for <var class="var">encoding</var> are: &lsquo;<samp class="samp">s</samp>&rsquo; = single-7-bit-byte
characters (default), &lsquo;<samp class="samp">S</samp>&rsquo; =
single-8-bit-byte characters, &lsquo;<samp class="samp">b</samp>&rsquo; = 16-bit bigendian, &lsquo;<samp class="samp">l</samp>&rsquo; =
16-bit littleendian, &lsquo;<samp class="samp">B</samp>&rsquo; = 32-bit bigendian, &lsquo;<samp class="samp">L</samp>&rsquo; = 32-bit
littleendian.  Useful for finding wide character strings. (&lsquo;<samp class="samp">l</samp>&rsquo;
and &lsquo;<samp class="samp">b</samp>&rsquo; apply to, for example, Unicode UTF-16/UCS-2 encodings).
</p>
</dd>
<dt><code class="env">-U <var class="var">[d|i|l|e|x|h]</var></code></dt>
<dt><code class="env">--unicode=<var class="var">[default|invalid|locale|escape|hex|highlight]</var></code></dt>
<dd><p>Controls the display of UTF-8 encoded multibyte characters in strings.
The default (<samp class="option">--unicode=default</samp>) is to give them no special
treatment, and instead rely upon the setting of the
<samp class="option">--encoding</samp> option.  The other values for this option
automatically enable <samp class="option">--encoding=S</samp>.
</p>
<p>The <samp class="option">--unicode=invalid</samp> option treats them as non-graphic
characters and hence not part of a valid string.  All the remaining
options treat them as valid string characters.
</p>
<p>The <samp class="option">--unicode=locale</samp> option displays them in the current
locale, which may or may not support UTF-8 encoding.  The
<samp class="option">--unicode=hex</samp> option displays them as hex byte sequences
enclosed between <var class="var">&lt;&gt;</var> characters.  The <samp class="option">--unicode=escape</samp>
option displays them as escape sequences (<var class="var">\uxxxx</var>) and the
<samp class="option">--unicode=highlight</samp> option displays them as escape sequences
highlighted in red (if supported by the output device).  The colouring
is intended to draw attention to the presence of unicode sequences
where they might not be expected.
</p>
</dd>
<dt id='index-object-code-format-3'><span><code class="env">-T <var class="var">bfdname</var></code><a class="copiable-link" href='#index-object-code-format-3'> &para;</a></span></dt>
<dt><code class="env">--target=<var class="var">bfdname</var></code></dt>
<dd><p>Specify an object code format other than your system&rsquo;s default format.
See <a class="xref" href="Target-Selection.html">Target Selection</a>, for more information.
</p>
</dd>
<dt><code class="env">-v</code></dt>
<dt><code class="env">-V</code></dt>
<dt><code class="env">--version</code></dt>
<dd><p>Print the program version number on the standard output and exit.
</p>
</dd>
<dt><code class="env">-w</code></dt>
<dt><code class="env">--include-all-whitespace</code></dt>
<dd><p>By default tab and space characters are included in the strings that
are displayed, but other whitespace characters, such a newlines and
carriage returns, are not.  The <samp class="option">-w</samp> option changes this so
that all whitespace characters are considered to be part of a string.
</p>
</dd>
<dt><code class="env">-s</code></dt>
<dt><code class="env">--output-separator</code></dt>
<dd><p>By default, output strings are delimited by a new-line. This option
allows you to supply any string to be used as the output record
separator.  Useful with &ndash;include-all-whitespace where strings
may contain new-lines internally.
</p></dd>
</dl>



</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="strip.html">strip</a>, Previous: <a href="size.html">size</a>, Up: <a href="index.html">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
