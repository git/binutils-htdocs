<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>i386-Memory (Using as)</title>

<meta name="description" content="i386-Memory (Using as)">
<meta name="keywords" content="i386-Memory (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="i386_002dDependent.html" rel="up" title="i386-Dependent">
<link href="i386_002dJumps.html" rel="next" title="i386-Jumps">
<link href="i386_002dPrefixes.html" rel="prev" title="i386-Prefixes">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="i386_002dMemory">
<div class="nav-panel">
<p>
Next: <a href="i386_002dJumps.html" accesskey="n" rel="next">Handling of Jump Instructions</a>, Previous: <a href="i386_002dPrefixes.html" accesskey="p" rel="prev">Instruction Prefixes</a>, Up: <a href="i386_002dDependent.html" accesskey="u" rel="up">80386 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="Memory-References">9.16.7 Memory References</h4>

<a class="index-entry-id" id="index-i386-memory-references"></a>
<a class="index-entry-id" id="index-memory-references_002c-i386"></a>
<a class="index-entry-id" id="index-x86_002d64-memory-references"></a>
<a class="index-entry-id" id="index-memory-references_002c-x86_002d64"></a>
<p>An Intel syntax indirect memory reference of the form
</p>
<div class="example smallexample">
<pre class="example-preformatted"><var class="var">section</var>:[<var class="var">base</var> + <var class="var">index</var>*<var class="var">scale</var> + <var class="var">disp</var>]
</pre></div>

<p>is translated into the AT&amp;T syntax
</p>
<div class="example smallexample">
<pre class="example-preformatted"><var class="var">section</var>:<var class="var">disp</var>(<var class="var">base</var>, <var class="var">index</var>, <var class="var">scale</var>)
</pre></div>

<p>where <var class="var">base</var> and <var class="var">index</var> are the optional 32-bit base and
index registers, <var class="var">disp</var> is the optional displacement, and
<var class="var">scale</var>, taking the values 1, 2, 4, and 8, multiplies <var class="var">index</var>
to calculate the address of the operand.  If no <var class="var">scale</var> is
specified, <var class="var">scale</var> is taken to be 1.  <var class="var">section</var> specifies the
optional section register for the memory operand, and may override the
default section register (see a 80386 manual for section register
defaults). Note that section overrides in AT&amp;T syntax <em class="emph">must</em>
be preceded by a &lsquo;<samp class="samp">%</samp>&rsquo;.  If you specify a section override which
coincides with the default section register, <code class="code">as</code> does <em class="emph">not</em>
output any section register override prefixes to assemble the given
instruction.  Thus, section overrides can be specified to emphasize which
section register is used for a given memory operand.
</p>
<p>Here are some examples of Intel and AT&amp;T style memory references:
</p>
<dl class="table">
<dt>AT&amp;T: &lsquo;<samp class="samp">-4(%ebp)</samp>&rsquo;, Intel:  &lsquo;<samp class="samp">[ebp - 4]</samp>&rsquo;</dt>
<dd><p><var class="var">base</var> is &lsquo;<samp class="samp">%ebp</samp>&rsquo;; <var class="var">disp</var> is &lsquo;<samp class="samp">-4</samp>&rsquo;. <var class="var">section</var> is
missing, and the default section is used (&lsquo;<samp class="samp">%ss</samp>&rsquo; for addressing with
&lsquo;<samp class="samp">%ebp</samp>&rsquo; as the base register).  <var class="var">index</var>, <var class="var">scale</var> are both missing.
</p>
</dd>
<dt>AT&amp;T: &lsquo;<samp class="samp">foo(,%eax,4)</samp>&rsquo;, Intel: &lsquo;<samp class="samp">[foo + eax*4]</samp>&rsquo;</dt>
<dd><p><var class="var">index</var> is &lsquo;<samp class="samp">%eax</samp>&rsquo; (scaled by a <var class="var">scale</var> 4); <var class="var">disp</var> is
&lsquo;<samp class="samp">foo</samp>&rsquo;.  All other fields are missing.  The section register here
defaults to &lsquo;<samp class="samp">%ds</samp>&rsquo;.
</p>
</dd>
<dt>AT&amp;T: &lsquo;<samp class="samp">foo(,1)</samp>&rsquo;; Intel &lsquo;<samp class="samp">[foo]</samp>&rsquo;</dt>
<dd><p>This uses the value pointed to by &lsquo;<samp class="samp">foo</samp>&rsquo; as a memory operand.
Note that <var class="var">base</var> and <var class="var">index</var> are both missing, but there is only
<em class="emph">one</em> &lsquo;<samp class="samp">,</samp>&rsquo;.  This is a syntactic exception.
</p>
</dd>
<dt>AT&amp;T: &lsquo;<samp class="samp">%gs:foo</samp>&rsquo;; Intel &lsquo;<samp class="samp">gs:foo</samp>&rsquo;</dt>
<dd><p>This selects the contents of the variable &lsquo;<samp class="samp">foo</samp>&rsquo; with section
register <var class="var">section</var> being &lsquo;<samp class="samp">%gs</samp>&rsquo;.
</p></dd>
</dl>

<p>Absolute (as opposed to PC relative) call and jump operands must be
prefixed with &lsquo;<samp class="samp">*</samp>&rsquo;.  If no &lsquo;<samp class="samp">*</samp>&rsquo; is specified, <code class="code">as</code>
always chooses PC relative addressing for jump/call labels.
</p>
<p>Any instruction that has a memory operand, but no register operand,
<em class="emph">must</em> specify its size (byte, word, long, or quadruple) with an
instruction mnemonic suffix (&lsquo;<samp class="samp">b</samp>&rsquo;, &lsquo;<samp class="samp">w</samp>&rsquo;, &lsquo;<samp class="samp">l</samp>&rsquo; or &lsquo;<samp class="samp">q</samp>&rsquo;,
respectively).
</p>
<p>The x86-64 architecture adds an RIP (instruction pointer relative)
addressing.  This addressing mode is specified by using &lsquo;<samp class="samp">rip</samp>&rsquo; as a
base register.  Only constant offsets are valid. For example:
</p>
<dl class="table">
<dt>AT&amp;T: &lsquo;<samp class="samp">1234(%rip)</samp>&rsquo;, Intel: &lsquo;<samp class="samp">[rip + 1234]</samp>&rsquo;</dt>
<dd><p>Points to the address 1234 bytes past the end of the current
instruction.
</p>
</dd>
<dt>AT&amp;T: &lsquo;<samp class="samp">symbol(%rip)</samp>&rsquo;, Intel: &lsquo;<samp class="samp">[rip + symbol]</samp>&rsquo;</dt>
<dd><p>Points to the <code class="code">symbol</code> in RIP relative way, this is shorter than
the default absolute addressing.
</p></dd>
</dl>

<p>Other addressing modes remain unchanged in x86-64 architecture, except
registers used are 64-bit instead of 32-bit.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="i386_002dJumps.html">Handling of Jump Instructions</a>, Previous: <a href="i386_002dPrefixes.html">Instruction Prefixes</a>, Up: <a href="i386_002dDependent.html">80386 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
