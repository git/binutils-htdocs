<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>i386-16bit (Using as)</title>

<meta name="description" content="i386-16bit (Using as)">
<meta name="keywords" content="i386-16bit (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="i386_002dDependent.html" rel="up" title="i386-Dependent">
<link href="i386_002dArch.html" rel="next" title="i386-Arch">
<link href="i386_002dTBM.html" rel="prev" title="i386-TBM">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="i386_002d16bit">
<div class="nav-panel">
<p>
Next: <a href="i386_002dArch.html" accesskey="n" rel="next">Specifying CPU Architecture</a>, Previous: <a href="i386_002dTBM.html" accesskey="p" rel="prev">AMD&rsquo;s Trailing Bit Manipulation Instructions</a>, Up: <a href="i386_002dDependent.html" accesskey="u" rel="up">80386 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="Writing-16_002dbit-Code">9.16.14 Writing 16-bit Code</h4>

<a class="index-entry-id" id="index-i386-16_002dbit-code"></a>
<a class="index-entry-id" id="index-16_002dbit-code_002c-i386"></a>
<a class="index-entry-id" id="index-real_002dmode-code_002c-i386"></a>
<a class="index-entry-id" id="index-code16gcc-directive_002c-i386"></a>
<a class="index-entry-id" id="index-code16-directive_002c-i386"></a>
<a class="index-entry-id" id="index-code32-directive_002c-i386"></a>
<a class="index-entry-id" id="index-code64-directive_002c-i386"></a>
<a class="index-entry-id" id="index-code64-directive_002c-x86_002d64"></a>
<p>While <code class="code">as</code> normally writes only &ldquo;pure&rdquo; 32-bit i386 code
or 64-bit x86-64 code depending on the default configuration,
it also supports writing code to run in real mode or in 16-bit protected
mode code segments.  To do this, put a &lsquo;<samp class="samp">.code16</samp>&rsquo; or
&lsquo;<samp class="samp">.code16gcc</samp>&rsquo; directive before the assembly language instructions to
be run in 16-bit mode.  You can switch <code class="code">as</code> to writing
32-bit code with the &lsquo;<samp class="samp">.code32</samp>&rsquo; directive or 64-bit code with the
&lsquo;<samp class="samp">.code64</samp>&rsquo; directive.
</p>
<p>&lsquo;<samp class="samp">.code16gcc</samp>&rsquo; provides experimental support for generating 16-bit
code from gcc, and differs from &lsquo;<samp class="samp">.code16</samp>&rsquo; in that &lsquo;<samp class="samp">call</samp>&rsquo;,
&lsquo;<samp class="samp">ret</samp>&rsquo;, &lsquo;<samp class="samp">enter</samp>&rsquo;, &lsquo;<samp class="samp">leave</samp>&rsquo;, &lsquo;<samp class="samp">push</samp>&rsquo;, &lsquo;<samp class="samp">pop</samp>&rsquo;,
&lsquo;<samp class="samp">pusha</samp>&rsquo;, &lsquo;<samp class="samp">popa</samp>&rsquo;, &lsquo;<samp class="samp">pushf</samp>&rsquo;, and &lsquo;<samp class="samp">popf</samp>&rsquo; instructions
default to 32-bit size.  This is so that the stack pointer is
manipulated in the same way over function calls, allowing access to
function parameters at the same stack offsets as in 32-bit mode.
&lsquo;<samp class="samp">.code16gcc</samp>&rsquo; also automatically adds address size prefixes where
necessary to use the 32-bit addressing modes that gcc generates.
</p>
<p>The code which <code class="code">as</code> generates in 16-bit mode will not
necessarily run on a 16-bit pre-80386 processor.  To write code that
runs on such a processor, you must refrain from using <em class="emph">any</em> 32-bit
constructs which require <code class="code">as</code> to output address or operand
size prefixes.
</p>
<p>Note that writing 16-bit code instructions by explicitly specifying a
prefix or an instruction mnemonic suffix within a 32-bit code section
generates different machine instructions than those generated for a
16-bit code segment.  In a 32-bit code section, the following code
generates the machine opcode bytes &lsquo;<samp class="samp">66 6a 04</samp>&rsquo;, which pushes the
value &lsquo;<samp class="samp">4</samp>&rsquo; onto the stack, decrementing &lsquo;<samp class="samp">%esp</samp>&rsquo; by 2.
</p>
<div class="example smallexample">
<pre class="example-preformatted">        pushw $4
</pre></div>

<p>The same code in a 16-bit code section would generate the machine
opcode bytes &lsquo;<samp class="samp">6a 04</samp>&rsquo; (i.e., without the operand size prefix), which
is correct since the processor default operand size is assumed to be 16
bits in a 16-bit code section.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="i386_002dArch.html">Specifying CPU Architecture</a>, Previous: <a href="i386_002dTBM.html">AMD&rsquo;s Trailing Bit Manipulation Instructions</a>, Up: <a href="i386_002dDependent.html">80386 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
