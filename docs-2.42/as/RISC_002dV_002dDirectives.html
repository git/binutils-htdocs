<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>RISC-V-Directives (Using as)</title>

<meta name="description" content="RISC-V-Directives (Using as)">
<meta name="keywords" content="RISC-V-Directives (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="RISC_002dV_002dDependent.html" rel="up" title="RISC-V-Dependent">
<link href="RISC_002dV_002dModifiers.html" rel="next" title="RISC-V-Modifiers">
<link href="RISC_002dV_002dOptions.html" rel="prev" title="RISC-V-Options">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="RISC_002dV_002dDirectives">
<div class="nav-panel">
<p>
Next: <a href="RISC_002dV_002dModifiers.html" accesskey="n" rel="next">RISC-V Assembler Modifiers</a>, Previous: <a href="RISC_002dV_002dOptions.html" accesskey="p" rel="prev">RISC-V Options</a>, Up: <a href="RISC_002dV_002dDependent.html" accesskey="u" rel="up">RISC-V Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="RISC_002dV-Directives">9.39.2 RISC-V Directives</h4>
<a class="index-entry-id" id="index-machine-directives_002c-RISC_002dV"></a>
<a class="index-entry-id" id="index-RISC_002dV-machine-directives"></a>

<p>The following table lists all available RISC-V specific directives.
</p>
<dl class="table">
<dd>
<a class="index-entry-id" id="index-align-directive-1"></a>
</dd>
<dt><code class="code">.align <var class="var">size-log-2</var></code></dt>
<dd><p>Align to the given boundary, with the size given as log2 the number of bytes to
align to.
</p>
<a class="index-entry-id" id="index-Data-directives"></a>
</dd>
<dt><code class="code">.half <var class="var">value</var></code></dt>
<dt><code class="code">.word <var class="var">value</var></code></dt>
<dt><code class="code">.dword <var class="var">value</var></code></dt>
<dd><p>Emits a half-word, word, or double-word value at the current position.
</p>
<a class="index-entry-id" id="index-DTP_002drelative-data-directives"></a>
</dd>
<dt><code class="code">.dtprelword <var class="var">value</var></code></dt>
<dt><code class="code">.dtpreldword <var class="var">value</var></code></dt>
<dd><p>Emits a DTP-relative word (or double-word) at the current position.  This is
meant to be used by the compiler in shared libraries for DWARF debug info for
thread local variables.
</p>
<a class="index-entry-id" id="index-LEB128-directives"></a>
</dd>
<dt><code class="code">.uleb128 <var class="var">value</var></code></dt>
<dt><code class="code">.sleb128 <var class="var">value</var></code></dt>
<dd><p>Emits a signed or unsigned LEB128 value at the current position.  This only
accepts constant expressions, because symbol addresses can change with
relaxation, and we don&rsquo;t support relocations to modify LEB128 values at link
time.
</p>
<a class="index-entry-id" id="index-Option-directive"></a>
<a class="index-entry-id" id="index-option-directive"></a>
</dd>
<dt><code class="code">.option <var class="var">argument</var></code></dt>
<dd><p>Modifies RISC-V specific assembler options inline with the assembly code.
This is used when particular instruction sequences must be assembled with a
specific set of options.  For example, since we relax addressing sequences to
shorter GP-relative sequences when possible the initial load of GP must not be
relaxed and should be emitted as something like
</p>
<div class="example smallexample">
<pre class="example-preformatted">	.option push
	.option norelax
	la gp, __global_pointer$
	.option pop
</pre></div>

<p>in order to produce after linker relaxation the expected
</p>
<div class="example smallexample">
<pre class="example-preformatted">	auipc gp, %pcrel_hi(__global_pointer$)
	addi gp, gp, %pcrel_lo(__global_pointer$)
</pre></div>

<p>instead of just
</p>
<div class="example smallexample">
<pre class="example-preformatted">	addi gp, gp, 0
</pre></div>

<p>It&rsquo;s not expected that options are changed in this manner during regular use,
but there are a handful of esoteric cases like the one above where users need
to disable particular features of the assembler for particular code sequences.
The complete list of option arguments is shown below:
</p>
<dl class="table">
<dt><code class="code">push</code></dt>
<dt><code class="code">pop</code></dt>
<dd><p>Pushes or pops the current option stack.  These should be used whenever
changing an option in line with assembly code in order to ensure the user&rsquo;s
command-line options are respected for the bulk of the file being assembled.
</p>
</dd>
<dt><code class="code">rvc</code></dt>
<dt><code class="code">norvc</code></dt>
<dd><p>Enables or disables the generation of compressed instructions.  Instructions
are opportunistically compressed by the RISC-V assembler when possible, but
sometimes this behavior is not desirable, especially when handling alignments.
</p>
</dd>
<dt><code class="code">pic</code></dt>
<dt><code class="code">nopic</code></dt>
<dd><p>Enables or disables position-independent code generation.  Unless you really
know what you&rsquo;re doing, this should only be at the top of a file.
</p>
</dd>
<dt><code class="code">relax</code></dt>
<dt><code class="code">norelax</code></dt>
<dd><p>Enables or disables relaxation.  The RISC-V assembler and linker
opportunistically relax some code sequences, but sometimes this behavior is not
desirable.
</p>
</dd>
<dt><code class="code">csr-check</code></dt>
<dt><code class="code">no-csr-check</code></dt>
<dd><p>Enables or disables the CSR checking.
</p>
</dd>
<dt><code class="code">arch, <var class="var">+extension[version]</var> [,...,<var class="var">+extension_n[version_n]</var>]</code></dt>
<dt><code class="code">arch, <var class="var">-extension</var> [,...,<var class="var">-extension_n</var>]</code></dt>
<dt><code class="code">arch, <var class="var">=ISA</var></code></dt>
<dd><p>Enables or disables the extensions for specific code region.  For example,
&lsquo;<samp class="samp">.option arch, +m2p0</samp>&rsquo; means add m extension with version 2.0, and
&lsquo;<samp class="samp">.option arch, -f, -d</samp>&rsquo; means remove extensions, f and d, from the
architecture string.  Note that, &lsquo;<samp class="samp">.option arch, +c, -c</samp>&rsquo; have the same
behavior as &lsquo;<samp class="samp">.option rvc, norvc</samp>&rsquo;.  However, they are also undesirable
sometimes.  Besides, &lsquo;<samp class="samp">.option arch, -i</samp>&rsquo; is illegal, since we cannot
remove the base i extension anytime.  If you want to reset the whole ISA
string, you can also use &lsquo;<samp class="samp">.option arch, =rv32imac</samp>&rsquo; to overwrite the
previous settings.
</p></dd>
</dl>

<a class="index-entry-id" id="index-INSN-directives"></a>
</dd>
<dt><code class="code">.insn <var class="var">type</var>, <var class="var">operand</var> [,...,<var class="var">operand_n</var>]</code></dt>
<dt><code class="code">.insn <var class="var">insn_length</var>, <var class="var">value</var></code></dt>
<dt><code class="code">.insn <var class="var">value</var></code></dt>
<dd><p>This directive permits the numeric representation of an instructions
and makes the assembler insert the operands according to one of the
instruction formats for &lsquo;<samp class="samp">.insn</samp>&rsquo; (<a class="ref" href="RISC_002dV_002dFormats.html">RISC-V Instruction Formats</a>).
For example, the instruction &lsquo;<samp class="samp">add a0, a1, a2</samp>&rsquo; could be written as
&lsquo;<samp class="samp">.insn r 0x33, 0, 0, a0, a1, a2</samp>&rsquo;.  But in fact, the instruction
formats are difficult to use for some users, so most of them are using
&lsquo;<samp class="samp">.word</samp>&rsquo; to encode the instruction directly, rather than using
&lsquo;<samp class="samp">.insn</samp>&rsquo;.  It is fine for now, but will be wrong when the mapping
symbols are supported, since &lsquo;<samp class="samp">.word</samp>&rsquo; will not be shown as an
instruction, it should be shown as data.  Therefore, we also support
two more formats of the &lsquo;<samp class="samp">.insn</samp>&rsquo;, the instruction &lsquo;<samp class="samp">add a0, a1, a2</samp>&rsquo;
could also be written as &lsquo;<samp class="samp">.insn 0x4, 0xc58533</samp>&rsquo; or &lsquo;<samp class="samp">.insn 0xc58533</samp>&rsquo;.
When the <var class="var">insn_length</var> is set, then assembler will check if the
<var class="var">value</var> is a valid <var class="var">insn_length</var> bytes instruction.
</p>
<a class="index-entry-id" id="index-_002eattribute-directive_002c-RISC_002dV"></a>
</dd>
<dt><code class="code">.attribute <var class="var">tag</var>, <var class="var">value</var></code></dt>
<dd><p>Set the object attribute <var class="var">tag</var> to <var class="var">value</var>.
</p>
<p>The <var class="var">tag</var> is either an attribute number, or one of the following:
<code class="code">Tag_RISCV_arch</code>, <code class="code">Tag_RISCV_stack_align</code>,
<code class="code">Tag_RISCV_unaligned_access</code>, <code class="code">Tag_RISCV_priv_spec</code>,
<code class="code">Tag_RISCV_priv_spec_minor</code>, <code class="code">Tag_RISCV_priv_spec_revision</code>.
</p>
</dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="RISC_002dV_002dModifiers.html">RISC-V Assembler Modifiers</a>, Previous: <a href="RISC_002dV_002dOptions.html">RISC-V Options</a>, Up: <a href="RISC_002dV_002dDependent.html">RISC-V Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
