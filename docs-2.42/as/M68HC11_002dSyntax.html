<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>M68HC11-Syntax (Using as)</title>

<meta name="description" content="M68HC11-Syntax (Using as)">
<meta name="keywords" content="M68HC11-Syntax (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="M68HC11_002dDependent.html" rel="up" title="M68HC11-Dependent">
<link href="M68HC11_002dModifiers.html" rel="next" title="M68HC11-Modifiers">
<link href="M68HC11_002dOpts.html" rel="prev" title="M68HC11-Opts">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="M68HC11_002dSyntax">
<div class="nav-panel">
<p>
Next: <a href="M68HC11_002dModifiers.html" accesskey="n" rel="next">Symbolic Operand Modifiers</a>, Previous: <a href="M68HC11_002dOpts.html" accesskey="p" rel="prev">M68HC11 and M68HC12 Options</a>, Up: <a href="M68HC11_002dDependent.html" accesskey="u" rel="up">M68HC11 and M68HC12 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="Syntax-17">9.24.2 Syntax</h4>

<a class="index-entry-id" id="index-M68HC11-syntax"></a>
<a class="index-entry-id" id="index-syntax_002c-M68HC11"></a>

<p>In the M68HC11 syntax, the instruction name comes first and it may
be followed by one or several operands (up to three). Operands are
separated by comma (&lsquo;<samp class="samp">,</samp>&rsquo;). In the normal mode,
<code class="code">as</code> will complain if too many operands are specified for
a given instruction. In the MRI mode (turned on with &lsquo;<samp class="samp">-M</samp>&rsquo; option),
it will treat them as comments. Example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">inx
lda  #23
bset 2,x #4
brclr *bot #8 foo
</pre></div>

<a class="index-entry-id" id="index-line-comment-character_002c-M68HC11"></a>
<a class="index-entry-id" id="index-M68HC11-line-comment-character"></a>
<p>The presence of a &lsquo;<samp class="samp">;</samp>&rsquo; character or a &lsquo;<samp class="samp">!</samp>&rsquo; character anywhere
on a line indicates the start of a comment that extends to the end of
that line.
</p>
<p>A &lsquo;<samp class="samp">*</samp>&rsquo; or a &lsquo;<samp class="samp">#</samp>&rsquo; character at the start of a line also
introduces a line comment, but these characters do not work elsewhere
on the line.  If the first character of the line is a &lsquo;<samp class="samp">#</samp>&rsquo; then as
well as starting a comment, the line could also be logical line number
directive (see <a class="pxref" href="Comments.html">Comments</a>) or a preprocessor control command
(see <a class="pxref" href="Preprocessing.html">Preprocessing</a>).
</p>
<a class="index-entry-id" id="index-line-separator_002c-M68HC11"></a>
<a class="index-entry-id" id="index-statement-separator_002c-M68HC11"></a>
<a class="index-entry-id" id="index-M68HC11-line-separator"></a>
<p>The M68HC11 assembler does not currently support a line separator
character.
</p>
<a class="index-entry-id" id="index-M68HC11-addressing-modes"></a>
<a class="index-entry-id" id="index-addressing-modes_002c-M68HC11"></a>
<p>The following addressing modes are understood for 68HC11 and 68HC12:
</p><dl class="table">
<dt><em class="dfn">Immediate</em></dt>
<dd><p>&lsquo;<samp class="samp">#<var class="var">number</var></samp>&rsquo;
</p>
</dd>
<dt><em class="dfn">Address Register</em></dt>
<dd><p>&lsquo;<samp class="samp"><var class="var">number</var>,X</samp>&rsquo;, &lsquo;<samp class="samp"><var class="var">number</var>,Y</samp>&rsquo;
</p>
<p>The <var class="var">number</var> may be omitted in which case 0 is assumed.
</p>
</dd>
<dt><em class="dfn">Direct Addressing mode</em></dt>
<dd><p>&lsquo;<samp class="samp">*<var class="var">symbol</var></samp>&rsquo;, or &lsquo;<samp class="samp">*<var class="var">digits</var></samp>&rsquo;
</p>
</dd>
<dt><em class="dfn">Absolute</em></dt>
<dd><p>&lsquo;<samp class="samp"><var class="var">symbol</var></samp>&rsquo;, or &lsquo;<samp class="samp"><var class="var">digits</var></samp>&rsquo;
</p></dd>
</dl>

<p>The M68HC12 has other more complex addressing modes. All of them
are supported and they are represented below:
</p>
<dl class="table">
<dt><em class="dfn">Constant Offset Indexed Addressing Mode</em></dt>
<dd><p>&lsquo;<samp class="samp"><var class="var">number</var>,<var class="var">reg</var></samp>&rsquo;
</p>
<p>The <var class="var">number</var> may be omitted in which case 0 is assumed.
The register can be either &lsquo;<samp class="samp">X</samp>&rsquo;, &lsquo;<samp class="samp">Y</samp>&rsquo;, &lsquo;<samp class="samp">SP</samp>&rsquo; or
&lsquo;<samp class="samp">PC</samp>&rsquo;.  The assembler will use the smaller post-byte definition
according to the constant value (5-bit constant offset, 9-bit constant
offset or 16-bit constant offset).  If the constant is not known by
the assembler it will use the 16-bit constant offset post-byte and the value
will be resolved at link time.
</p>
</dd>
<dt><em class="dfn">Offset Indexed Indirect</em></dt>
<dd><p>&lsquo;<samp class="samp">[<var class="var">number</var>,<var class="var">reg</var>]</samp>&rsquo;
</p>
<p>The register can be either &lsquo;<samp class="samp">X</samp>&rsquo;, &lsquo;<samp class="samp">Y</samp>&rsquo;, &lsquo;<samp class="samp">SP</samp>&rsquo; or &lsquo;<samp class="samp">PC</samp>&rsquo;.
</p>
</dd>
<dt><em class="dfn">Auto Pre-Increment/Pre-Decrement/Post-Increment/Post-Decrement</em></dt>
<dd><p>&lsquo;<samp class="samp"><var class="var">number</var>,-<var class="var">reg</var></samp>&rsquo;
&lsquo;<samp class="samp"><var class="var">number</var>,+<var class="var">reg</var></samp>&rsquo;
&lsquo;<samp class="samp"><var class="var">number</var>,<var class="var">reg</var>-</samp>&rsquo;
&lsquo;<samp class="samp"><var class="var">number</var>,<var class="var">reg</var>+</samp>&rsquo;
</p>
<p>The number must be in the range &lsquo;<samp class="samp">-8</samp>&rsquo;..&lsquo;<samp class="samp">+8</samp>&rsquo; and must not be 0.
The register can be either &lsquo;<samp class="samp">X</samp>&rsquo;, &lsquo;<samp class="samp">Y</samp>&rsquo;, &lsquo;<samp class="samp">SP</samp>&rsquo; or &lsquo;<samp class="samp">PC</samp>&rsquo;.
</p>
</dd>
<dt><em class="dfn">Accumulator Offset</em></dt>
<dd><p>&lsquo;<samp class="samp"><var class="var">acc</var>,<var class="var">reg</var></samp>&rsquo;
</p>
<p>The accumulator register can be either &lsquo;<samp class="samp">A</samp>&rsquo;, &lsquo;<samp class="samp">B</samp>&rsquo; or &lsquo;<samp class="samp">D</samp>&rsquo;.
The register can be either &lsquo;<samp class="samp">X</samp>&rsquo;, &lsquo;<samp class="samp">Y</samp>&rsquo;, &lsquo;<samp class="samp">SP</samp>&rsquo; or &lsquo;<samp class="samp">PC</samp>&rsquo;.
</p>
</dd>
<dt><em class="dfn">Accumulator D offset indexed-indirect</em></dt>
<dd><p>&lsquo;<samp class="samp">[D,<var class="var">reg</var>]</samp>&rsquo;
</p>
<p>The register can be either &lsquo;<samp class="samp">X</samp>&rsquo;, &lsquo;<samp class="samp">Y</samp>&rsquo;, &lsquo;<samp class="samp">SP</samp>&rsquo; or &lsquo;<samp class="samp">PC</samp>&rsquo;.
</p>
</dd>
</dl>

<p>For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">ldab 1024,sp
ldd [10,x]
orab 3,+x
stab -2,y-
ldx a,pc
sty [d,sp]
</pre></div>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="M68HC11_002dModifiers.html">Symbolic Operand Modifiers</a>, Previous: <a href="M68HC11_002dOpts.html">M68HC11 and M68HC12 Options</a>, Up: <a href="M68HC11_002dDependent.html">M68HC11 and M68HC12 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
