<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>AVR-Modifiers (Using as)</title>

<meta name="description" content="AVR-Modifiers (Using as)">
<meta name="keywords" content="AVR-Modifiers (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="AVR-Syntax.html" rel="up" title="AVR Syntax">
<link href="AVR_002dRegs.html" rel="prev" title="AVR-Regs">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="AVR_002dModifiers">
<div class="nav-panel">
<p>
Previous: <a href="AVR_002dRegs.html" accesskey="p" rel="prev">Register Names</a>, Up: <a href="AVR-Syntax.html" accesskey="u" rel="up">Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection subsection-level-set-subsubsection" id="Relocatable-Expression-Modifiers">9.5.2.3 Relocatable Expression Modifiers</h4>

<a class="index-entry-id" id="index-AVR-modifiers"></a>
<a class="index-entry-id" id="index-syntax_002c-AVR"></a>

<p>The assembler supports several modifiers when using relocatable addresses
in AVR instruction operands.  The general syntax is the following:
</p>
<div class="example smallexample">
<pre class="example-preformatted">modifier(relocatable-expression)
</pre></div>

<dl class="table">
<dd><a class="index-entry-id" id="index-symbol-modifiers"></a>

</dd>
<dt><code class="code">lo8</code></dt>
<dd>
<p>This modifier allows you to use bits 0 through 7 of
an address expression as an 8 bit relocatable expression.
</p>
</dd>
<dt><code class="code">hi8</code></dt>
<dd>
<p>This modifier allows you to use bits 7 through 15 of an address expression
as an 8 bit relocatable expression. This is useful with, for example, the
AVR &lsquo;<samp class="samp">ldi</samp>&rsquo; instruction and &lsquo;<samp class="samp">lo8</samp>&rsquo; modifier.
</p>
<p>For example
</p>
<div class="example smallexample">
<pre class="example-preformatted">ldi r26, lo8(sym+10)
ldi r27, hi8(sym+10)
</pre></div>

</dd>
<dt><code class="code">hh8</code></dt>
<dd>
<p>This modifier allows you to use bits 16 through 23 of
an address expression as an 8 bit relocatable expression.
Also, can be useful for loading 32 bit constants.
</p>
</dd>
<dt><code class="code">hlo8</code></dt>
<dd>
<p>Synonym of &lsquo;<samp class="samp">hh8</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">hhi8</code></dt>
<dd>
<p>This modifier allows you to use bits 24 through 31 of
an expression as an 8 bit expression. This is useful with, for example, the
AVR &lsquo;<samp class="samp">ldi</samp>&rsquo; instruction and &lsquo;<samp class="samp">lo8</samp>&rsquo;, &lsquo;<samp class="samp">hi8</samp>&rsquo;, &lsquo;<samp class="samp">hlo8</samp>&rsquo;,
&lsquo;<samp class="samp">hhi8</samp>&rsquo;, modifier.
</p>
<p>For example
</p>
<div class="example smallexample">
<pre class="example-preformatted">ldi r26, lo8(285774925)
ldi r27, hi8(285774925)
ldi r28, hlo8(285774925)
ldi r29, hhi8(285774925)
; r29,r28,r27,r26 = 285774925
</pre></div>

</dd>
<dt><code class="code">pm_lo8</code></dt>
<dd>
<p>This modifier allows you to use bits 0 through 7 of
an address expression as an 8 bit relocatable expression.
This modifier is useful for addressing data or code from
Flash/Program memory by two-byte words. The use of &lsquo;<samp class="samp">pm_lo8</samp>&rsquo;
is similar to &lsquo;<samp class="samp">lo8</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">pm_hi8</code></dt>
<dd>
<p>This modifier allows you to use bits 8 through 15 of
an address expression as an 8 bit relocatable expression.
This modifier is useful for addressing data or code from
Flash/Program memory by two-byte words.
</p>
<p>For example, when setting the AVR &lsquo;<samp class="samp">Z</samp>&rsquo; register with the &lsquo;<samp class="samp">ldi</samp>&rsquo;
instruction for subsequent use by the &lsquo;<samp class="samp">ijmp</samp>&rsquo; instruction:
</p>
<div class="example smallexample">
<pre class="example-preformatted">ldi r30, pm_lo8(sym)
ldi r31, pm_hi8(sym)
ijmp
</pre></div>

</dd>
<dt><code class="code">pm_hh8</code></dt>
<dd>
<p>This modifier allows you to use bits 15 through 23 of
an address expression as an 8 bit relocatable expression.
This modifier is useful for addressing data or code from
Flash/Program memory by two-byte words.
</p>
</dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="AVR_002dRegs.html">Register Names</a>, Up: <a href="AVR-Syntax.html">Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
