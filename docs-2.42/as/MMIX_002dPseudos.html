<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>MMIX-Pseudos (Using as)</title>

<meta name="description" content="MMIX-Pseudos (Using as)">
<meta name="keywords" content="MMIX-Pseudos (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="MMIX_002dSyntax.html" rel="up" title="MMIX-Syntax">
<link href="MMIX_002dRegs.html" rel="prev" title="MMIX-Regs">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="MMIX_002dPseudos">
<div class="nav-panel">
<p>
Previous: <a href="MMIX_002dRegs.html" accesskey="p" rel="prev">Register names</a>, Up: <a href="MMIX_002dSyntax.html" accesskey="u" rel="up">Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection subsection-level-set-subsubsection" id="Assembler-Directives-3">9.29.3.4 Assembler Directives</h4>
<a class="index-entry-id" id="index-assembler-directives_002c-MMIX"></a>
<a class="index-entry-id" id="index-pseudo_002dops_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directives"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dops"></a>

<dl class="table">
<dt id='index-assembler-directive-LOC_002c-MMIX'><span><code class="code">LOC</code><a class="copiable-link" href='#index-assembler-directive-LOC_002c-MMIX'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-pseudo_002dop-LOC_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directive-LOC"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dop-LOC"></a>

<a class="anchor" id="MMIX_002dloc"></a><p>The <code class="code">LOC</code> directive sets the current location to the value of the
operand field, which may include changing sections.  If the operand is a
constant, the section is set to either <code class="code">.data</code> if the value is
<code class="code">0x2000000000000000</code> or larger, else it is set to <code class="code">.text</code>.
Within a section, the current location may only be changed to
monotonically higher addresses.  A LOC expression must be a previously
defined symbol or a &ldquo;pure&rdquo; constant.
</p>
<p>An example, which sets the label <var class="var">prev</var> to the current location, and
updates the current location to eight bytes forward:
</p><div class="example smallexample">
<pre class="example-preformatted">prev LOC @+8
</pre></div>

<p>When a LOC has a constant as its operand, a symbol
<code class="code">__.MMIX.start..text</code> or <code class="code">__.MMIX.start..data</code> is defined
depending on the address as mentioned above.  Each such symbol is
interpreted as special by the linker, locating the section at that
address.  Note that if multiple files are linked, the first object file
with that section will be mapped to that address (not necessarily the file
with the LOC definition).
</p>
</dd>
<dt id='index-assembler-directive-LOCAL_002c-MMIX'><span><code class="code">LOCAL</code><a class="copiable-link" href='#index-assembler-directive-LOCAL_002c-MMIX'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-pseudo_002dop-LOCAL_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directive-LOCAL"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dop-LOCAL"></a>

<a class="anchor" id="MMIX_002dlocal"></a><p>Example:
</p><div class="example smallexample">
<pre class="example-preformatted"> LOCAL external_symbol
 LOCAL 42
 .local asymbol
</pre></div>

<p>This directive-operation generates a link-time assertion that the operand
does not correspond to a global register.  The operand is an expression
that at link-time resolves to a register symbol or a number.  A number is
treated as the register having that number.  There is one restriction on
the use of this directive: the pseudo-directive must be placed in a
section with contents, code or data.
</p>
</dd>
<dt id='index-assembler-directive-IS_002c-MMIX'><span><code class="code">IS</code><a class="copiable-link" href='#index-assembler-directive-IS_002c-MMIX'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-pseudo_002dop-IS_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directive-IS"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dop-IS"></a>

<a class="anchor" id="MMIX_002dis"></a><p>The <code class="code">IS</code> directive:
</p><div class="example smallexample">
<pre class="example-preformatted">asymbol IS an_expression
</pre></div>
<p>sets the symbol &lsquo;<samp class="samp">asymbol</samp>&rsquo; to &lsquo;<samp class="samp">an_expression</samp>&rsquo;.  A symbol may not
be set more than once using this directive.  Local labels may be set using
this directive, for example:
</p><div class="example smallexample">
<pre class="example-preformatted">5H IS @+4
</pre></div>

</dd>
<dt id='index-assembler-directive-GREG_002c-MMIX'><span><code class="code">GREG</code><a class="copiable-link" href='#index-assembler-directive-GREG_002c-MMIX'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-pseudo_002dop-GREG_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directive-GREG"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dop-GREG"></a>

<a class="anchor" id="MMIX_002dgreg"></a><p>This directive reserves a global register, gives it an initial value and
optionally gives it a symbolic name.  Some examples:
</p>
<div class="example smallexample">
<pre class="example-preformatted">areg GREG
breg GREG data_value
     GREG data_buffer
     .greg creg, another_data_value
</pre></div>

<p>The symbolic register name can be used in place of a (non-special)
register.  If a value isn&rsquo;t provided, it defaults to zero.  Unless the
option &lsquo;<samp class="samp">--no-merge-gregs</samp>&rsquo; is specified, non-zero registers allocated
with this directive may be eliminated by <code class="code">as</code>; another
register with the same value used in its place.
Any of the instructions
&lsquo;<samp class="samp">CSWAP</samp>&rsquo;,
&lsquo;<samp class="samp">GO</samp>&rsquo;,
&lsquo;<samp class="samp">LDA</samp>&rsquo;,
&lsquo;<samp class="samp">LDBU</samp>&rsquo;,
&lsquo;<samp class="samp">LDB</samp>&rsquo;,
&lsquo;<samp class="samp">LDHT</samp>&rsquo;,
&lsquo;<samp class="samp">LDOU</samp>&rsquo;,
&lsquo;<samp class="samp">LDO</samp>&rsquo;,
&lsquo;<samp class="samp">LDSF</samp>&rsquo;,
&lsquo;<samp class="samp">LDTU</samp>&rsquo;,
&lsquo;<samp class="samp">LDT</samp>&rsquo;,
&lsquo;<samp class="samp">LDUNC</samp>&rsquo;,
&lsquo;<samp class="samp">LDVTS</samp>&rsquo;,
&lsquo;<samp class="samp">LDWU</samp>&rsquo;,
&lsquo;<samp class="samp">LDW</samp>&rsquo;,
&lsquo;<samp class="samp">PREGO</samp>&rsquo;,
&lsquo;<samp class="samp">PRELD</samp>&rsquo;,
&lsquo;<samp class="samp">PREST</samp>&rsquo;,
&lsquo;<samp class="samp">PUSHGO</samp>&rsquo;,
&lsquo;<samp class="samp">STBU</samp>&rsquo;,
&lsquo;<samp class="samp">STB</samp>&rsquo;,
&lsquo;<samp class="samp">STCO</samp>&rsquo;,
&lsquo;<samp class="samp">STHT</samp>&rsquo;,
&lsquo;<samp class="samp">STOU</samp>&rsquo;,
&lsquo;<samp class="samp">STSF</samp>&rsquo;,
&lsquo;<samp class="samp">STTU</samp>&rsquo;,
&lsquo;<samp class="samp">STT</samp>&rsquo;,
&lsquo;<samp class="samp">STUNC</samp>&rsquo;,
&lsquo;<samp class="samp">SYNCD</samp>&rsquo;,
&lsquo;<samp class="samp">SYNCID</samp>&rsquo;,
can have a value nearby <a class="anchor" id="GREG_002dbase"></a>an initial value in place of its
second and third operands.  Here, &ldquo;nearby&rdquo; is defined as within the
range 0&hellip;255 from the initial value of such an allocated register.
</p>
<div class="example smallexample">
<pre class="example-preformatted">buffer1 BYTE 0,0,0,0,0
buffer2 BYTE 0,0,0,0,0
 &hellip;
 GREG buffer1
 LDOU $42,buffer2
</pre></div>
<p>In the example above, the &lsquo;<samp class="samp">Y</samp>&rsquo; field of the <code class="code">LDOUI</code> instruction
(LDOU with a constant Z) will be replaced with the global register
allocated for &lsquo;<samp class="samp">buffer1</samp>&rsquo;, and the &lsquo;<samp class="samp">Z</samp>&rsquo; field will have the value
5, the offset from &lsquo;<samp class="samp">buffer1</samp>&rsquo; to &lsquo;<samp class="samp">buffer2</samp>&rsquo;.  The result is
equivalent to this code:
</p><div class="example smallexample">
<pre class="example-preformatted">buffer1 BYTE 0,0,0,0,0
buffer2 BYTE 0,0,0,0,0
 &hellip;
tmpreg GREG buffer1
 LDOU $42,tmpreg,(buffer2-buffer1)
</pre></div>

<p>Global registers allocated with this directive are allocated in order
higher-to-lower within a file.  Other than that, the exact order of
register allocation and elimination is undefined.  For example, the order
is undefined when more than one file with such directives are linked
together.  With the options &lsquo;<samp class="samp">-x</samp>&rsquo; and &lsquo;<samp class="samp">--linker-allocated-gregs</samp>&rsquo;,
&lsquo;<samp class="samp">GREG</samp>&rsquo; directives for two-operand cases like the one mentioned above
can be omitted.  Sufficient global registers will then be allocated by the
linker.
</p>
</dd>
<dt id='index-assembler-directive-BYTE_002c-MMIX'><span><code class="code">BYTE</code><a class="copiable-link" href='#index-assembler-directive-BYTE_002c-MMIX'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-pseudo_002dop-BYTE_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directive-BYTE"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dop-BYTE"></a>

<a class="anchor" id="MMIX_002dbyte"></a><p>The &lsquo;<samp class="samp">BYTE</samp>&rsquo; directive takes a series of operands separated by a comma.
If an operand is a string (see <a class="pxref" href="Strings.html">Strings</a>), each character of that string
is emitted as a byte.  Other operands must be constant expressions without
forward references, in the range 0&hellip;255.  If you need operands having
expressions with forward references, use &lsquo;<samp class="samp">.byte</samp>&rsquo; (see <a class="pxref" href="Byte.html"><code class="code">.byte <var class="var">expressions</var></code></a>).  An
operand can be omitted, defaulting to a zero value.
</p>
</dd>
<dt id='index-assembler-directive-WYDE_002c-MMIX'><span><code class="code">WYDE</code><a class="copiable-link" href='#index-assembler-directive-WYDE_002c-MMIX'> &para;</a></span></dt>
<dt><code class="code">TETRA</code></dt>
<dt><code class="code">OCTA</code></dt>
<dd><a class="index-entry-id" id="index-pseudo_002dop-WYDE_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directive-WYDE"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dop-WYDE"></a>
<a class="index-entry-id" id="index-assembler-directive-TETRA_002c-MMIX"></a>
<a class="index-entry-id" id="index-pseudo_002dop-TETRA_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directive-TETRA"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dop-TETRA"></a>
<a class="index-entry-id" id="index-assembler-directive-OCTA_002c-MMIX"></a>
<a class="index-entry-id" id="index-pseudo_002dop-OCTA_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directive-OCTA"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dop-OCTA"></a>

<a class="anchor" id="MMIX_002dconstants"></a><p>The directives &lsquo;<samp class="samp">WYDE</samp>&rsquo;, &lsquo;<samp class="samp">TETRA</samp>&rsquo; and &lsquo;<samp class="samp">OCTA</samp>&rsquo; emit constants of
two, four and eight bytes size respectively.  Before anything else happens
for the directive, the current location is aligned to the respective
constant-size boundary.  If a label is defined at the beginning of the
line, its value will be that after the alignment.  A single operand can be
omitted, defaulting to a zero value emitted for the directive.  Operands
can be expressed as strings (see <a class="pxref" href="Strings.html">Strings</a>), in which case each
character in the string is emitted as a separate constant of the size
indicated by the directive.
</p>
</dd>
<dt id='index-assembler-directive-PREFIX_002c-MMIX'><span><code class="code">PREFIX</code><a class="copiable-link" href='#index-assembler-directive-PREFIX_002c-MMIX'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-pseudo_002dop-PREFIX_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directive-PREFIX"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dop-PREFIX"></a>

<a class="anchor" id="MMIX_002dprefix"></a><p>The &lsquo;<samp class="samp">PREFIX</samp>&rsquo; directive sets a symbol name prefix to be prepended to
all symbols (except local symbols, see <a class="pxref" href="MMIX_002dSymbols.html">Symbols</a>), that are not
prefixed with &lsquo;<samp class="samp">:</samp>&rsquo;, until the next &lsquo;<samp class="samp">PREFIX</samp>&rsquo; directive.  Such
prefixes accumulate.  For example,
</p><div class="example smallexample">
<pre class="example-preformatted"> PREFIX a
 PREFIX b
c IS 0
</pre></div>
<p>defines a symbol &lsquo;<samp class="samp">abc</samp>&rsquo; with the value 0.
</p>
</dd>
<dt id='index-assembler-directive-BSPEC_002c-MMIX'><span><code class="code">BSPEC</code><a class="copiable-link" href='#index-assembler-directive-BSPEC_002c-MMIX'> &para;</a></span></dt>
<dt><code class="code">ESPEC</code></dt>
<dd><a class="index-entry-id" id="index-pseudo_002dop-BSPEC_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directive-BSPEC"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dop-BSPEC"></a>
<a class="index-entry-id" id="index-assembler-directive-ESPEC_002c-MMIX"></a>
<a class="index-entry-id" id="index-pseudo_002dop-ESPEC_002c-MMIX"></a>
<a class="index-entry-id" id="index-MMIX-assembler-directive-ESPEC"></a>
<a class="index-entry-id" id="index-MMIX-pseudo_002dop-ESPEC"></a>

<a class="anchor" id="MMIX_002dspec"></a><p>A pair of &lsquo;<samp class="samp">BSPEC</samp>&rsquo; and &lsquo;<samp class="samp">ESPEC</samp>&rsquo; directives delimit a section of
special contents (without specified semantics).  Example:
</p><div class="example smallexample">
<pre class="example-preformatted"> BSPEC 42
 TETRA 1,2,3
 ESPEC
</pre></div>
<p>The single operand to &lsquo;<samp class="samp">BSPEC</samp>&rsquo; must be number in the range
0&hellip;255.  The &lsquo;<samp class="samp">BSPEC</samp>&rsquo; number 80 is used by the GNU binutils
implementation.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="MMIX_002dRegs.html">Register names</a>, Up: <a href="MMIX_002dSyntax.html">Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
