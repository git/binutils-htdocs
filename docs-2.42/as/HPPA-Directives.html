<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>HPPA Directives (Using as)</title>

<meta name="description" content="HPPA Directives (Using as)">
<meta name="keywords" content="HPPA Directives (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="HPPA_002dDependent.html" rel="up" title="HPPA-Dependent">
<link href="HPPA-Opcodes.html" rel="next" title="HPPA Opcodes">
<link href="HPPA-Floating-Point.html" rel="prev" title="HPPA Floating Point">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="HPPA-Directives">
<div class="nav-panel">
<p>
Next: <a href="HPPA-Opcodes.html" accesskey="n" rel="next">Opcodes</a>, Previous: <a href="HPPA-Floating-Point.html" accesskey="p" rel="prev">Floating Point</a>, Up: <a href="HPPA_002dDependent.html" accesskey="u" rel="up">HPPA Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="HPPA-Assembler-Directives">9.15.5 HPPA Assembler Directives</h4>

<p><code class="code">as</code> for the HPPA supports many additional directives for
compatibility with the native assembler.  This section describes them only
briefly.  For detailed information on HPPA-specific assembler directives, see
<cite class="cite">HP9000 Series 800 Assembly Language Reference Manual</cite> (HP 92432-90001).
</p>
<a class="index-entry-id" id="index-HPPA-directives-not-supported"></a>
<p><code class="code">as</code> does <em class="emph">not</em> support the following assembler directives
described in the HP manual:
</p>
<div class="example">
<pre class="example-preformatted">.endm           .liston
.enter          .locct
.leave          .macro
.listoff
</pre></div>

<a class="index-entry-id" id="index-_002eparam-on-HPPA"></a>
<p>Beyond those implemented for compatibility, <code class="code">as</code> supports one
additional assembler directive for the HPPA: <code class="code">.param</code>.  It conveys
register argument locations for static functions.  Its syntax closely follows
the <code class="code">.export</code> directive.
</p>
<a class="index-entry-id" id="index-HPPA_002donly-directives"></a>
<p>These are the additional directives in <code class="code">as</code> for the HPPA:
</p>
<dl class="table">
<dt><code class="code">.block <var class="var">n</var></code></dt>
<dt><code class="code">.blockz <var class="var">n</var></code></dt>
<dd><p>Reserve <var class="var">n</var> bytes of storage, and initialize them to zero.
</p>
</dd>
<dt><code class="code">.call</code></dt>
<dd><p>Mark the beginning of a procedure call.  Only the special case with <em class="emph">no
arguments</em> is allowed.
</p>
</dd>
<dt><code class="code">.callinfo [ <var class="var">param</var>=<var class="var">value</var>, &hellip; ]  [ <var class="var">flag</var>, &hellip; ]</code></dt>
<dd><p>Specify a number of parameters and flags that define the environment for a
procedure.
</p>
<p><var class="var">param</var> may be any of &lsquo;<samp class="samp">frame</samp>&rsquo; (frame size), &lsquo;<samp class="samp">entry_gr</samp>&rsquo; (end of
general register range), &lsquo;<samp class="samp">entry_fr</samp>&rsquo; (end of float register range),
&lsquo;<samp class="samp">entry_sr</samp>&rsquo; (end of space register range).
</p>
<p>The values for <var class="var">flag</var> are &lsquo;<samp class="samp">calls</samp>&rsquo; or &lsquo;<samp class="samp">caller</samp>&rsquo; (proc has
subroutines), &lsquo;<samp class="samp">no_calls</samp>&rsquo; (proc does not call subroutines), &lsquo;<samp class="samp">save_rp</samp>&rsquo;
(preserve return pointer), &lsquo;<samp class="samp">save_sp</samp>&rsquo; (proc preserves stack pointer),
&lsquo;<samp class="samp">no_unwind</samp>&rsquo; (do not unwind this proc), &lsquo;<samp class="samp">hpux_int</samp>&rsquo; (proc is interrupt
routine).
</p>
</dd>
<dt><code class="code">.code</code></dt>
<dd><p>Assemble into the standard section called &lsquo;<samp class="samp">$TEXT$</samp>&rsquo;, subsection
&lsquo;<samp class="samp">$CODE$</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">.copyright &quot;<var class="var">string</var>&quot;</code></dt>
<dd><p>In the SOM object format, insert <var class="var">string</var> into the object code, marked as a
copyright string.
</p>
</dd>
<dt><code class="code">.copyright &quot;<var class="var">string</var>&quot;</code></dt>
<dd><p>In the ELF object format, insert <var class="var">string</var> into the object code, marked as a
version string.
</p>
</dd>
<dt><code class="code">.enter</code></dt>
<dd><p>Not yet supported; the assembler rejects programs containing this directive.
</p>
</dd>
<dt><code class="code">.entry</code></dt>
<dd><p>Mark the beginning of a procedure.
</p>
</dd>
<dt><code class="code">.exit</code></dt>
<dd><p>Mark the end of a procedure.
</p>
</dd>
<dt><code class="code">.export <var class="var">name</var> [ ,<var class="var">typ</var> ]  [ ,<var class="var">param</var>=<var class="var">r</var> ]</code></dt>
<dd><p>Make a procedure <var class="var">name</var> available to callers.  <var class="var">typ</var>, if present, must
be one of &lsquo;<samp class="samp">absolute</samp>&rsquo;, &lsquo;<samp class="samp">code</samp>&rsquo; (ELF only, not SOM), &lsquo;<samp class="samp">data</samp>&rsquo;,
&lsquo;<samp class="samp">entry</samp>&rsquo;, &lsquo;<samp class="samp">data</samp>&rsquo;, &lsquo;<samp class="samp">entry</samp>&rsquo;, &lsquo;<samp class="samp">millicode</samp>&rsquo;, &lsquo;<samp class="samp">plabel</samp>&rsquo;,
&lsquo;<samp class="samp">pri_prog</samp>&rsquo;, or &lsquo;<samp class="samp">sec_prog</samp>&rsquo;.
</p>
<p><var class="var">param</var>, if present, provides either relocation information for the
procedure arguments and result, or a privilege level.  <var class="var">param</var> may be
&lsquo;<samp class="samp">argw<var class="var">n</var></samp>&rsquo; (where <var class="var">n</var> ranges from <code class="code">0</code> to <code class="code">3</code>, and
indicates one of four one-word arguments); &lsquo;<samp class="samp">rtnval</samp>&rsquo; (the procedure&rsquo;s
result); or &lsquo;<samp class="samp">priv_lev</samp>&rsquo; (privilege level).  For arguments or the result,
<var class="var">r</var> specifies how to relocate, and must be one of &lsquo;<samp class="samp">no</samp>&rsquo; (not
relocatable), &lsquo;<samp class="samp">gr</samp>&rsquo; (argument is in general register), &lsquo;<samp class="samp">fr</samp>&rsquo; (in
floating point register), or &lsquo;<samp class="samp">fu</samp>&rsquo; (upper half of float register).
For &lsquo;<samp class="samp">priv_lev</samp>&rsquo;, <var class="var">r</var> is an integer.
</p>
</dd>
<dt><code class="code">.half <var class="var">n</var></code></dt>
<dd><p>Define a two-byte integer constant <var class="var">n</var>; synonym for the portable
<code class="code">as</code> directive <code class="code">.short</code>.
</p>
</dd>
<dt><code class="code">.import <var class="var">name</var> [ ,<var class="var">typ</var> ]</code></dt>
<dd><p>Converse of <code class="code">.export</code>; make a procedure available to call.  The arguments
use the same conventions as the first two arguments for <code class="code">.export</code>.
</p>
</dd>
<dt><code class="code">.label <var class="var">name</var></code></dt>
<dd><p>Define <var class="var">name</var> as a label for the current assembly location.
</p>
</dd>
<dt><code class="code">.leave</code></dt>
<dd><p>Not yet supported; the assembler rejects programs containing this directive.
</p>
</dd>
<dt><code class="code">.origin <var class="var">lc</var></code></dt>
<dd><p>Advance location counter to <var class="var">lc</var>. Synonym for the <code class="code">as</code>
portable directive <code class="code">.org</code>.
</p>
</dd>
<dt><code class="code">.param <var class="var">name</var> [ ,<var class="var">typ</var> ]  [ ,<var class="var">param</var>=<var class="var">r</var> ]</code></dt>
<dd><p>Similar to <code class="code">.export</code>, but used for static procedures.
</p>
</dd>
<dt><code class="code">.proc</code></dt>
<dd><p>Use preceding the first statement of a procedure.
</p>
</dd>
<dt><code class="code">.procend</code></dt>
<dd><p>Use following the last statement of a procedure.
</p>
</dd>
<dt><code class="code"><var class="var">label</var> .reg <var class="var">expr</var></code></dt>
<dd><p>Synonym for <code class="code">.equ</code>; define <var class="var">label</var> with the absolute expression
<var class="var">expr</var> as its value.
</p>
</dd>
<dt><code class="code">.space <var class="var">secname</var> [ ,<var class="var">params</var> ]</code></dt>
<dd><p>Switch to section <var class="var">secname</var>, creating a new section by that name if
necessary.  You may only use <var class="var">params</var> when creating a new section, not
when switching to an existing one.  <var class="var">secname</var> may identify a section by
number rather than by name.
</p>
<p>If specified, the list <var class="var">params</var> declares attributes of the section,
identified by keywords.  The keywords recognized are &lsquo;<samp class="samp">spnum=<var class="var">exp</var></samp>&rsquo;
(identify this section by the number <var class="var">exp</var>, an absolute expression),
&lsquo;<samp class="samp">sort=<var class="var">exp</var></samp>&rsquo; (order sections according to this sort key when linking;
<var class="var">exp</var> is an absolute expression), &lsquo;<samp class="samp">unloadable</samp>&rsquo; (section contains no
loadable data), &lsquo;<samp class="samp">notdefined</samp>&rsquo; (this section defined elsewhere), and
&lsquo;<samp class="samp">private</samp>&rsquo; (data in this section not available to other programs).
</p>
</dd>
<dt><code class="code">.spnum <var class="var">secnam</var></code></dt>
<dd><p>Allocate four bytes of storage, and initialize them with the section number of
the section named <var class="var">secnam</var>.  (You can define the section number with the
HPPA <code class="code">.space</code> directive.)
</p>
<a class="index-entry-id" id="index-string-directive-on-HPPA"></a>
</dd>
<dt><code class="code">.string &quot;<var class="var">str</var>&quot;</code></dt>
<dd><p>Copy the characters in the string <var class="var">str</var> to the object file.
See <a class="xref" href="Strings.html">Strings</a>, for information on escape sequences you can use in
<code class="code">as</code> strings.
</p>
<p><em class="emph">Warning!</em> The HPPA version of <code class="code">.string</code> differs from the
usual <code class="code">as</code> definition: it does <em class="emph">not</em> write a zero byte
after copying <var class="var">str</var>.
</p>
</dd>
<dt><code class="code">.stringz &quot;<var class="var">str</var>&quot;</code></dt>
<dd><p>Like <code class="code">.string</code>, but appends a zero byte after copying <var class="var">str</var> to object
file.
</p>
</dd>
<dt><code class="code">.subspa <var class="var">name</var> [ ,<var class="var">params</var> ]</code></dt>
<dt><code class="code">.nsubspa <var class="var">name</var> [ ,<var class="var">params</var> ]</code></dt>
<dd><p>Similar to <code class="code">.space</code>, but selects a subsection <var class="var">name</var> within the
current section.  You may only specify <var class="var">params</var> when you create a
subsection (in the first instance of <code class="code">.subspa</code> for this <var class="var">name</var>).
</p>
<p>If specified, the list <var class="var">params</var> declares attributes of the subsection,
identified by keywords.  The keywords recognized are &lsquo;<samp class="samp">quad=<var class="var">expr</var></samp>&rsquo;
(&ldquo;quadrant&rdquo; for this subsection), &lsquo;<samp class="samp">align=<var class="var">expr</var></samp>&rsquo; (alignment for
beginning of this subsection; a power of two), &lsquo;<samp class="samp">access=<var class="var">expr</var></samp>&rsquo; (value
for &ldquo;access rights&rdquo; field), &lsquo;<samp class="samp">sort=<var class="var">expr</var></samp>&rsquo; (sorting order for this
subspace in link), &lsquo;<samp class="samp">code_only</samp>&rsquo; (subsection contains only code),
&lsquo;<samp class="samp">unloadable</samp>&rsquo; (subsection cannot be loaded into memory), &lsquo;<samp class="samp">comdat</samp>&rsquo;
(subsection is comdat), &lsquo;<samp class="samp">common</samp>&rsquo; (subsection is common block),
&lsquo;<samp class="samp">dup_comm</samp>&rsquo; (subsection may have duplicate names), or &lsquo;<samp class="samp">zero</samp>&rsquo;
(subsection is all zeros, do not write in object file).
</p>
<p><code class="code">.nsubspa</code> always creates a new subspace with the given name, even
if one with the same name already exists.
</p>
<p>&lsquo;<samp class="samp">comdat</samp>&rsquo;, &lsquo;<samp class="samp">common</samp>&rsquo; and &lsquo;<samp class="samp">dup_comm</samp>&rsquo; can be used to implement
various flavors of one-only support when using the SOM linker.  The SOM
linker only supports specific combinations of these flags.  The details
are not documented.  A brief description is provided here.
</p>
<p>&lsquo;<samp class="samp">comdat</samp>&rsquo; provides a form of linkonce support.  It is useful for
both code and data subspaces.  A &lsquo;<samp class="samp">comdat</samp>&rsquo; subspace has a key symbol
marked by the &lsquo;<samp class="samp">is_comdat</samp>&rsquo; flag or &lsquo;<samp class="samp">ST_COMDAT</samp>&rsquo;.  Only the first
subspace for any given key is selected.  The key symbol becomes universal
in shared links.  This is similar to the behavior of &lsquo;<samp class="samp">secondary_def</samp>&rsquo;
symbols.
</p>
<p>&lsquo;<samp class="samp">common</samp>&rsquo; provides Fortran named common support.  It is only useful
for data subspaces.  Symbols with the flag &lsquo;<samp class="samp">is_common</samp>&rsquo; retain this
flag in shared links.  Referencing a &lsquo;<samp class="samp">is_common</samp>&rsquo; symbol in a shared
library from outside the library doesn&rsquo;t work.  Thus, &lsquo;<samp class="samp">is_common</samp>&rsquo;
symbols must be output whenever they are needed.
</p>
<p>&lsquo;<samp class="samp">common</samp>&rsquo; and &lsquo;<samp class="samp">dup_comm</samp>&rsquo; together provide Cobol common support.
The subspaces in this case must all be the same length.  Otherwise, this
support is similar to the Fortran common support.
</p>
<p>&lsquo;<samp class="samp">dup_comm</samp>&rsquo; by itself provides a type of one-only support for code.
Only the first &lsquo;<samp class="samp">dup_comm</samp>&rsquo; subspace is selected.  There is a rather
complex algorithm to compare subspaces.  Code symbols marked with the
&lsquo;<samp class="samp">dup_common</samp>&rsquo; flag are hidden.  This support was intended for &quot;C++
duplicate inlines&quot;.
</p>
<p>A simplified technique is used to mark the flags of symbols based on
the flags of their subspace.  A symbol with the scope SS_UNIVERSAL and
type ST_ENTRY, ST_CODE or ST_DATA is marked with the corresponding
settings of &lsquo;<samp class="samp">comdat</samp>&rsquo;, &lsquo;<samp class="samp">common</samp>&rsquo; and &lsquo;<samp class="samp">dup_comm</samp>&rsquo; from the
subspace, respectively.  This avoids having to introduce additional
directives to mark these symbols.  The HP assembler sets &lsquo;<samp class="samp">is_common</samp>&rsquo;
from &lsquo;<samp class="samp">common</samp>&rsquo;.  However, it doesn&rsquo;t set the &lsquo;<samp class="samp">dup_common</samp>&rsquo; from
&lsquo;<samp class="samp">dup_comm</samp>&rsquo;.  It doesn&rsquo;t have &lsquo;<samp class="samp">comdat</samp>&rsquo; support.
</p>
</dd>
<dt><code class="code">.version &quot;<var class="var">str</var>&quot;</code></dt>
<dd><p>Write <var class="var">str</var> as version identifier in object code.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="HPPA-Opcodes.html">Opcodes</a>, Previous: <a href="HPPA-Floating-Point.html">Floating Point</a>, Up: <a href="HPPA_002dDependent.html">HPPA Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
