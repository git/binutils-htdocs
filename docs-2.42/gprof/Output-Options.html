<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the gprof profiler of the GNU system.

Copyright © 1988-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Output Options (GNU gprof)</title>

<meta name="description" content="Output Options (GNU gprof)">
<meta name="keywords" content="Output Options (GNU gprof)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Invoking.html" rel="up" title="Invoking">
<link href="Analysis-Options.html" rel="next" title="Analysis Options">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
kbd.kbd {font-style: oblique}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Output-Options">
<div class="nav-panel">
<p>
Next: <a href="Analysis-Options.html" accesskey="n" rel="next">Analysis Options</a>, Up: <a href="Invoking.html" accesskey="u" rel="up"><code class="code">gprof</code> Command Summary</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h3 class="section" id="Output-Options-1">4.1 Output Options</h3>

<p>These options specify which of several output formats
<code class="code">gprof</code> should produce.
</p>
<p>Many of these options take an optional <em class="dfn">symspec</em> to specify
functions to be included or excluded.  These options can be
specified multiple times, with different symspecs, to include
or exclude sets of symbols.  See <a class="xref" href="Symspecs.html">Symspecs</a>.
</p>
<p>Specifying any of these options overrides the default (&lsquo;<samp class="samp">-p -q</samp>&rsquo;),
which prints a flat profile and call graph analysis
for all functions.
</p>
<dl class="table">
<dt><code class="code">-A[<var class="var">symspec</var>]</code></dt>
<dt><code class="code">--annotated-source[=<var class="var">symspec</var>]</code></dt>
<dd><p>The &lsquo;<samp class="samp">-A</samp>&rsquo; option causes <code class="code">gprof</code> to print annotated source code.
If <var class="var">symspec</var> is specified, print output only for matching symbols.
See <a class="xref" href="Annotated-Source.html">The Annotated Source Listing</a>.
</p>
</dd>
<dt><code class="code">-b</code></dt>
<dt><code class="code">--brief</code></dt>
<dd><p>If the &lsquo;<samp class="samp">-b</samp>&rsquo; option is given, <code class="code">gprof</code> doesn&rsquo;t print the
verbose blurbs that try to explain the meaning of all of the fields in
the tables.  This is useful if you intend to print out the output, or
are tired of seeing the blurbs.
</p>
</dd>
<dt><code class="code">-B</code></dt>
<dd><p>The &lsquo;<samp class="samp">-B</samp>&rsquo; option causes <code class="code">gprof</code> to print the call graph analysis.
</p>
</dd>
<dt><code class="code">-C[<var class="var">symspec</var>]</code></dt>
<dt><code class="code">--exec-counts[=<var class="var">symspec</var>]</code></dt>
<dd><p>The &lsquo;<samp class="samp">-C</samp>&rsquo; option causes <code class="code">gprof</code> to
print a tally of functions and the number of times each was called.
If <var class="var">symspec</var> is specified, print tally only for matching symbols.
</p>
<p>If the profile data file contains basic-block count records, specifying
the &lsquo;<samp class="samp">-l</samp>&rsquo; option, along with &lsquo;<samp class="samp">-C</samp>&rsquo;, will cause basic-block
execution counts to be tallied and displayed.
</p>
</dd>
<dt><code class="code">-i</code></dt>
<dt><code class="code">--file-info</code></dt>
<dd><p>The &lsquo;<samp class="samp">-i</samp>&rsquo; option causes <code class="code">gprof</code> to display summary information
about the profile data file(s) and then exit.  The number of histogram,
call graph, and basic-block count records is displayed.
</p>
</dd>
<dt><code class="code">-I <var class="var">dirs</var></code></dt>
<dt><code class="code">--directory-path=<var class="var">dirs</var></code></dt>
<dd><p>The &lsquo;<samp class="samp">-I</samp>&rsquo; option specifies a list of search directories in
which to find source files.  Environment variable <var class="var">GPROF_PATH</var>
can also be used to convey this information.
Used mostly for annotated source output.
</p>
</dd>
<dt><code class="code">-J[<var class="var">symspec</var>]</code></dt>
<dt><code class="code">--no-annotated-source[=<var class="var">symspec</var>]</code></dt>
<dd><p>The &lsquo;<samp class="samp">-J</samp>&rsquo; option causes <code class="code">gprof</code> not to
print annotated source code.
If <var class="var">symspec</var> is specified, <code class="code">gprof</code> prints annotated source,
but excludes matching symbols.
</p>
</dd>
<dt><code class="code">-L</code></dt>
<dt><code class="code">--print-path</code></dt>
<dd><p>Normally, source filenames are printed with the path
component suppressed.  The &lsquo;<samp class="samp">-L</samp>&rsquo; option causes <code class="code">gprof</code>
to print the full pathname of
source filenames, which is determined
from symbolic debugging information in the image file
and is relative to the directory in which the compiler
was invoked.
</p>
</dd>
<dt><code class="code">-p[<var class="var">symspec</var>]</code></dt>
<dt><code class="code">--flat-profile[=<var class="var">symspec</var>]</code></dt>
<dd><p>The &lsquo;<samp class="samp">-p</samp>&rsquo; option causes <code class="code">gprof</code> to print a flat profile.
If <var class="var">symspec</var> is specified, print flat profile only for matching symbols.
See <a class="xref" href="Flat-Profile.html">The Flat Profile</a>.
</p>
</dd>
<dt><code class="code">-P[<var class="var">symspec</var>]</code></dt>
<dt><code class="code">--no-flat-profile[=<var class="var">symspec</var>]</code></dt>
<dd><p>The &lsquo;<samp class="samp">-P</samp>&rsquo; option causes <code class="code">gprof</code> to suppress printing a flat profile.
If <var class="var">symspec</var> is specified, <code class="code">gprof</code> prints a flat profile,
but excludes matching symbols.
</p>
</dd>
<dt><code class="code">-q[<var class="var">symspec</var>]</code></dt>
<dt><code class="code">--graph[=<var class="var">symspec</var>]</code></dt>
<dd><p>The &lsquo;<samp class="samp">-q</samp>&rsquo; option causes <code class="code">gprof</code> to print the call graph analysis.
If <var class="var">symspec</var> is specified, print call graph only for matching symbols
and their children.
See <a class="xref" href="Call-Graph.html">The Call Graph</a>.
</p>
</dd>
<dt><code class="code">-Q[<var class="var">symspec</var>]</code></dt>
<dt><code class="code">--no-graph[=<var class="var">symspec</var>]</code></dt>
<dd><p>The &lsquo;<samp class="samp">-Q</samp>&rsquo; option causes <code class="code">gprof</code> to suppress printing the
call graph.
If <var class="var">symspec</var> is specified, <code class="code">gprof</code> prints a call graph,
but excludes matching symbols.
</p>
</dd>
<dt><code class="code">-t</code></dt>
<dt><code class="code">--table-length=<var class="var">num</var></code></dt>
<dd><p>The &lsquo;<samp class="samp">-t</samp>&rsquo; option causes the <var class="var">num</var> most active source lines in
each source file to be listed when source annotation is enabled.  The
default is 10.
</p>
</dd>
<dt><code class="code">-y</code></dt>
<dt><code class="code">--separate-files</code></dt>
<dd><p>This option affects annotated source output only.
Normally, <code class="code">gprof</code> prints annotated source files
to standard-output.  If this option is specified,
annotated source for a file named <samp class="file">path/<var class="var">filename</var></samp>
is generated in the file <samp class="file"><var class="var">filename</var>-ann</samp>.  If the underlying
file system would truncate <samp class="file"><var class="var">filename</var>-ann</samp> so that it
overwrites the original <samp class="file"><var class="var">filename</var></samp>, <code class="code">gprof</code> generates
annotated source in the file <samp class="file"><var class="var">filename</var>.ann</samp> instead (if the
original file name has an extension, that extension is <em class="emph">replaced</em>
with <samp class="file">.ann</samp>).
</p>
</dd>
<dt><code class="code">-Z[<var class="var">symspec</var>]</code></dt>
<dt><code class="code">--no-exec-counts[=<var class="var">symspec</var>]</code></dt>
<dd><p>The &lsquo;<samp class="samp">-Z</samp>&rsquo; option causes <code class="code">gprof</code> not to
print a tally of functions and the number of times each was called.
If <var class="var">symspec</var> is specified, print tally, but exclude matching symbols.
</p>
</dd>
<dt><code class="code">-r</code></dt>
<dt><code class="code">--function-ordering</code></dt>
<dd><p>The &lsquo;<samp class="samp">--function-ordering</samp>&rsquo; option causes <code class="code">gprof</code> to print a
suggested function ordering for the program based on profiling data.
This option suggests an ordering which may improve paging, tlb and
cache behavior for the program on systems which support arbitrary
ordering of functions in an executable.
</p>
<p>The exact details of how to force the linker to place functions
in a particular order is system dependent and out of the scope of this
manual.
</p>
</dd>
<dt><code class="code">-R <var class="var">map_file</var></code></dt>
<dt><code class="code">--file-ordering <var class="var">map_file</var></code></dt>
<dd><p>The &lsquo;<samp class="samp">--file-ordering</samp>&rsquo; option causes <code class="code">gprof</code> to print a
suggested .o link line ordering for the program based on profiling data.
This option suggests an ordering which may improve paging, tlb and
cache behavior for the program on systems which do not support arbitrary
ordering of functions in an executable.
</p>
<p>Use of the &lsquo;<samp class="samp">-a</samp>&rsquo; argument is highly recommended with this option.
</p>
<p>The <var class="var">map_file</var> argument is a pathname to a file which provides
function name to object file mappings.  The format of the file is similar to
the output of the program <code class="code">nm</code>.
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">c-parse.o:00000000 T yyparse
c-parse.o:00000004 C yyerrflag
c-lang.o:00000000 T maybe_objc_method_name
c-lang.o:00000000 T print_lang_statistics
c-lang.o:00000000 T recognize_objc_keyword
c-decl.o:00000000 T print_lang_identifier
c-decl.o:00000000 T print_lang_type
&hellip;

</pre></div></div>

<p>To create a <var class="var">map_file</var> with <small class="sc">GNU</small> <code class="code">nm</code>, type a command like
<kbd class="kbd">nm --extern-only --defined-only -v --print-file-name program-name</kbd>.
</p>
</dd>
<dt><code class="code">-T</code></dt>
<dt><code class="code">--traditional</code></dt>
<dd><p>The &lsquo;<samp class="samp">-T</samp>&rsquo; option causes <code class="code">gprof</code> to print its output in
&ldquo;traditional&rdquo; BSD style.
</p>
</dd>
<dt><code class="code">-w <var class="var">width</var></code></dt>
<dt><code class="code">--width=<var class="var">width</var></code></dt>
<dd><p>Sets width of output lines to <var class="var">width</var>.
Currently only used when printing the function index at the bottom
of the call graph.
</p>
</dd>
<dt><code class="code">-x</code></dt>
<dt><code class="code">--all-lines</code></dt>
<dd><p>This option affects annotated source output only.
By default, only the lines at the beginning of a basic-block
are annotated.  If this option is specified, every line in
a basic-block is annotated by repeating the annotation for the
first line.  This behavior is similar to <code class="code">tcov</code>&rsquo;s &lsquo;<samp class="samp">-a</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">--demangle[=<var class="var">style</var>]</code></dt>
<dt><code class="code">--no-demangle</code></dt>
<dd><p>These options control whether C++ symbol names should be demangled when
printing output.  The default is to demangle symbols.  The
<code class="code">--no-demangle</code> option may be used to turn off demangling. Different
compilers have different mangling styles.  The optional demangling style
argument can be used to choose an appropriate demangling style for your
compiler.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Analysis-Options.html">Analysis Options</a>, Up: <a href="Invoking.html"><code class="code">gprof</code> Command Summary</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
