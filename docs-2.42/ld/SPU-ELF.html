<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.42.

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>SPU ELF (LD)</title>

<meta name="description" content="SPU ELF (LD)">
<meta name="keywords" content="SPU ELF (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Machine-Dependent.html" rel="up" title="Machine Dependent">
<link href="TI-COFF.html" rel="next" title="TI COFF">
<link href="S_002f390-ELF.html" rel="prev" title="S/390 ELF">


</head>

<body lang="en">
<div class="section-level-extent" id="SPU-ELF">
<div class="nav-panel">
<p>
Next: <a href="TI-COFF.html" accesskey="n" rel="next"><code class="command">ld</code>&rsquo;s Support for Various TI COFF Versions</a>, Previous: <a href="S_002f390-ELF.html" accesskey="p" rel="prev"><code class="command">ld</code> and S/390 ELF Support</a>, Up: <a href="Machine-Dependent.html" accesskey="u" rel="up">Machine Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="ld-and-SPU-ELF-Support">6.14 <code class="command">ld</code> and SPU ELF Support</h3>

<a class="index-entry-id" id="index-SPU-ELF-options"></a>
<dl class="table">
<dd>
<a class="index-entry-id" id="index-SPU-plugins"></a>
<a class="index-entry-id" id="index-_002d_002dplugin"></a>
</dd>
<dt><samp class="option">--plugin</samp></dt>
<dd><p>This option marks an executable as a PIC plugin module.
</p>
<a class="index-entry-id" id="index-SPU-overlays"></a>
<a class="index-entry-id" id="index-_002d_002dno_002doverlays"></a>
</dd>
<dt><samp class="option">--no-overlays</samp></dt>
<dd><p>Normally, <code class="command">ld</code> recognizes calls to functions within overlay
regions, and redirects such calls to an overlay manager via a stub.
<code class="command">ld</code> also provides a built-in overlay manager.  This option
turns off all this special overlay handling.
</p>
<a class="index-entry-id" id="index-SPU-overlay-stub-symbols"></a>
<a class="index-entry-id" id="index-_002d_002demit_002dstub_002dsyms-2"></a>
</dd>
<dt><samp class="option">--emit-stub-syms</samp></dt>
<dd><p>This option causes <code class="command">ld</code> to label overlay stubs with a local
symbol that encodes the stub type and destination.
</p>
<a class="index-entry-id" id="index-SPU-extra-overlay-stubs"></a>
<a class="index-entry-id" id="index-_002d_002dextra_002doverlay_002dstubs"></a>
</dd>
<dt><samp class="option">--extra-overlay-stubs</samp></dt>
<dd><p>This option causes <code class="command">ld</code> to add overlay call stubs on all
function calls out of overlay regions.  Normally stubs are not added
on calls to non-overlay regions.
</p>
<a class="index-entry-id" id="index-SPU-local-store-size"></a>
<a class="index-entry-id" id="index-_002d_002dlocal_002dstore_003dlo_003ahi"></a>
</dd>
<dt><samp class="option">--local-store=lo:hi</samp></dt>
<dd><p><code class="command">ld</code> usually checks that a final executable for SPU fits in
the address range 0 to 256k.  This option may be used to change the
range.  Disable the check entirely with <samp class="option">--local-store=0:0</samp>.
</p>
<a class="index-entry-id" id="index-SPU"></a>
<a class="index-entry-id" id="index-_002d_002dstack_002danalysis"></a>
</dd>
<dt><samp class="option">--stack-analysis</samp></dt>
<dd><p>SPU local store space is limited.  Over-allocation of stack space
unnecessarily limits space available for code and data, while
under-allocation results in runtime failures.  If given this option,
<code class="command">ld</code> will provide an estimate of maximum stack usage.
<code class="command">ld</code> does this by examining symbols in code sections to
determine the extents of functions, and looking at function prologues
for stack adjusting instructions.  A call-graph is created by looking
for relocations on branch instructions.  The graph is then searched
for the maximum stack usage path.  Note that this analysis does not
find calls made via function pointers, and does not handle recursion
and other cycles in the call graph.  Stack usage may be
under-estimated if your code makes such calls.  Also, stack usage for
dynamic allocation, e.g. alloca, will not be detected.  If a link map
is requested, detailed information about each function&rsquo;s stack usage
and calls will be given.
</p>
<a class="index-entry-id" id="index-SPU-1"></a>
<a class="index-entry-id" id="index-_002d_002demit_002dstack_002dsyms"></a>
</dd>
<dt><samp class="option">--emit-stack-syms</samp></dt>
<dd><p>This option, if given along with <samp class="option">--stack-analysis</samp> will result
in <code class="command">ld</code> emitting stack sizing symbols for each function.
These take the form <code class="code">__stack_&lt;function_name&gt;</code> for global
functions, and <code class="code">__stack_&lt;number&gt;_&lt;function_name&gt;</code> for static
functions.  <code class="code">&lt;number&gt;</code> is the section id in hex.  The value of
such symbols is the stack requirement for the corresponding function.
The symbol size will be zero, type <code class="code">STT_NOTYPE</code>, binding
<code class="code">STB_LOCAL</code>, and section <code class="code">SHN_ABS</code>.
</p></dd>
</dl>



</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="TI-COFF.html"><code class="command">ld</code>&rsquo;s Support for Various TI COFF Versions</a>, Previous: <a href="S_002f390-ELF.html"><code class="command">ld</code> and S/390 ELF Support</a>, Up: <a href="Machine-Dependent.html">Machine Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
