<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.42.

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>MRI (LD)</title>

<meta name="description" content="MRI (LD)">
<meta name="keywords" content="MRI (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="GNU-Free-Documentation-License.html" rel="next" title="GNU Free Documentation License">
<link href="Reporting-Bugs.html" rel="prev" title="Reporting Bugs">


</head>

<body lang="en">
<div class="appendix-level-extent" id="MRI">
<div class="nav-panel">
<p>
Next: <a href="GNU-Free-Documentation-License.html" accesskey="n" rel="next">GNU Free Documentation License</a>, Previous: <a href="Reporting-Bugs.html" accesskey="p" rel="prev">Reporting Bugs</a>, Up: <a href="index.html" accesskey="u" rel="up">LD</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h2 class="appendix" id="MRI-Compatible-Script-Files">Appendix A MRI Compatible Script Files</h2>
<a class="index-entry-id" id="index-MRI-compatibility"></a>
<p>To aid users making the transition to <small class="sc">GNU</small> <code class="command">ld</code> from the MRI
linker, <code class="command">ld</code> can use MRI compatible linker scripts as an
alternative to the more general-purpose linker scripting language
described in <a class="ref" href="Scripts.html">Linker Scripts</a>.  MRI compatible linker scripts have a much
simpler command set than the scripting language otherwise used with
<code class="command">ld</code>.  <small class="sc">GNU</small> <code class="command">ld</code> supports the most commonly used MRI
linker commands; these commands are described here.
</p>
<p>In general, MRI scripts aren&rsquo;t of much use with the <code class="code">a.out</code> object
file format, since it only has three sections and MRI scripts lack some
features to make use of them.
</p>
<p>You can specify a file containing an MRI-compatible script using the
&lsquo;<samp class="samp">-c</samp>&rsquo; command-line option.
</p>
<p>Each command in an MRI-compatible script occupies its own line; each
command line starts with the keyword that identifies the command (though
blank lines are also allowed for punctuation).  If a line of an
MRI-compatible script begins with an unrecognized keyword, <code class="command">ld</code>
issues a warning message, but continues processing the script.
</p>
<p>Lines beginning with &lsquo;<samp class="samp">*</samp>&rsquo; are comments.
</p>
<p>You can write these commands using all upper-case letters, or all
lower case; for example, &lsquo;<samp class="samp">chip</samp>&rsquo; is the same as &lsquo;<samp class="samp">CHIP</samp>&rsquo;.
The following list shows only the upper-case form of each command.
</p>
<dl class="table">
<dd><a class="index-entry-id" id="index-ABSOLUTE-_0028MRI_0029"></a>
</dd>
<dt><code class="code">ABSOLUTE <var class="var">secname</var></code></dt>
<dt><code class="code">ABSOLUTE <var class="var">secname</var>, <var class="var">secname</var>, &hellip; <var class="var">secname</var></code></dt>
<dd><p>Normally, <code class="command">ld</code> includes in the output file all sections from all
the input files.  However, in an MRI-compatible script, you can use the
<code class="code">ABSOLUTE</code> command to restrict the sections that will be present in
your output program.  If the <code class="code">ABSOLUTE</code> command is used at all in a
script, then only the sections named explicitly in <code class="code">ABSOLUTE</code>
commands will appear in the linker output.  You can still use other
input sections (whatever you select on the command line, or using
<code class="code">LOAD</code>) to resolve addresses in the output file.
</p>
<a class="index-entry-id" id="index-ALIAS-_0028MRI_0029"></a>
</dd>
<dt><code class="code">ALIAS <var class="var">out-secname</var>, <var class="var">in-secname</var></code></dt>
<dd><p>Use this command to place the data from input section <var class="var">in-secname</var>
in a section called <var class="var">out-secname</var> in the linker output file.
</p>
<p><var class="var">in-secname</var> may be an integer.
</p>
<a class="index-entry-id" id="index-ALIGN-_0028MRI_0029"></a>
</dd>
<dt><code class="code">ALIGN <var class="var">secname</var> = <var class="var">expression</var></code></dt>
<dd><p>Align the section called <var class="var">secname</var> to <var class="var">expression</var>.  The
<var class="var">expression</var> should be a power of two.
</p>
<a class="index-entry-id" id="index-BASE-_0028MRI_0029"></a>
</dd>
<dt><code class="code">BASE <var class="var">expression</var></code></dt>
<dd><p>Use the value of <var class="var">expression</var> as the lowest address (other than
absolute addresses) in the output file.
</p>
<a class="index-entry-id" id="index-CHIP-_0028MRI_0029"></a>
</dd>
<dt><code class="code">CHIP <var class="var">expression</var></code></dt>
<dt><code class="code">CHIP <var class="var">expression</var>, <var class="var">expression</var></code></dt>
<dd><p>This command does nothing; it is accepted only for compatibility.
</p>
<a class="index-entry-id" id="index-END-_0028MRI_0029"></a>
</dd>
<dt><code class="code">END</code></dt>
<dd><p>This command does nothing whatever; it&rsquo;s only accepted for compatibility.
</p>
<a class="index-entry-id" id="index-FORMAT-_0028MRI_0029"></a>
</dd>
<dt><code class="code">FORMAT <var class="var">output-format</var></code></dt>
<dd><p>Similar to the <code class="code">OUTPUT_FORMAT</code> command in the more general linker
language, but restricted to S-records, if <var class="var">output-format</var> is &lsquo;<samp class="samp">S</samp>&rsquo;
</p>
<a class="index-entry-id" id="index-LIST-_0028MRI_0029"></a>
</dd>
<dt><code class="code">LIST <var class="var">anything</var>&hellip;</code></dt>
<dd><p>Print (to the standard output file) a link map, as produced by the
<code class="command">ld</code> command-line option &lsquo;<samp class="samp">-M</samp>&rsquo;.
</p>
<p>The keyword <code class="code">LIST</code> may be followed by anything on the
same line, with no change in its effect.
</p>
<a class="index-entry-id" id="index-LOAD-_0028MRI_0029"></a>
</dd>
<dt><code class="code">LOAD <var class="var">filename</var></code></dt>
<dt><code class="code">LOAD <var class="var">filename</var>, <var class="var">filename</var>, &hellip; <var class="var">filename</var></code></dt>
<dd><p>Include one or more object file <var class="var">filename</var> in the link; this has the
same effect as specifying <var class="var">filename</var> directly on the <code class="command">ld</code>
command line.
</p>
<a class="index-entry-id" id="index-NAME-_0028MRI_0029"></a>
</dd>
<dt><code class="code">NAME <var class="var">output-name</var></code></dt>
<dd><p><var class="var">output-name</var> is the name for the program produced by <code class="command">ld</code>; the
MRI-compatible command <code class="code">NAME</code> is equivalent to the command-line
option &lsquo;<samp class="samp">-o</samp>&rsquo; or the general script language command <code class="code">OUTPUT</code>.
</p>
<a class="index-entry-id" id="index-ORDER-_0028MRI_0029"></a>
</dd>
<dt><code class="code">ORDER <var class="var">secname</var>, <var class="var">secname</var>, &hellip; <var class="var">secname</var></code></dt>
<dt><code class="code">ORDER <var class="var">secname</var> <var class="var">secname</var> <var class="var">secname</var></code></dt>
<dd><p>Normally, <code class="command">ld</code> orders the sections in its output file in the
order in which they first appear in the input files.  In an MRI-compatible
script, you can override this ordering with the <code class="code">ORDER</code> command.  The
sections you list with <code class="code">ORDER</code> will appear first in your output
file, in the order specified.
</p>
<a class="index-entry-id" id="index-PUBLIC-_0028MRI_0029"></a>
</dd>
<dt><code class="code">PUBLIC <var class="var">name</var>=<var class="var">expression</var></code></dt>
<dt><code class="code">PUBLIC <var class="var">name</var>,<var class="var">expression</var></code></dt>
<dt><code class="code">PUBLIC <var class="var">name</var> <var class="var">expression</var></code></dt>
<dd><p>Supply a value (<var class="var">expression</var>) for external symbol
<var class="var">name</var> used in the linker input files.
</p>
<a class="index-entry-id" id="index-SECT-_0028MRI_0029"></a>
</dd>
<dt><code class="code">SECT <var class="var">secname</var>, <var class="var">expression</var></code></dt>
<dt><code class="code">SECT <var class="var">secname</var>=<var class="var">expression</var></code></dt>
<dt><code class="code">SECT <var class="var">secname</var> <var class="var">expression</var></code></dt>
<dd><p>You can use any of these three forms of the <code class="code">SECT</code> command to
specify the start address (<var class="var">expression</var>) for section <var class="var">secname</var>.
If you have more than one <code class="code">SECT</code> statement for the same
<var class="var">secname</var>, only the <em class="emph">first</em> sets the start address.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="GNU-Free-Documentation-License.html">GNU Free Documentation License</a>, Previous: <a href="Reporting-Bugs.html">Reporting Bugs</a>, Up: <a href="index.html">LD</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
