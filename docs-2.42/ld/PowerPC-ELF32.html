<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.42.

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>PowerPC ELF32 (LD)</title>

<meta name="description" content="PowerPC ELF32 (LD)">
<meta name="keywords" content="PowerPC ELF32 (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Machine-Dependent.html" rel="up" title="Machine Dependent">
<link href="PowerPC64-ELF64.html" rel="next" title="PowerPC64 ELF64">
<link href="Nios-II.html" rel="prev" title="Nios II">


</head>

<body lang="en">
<div class="section-level-extent" id="PowerPC-ELF32">
<div class="nav-panel">
<p>
Next: <a href="PowerPC64-ELF64.html" accesskey="n" rel="next"><code class="command">ld</code> and PowerPC64 64-bit ELF Support</a>, Previous: <a href="Nios-II.html" accesskey="p" rel="prev"><code class="command">ld</code> and the Altera Nios II</a>, Up: <a href="Machine-Dependent.html" accesskey="u" rel="up">Machine Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="ld-and-PowerPC-32_002dbit-ELF-Support">6.11 <code class="command">ld</code> and PowerPC 32-bit ELF Support</h3>
<a class="index-entry-id" id="index-PowerPC-long-branches"></a>
<a class="index-entry-id" id="index-_002d_002drelax-on-PowerPC"></a>
<p>Branches on PowerPC processors are limited to a signed 26-bit
displacement, which may result in <code class="command">ld</code> giving
&lsquo;<samp class="samp">relocation truncated to fit</samp>&rsquo; errors with very large programs.
&lsquo;<samp class="samp">--relax</samp>&rsquo; enables the generation of trampolines that can access
the entire 32-bit address space.  These trampolines are inserted at
section boundaries, so may not themselves be reachable if an input
section exceeds 33M in size.  You may combine &lsquo;<samp class="samp">-r</samp>&rsquo; and
&lsquo;<samp class="samp">--relax</samp>&rsquo; to add trampolines in a partial link.  In that case
both branches to undefined symbols and inter-section branches are also
considered potentially out of range, and trampolines inserted.
</p>
<a class="index-entry-id" id="index-PowerPC-ELF32-options"></a>
<dl class="table">
<dd><a class="index-entry-id" id="index-PowerPC-PLT"></a>
<a class="index-entry-id" id="index-_002d_002dbss_002dplt"></a>
</dd>
<dt><samp class="option">--bss-plt</samp></dt>
<dd><p>Current PowerPC GCC accepts a &lsquo;<samp class="samp">-msecure-plt</samp>&rsquo; option that
generates code capable of using a newer PLT and GOT layout that has
the security advantage of no executable section ever needing to be
writable and no writable section ever being executable.  PowerPC
<code class="command">ld</code> will generate this layout, including stubs to access the
PLT, if all input files (including startup and static libraries) were
compiled with &lsquo;<samp class="samp">-msecure-plt</samp>&rsquo;.  &lsquo;<samp class="samp">--bss-plt</samp>&rsquo; forces the old
BSS PLT (and GOT layout) which can give slightly better performance.
</p>
<a class="index-entry-id" id="index-_002d_002dsecure_002dplt"></a>
</dd>
<dt><samp class="option">--secure-plt</samp></dt>
<dd><p><code class="command">ld</code> will use the new PLT and GOT layout if it is linking new
&lsquo;<samp class="samp">-fpic</samp>&rsquo; or &lsquo;<samp class="samp">-fPIC</samp>&rsquo; code, but does not do so automatically
when linking non-PIC code.  This option requests the new PLT and GOT
layout.  A warning will be given if some object file requires the old
style BSS PLT.
</p>
<a class="index-entry-id" id="index-PowerPC-GOT"></a>
<a class="index-entry-id" id="index-_002d_002dsdata_002dgot"></a>
</dd>
<dt><samp class="option">--sdata-got</samp></dt>
<dd><p>The new secure PLT and GOT are placed differently relative to other
sections compared to older BSS PLT and GOT placement.  The location of
<code class="code">.plt</code> must change because the new secure PLT is an initialized
section while the old PLT is uninitialized.  The reason for the
<code class="code">.got</code> change is more subtle:  The new placement allows
<code class="code">.got</code> to be read-only in applications linked with
&lsquo;<samp class="samp">-z relro -z now</samp>&rsquo;.  However, this placement means that
<code class="code">.sdata</code> cannot always be used in shared libraries, because the
PowerPC ABI accesses <code class="code">.sdata</code> in shared libraries from the GOT
pointer.  &lsquo;<samp class="samp">--sdata-got</samp>&rsquo; forces the old GOT placement.  PowerPC
GCC doesn&rsquo;t use <code class="code">.sdata</code> in shared libraries, so this option is
really only useful for other compilers that may do so.
</p>
<a class="index-entry-id" id="index-PowerPC-stub-symbols"></a>
<a class="index-entry-id" id="index-_002d_002demit_002dstub_002dsyms"></a>
</dd>
<dt><samp class="option">--emit-stub-syms</samp></dt>
<dd><p>This option causes <code class="command">ld</code> to label linker stubs with a local
symbol that encodes the stub type and destination.
</p>
<a class="index-entry-id" id="index-PowerPC-TLS-optimization"></a>
<a class="index-entry-id" id="index-_002d_002dno_002dtls_002doptimize"></a>
</dd>
<dt><samp class="option">--no-tls-optimize</samp></dt>
<dd><p>PowerPC <code class="command">ld</code> normally performs some optimization of code
sequences used to access Thread-Local Storage.  Use this option to
disable the optimization.
</p></dd>
</dl>



</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="PowerPC64-ELF64.html"><code class="command">ld</code> and PowerPC64 64-bit ELF Support</a>, Previous: <a href="Nios-II.html"><code class="command">ld</code> and the Altera Nios II</a>, Up: <a href="Machine-Dependent.html">Machine Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
