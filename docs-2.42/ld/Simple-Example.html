<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.42.

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Simple Example (LD)</title>

<meta name="description" content="Simple Example (LD)">
<meta name="keywords" content="Simple Example (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Scripts.html" rel="up" title="Scripts">
<link href="Simple-Commands.html" rel="next" title="Simple Commands">
<link href="Script-Format.html" rel="prev" title="Script Format">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Simple-Example">
<div class="nav-panel">
<p>
Next: <a href="Simple-Commands.html" accesskey="n" rel="next">Simple Linker Script Commands</a>, Previous: <a href="Script-Format.html" accesskey="p" rel="prev">Linker Script Format</a>, Up: <a href="Scripts.html" accesskey="u" rel="up">Linker Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Simple-Linker-Script-Example">3.3 Simple Linker Script Example</h3>
<a class="index-entry-id" id="index-linker-script-example"></a>
<a class="index-entry-id" id="index-example-of-linker-script"></a>
<p>Many linker scripts are fairly simple.
</p>
<p>The simplest possible linker script has just one command:
&lsquo;<samp class="samp">SECTIONS</samp>&rsquo;.  You use the &lsquo;<samp class="samp">SECTIONS</samp>&rsquo; command to describe the
memory layout of the output file.
</p>
<p>The &lsquo;<samp class="samp">SECTIONS</samp>&rsquo; command is a powerful command.  Here we will
describe a simple use of it.  Let&rsquo;s assume your program consists only of
code, initialized data, and uninitialized data.  These will be in the
&lsquo;<samp class="samp">.text</samp>&rsquo;, &lsquo;<samp class="samp">.data</samp>&rsquo;, and &lsquo;<samp class="samp">.bss</samp>&rsquo; sections, respectively.
Let&rsquo;s assume further that these are the only sections which appear in
your input files.
</p>
<p>For this example, let&rsquo;s say that the code should be loaded at address
0x10000, and that the data should start at address 0x8000000.  Here is a
linker script which will do that:
</p><div class="example smallexample">
<pre class="example-preformatted">SECTIONS
{
  . = 0x10000;
  .text : { *(.text) }
  . = 0x8000000;
  .data : { *(.data) }
  .bss : { *(.bss) }
}
</pre></div>

<p>You write the &lsquo;<samp class="samp">SECTIONS</samp>&rsquo; command as the keyword &lsquo;<samp class="samp">SECTIONS</samp>&rsquo;,
followed by a series of symbol assignments and output section
descriptions enclosed in curly braces.
</p>
<p>The first line inside the &lsquo;<samp class="samp">SECTIONS</samp>&rsquo; command of the above example
sets the value of the special symbol &lsquo;<samp class="samp">.</samp>&rsquo;, which is the location
counter.  If you do not specify the address of an output section in some
other way (other ways are described later), the address is set from the
current value of the location counter.  The location counter is then
incremented by the size of the output section.  At the start of the
&lsquo;<samp class="samp">SECTIONS</samp>&rsquo; command, the location counter has the value &lsquo;<samp class="samp">0</samp>&rsquo;.
</p>
<p>The second line defines an output section, &lsquo;<samp class="samp">.text</samp>&rsquo;.  The colon is
required syntax which may be ignored for now.  Within the curly braces
after the output section name, you list the names of the input sections
which should be placed into this output section.  The &lsquo;<samp class="samp">*</samp>&rsquo; is a
wildcard which matches any file name.  The expression &lsquo;<samp class="samp">*(.text)</samp>&rsquo;
means all &lsquo;<samp class="samp">.text</samp>&rsquo; input sections in all input files.
</p>
<p>Since the location counter is &lsquo;<samp class="samp">0x10000</samp>&rsquo; when the output section
&lsquo;<samp class="samp">.text</samp>&rsquo; is defined, the linker will set the address of the
&lsquo;<samp class="samp">.text</samp>&rsquo; section in the output file to be &lsquo;<samp class="samp">0x10000</samp>&rsquo;.
</p>
<p>The remaining lines define the &lsquo;<samp class="samp">.data</samp>&rsquo; and &lsquo;<samp class="samp">.bss</samp>&rsquo; sections in
the output file.  The linker will place the &lsquo;<samp class="samp">.data</samp>&rsquo; output section
at address &lsquo;<samp class="samp">0x8000000</samp>&rsquo;.  After the linker places the &lsquo;<samp class="samp">.data</samp>&rsquo;
output section, the value of the location counter will be
&lsquo;<samp class="samp">0x8000000</samp>&rsquo; plus the size of the &lsquo;<samp class="samp">.data</samp>&rsquo; output section.  The
effect is that the linker will place the &lsquo;<samp class="samp">.bss</samp>&rsquo; output section
immediately after the &lsquo;<samp class="samp">.data</samp>&rsquo; output section in memory.
</p>
<p>The linker will ensure that each output section has the required
alignment, by increasing the location counter if necessary.  In this
example, the specified addresses for the &lsquo;<samp class="samp">.text</samp>&rsquo; and &lsquo;<samp class="samp">.data</samp>&rsquo;
sections will probably satisfy any alignment constraints, but the linker
may have to create a small gap between the &lsquo;<samp class="samp">.data</samp>&rsquo; and &lsquo;<samp class="samp">.bss</samp>&rsquo;
sections.
</p>
<p>That&rsquo;s it!  That&rsquo;s a simple and complete linker script.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Simple-Commands.html">Simple Linker Script Commands</a>, Previous: <a href="Script-Format.html">Linker Script Format</a>, Up: <a href="Scripts.html">Linker Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
