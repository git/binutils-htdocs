<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.42.

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Overlay Description (LD)</title>

<meta name="description" content="Overlay Description (LD)">
<meta name="keywords" content="Overlay Description (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="SECTIONS.html" rel="up" title="SECTIONS">
<link href="Output-Section-Attributes.html" rel="prev" title="Output Section Attributes">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Overlay-Description">
<div class="nav-panel">
<p>
Previous: <a href="Output-Section-Attributes.html" accesskey="p" rel="prev">Output Section Attributes</a>, Up: <a href="SECTIONS.html" accesskey="u" rel="up">SECTIONS Command</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Overlay-Description-1">3.6.9 Overlay Description</h4>
<a class="index-entry-id" id="index-OVERLAY"></a>
<a class="index-entry-id" id="index-overlays"></a>
<p>An overlay description provides an easy way to describe sections which
are to be loaded as part of a single memory image but are to be run at
the same memory address.  At run time, some sort of overlay manager will
copy the overlaid sections in and out of the runtime memory address as
required, perhaps by simply manipulating addressing bits.  This approach
can be useful, for example, when a certain region of memory is faster
than another.
</p>
<p>Overlays are described using the <code class="code">OVERLAY</code> command.  The
<code class="code">OVERLAY</code> command is used within a <code class="code">SECTIONS</code> command, like an
output section description.  The full syntax of the <code class="code">OVERLAY</code>
command is as follows:
</p><div class="example smallexample">
<div class="group"><pre class="example-preformatted">OVERLAY [<var class="var">start</var>] : [NOCROSSREFS] [AT ( <var class="var">ldaddr</var> )]
  {
    <var class="var">secname1</var>
      {
        <var class="var">output-section-command</var>
        <var class="var">output-section-command</var>
        &hellip;
      } [:<var class="var">phdr</var>&hellip;] [=<var class="var">fill</var>]
    <var class="var">secname2</var>
      {
        <var class="var">output-section-command</var>
        <var class="var">output-section-command</var>
        &hellip;
      } [:<var class="var">phdr</var>&hellip;] [=<var class="var">fill</var>]
    &hellip;
  } [&gt;<var class="var">region</var>] [:<var class="var">phdr</var>&hellip;] [=<var class="var">fill</var>] [,]
</pre></div></div>

<p>Everything is optional except <code class="code">OVERLAY</code> (a keyword), and each
section must have a name (<var class="var">secname1</var> and <var class="var">secname2</var> above).  The
section definitions within the <code class="code">OVERLAY</code> construct are identical to
those within the general <code class="code">SECTIONS</code> construct (see <a class="pxref" href="SECTIONS.html">SECTIONS Command</a>),
except that no addresses and no memory regions may be defined for
sections within an <code class="code">OVERLAY</code>.
</p>
<p>The comma at the end may be required if a <var class="var">fill</var> is used and
the next <var class="var">sections-command</var> looks like a continuation of the expression.
</p>
<p>The sections are all defined with the same starting address.  The load
addresses of the sections are arranged such that they are consecutive in
memory starting at the load address used for the <code class="code">OVERLAY</code> as a
whole (as with normal section definitions, the load address is optional,
and defaults to the start address; the start address is also optional,
and defaults to the current value of the location counter).
</p>
<p>If the <code class="code">NOCROSSREFS</code> keyword is used, and there are any
references among the sections, the linker will report an error.  Since
the sections all run at the same address, it normally does not make
sense for one section to refer directly to another.
See <a class="xref" href="Miscellaneous-Commands.html">NOCROSSREFS</a>.
</p>
<p>For each section within the <code class="code">OVERLAY</code>, the linker automatically
provides two symbols.  The symbol <code class="code">__load_start_<var class="var">secname</var></code> is
defined as the starting load address of the section.  The symbol
<code class="code">__load_stop_<var class="var">secname</var></code> is defined as the final load address of
the section.  Any characters within <var class="var">secname</var> which are not legal
within C identifiers are removed.  C (or assembler) code may use these
symbols to move the overlaid sections around as necessary.
</p>
<p>At the end of the overlay, the value of the location counter is set to
the start address of the overlay plus the size of the largest section.
</p>
<p>Here is an example.  Remember that this would appear inside a
<code class="code">SECTIONS</code> construct.
</p><div class="example smallexample">
<div class="group"><pre class="example-preformatted">  OVERLAY 0x1000 : AT (0x4000)
   {
     .text0 { o1/*.o(.text) }
     .text1 { o2/*.o(.text) }
   }
</pre></div></div>
<p>This will define both &lsquo;<samp class="samp">.text0</samp>&rsquo; and &lsquo;<samp class="samp">.text1</samp>&rsquo; to start at
address 0x1000.  &lsquo;<samp class="samp">.text0</samp>&rsquo; will be loaded at address 0x4000, and
&lsquo;<samp class="samp">.text1</samp>&rsquo; will be loaded immediately after &lsquo;<samp class="samp">.text0</samp>&rsquo;.  The
following symbols will be defined if referenced: <code class="code">__load_start_text0</code>,
<code class="code">__load_stop_text0</code>, <code class="code">__load_start_text1</code>,
<code class="code">__load_stop_text1</code>.
</p>
<p>C code to copy overlay <code class="code">.text1</code> into the overlay area might look
like the following.
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">  extern char __load_start_text1, __load_stop_text1;
  memcpy ((char *) 0x1000, &amp;__load_start_text1,
          &amp;__load_stop_text1 - &amp;__load_start_text1);
</pre></div></div>

<p>Note that the <code class="code">OVERLAY</code> command is just syntactic sugar, since
everything it does can be done using the more basic commands.  The above
example could have been written identically as follows.
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">  .text0 0x1000 : AT (0x4000) { o1/*.o(.text) }
  PROVIDE (__load_start_text0 = LOADADDR (.text0));
  PROVIDE (__load_stop_text0 = LOADADDR (.text0) + SIZEOF (.text0));
  .text1 0x1000 : AT (0x4000 + SIZEOF (.text0)) { o2/*.o(.text) }
  PROVIDE (__load_start_text1 = LOADADDR (.text1));
  PROVIDE (__load_stop_text1 = LOADADDR (.text1) + SIZEOF (.text1));
  . = 0x1000 + MAX (SIZEOF (.text0), SIZEOF (.text1));
</pre></div></div>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Output-Section-Attributes.html">Output Section Attributes</a>, Up: <a href="SECTIONS.html">SECTIONS Command</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
