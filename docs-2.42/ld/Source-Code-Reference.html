<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.42.

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Source Code Reference (LD)</title>

<meta name="description" content="Source Code Reference (LD)">
<meta name="keywords" content="Source Code Reference (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Assignments.html" rel="up" title="Assignments">
<link href="PROVIDE_005fHIDDEN.html" rel="prev" title="PROVIDE_HIDDEN">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Source-Code-Reference">
<div class="nav-panel">
<p>
Previous: <a href="PROVIDE_005fHIDDEN.html" accesskey="p" rel="prev">PROVIDE_HIDDEN</a>, Up: <a href="Assignments.html" accesskey="u" rel="up">Assigning Values to Symbols</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Source-Code-Reference-1">3.5.5 Source Code Reference</h4>

<p>Accessing a linker script defined variable from source code is not
intuitive.  In particular a linker script symbol is not equivalent to
a variable declaration in a high level language, it is instead a
symbol that does not have a value.
</p>
<p>Before going further, it is important to note that compilers often
transform names in the source code into different names when they are
stored in the symbol table.  For example, Fortran compilers commonly
prepend or append an underscore, and C++ performs extensive &lsquo;<samp class="samp">name
mangling</samp>&rsquo;.  Therefore there might be a discrepancy between the name
of a variable as it is used in source code and the name of the same
variable as it is defined in a linker script.  For example in C a
linker script variable might be referred to as:
</p>
<div class="example smallexample">
<pre class="example-preformatted">  extern int foo;
</pre></div>

<p>But in the linker script it might be defined as:
</p>
<div class="example smallexample">
<pre class="example-preformatted">  _foo = 1000;
</pre></div>

<p>In the remaining examples however it is assumed that no name
transformation has taken place.
</p>
<p>When a symbol is declared in a high level language such as C, two
things happen.  The first is that the compiler reserves enough space
in the program&rsquo;s memory to hold the <em class="emph">value</em> of the symbol.  The
second is that the compiler creates an entry in the program&rsquo;s symbol
table which holds the symbol&rsquo;s <em class="emph">address</em>.  ie the symbol table
contains the address of the block of memory holding the symbol&rsquo;s
value.  So for example the following C declaration, at file scope:
</p>
<div class="example smallexample">
<pre class="example-preformatted">  int foo = 1000;
</pre></div>

<p>creates an entry called &lsquo;<samp class="samp">foo</samp>&rsquo; in the symbol table.  This entry
holds the address of an &lsquo;<samp class="samp">int</samp>&rsquo; sized block of memory where the
number 1000 is initially stored.
</p>
<p>When a program references a symbol the compiler generates code that
first accesses the symbol table to find the address of the symbol&rsquo;s
memory block and then code to read the value from that memory block.
So:
</p>
<div class="example smallexample">
<pre class="example-preformatted">  foo = 1;
</pre></div>

<p>looks up the symbol &lsquo;<samp class="samp">foo</samp>&rsquo; in the symbol table, gets the address
associated with this symbol and then writes the value 1 into that
address.  Whereas:
</p>
<div class="example smallexample">
<pre class="example-preformatted">  int * a = &amp; foo;
</pre></div>

<p>looks up the symbol &lsquo;<samp class="samp">foo</samp>&rsquo; in the symbol table, gets its address
and then copies this address into the block of memory associated with
the variable &lsquo;<samp class="samp">a</samp>&rsquo;.
</p>
<p>Linker scripts symbol declarations, by contrast, create an entry in
the symbol table but do not assign any memory to them.  Thus they are
an address without a value.  So for example the linker script definition:
</p>
<div class="example smallexample">
<pre class="example-preformatted">  foo = 1000;
</pre></div>

<p>creates an entry in the symbol table called &lsquo;<samp class="samp">foo</samp>&rsquo; which holds
the address of memory location 1000, but nothing special is stored at
address 1000.  This means that you cannot access the <em class="emph">value</em> of a
linker script defined symbol - it has no value - all you can do is
access the <em class="emph">address</em> of a linker script defined symbol.
</p>
<p>Hence when you are using a linker script defined symbol in source code
you should always take the address of the symbol, and never attempt to
use its value.  For example suppose you want to copy the contents of a
section of memory called .ROM into a section called .FLASH and the
linker script contains these declarations:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">  start_of_ROM   = .ROM;
  end_of_ROM     = .ROM + sizeof (.ROM);
  start_of_FLASH = .FLASH;
</pre></div></div>

<p>Then the C source code to perform the copy would be:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">  extern char start_of_ROM, end_of_ROM, start_of_FLASH;

  memcpy (&amp; start_of_FLASH, &amp; start_of_ROM, &amp; end_of_ROM - &amp; start_of_ROM);
</pre></div></div>

<p>Note the use of the &lsquo;<samp class="samp">&amp;</samp>&rsquo; operators.  These are correct.
Alternatively the symbols can be treated as the names of vectors or
arrays and then the code will again work as expected:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">  extern char start_of_ROM[], end_of_ROM[], start_of_FLASH[];

  memcpy (start_of_FLASH, start_of_ROM, end_of_ROM - start_of_ROM);
</pre></div></div>

<p>Note how using this method does not require the use of &lsquo;<samp class="samp">&amp;</samp>&rsquo;
operators.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="PROVIDE_005fHIDDEN.html">PROVIDE_HIDDEN</a>, Up: <a href="Assignments.html">Assigning Values to Symbols</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
