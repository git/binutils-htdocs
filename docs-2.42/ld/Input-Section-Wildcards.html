<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.42.

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Input Section Wildcards (LD)</title>

<meta name="description" content="Input Section Wildcards (LD)">
<meta name="keywords" content="Input Section Wildcards (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Input-Section.html" rel="up" title="Input Section">
<link href="Input-Section-Common.html" rel="next" title="Input Section Common">
<link href="Input-Section-Basics.html" rel="prev" title="Input Section Basics">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Input-Section-Wildcards">
<div class="nav-panel">
<p>
Next: <a href="Input-Section-Common.html" accesskey="n" rel="next">Input Section for Common Symbols</a>, Previous: <a href="Input-Section-Basics.html" accesskey="p" rel="prev">Input Section Basics</a>, Up: <a href="Input-Section.html" accesskey="u" rel="up">Input Section Description</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Input-Section-Wildcard-Patterns">3.6.4.2 Input Section Wildcard Patterns</h4>
<a class="index-entry-id" id="index-input-section-wildcards"></a>
<a class="index-entry-id" id="index-wildcard-file-name-patterns"></a>
<a class="index-entry-id" id="index-file-name-wildcard-patterns"></a>
<a class="index-entry-id" id="index-section-name-wildcard-patterns"></a>
<p>In an input section description, either the file name or the section
name or both may be wildcard patterns.
</p>
<p>The file name of &lsquo;<samp class="samp">*</samp>&rsquo; seen in many examples is a simple wildcard
pattern for the file name.
</p>
<p>The wildcard patterns are like those used by the Unix shell.
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">*</samp>&rsquo;</dt>
<dd><p>matches any number of characters
</p></dd>
<dt>&lsquo;<samp class="samp">?</samp>&rsquo;</dt>
<dd><p>matches any single character
</p></dd>
<dt>&lsquo;<samp class="samp">[<var class="var">chars</var>]</samp>&rsquo;</dt>
<dd><p>matches a single instance of any of the <var class="var">chars</var>; the &lsquo;<samp class="samp">-</samp>&rsquo;
character may be used to specify a range of characters, as in
&lsquo;<samp class="samp">[a-z]</samp>&rsquo; to match any lower case letter
</p></dd>
<dt>&lsquo;<samp class="samp">\</samp>&rsquo;</dt>
<dd><p>quotes the following character
</p></dd>
</dl>

<p>File name wildcard patterns only match files which are explicitly
specified on the command line or in an <code class="code">INPUT</code> command.  The linker
does not search directories to expand wildcards.
</p>
<p>If a file name matches more than one wildcard pattern, or if a file name
appears explicitly and is also matched by a wildcard pattern, the linker
will use the first match in the linker script.  For example, this
sequence of input section descriptions is probably in error, because the
<samp class="file">data.o</samp> rule will not be used:
</p><div class="example smallexample">
<pre class="example-preformatted">.data : { *(.data) }
.data1 : { data.o(.data) }
</pre></div>

<a class="index-entry-id" id="index-SORT_005fBY_005fNAME"></a>
<p>Normally, the linker will place files and sections matched by wildcards
in the order in which they are seen during the link.  You can change
this by using the <code class="code">SORT_BY_NAME</code> keyword, which appears before a wildcard
pattern in parentheses (e.g., <code class="code">SORT_BY_NAME(.text*)</code>).  When the
<code class="code">SORT_BY_NAME</code> keyword is used, the linker will sort the files or sections
into ascending order by name before placing them in the output file.
</p>
<a class="index-entry-id" id="index-SORT_005fBY_005fALIGNMENT"></a>
<p><code class="code">SORT_BY_ALIGNMENT</code> is similar to <code class="code">SORT_BY_NAME</code>.
<code class="code">SORT_BY_ALIGNMENT</code> will sort sections into descending order of
alignment before placing them in the output file.  Placing larger
alignments before smaller alignments can reduce the amount of padding
needed.
</p>
<a class="index-entry-id" id="index-SORT_005fBY_005fINIT_005fPRIORITY"></a>
<p><code class="code">SORT_BY_INIT_PRIORITY</code> is also similar to <code class="code">SORT_BY_NAME</code>.
<code class="code">SORT_BY_INIT_PRIORITY</code> will sort sections into ascending
numerical order of the GCC init_priority attribute encoded in the
section name before placing them in the output file.  In
<code class="code">.init_array.NNNNN</code> and <code class="code">.fini_array.NNNNN</code>, <code class="code">NNNNN</code> is
the init_priority.  In <code class="code">.ctors.NNNNN</code> and <code class="code">.dtors.NNNNN</code>,
<code class="code">NNNNN</code> is 65535 minus the init_priority.
</p>
<a class="index-entry-id" id="index-SORT"></a>
<p><code class="code">SORT</code> is an alias for <code class="code">SORT_BY_NAME</code>.
</p>
<a class="index-entry-id" id="index-REVERSE"></a>
<p><code class="code">REVERSE</code> indicates that the sorting should be reversed.  If used
on its own then <code class="code">REVERSE</code> implies <code class="code">SORT_BY_NAME</code>, otherwise
it reverses the enclosed <code class="code">SORT..</code> command.  Note - reverse
sorting of alignment is not currently supported.
</p>
<p>Note - the sorting commands only accept a single wildcard pattern.  So
for example the following will not work:
</p><div class="example smallexample">
<pre class="example-preformatted">  *(REVERSE(.text* .init*))
</pre></div>
<p>To resolve this problem list the patterns individually, like this:
</p><div class="example smallexample">
<pre class="example-preformatted">  *(REVERSE(.text*))
  *(REVERSE(.init*))
</pre></div>

<p>Note - you can put the <code class="code">EXCLUDE_FILE</code> command inside a sorting
command, but not the other way around.  So for example:
</p><div class="example smallexample">
<pre class="example-preformatted">  *(SORT_BY_NAME(EXCLUDE_FILE(foo) .text*))
</pre></div>
<p>will work, but:
</p><div class="example smallexample">
<pre class="example-preformatted">  *(EXCLUDE_FILE(foo) SORT_BY_NAME(.text*))
</pre></div>
<p>will not.
</p>

<p>When there are nested section sorting commands in linker script, there
can be at most 1 level of nesting for section sorting commands.
</p>
<ol class="enumerate">
<li> <code class="code">SORT_BY_NAME</code> (<code class="code">SORT_BY_ALIGNMENT</code> (wildcard section pattern)).
It will sort the input sections by name first, then by alignment if two
sections have the same name.
</li><li> <code class="code">SORT_BY_ALIGNMENT</code> (<code class="code">SORT_BY_NAME</code> (wildcard section pattern)).
It will sort the input sections by alignment first, then by name if two
sections have the same alignment.
</li><li> <code class="code">SORT_BY_NAME</code> (<code class="code">SORT_BY_NAME</code> (wildcard section pattern)) is
treated the same as <code class="code">SORT_BY_NAME</code> (wildcard section pattern).
</li><li> <code class="code">SORT_BY_ALIGNMENT</code> (<code class="code">SORT_BY_ALIGNMENT</code> (wildcard section pattern))
is treated the same as <code class="code">SORT_BY_ALIGNMENT</code> (wildcard section pattern).
</li><li> <code class="code">SORT_BY_NAME</code> (<code class="code">REVERSE</code> (wildcard section pattern))
reverse sorts by name.
</li><li> <code class="code">REVERSE</code> (<code class="code">SORT_BY_NAME</code> (wildcard section pattern))
reverse sorts by name.
</li><li> <code class="code">SORT_BY_INIT_PRIORITY</code> (<code class="code">REVERSE</code> (wildcard section pattern))
reverse sorts by init priority.
</li><li> All other nested section sorting commands are invalid.
</li></ol>

<p>When both command-line section sorting option and linker script
section sorting command are used, section sorting command always
takes precedence over the command-line option.
</p>
<p>If the section sorting command in linker script isn&rsquo;t nested, the
command-line option will make the section sorting command to be
treated as nested sorting command.
</p>
<ol class="enumerate">
<li> <code class="code">SORT_BY_NAME</code> (wildcard section pattern ) with
<samp class="option">--sort-sections alignment</samp> is equivalent to
<code class="code">SORT_BY_NAME</code> (<code class="code">SORT_BY_ALIGNMENT</code> (wildcard section pattern)).
</li><li> <code class="code">SORT_BY_ALIGNMENT</code> (wildcard section pattern) with
<samp class="option">--sort-section name</samp> is equivalent to
<code class="code">SORT_BY_ALIGNMENT</code> (<code class="code">SORT_BY_NAME</code> (wildcard section pattern)).
</li></ol>

<p>If the section sorting command in linker script is nested, the
command-line option will be ignored.
</p>
<a class="index-entry-id" id="index-SORT_005fNONE"></a>
<p><code class="code">SORT_NONE</code> disables section sorting by ignoring the command-line
section sorting option.
</p>
<p>If you ever get confused about where input sections are going, use the
&lsquo;<samp class="samp">-M</samp>&rsquo; linker option to generate a map file.  The map file shows
precisely how input sections are mapped to output sections.
</p>
<p>This example shows how wildcard patterns might be used to partition
files.  This linker script directs the linker to place all &lsquo;<samp class="samp">.text</samp>&rsquo;
sections in &lsquo;<samp class="samp">.text</samp>&rsquo; and all &lsquo;<samp class="samp">.bss</samp>&rsquo; sections in &lsquo;<samp class="samp">.bss</samp>&rsquo;.
The linker will place the &lsquo;<samp class="samp">.data</samp>&rsquo; section from all files beginning
with an upper case character in &lsquo;<samp class="samp">.DATA</samp>&rsquo;; for all other files, the
linker will place the &lsquo;<samp class="samp">.data</samp>&rsquo; section in &lsquo;<samp class="samp">.data</samp>&rsquo;.
</p><div class="example smallexample">
<div class="group"><pre class="example-preformatted">SECTIONS {
  .text : { *(.text) }
  .DATA : { [A-Z]*(.data) }
  .data : { *(.data) }
  .bss : { *(.bss) }
}
</pre></div></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Input-Section-Common.html">Input Section for Common Symbols</a>, Previous: <a href="Input-Section-Basics.html">Input Section Basics</a>, Up: <a href="Input-Section.html">Input Section Description</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
