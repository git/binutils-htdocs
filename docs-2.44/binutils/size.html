<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>size (GNU Binary Utilities)</title>

<meta name="description" content="size (GNU Binary Utilities)">
<meta name="keywords" content="size (GNU Binary Utilities)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Binutils-Index.html" rel="index" title="Binutils Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="strings.html" rel="next" title="strings">
<link href="ranlib.html" rel="prev" title="ranlib">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="chapter-level-extent" id="size">
<div class="nav-panel">
<p>
Next: <a href="strings.html" accesskey="n" rel="next">strings</a>, Previous: <a href="ranlib.html" accesskey="p" rel="prev">ranlib</a>, Up: <a href="index.html" accesskey="u" rel="up">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h2 class="chapter" id="size-1"><span>6 size<a class="copiable-link" href="#size-1"> &para;</a></span></h2>

<a class="index-entry-id" id="index-size"></a>
<a class="index-entry-id" id="index-section-sizes"></a>


<div class="example smallexample">
<pre class="example-preformatted">size [<samp class="option">-A</samp>|<samp class="option">-B</samp>|<samp class="option">-G</samp>|<samp class="option">--format=</samp><var class="var">compatibility</var>]
     [<samp class="option">--help</samp>]
     [<samp class="option">-d</samp>|<samp class="option">-o</samp>|<samp class="option">-x</samp>|<samp class="option">--radix=</samp><var class="var">number</var>]
     [<samp class="option">--common</samp>]
     [<samp class="option">-t</samp>|<samp class="option">--totals</samp>]
     [<samp class="option">--target=</samp><var class="var">bfdname</var>] [<samp class="option">-V</samp>|<samp class="option">--version</samp>]
     [<samp class="option">-f</samp>]
     [<var class="var">objfile</var>...]
</pre></div>


<p>The <small class="sc">GNU</small> <code class="command">size</code> utility lists the section sizes and the total
size for each of the binary files <var class="var">objfile</var> on its argument list.
By default, one line of output is generated for each file or each
module if the file is an archive.
</p>
<p><var class="var">objfile</var>&hellip; are the files to be examined.  If none are
specified, the file <code class="code">a.out</code> will be used instead.
</p>


<p>The command-line options have the following meanings:
</p>
<dl class="table">
<dt><a id="index-size-display-format"></a><span><code class="env">-A</code><a class="copiable-link" href="#index-size-display-format"> &para;</a></span></dt>
<dt><code class="env">-B</code></dt>
<dt><code class="env">-G</code></dt>
<dt><code class="env">--format=<var class="var">compatibility</var></code></dt>
<dd><p>Using one of these options, you can choose whether the output from <small class="sc">GNU</small>
<code class="command">size</code> resembles output from System V <code class="command">size</code> (using <samp class="option">-A</samp>,
or <samp class="option">--format=sysv</samp>), or Berkeley <code class="command">size</code> (using <samp class="option">-B</samp>, or
<samp class="option">--format=berkeley</samp>).  The default is the one-line format similar to
Berkeley&rsquo;s.  Alternatively, you can choose the GNU format output
(using <samp class="option">-G</samp>, or <samp class="option">--format=gnu</samp>), this is similar to
Berkeley&rsquo;s output format, but sizes are counted differently.
</p>
<p>Here is an example of the Berkeley (default) format of output from
<code class="command">size</code>:
</p><div class="example smallexample">
<pre class="example-preformatted">$ size --format=Berkeley ranlib size
   text    data     bss     dec     hex filename
 294880   81920   11592  388392   5ed28 ranlib
 294880   81920   11888  388688   5ee50 size
</pre></div>

<p>The Berkeley style output counts read only data in the <code class="code">text</code>
column, not in the <code class="code">data</code> column, the <code class="code">dec</code> and <code class="code">hex</code>
columns both display the sum of the <code class="code">text</code>, <code class="code">data</code>, and
<code class="code">bss</code> columns in decimal and hexadecimal respectively.
</p>
<p>The GNU format counts read only data in the <code class="code">data</code> column, not
the <code class="code">text</code> column, and only displays the sum of the <code class="code">text</code>,
<code class="code">data</code>, and <code class="code">bss</code> columns once, in the <code class="code">total</code> column.
The <samp class="option">--radix</samp> option can be used to change the number base for
all columns.  Here is the same data displayed with GNU conventions:
</p>
<div class="example smallexample">
<pre class="example-preformatted">$ size --format=GNU ranlib size
      text       data        bss      total filename
    279880      96920      11592     388392 ranlib
    279880      96920      11888     388688 size
</pre></div>

<p>This is the same data, but displayed closer to System V conventions:
</p>
<div class="example smallexample">
<pre class="example-preformatted">$ size --format=SysV ranlib size
ranlib  :
section         size         addr
.text         294880         8192
.data          81920       303104
.bss           11592       385024
Total         388392


size  :
section         size         addr
.text         294880         8192
.data          81920       303104
.bss           11888       385024
Total         388688
</pre></div>

</dd>
<dt><code class="env">--help</code></dt>
<dt><code class="env">-h</code></dt>
<dt><code class="env">-H</code></dt>
<dt><code class="env">-?</code></dt>
<dd><p>Show a summary of acceptable arguments and options.
</p>
</dd>
<dt><a class="index-entry-id" id="index-radix-for-section-sizes"></a>
<a id="index-size-number-format"></a><span><code class="env">-d</code><a class="copiable-link" href="#index-size-number-format"> &para;</a></span></dt>
<dt><code class="env">-o</code></dt>
<dt><code class="env">-x</code></dt>
<dt><code class="env">--radix=<var class="var">number</var></code></dt>
<dd><p>Using one of these options, you can control whether the size of each
section is given in decimal (<samp class="option">-d</samp>, or <samp class="option">--radix=10</samp>); octal
(<samp class="option">-o</samp>, or <samp class="option">--radix=8</samp>); or hexadecimal (<samp class="option">-x</samp>, or
<samp class="option">--radix=16</samp>).  In <samp class="option">--radix=<var class="var">number</var></samp>, only the three
values (8, 10, 16) are supported.  The total size is always given in two
radices; decimal and hexadecimal for <samp class="option">-d</samp> or <samp class="option">-x</samp> output, or
octal and hexadecimal if you&rsquo;re using <samp class="option">-o</samp>.
</p>
</dd>
<dt><code class="env">--common</code></dt>
<dd><p>Print total size of common symbols in each file.  When using Berkeley
or GNU format these are included in the bss size.
</p>
</dd>
<dt><code class="env">-t</code></dt>
<dt><code class="env">--totals</code></dt>
<dd><p>Show totals of all objects listed (Berkeley or GNU format mode only).
</p>
</dd>
<dt><a id="index-object-code-format-2"></a><span><code class="env">--target=<var class="var">bfdname</var></code><a class="copiable-link" href="#index-object-code-format-2"> &para;</a></span></dt>
<dd><p>Specify that the object-code format for <var class="var">objfile</var> is
<var class="var">bfdname</var>.  This option may not be necessary; <code class="command">size</code> can
automatically recognize many formats.
See <a class="xref" href="Target-Selection.html">Target Selection</a>, for more information.
</p>
</dd>
<dt><code class="env">-v</code></dt>
<dt><code class="env">-V</code></dt>
<dt><code class="env">--version</code></dt>
<dd><p>Display the version number of <code class="command">size</code>.
</p>
</dd>
<dt><code class="env">-f</code></dt>
<dd><p>Ignored.  This option is used by other versions of the <code class="command">size</code>
program, but it is not supported by the GNU Binutils version.
</p>
</dd>
</dl>



</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="strings.html">strings</a>, Previous: <a href="ranlib.html">ranlib</a>, Up: <a href="index.html">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
