<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>ar scripts (GNU Binary Utilities)</title>

<meta name="description" content="ar scripts (GNU Binary Utilities)">
<meta name="keywords" content="ar scripts (GNU Binary Utilities)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Binutils-Index.html" rel="index" title="Binutils Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="ar.html" rel="up" title="ar">
<link href="ar-cmdline.html" rel="prev" title="ar cmdline">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="ar-scripts">
<div class="nav-panel">
<p>
Previous: <a href="ar-cmdline.html" accesskey="p" rel="prev">Controlling <code class="command">ar</code> on the Command Line</a>, Up: <a href="ar.html" accesskey="u" rel="up">ar</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Controlling-ar-with-a-Script"><span>1.2 Controlling <code class="command">ar</code> with a Script<a class="copiable-link" href="#Controlling-ar-with-a-Script"> &para;</a></span></h3>

<div class="example smallexample">
<pre class="example-preformatted">ar -M [ &lt;<var class="var">script</var> ]
</pre></div>

<a class="index-entry-id" id="index-MRI-compatibility_002c-ar"></a>
<a class="index-entry-id" id="index-scripts_002c-ar"></a>
<p>If you use the single command-line option &lsquo;<samp class="samp">-M</samp>&rsquo; with <code class="command">ar</code>, you
can control its operation with a rudimentary command language.  This
form of <code class="command">ar</code> operates interactively if standard input is coming
directly from a terminal.  During interactive use, <code class="command">ar</code> prompts for
input (the prompt is &lsquo;<samp class="samp">AR &gt;</samp>&rsquo;), and continues executing even after
errors.  If you redirect standard input to a script file, no prompts are
issued, and <code class="command">ar</code> abandons execution (with a nonzero exit code)
on any error.
</p>
<p>The <code class="command">ar</code> command language is <em class="emph">not</em> designed to be equivalent
to the command-line options; in fact, it provides somewhat less control
over archives.  The only purpose of the command language is to ease the
transition to <small class="sc">GNU</small> <code class="command">ar</code> for developers who already have scripts
written for the MRI &ldquo;librarian&rdquo; program.
</p>
<p>The syntax for the <code class="command">ar</code> command language is straightforward:
</p><ul class="itemize mark-bullet">
<li>commands are recognized in upper or lower case; for example, <code class="code">LIST</code>
is the same as <code class="code">list</code>.  In the following descriptions, commands are
shown in upper case for clarity.

</li><li>a single command may appear on each line; it is the first word on the
line.

</li><li>empty lines are allowed, and have no effect.

</li><li>comments are allowed; text after either of the characters &lsquo;<samp class="samp">*</samp>&rsquo;
or &lsquo;<samp class="samp">;</samp>&rsquo; is ignored.

</li><li>Whenever you use a list of names as part of the argument to an <code class="command">ar</code>
command, you can separate the individual names with either commas or
blanks.  Commas are shown in the explanations below, for clarity.

</li><li>&lsquo;<samp class="samp">+</samp>&rsquo; is used as a line continuation character; if &lsquo;<samp class="samp">+</samp>&rsquo; appears
at the end of a line, the text on the following line is considered part
of the current command.
</li></ul>

<p>Here are the commands you can use in <code class="command">ar</code> scripts, or when using
<code class="command">ar</code> interactively.  Three of them have special significance:
</p>
<p><code class="code">OPEN</code> or <code class="code">CREATE</code> specify a <em class="dfn">current archive</em>, which is
a temporary file required for most of the other commands.
</p>
<p><code class="code">SAVE</code> commits the changes so far specified by the script.  Prior
to <code class="code">SAVE</code>, commands affect only the temporary copy of the current
archive.
</p>
<dl class="table">
<dt><code class="code">ADDLIB <var class="var">archive</var></code></dt>
<dt><code class="code">ADDLIB <var class="var">archive</var> (<var class="var">module</var>, <var class="var">module</var>, &hellip; <var class="var">module</var>)</code></dt>
<dd><p>Add all the contents of <var class="var">archive</var> (or, if specified, each named
<var class="var">module</var> from <var class="var">archive</var>) to the current archive.
</p>
<p>Requires prior use of <code class="code">OPEN</code> or <code class="code">CREATE</code>.
</p>
</dd>
<dt><code class="code">ADDMOD <var class="var">member</var>, <var class="var">member</var>, &hellip; <var class="var">member</var></code></dt>
<dd><p>Add each named <var class="var">member</var> as a module in the current archive.
</p>
<p>Requires prior use of <code class="code">OPEN</code> or <code class="code">CREATE</code>.
</p>
</dd>
<dt><code class="code">CLEAR</code></dt>
<dd><p>Discard the contents of the current archive, canceling the effect of
any operations since the last <code class="code">SAVE</code>.  May be executed (with no
effect) even if  no current archive is specified.
</p>
</dd>
<dt><code class="code">CREATE <var class="var">archive</var></code></dt>
<dd><p>Creates an archive, and makes it the current archive (required for many
other commands).  The new archive is created with a temporary name; it
is not actually saved as <var class="var">archive</var> until you use <code class="code">SAVE</code>.
You can overwrite existing archives; similarly, the contents of any
existing file named <var class="var">archive</var> will not be destroyed until <code class="code">SAVE</code>.
</p>
</dd>
<dt><code class="code">DELETE <var class="var">module</var>, <var class="var">module</var>, &hellip; <var class="var">module</var></code></dt>
<dd><p>Delete each listed <var class="var">module</var> from the current archive; equivalent to
&lsquo;<samp class="samp">ar -d <var class="var">archive</var> <var class="var">module</var> &hellip; <var class="var">module</var></samp>&rsquo;.
</p>
<p>Requires prior use of <code class="code">OPEN</code> or <code class="code">CREATE</code>.
</p>
</dd>
<dt><code class="code">DIRECTORY <var class="var">archive</var> (<var class="var">module</var>, &hellip; <var class="var">module</var>)</code></dt>
<dt><code class="code">DIRECTORY <var class="var">archive</var> (<var class="var">module</var>, &hellip; <var class="var">module</var>) <var class="var">outputfile</var></code></dt>
<dd><p>List each named <var class="var">module</var> present in <var class="var">archive</var>.  The separate
command <code class="code">VERBOSE</code> specifies the form of the output: when verbose
output is off, output is like that of &lsquo;<samp class="samp">ar -t <var class="var">archive</var>
<var class="var">module</var>&hellip;</samp>&rsquo;.  When verbose output is on, the listing is like
&lsquo;<samp class="samp">ar -tv <var class="var">archive</var> <var class="var">module</var>&hellip;</samp>&rsquo;.
</p>
<p>Output normally goes to the standard output stream; however, if you
specify <var class="var">outputfile</var> as a final argument, <code class="command">ar</code> directs the
output to that file.
</p>
</dd>
<dt><code class="code">END</code></dt>
<dd><p>Exit from <code class="command">ar</code>, with a <code class="code">0</code> exit code to indicate successful
completion.  This command does not save the output file; if you have
changed the current archive since the last <code class="code">SAVE</code> command, those
changes are lost.
</p>
</dd>
<dt><code class="code">EXTRACT <var class="var">module</var>, <var class="var">module</var>, &hellip; <var class="var">module</var></code></dt>
<dd><p>Extract each named <var class="var">module</var> from the current archive, writing them
into the current directory as separate files.  Equivalent to &lsquo;<samp class="samp">ar -x
<var class="var">archive</var> <var class="var">module</var>&hellip;</samp>&rsquo;.
</p>
<p>Requires prior use of <code class="code">OPEN</code> or <code class="code">CREATE</code>.
</p>

</dd>
<dt><code class="code">LIST</code></dt>
<dd><p>Display full contents of the current archive, in &ldquo;verbose&rdquo; style
regardless of the state of <code class="code">VERBOSE</code>.  The effect is like &lsquo;<samp class="samp">ar
tv <var class="var">archive</var></samp>&rsquo;.  (This single command is a <small class="sc">GNU</small> <code class="command">ar</code>
enhancement, rather than present for MRI compatibility.)
</p>
<p>Requires prior use of <code class="code">OPEN</code> or <code class="code">CREATE</code>.
</p>
</dd>
<dt><code class="code">OPEN <var class="var">archive</var></code></dt>
<dd><p>Opens an existing archive for use as the current archive (required for
many other commands).  Any changes as the result of subsequent commands
will not actually affect <var class="var">archive</var> until you next use <code class="code">SAVE</code>.
</p>
</dd>
<dt><code class="code">REPLACE <var class="var">module</var>, <var class="var">module</var>, &hellip; <var class="var">module</var></code></dt>
<dd><p>In the current archive, replace each existing <var class="var">module</var> (named in
the <code class="code">REPLACE</code> arguments) from files in the current working directory.
To execute this command without errors, both the file, and the module in
the current archive, must exist.
</p>
<p>Requires prior use of <code class="code">OPEN</code> or <code class="code">CREATE</code>.
</p>
</dd>
<dt><code class="code">VERBOSE</code></dt>
<dd><p>Toggle an internal flag governing the output from <code class="code">DIRECTORY</code>.
When the flag is on, <code class="code">DIRECTORY</code> output matches output from
&lsquo;<samp class="samp">ar -tv </samp>&rsquo;&hellip;.
</p>
</dd>
<dt><code class="code">SAVE</code></dt>
<dd><p>Commit your changes to the current archive, and actually save it as a
file with the name specified in the last <code class="code">CREATE</code> or <code class="code">OPEN</code>
command.
</p>
<p>Requires prior use of <code class="code">OPEN</code> or <code class="code">CREATE</code>.
</p>
</dd>
</dl>


</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="ar-cmdline.html">Controlling <code class="command">ar</code> on the Command Line</a>, Up: <a href="ar.html">ar</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
