<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.44.

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>REGION_ALIAS (LD)</title>

<meta name="description" content="REGION_ALIAS (LD)">
<meta name="keywords" content="REGION_ALIAS (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Simple-Commands.html" rel="up" title="Simple Commands">
<link href="Miscellaneous-Commands.html" rel="next" title="Miscellaneous Commands">
<link href="Format-Commands.html" rel="prev" title="Format Commands">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="REGION_005fALIAS">
<div class="nav-panel">
<p>
Next: <a href="Miscellaneous-Commands.html" accesskey="n" rel="next">Other Linker Script Commands</a>, Previous: <a href="Format-Commands.html" accesskey="p" rel="prev">Commands Dealing with Object File Formats</a>, Up: <a href="Simple-Commands.html" accesskey="u" rel="up">Simple Linker Script Commands</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Assign-alias-names-to-memory-regions"><span>3.4.4 Assign alias names to memory regions<a class="copiable-link" href="#Assign-alias-names-to-memory-regions"> &para;</a></span></h4>
<a class="index-entry-id" id="index-REGION_005fALIAS_0028alias_002c-region_0029"></a>
<a class="index-entry-id" id="index-region-alias"></a>
<a class="index-entry-id" id="index-region-names"></a>

<p>Alias names can be added to existing memory regions created with the
<a class="ref" href="MEMORY.html">MEMORY Command</a> command.  Each name corresponds to at most one memory region.
</p>
<div class="example smallexample">
<pre class="example-preformatted">REGION_ALIAS(<var class="var">alias</var>, <var class="var">region</var>)
</pre></div>

<p>The <code class="code">REGION_ALIAS</code> function creates an alias name <var class="var">alias</var> for the
memory region <var class="var">region</var>.  This allows a flexible mapping of output sections
to memory regions.  An example follows.
</p>
<p>Suppose we have an application for embedded systems which come with various
memory storage devices.  All have a general purpose, volatile memory <code class="code">RAM</code>
that allows code execution or data storage.  Some may have a read-only,
non-volatile memory <code class="code">ROM</code> that allows code execution and read-only data
access.  The last variant is a read-only, non-volatile memory <code class="code">ROM2</code> with
read-only data access and no code execution capability.  We have four output
sections:
</p>
<ul class="itemize mark-bullet">
<li><code class="code">.text</code> program code;
</li><li><code class="code">.rodata</code> read-only data;
</li><li><code class="code">.data</code> read-write initialized data;
</li><li><code class="code">.bss</code> read-write zero initialized data.
</li></ul>

<p>The goal is to provide a linker command file that contains a system independent
part defining the output sections and a system dependent part mapping the
output sections to the memory regions available on the system.  Our embedded
systems come with three different memory setups <code class="code">A</code>, <code class="code">B</code> and
<code class="code">C</code>:
</p><table class="multitable">
<tbody><tr><td width="25%">Section</td><td width="25%">Variant A</td><td width="25%">Variant B</td><td width="25%">Variant C</td></tr>
<tr><td width="25%">.text</td><td width="25%">RAM</td><td width="25%">ROM</td><td width="25%">ROM</td></tr>
<tr><td width="25%">.rodata</td><td width="25%">RAM</td><td width="25%">ROM</td><td width="25%">ROM2</td></tr>
<tr><td width="25%">.data</td><td width="25%">RAM</td><td width="25%">RAM/ROM</td><td width="25%">RAM/ROM2</td></tr>
<tr><td width="25%">.bss</td><td width="25%">RAM</td><td width="25%">RAM</td><td width="25%">RAM</td></tr>
</tbody>
</table>
<p>The notation <code class="code">RAM/ROM</code> or <code class="code">RAM/ROM2</code> means that this section is
loaded into region <code class="code">ROM</code> or <code class="code">ROM2</code> respectively.  Please note that
the load address of the <code class="code">.data</code> section starts in all three variants at
the end of the <code class="code">.rodata</code> section.
</p>
<p>The base linker script that deals with the output sections follows.  It
includes the system dependent <code class="code">linkcmds.memory</code> file that describes the
memory layout:
</p><div class="example smallexample">
<pre class="example-preformatted">INCLUDE linkcmds.memory

SECTIONS
  {
    .text :
      {
        *(.text)
      } &gt; REGION_TEXT
    .rodata :
      {
        *(.rodata)
        rodata_end = .;
      } &gt; REGION_RODATA
    .data : AT (rodata_end)
      {
        data_start = .;
        *(.data)
      } &gt; REGION_DATA
    data_size = SIZEOF(.data);
    data_load_start = LOADADDR(.data);
    .bss :
      {
        *(.bss)
      } &gt; REGION_BSS
  }
</pre></div>

<p>Now we need three different <code class="code">linkcmds.memory</code> files to define memory
regions and alias names.  The content of <code class="code">linkcmds.memory</code> for the three
variants <code class="code">A</code>, <code class="code">B</code> and <code class="code">C</code>:
</p><dl class="table">
<dt><code class="code">A</code></dt>
<dd><p>Here everything goes into the <code class="code">RAM</code>.
</p><div class="example smallexample">
<pre class="example-preformatted">MEMORY
  {
    RAM : ORIGIN = 0, LENGTH = 4M
  }

REGION_ALIAS(&quot;REGION_TEXT&quot;, RAM);
REGION_ALIAS(&quot;REGION_RODATA&quot;, RAM);
REGION_ALIAS(&quot;REGION_DATA&quot;, RAM);
REGION_ALIAS(&quot;REGION_BSS&quot;, RAM);
</pre></div>
</dd>
<dt><code class="code">B</code></dt>
<dd><p>Program code and read-only data go into the <code class="code">ROM</code>.  Read-write data goes
into the <code class="code">RAM</code>.  An image of the initialized data is loaded into the
<code class="code">ROM</code> and will be copied during system start into the <code class="code">RAM</code>.
</p><div class="example smallexample">
<pre class="example-preformatted">MEMORY
  {
    ROM : ORIGIN = 0, LENGTH = 3M
    RAM : ORIGIN = 0x10000000, LENGTH = 1M
  }

REGION_ALIAS(&quot;REGION_TEXT&quot;, ROM);
REGION_ALIAS(&quot;REGION_RODATA&quot;, ROM);
REGION_ALIAS(&quot;REGION_DATA&quot;, RAM);
REGION_ALIAS(&quot;REGION_BSS&quot;, RAM);
</pre></div>
</dd>
<dt><code class="code">C</code></dt>
<dd><p>Program code goes into the <code class="code">ROM</code>.  Read-only data goes into the
<code class="code">ROM2</code>.  Read-write data goes into the <code class="code">RAM</code>.  An image of the
initialized data is loaded into the <code class="code">ROM2</code> and will be copied during
system start into the <code class="code">RAM</code>.
</p><div class="example smallexample">
<pre class="example-preformatted">MEMORY
  {
    ROM : ORIGIN = 0, LENGTH = 2M
    ROM2 : ORIGIN = 0x10000000, LENGTH = 1M
    RAM : ORIGIN = 0x20000000, LENGTH = 1M
  }

REGION_ALIAS(&quot;REGION_TEXT&quot;, ROM);
REGION_ALIAS(&quot;REGION_RODATA&quot;, ROM2);
REGION_ALIAS(&quot;REGION_DATA&quot;, RAM);
REGION_ALIAS(&quot;REGION_BSS&quot;, RAM);
</pre></div>
</dd>
</dl>

<p>It is possible to write a common system initialization routine to copy the
<code class="code">.data</code> section from <code class="code">ROM</code> or <code class="code">ROM2</code> into the <code class="code">RAM</code> if
necessary:
</p><div class="example smallexample">
<pre class="example-preformatted">#include &lt;string.h&gt;

extern char data_start [];
extern char data_size [];
extern char data_load_start [];

void copy_data(void)
{
  if (data_start != data_load_start)
    {
      memcpy(data_start, data_load_start, (size_t) data_size);
    }
}
</pre></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Miscellaneous-Commands.html">Other Linker Script Commands</a>, Previous: <a href="Format-Commands.html">Commands Dealing with Object File Formats</a>, Up: <a href="Simple-Commands.html">Simple Linker Script Commands</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
