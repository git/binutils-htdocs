<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.44.

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Xtensa (LD)</title>

<meta name="description" content="Xtensa (LD)">
<meta name="keywords" content="Xtensa (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Machine-Dependent.html" rel="up" title="Machine Dependent">
<link href="WIN32.html" rel="prev" title="WIN32">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Xtensa">
<div class="nav-panel">
<p>
Previous: <a href="WIN32.html" accesskey="p" rel="prev"><code class="command">ld</code> and WIN32 (cygwin/mingw)</a>, Up: <a href="Machine-Dependent.html" accesskey="u" rel="up">Machine Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="ld-and-Xtensa-Processors"><span>6.16 <code class="code">ld</code> and Xtensa Processors<a class="copiable-link" href="#ld-and-Xtensa-Processors"> &para;</a></span></h3>

<a class="index-entry-id" id="index-Xtensa-processors"></a>
<p>The default <code class="command">ld</code> behavior for Xtensa processors is to interpret
<code class="code">SECTIONS</code> commands so that lists of explicitly named sections in a
specification with a wildcard file will be interleaved when necessary to
keep literal pools within the range of PC-relative load offsets.  For
example, with the command:
</p>
<div class="example smallexample">
<pre class="example-preformatted">SECTIONS
{
  .text : {
    *(.literal .text)
  }
}
</pre></div>

<p><code class="command">ld</code> may interleave some of the <code class="code">.literal</code>
and <code class="code">.text</code> sections from different object files to ensure that the
literal pools are within the range of PC-relative load offsets.  A valid
interleaving might place the <code class="code">.literal</code> sections from an initial
group of files followed by the <code class="code">.text</code> sections of that group of
files.  Then, the <code class="code">.literal</code> sections from the rest of the files
and the <code class="code">.text</code> sections from the rest of the files would follow.
</p>
<a class="index-entry-id" id="index-_002d_002drelax-on-Xtensa"></a>
<a class="index-entry-id" id="index-relaxing-on-Xtensa"></a>
<p>Relaxation is enabled by default for the Xtensa version of <code class="command">ld</code> and
provides two important link-time optimizations.  The first optimization
is to combine identical literal values to reduce code size.  A redundant
literal will be removed and all the <code class="code">L32R</code> instructions that use it
will be changed to reference an identical literal, as long as the
location of the replacement literal is within the offset range of all
the <code class="code">L32R</code> instructions.  The second optimization is to remove
unnecessary overhead from assembler-generated &ldquo;longcall&rdquo; sequences of
<code class="code">L32R</code>/<code class="code">CALLX<var class="var">n</var></code> when the target functions are within
range of direct <code class="code">CALL<var class="var">n</var></code> instructions.
</p>
<p>For each of these cases where an indirect call sequence can be optimized
to a direct call, the linker will change the <code class="code">CALLX<var class="var">n</var></code>
instruction to a <code class="code">CALL<var class="var">n</var></code> instruction, remove the <code class="code">L32R</code>
instruction, and remove the literal referenced by the <code class="code">L32R</code>
instruction if it is not used for anything else.  Removing the
<code class="code">L32R</code> instruction always reduces code size but can potentially
hurt performance by changing the alignment of subsequent branch targets.
By default, the linker will always preserve alignments, either by
switching some instructions between 24-bit encodings and the equivalent
density instructions or by inserting a no-op in place of the <code class="code">L32R</code>
instruction that was removed.  If code size is more important than
performance, the <samp class="option">--size-opt</samp> option can be used to prevent the
linker from widening density instructions or inserting no-ops, except in
a few cases where no-ops are required for correctness.
</p>
<p>The following Xtensa-specific command-line options can be used to
control the linker:
</p>
<a class="index-entry-id" id="index-Xtensa-options"></a>
<dl class="table">
<dt><samp class="option">--size-opt</samp></dt>
<dd><p>When optimizing indirect calls to direct calls, optimize for code size
more than performance.  With this option, the linker will not insert
no-ops or widen density instructions to preserve branch target
alignment.  There may still be some cases where no-ops are required to
preserve the correctness of the code.
</p>
</dd>
<dt><samp class="option">--abi-windowed</samp></dt>
<dt><samp class="option">--abi-call0</samp></dt>
<dd><p>Choose ABI for the output object and for the generated PLT code.
PLT code inserted by the linker must match ABI of the output object
because windowed and call0 ABI use incompatible function call
conventions.
Default ABI is chosen by the ABI tag in the <code class="code">.xtensa.info</code> section
of the first input object.
A warning is issued if ABI tags of input objects do not match each other
or the chosen output object ABI.
</p></dd>
</dl>


</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="WIN32.html"><code class="command">ld</code> and WIN32 (cygwin/mingw)</a>, Up: <a href="Machine-Dependent.html">Machine Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
