<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.44.

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Format Commands (LD)</title>

<meta name="description" content="Format Commands (LD)">
<meta name="keywords" content="Format Commands (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Simple-Commands.html" rel="up" title="Simple Commands">
<link href="REGION_005fALIAS.html" rel="next" title="REGION_ALIAS">
<link href="File-Commands.html" rel="prev" title="File Commands">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Format-Commands">
<div class="nav-panel">
<p>
Next: <a href="REGION_005fALIAS.html" accesskey="n" rel="next">Assign alias names to memory regions</a>, Previous: <a href="File-Commands.html" accesskey="p" rel="prev">Commands Dealing with Files</a>, Up: <a href="Simple-Commands.html" accesskey="u" rel="up">Simple Linker Script Commands</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Commands-Dealing-with-Object-File-Formats"><span>3.4.3 Commands Dealing with Object File Formats<a class="copiable-link" href="#Commands-Dealing-with-Object-File-Formats"> &para;</a></span></h4>
<p>A couple of linker script commands deal with object file formats.
</p>
<dl class="table">
<dt><a class="index-entry-id" id="index-output-file-format-in-linker-script"></a>
<a id="index-OUTPUT_005fFORMAT_0028bfdname_0029"></a><span><code class="code">OUTPUT_FORMAT(<var class="var">bfdname</var>)</code><a class="copiable-link" href="#index-OUTPUT_005fFORMAT_0028bfdname_0029"> &para;</a></span></dt>
<dt><code class="code">OUTPUT_FORMAT(<var class="var">default</var>, <var class="var">big</var>, <var class="var">little</var>)</code></dt>
<dd><p>The <code class="code">OUTPUT_FORMAT</code> command names the BFD format to use for the
output file (see <a class="pxref" href="BFD.html">BFD</a>).  Using <code class="code">OUTPUT_FORMAT(<var class="var">bfdname</var>)</code> is
exactly like using &lsquo;<samp class="samp">--oformat <var class="var">bfdname</var></samp>&rsquo; on the command line
(see <a class="pxref" href="Options.html">Command-line Options</a>).  If both are used, the command
line option takes precedence.
</p>
<p>You can use <code class="code">OUTPUT_FORMAT</code> with three arguments to use different
formats based on the &lsquo;<samp class="samp">-EB</samp>&rsquo; and &lsquo;<samp class="samp">-EL</samp>&rsquo; command-line options.
This permits the linker script to set the output format based on the
desired endianness.
</p>
<p>If neither &lsquo;<samp class="samp">-EB</samp>&rsquo; nor &lsquo;<samp class="samp">-EL</samp>&rsquo; are used, then the output format
will be the first argument, <var class="var">default</var>.  If &lsquo;<samp class="samp">-EB</samp>&rsquo; is used, the
output format will be the second argument, <var class="var">big</var>.  If &lsquo;<samp class="samp">-EL</samp>&rsquo; is
used, the output format will be the third argument, <var class="var">little</var>.
</p>
<p>For example, the default linker script for the MIPS ELF target uses this
command:
</p><div class="example smallexample">
<pre class="example-preformatted">OUTPUT_FORMAT(elf32-bigmips, elf32-bigmips, elf32-littlemips)
</pre></div>
<p>This says that the default format for the output file is
&lsquo;<samp class="samp">elf32-bigmips</samp>&rsquo;, but if the user uses the &lsquo;<samp class="samp">-EL</samp>&rsquo; command-line
option, the output file will be created in the &lsquo;<samp class="samp">elf32-littlemips</samp>&rsquo;
format.
</p>
</dd>
<dt><a class="index-entry-id" id="index-input-file-format-in-linker-script"></a>
<a id="index-TARGET_0028bfdname_0029"></a><span><code class="code">TARGET(<var class="var">bfdname</var>)</code><a class="copiable-link" href="#index-TARGET_0028bfdname_0029"> &para;</a></span></dt>
<dd><p>The <code class="code">TARGET</code> command names the BFD format to use when reading input
files.  It affects subsequent <code class="code">INPUT</code> and <code class="code">GROUP</code> commands.
This command is like using &lsquo;<samp class="samp">-b <var class="var">bfdname</var></samp>&rsquo; on the command line
(see <a class="pxref" href="Options.html">Command-line Options</a>).  If the <code class="code">TARGET</code> command
is used but <code class="code">OUTPUT_FORMAT</code> is not, then the last <code class="code">TARGET</code>
command is also used to set the format for the output file.  See <a class="xref" href="BFD.html">BFD</a>.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="REGION_005fALIAS.html">Assign alias names to memory regions</a>, Previous: <a href="File-Commands.html">Commands Dealing with Files</a>, Up: <a href="Simple-Commands.html">Simple Linker Script Commands</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
