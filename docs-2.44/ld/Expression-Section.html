<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.44.

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Expression Section (LD)</title>

<meta name="description" content="Expression Section (LD)">
<meta name="keywords" content="Expression Section (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Expressions.html" rel="up" title="Expressions">
<link href="Builtin-Functions.html" rel="next" title="Builtin Functions">
<link href="Evaluation.html" rel="prev" title="Evaluation">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Expression-Section">
<div class="nav-panel">
<p>
Next: <a href="Builtin-Functions.html" accesskey="n" rel="next">Builtin Functions</a>, Previous: <a href="Evaluation.html" accesskey="p" rel="prev">Evaluation</a>, Up: <a href="Expressions.html" accesskey="u" rel="up">Expressions in Linker Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="The-Section-of-an-Expression"><span>3.10.8 The Section of an Expression<a class="copiable-link" href="#The-Section-of-an-Expression"> &para;</a></span></h4>
<a class="index-entry-id" id="index-expression-sections"></a>
<a class="index-entry-id" id="index-absolute-expressions"></a>
<a class="index-entry-id" id="index-relative-expressions"></a>
<a class="index-entry-id" id="index-absolute-and-relocatable-symbols"></a>
<a class="index-entry-id" id="index-relocatable-and-absolute-symbols"></a>
<a class="index-entry-id" id="index-symbols_002c-relocatable-and-absolute"></a>
<p>Addresses and symbols may be section relative, or absolute.  A section
relative symbol is relocatable.  If you request relocatable output
using the &lsquo;<samp class="samp">-r</samp>&rsquo; option, a further link operation may change the
value of a section relative symbol.  On the other hand, an absolute
symbol will retain the same value throughout any further link
operations.
</p>
<p>Some terms in linker expressions are addresses.  This is true of
section relative symbols and for builtin functions that return an
address, such as <code class="code">ADDR</code>, <code class="code">LOADADDR</code>, <code class="code">ORIGIN</code> and
<code class="code">SEGMENT_START</code>.  Other terms are simply numbers, or are builtin
functions that return a non-address value, such as <code class="code">LENGTH</code>.
One complication is that unless you set <code class="code">LD_FEATURE (&quot;SANE_EXPR&quot;)</code>
(see <a class="pxref" href="Miscellaneous-Commands.html">Other Linker Script Commands</a>), numbers and absolute symbols are treated
differently depending on their location, for compatibility with older
versions of <code class="code">ld</code>.  Expressions appearing outside an output
section definition treat all numbers as absolute addresses.
Expressions appearing inside an output section definition treat
absolute symbols as numbers.  If <code class="code">LD_FEATURE (&quot;SANE_EXPR&quot;)</code> is
given, then absolute symbols and numbers are simply treated as numbers
everywhere.
</p>
<p>In the following simple example,
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">SECTIONS
  {
    . = 0x100;
    __executable_start = 0x100;
    .data :
    {
      . = 0x10;
      __data_start = 0x10;
      *(.data)
    }
    ...
  }
</pre></div></div>

<p>both <code class="code">.</code> and <code class="code">__executable_start</code> are set to the absolute
address 0x100 in the first two assignments, then both <code class="code">.</code> and
<code class="code">__data_start</code> are set to 0x10 relative to the <code class="code">.data</code>
section in the second two assignments.
</p>
<p>For expressions involving numbers, relative addresses and absolute
addresses, ld follows these rules to evaluate terms:
</p>
<ul class="itemize mark-bullet">
<li>Unary operations on an absolute address or number, and binary
operations on two absolute addresses or two numbers, or between one
absolute address and a number, apply the operator to the value(s).
</li><li>Unary operations on a relative address, and binary operations on two
relative addresses in the same section or between one relative address
and a number, apply the operator to the offset part of the address(es).
</li><li>Other binary operations, that is, between two relative addresses not
in the same section, or between a relative address and an absolute
address, first convert any non-absolute term to an absolute address
before applying the operator.
</li></ul>

<p>The result section of each sub-expression is as follows:
</p>
<ul class="itemize mark-bullet">
<li>An operation involving only numbers results in a number.
</li><li>The result of comparisons, &lsquo;<samp class="samp">&amp;&amp;</samp>&rsquo; and &lsquo;<samp class="samp">||</samp>&rsquo; is also a number.
</li><li>The result of other binary arithmetic and logical operations on two
relative addresses in the same section or two absolute addresses
(after above conversions) is also a number when
<code class="code">LD_FEATURE (&quot;SANE_EXPR&quot;)</code> or inside an output section definition
but an absolute address otherwise.
</li><li>The result of other operations on relative addresses or one
relative address and a number, is a relative address in the same
section as the relative operand(s).
</li><li>The result of other operations on absolute addresses (after above
conversions) is an absolute address.
</li></ul>

<p>You can use the builtin function <code class="code">ABSOLUTE</code> to force an expression
to be absolute when it would otherwise be relative.  For example, to
create an absolute symbol set to the address of the end of the output
section &lsquo;<samp class="samp">.data</samp>&rsquo;:
</p><div class="example smallexample">
<pre class="example-preformatted">SECTIONS
  {
    .data : { *(.data) _edata = ABSOLUTE(.); }
  }
</pre></div>
<p>If &lsquo;<samp class="samp">ABSOLUTE</samp>&rsquo; were not used, &lsquo;<samp class="samp">_edata</samp>&rsquo; would be relative to the
&lsquo;<samp class="samp">.data</samp>&rsquo; section.
</p>
<p>Using <code class="code">LOADADDR</code> also forces an expression absolute, since this
particular builtin function returns an absolute address.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Builtin-Functions.html">Builtin Functions</a>, Previous: <a href="Evaluation.html">Evaluation</a>, Up: <a href="Expressions.html">Expressions in Linker Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
