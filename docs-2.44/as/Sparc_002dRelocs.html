<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Sparc-Relocs (Using as)</title>

<meta name="description" content="Sparc-Relocs (Using as)">
<meta name="keywords" content="Sparc-Relocs (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Sparc_002dSyntax.html" rel="up" title="Sparc-Syntax">
<link href="Sparc_002dSize_002dTranslations.html" rel="next" title="Sparc-Size-Translations">
<link href="Sparc_002dConstants.html" rel="prev" title="Sparc-Constants">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Sparc_002dRelocs">
<div class="nav-panel">
<p>
Next: <a href="Sparc_002dSize_002dTranslations.html" accesskey="n" rel="next">Size Translations</a>, Previous: <a href="Sparc_002dConstants.html" accesskey="p" rel="prev">Constants</a>, Up: <a href="Sparc_002dSyntax.html" accesskey="u" rel="up">Sparc Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection subsection-level-set-subsubsection" id="Relocations-4"><span>9.44.3.4 Relocations<a class="copiable-link" href="#Relocations-4"> &para;</a></span></h4>
<a class="index-entry-id" id="index-Sparc-relocations"></a>
<a class="index-entry-id" id="index-relocations_002c-Sparc"></a>

<p>ELF relocations are available as defined in the 32-bit and 64-bit
Sparc ELF specifications.
</p>
<p><code class="code">R_SPARC_HI22</code> is obtained using &lsquo;<samp class="samp">%hi</samp>&rsquo; and <code class="code">R_SPARC_LO10</code>
is obtained using &lsquo;<samp class="samp">%lo</samp>&rsquo;.  Likewise <code class="code">R_SPARC_HIX22</code> is
obtained from &lsquo;<samp class="samp">%hix</samp>&rsquo; and <code class="code">R_SPARC_LOX10</code> is obtained
using &lsquo;<samp class="samp">%lox</samp>&rsquo;.  For example:
</p>
<div class="example">
<pre class="example-preformatted">sethi %hi(symbol), %g1
or    %g1, %lo(symbol), %g1

sethi %hix(symbol), %g1
xor   %g1, %lox(symbol), %g1
</pre></div>

<p>These &ldquo;high&rdquo; mnemonics extract bits 31:10 of their operand,
and the &ldquo;low&rdquo; mnemonics extract bits 9:0 of their operand.
</p>
<p>V9 code model relocations can be requested as follows:
</p>
<ul class="itemize mark-bullet">
<li><code class="code">R_SPARC_HH22</code> is requested using &lsquo;<samp class="samp">%hh</samp>&rsquo;.  It can
also be generated using &lsquo;<samp class="samp">%uhi</samp>&rsquo;.
</li><li><code class="code">R_SPARC_HM10</code> is requested using &lsquo;<samp class="samp">%hm</samp>&rsquo;.  It can
also be generated using &lsquo;<samp class="samp">%ulo</samp>&rsquo;.
</li><li><code class="code">R_SPARC_LM22</code> is requested using &lsquo;<samp class="samp">%lm</samp>&rsquo;.

</li><li><code class="code">R_SPARC_H44</code> is requested using &lsquo;<samp class="samp">%h44</samp>&rsquo;.
</li><li><code class="code">R_SPARC_M44</code> is requested using &lsquo;<samp class="samp">%m44</samp>&rsquo;.
</li><li><code class="code">R_SPARC_L44</code> is requested using &lsquo;<samp class="samp">%l44</samp>&rsquo; or &lsquo;<samp class="samp">%l34</samp>&rsquo;.
</li><li><code class="code">R_SPARC_H34</code> is requested using &lsquo;<samp class="samp">%h34</samp>&rsquo;.
</li></ul>

<p>The &lsquo;<samp class="samp">%l34</samp>&rsquo; generates a <code class="code">R_SPARC_L44</code> relocation because it
calculates the necessary value, and therefore no explicit
<code class="code">R_SPARC_L34</code> relocation needed to be created for this purpose.
</p>
<p>The &lsquo;<samp class="samp">%h34</samp>&rsquo; and &lsquo;<samp class="samp">%l34</samp>&rsquo; relocations are used for the abs34 code
model.  Here is an example abs34 address generation sequence:
</p>
<div class="example">
<pre class="example-preformatted">sethi %h34(symbol), %g1
sllx  %g1, 2, %g1
or    %g1, %l34(symbol), %g1
</pre></div>

<p>The PC relative relocation <code class="code">R_SPARC_PC22</code> can be obtained by
enclosing an operand inside of &lsquo;<samp class="samp">%pc22</samp>&rsquo;.  Likewise, the
<code class="code">R_SPARC_PC10</code> relocation can be obtained using &lsquo;<samp class="samp">%pc10</samp>&rsquo;.
These are mostly used when assembling PIC code.  For example, the
standard PIC sequence on Sparc to get the base of the global offset
table, PC relative, into a register, can be performed as:
</p>
<div class="example">
<pre class="example-preformatted">sethi %pc22(_GLOBAL_OFFSET_TABLE_-4), %l7
add   %l7, %pc10(_GLOBAL_OFFSET_TABLE_+4), %l7
</pre></div>

<p>Several relocations exist to allow the link editor to potentially
optimize GOT data references.  The <code class="code">R_SPARC_GOTDATA_OP_HIX22</code>
relocation can obtained by enclosing an operand inside of
&lsquo;<samp class="samp">%gdop_hix22</samp>&rsquo;.  The <code class="code">R_SPARC_GOTDATA_OP_LOX10</code>
relocation can obtained by enclosing an operand inside of
&lsquo;<samp class="samp">%gdop_lox10</samp>&rsquo;.  Likewise, <code class="code">R_SPARC_GOTDATA_OP</code> can be
obtained by enclosing an operand inside of &lsquo;<samp class="samp">%gdop</samp>&rsquo;.
For example, assuming the GOT base is in register <code class="code">%l7</code>:
</p>
<div class="example">
<pre class="example-preformatted">sethi %gdop_hix22(symbol), %l1
xor   %l1, %gdop_lox10(symbol), %l1
ld    [%l7 + %l1], %l2, %gdop(symbol)
</pre></div>

<p>There are many relocations that can be requested for access to
thread local storage variables.  All of the Sparc TLS mnemonics
are supported:
</p>
<ul class="itemize mark-bullet">
<li><code class="code">R_SPARC_TLS_GD_HI22</code> is requested using &lsquo;<samp class="samp">%tgd_hi22</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_GD_LO10</code> is requested using &lsquo;<samp class="samp">%tgd_lo10</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_GD_ADD</code> is requested using &lsquo;<samp class="samp">%tgd_add</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_GD_CALL</code> is requested using &lsquo;<samp class="samp">%tgd_call</samp>&rsquo;.

</li><li><code class="code">R_SPARC_TLS_LDM_HI22</code> is requested using &lsquo;<samp class="samp">%tldm_hi22</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_LDM_LO10</code> is requested using &lsquo;<samp class="samp">%tldm_lo10</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_LDM_ADD</code> is requested using &lsquo;<samp class="samp">%tldm_add</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_LDM_CALL</code> is requested using &lsquo;<samp class="samp">%tldm_call</samp>&rsquo;.

</li><li><code class="code">R_SPARC_TLS_LDO_HIX22</code> is requested using &lsquo;<samp class="samp">%tldo_hix22</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_LDO_LOX10</code> is requested using &lsquo;<samp class="samp">%tldo_lox10</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_LDO_ADD</code> is requested using &lsquo;<samp class="samp">%tldo_add</samp>&rsquo;.

</li><li><code class="code">R_SPARC_TLS_IE_HI22</code> is requested using &lsquo;<samp class="samp">%tie_hi22</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_IE_LO10</code> is requested using &lsquo;<samp class="samp">%tie_lo10</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_IE_LD</code> is requested using &lsquo;<samp class="samp">%tie_ld</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_IE_LDX</code> is requested using &lsquo;<samp class="samp">%tie_ldx</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_IE_ADD</code> is requested using &lsquo;<samp class="samp">%tie_add</samp>&rsquo;.

</li><li><code class="code">R_SPARC_TLS_LE_HIX22</code> is requested using &lsquo;<samp class="samp">%tle_hix22</samp>&rsquo;.
</li><li><code class="code">R_SPARC_TLS_LE_LOX10</code> is requested using &lsquo;<samp class="samp">%tle_lox10</samp>&rsquo;.
</li></ul>

<p>Here are some example TLS model sequences.
</p>
<p>First, General Dynamic:
</p>
<div class="example">
<pre class="example-preformatted">sethi  %tgd_hi22(symbol), %l1
add    %l1, %tgd_lo10(symbol), %l1
add    %l7, %l1, %o0, %tgd_add(symbol)
call   __tls_get_addr, %tgd_call(symbol)
nop
</pre></div>

<p>Local Dynamic:
</p>
<div class="example">
<pre class="example-preformatted">sethi  %tldm_hi22(symbol), %l1
add    %l1, %tldm_lo10(symbol), %l1
add    %l7, %l1, %o0, %tldm_add(symbol)
call   __tls_get_addr, %tldm_call(symbol)
nop

sethi  %tldo_hix22(symbol), %l1
xor    %l1, %tldo_lox10(symbol), %l1
add    %o0, %l1, %l1, %tldo_add(symbol)
</pre></div>

<p>Initial Exec:
</p>
<div class="example">
<pre class="example-preformatted">sethi  %tie_hi22(symbol), %l1
add    %l1, %tie_lo10(symbol), %l1
ld     [%l7 + %l1], %o0, %tie_ld(symbol)
add    %g7, %o0, %o0, %tie_add(symbol)

sethi  %tie_hi22(symbol), %l1
add    %l1, %tie_lo10(symbol), %l1
ldx    [%l7 + %l1], %o0, %tie_ldx(symbol)
add    %g7, %o0, %o0, %tie_add(symbol)
</pre></div>

<p>And finally, Local Exec:
</p>
<div class="example">
<pre class="example-preformatted">sethi  %tle_hix22(symbol), %l1
add    %l1, %tle_lox10(symbol), %l1
add    %g7, %l1, %l1
</pre></div>

<p>When assembling for 64-bit, and a secondary constant addend is
specified in an address expression that would normally generate
an <code class="code">R_SPARC_LO10</code> relocation, the assembler will emit an
<code class="code">R_SPARC_OLO10</code> instead.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Sparc_002dSize_002dTranslations.html">Size Translations</a>, Previous: <a href="Sparc_002dConstants.html">Constants</a>, Up: <a href="Sparc_002dSyntax.html">Sparc Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
