<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>RISC-V-Modifiers (Using as)</title>

<meta name="description" content="RISC-V-Modifiers (Using as)">
<meta name="keywords" content="RISC-V-Modifiers (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="RISC_002dV_002dDependent.html" rel="up" title="RISC-V-Dependent">
<link href="RISC_002dV_002dFloating_002dPoint.html" rel="next" title="RISC-V-Floating-Point">
<link href="RISC_002dV_002dDirectives.html" rel="prev" title="RISC-V-Directives">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="RISC_002dV_002dModifiers">
<div class="nav-panel">
<p>
Next: <a href="RISC_002dV_002dFloating_002dPoint.html" accesskey="n" rel="next">RISC-V Floating Point</a>, Previous: <a href="RISC_002dV_002dDirectives.html" accesskey="p" rel="prev">RISC-V Directives</a>, Up: <a href="RISC_002dV_002dDependent.html" accesskey="u" rel="up">RISC-V Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="RISC_002dV-Assembler-Modifiers"><span>9.38.3 RISC-V Assembler Modifiers<a class="copiable-link" href="#RISC_002dV-Assembler-Modifiers"> &para;</a></span></h4>

<p>The RISC-V assembler supports following modifiers for relocatable addresses
used in RISC-V instruction operands.  However, we also support some pseudo
instructions that are easier to use than these modifiers.
</p>
<dl class="table">
<dt><code class="code">%lo(<var class="var">symbol</var>)</code></dt>
<dd><p>The low 12 bits of absolute address for <var class="var">symbol</var>.
</p>
</dd>
<dt><code class="code">%hi(<var class="var">symbol</var>)</code></dt>
<dd><p>The high 20 bits of absolute address for <var class="var">symbol</var>.  This is usually
used with the %lo modifier to represent a 32-bit absolute address.
</p>
<div class="example smallexample">
<pre class="example-preformatted">	lui        a0, %hi(<var class="var">symbol</var>)     // R_RISCV_HI20
	addi       a0, a0, %lo(<var class="var">symbol</var>) // R_RISCV_LO12_I

	lui        a0, %hi(<var class="var">symbol</var>)     // R_RISCV_HI20
	load/store a0, %lo(<var class="var">symbol</var>)(a0) // R_RISCV_LO12_I/S
</pre></div>

</dd>
<dt><code class="code">%pcrel_lo(<var class="var">label</var>)</code></dt>
<dd><p>The low 12 bits of relative address between pc and <var class="var">symbol</var>.
The <var class="var">symbol</var> is related to the high part instruction which is marked
by <var class="var">label</var>.
</p>
</dd>
<dt><code class="code">%pcrel_hi(<var class="var">symbol</var>)</code></dt>
<dd><p>The high 20 bits of relative address between pc and <var class="var">symbol</var>.
This is usually used with the %pcrel_lo modifier to represent a +/-2GB
pc-relative range.
</p>
<div class="example smallexample">
<pre class="example-preformatted"><var class="var">label</var>:
	auipc      a0, %pcrel_hi(<var class="var">symbol</var>)    // R_RISCV_PCREL_HI20
	addi       a0, a0, %pcrel_lo(<var class="var">label</var>) // R_RISCV_PCREL_LO12_I

<var class="var">label</var>:
	auipc      a0, %pcrel_hi(<var class="var">symbol</var>)    // R_RISCV_PCREL_HI20
	load/store a0, %pcrel_lo(<var class="var">label</var>)(a0) // R_RISCV_PCREL_LO12_I/S
</pre></div>

<p>Or you can use the pseudo lla/lw/sw/... instruction to do this.
</p>
<div class="example smallexample">
<pre class="example-preformatted">	lla  a0, <var class="var">symbol</var>
</pre></div>

</dd>
<dt><code class="code">%got_pcrel_hi(<var class="var">symbol</var>)</code></dt>
<dd><p>The high 20 bits of relative address between pc and the GOT entry of
<var class="var">symbol</var>.  This is usually used with the %pcrel_lo modifier to access
the GOT entry.
</p>
<div class="example smallexample">
<pre class="example-preformatted"><var class="var">label</var>:
	auipc      a0, %got_pcrel_hi(<var class="var">symbol</var>) // R_RISCV_GOT_HI20
	addi       a0, a0, %pcrel_lo(<var class="var">label</var>)  // R_RISCV_PCREL_LO12_I

<var class="var">label</var>:
	auipc      a0, %got_pcrel_hi(<var class="var">symbol</var>) // R_RISCV_GOT_HI20
	load/store a0, %pcrel_lo(<var class="var">label</var>)(a0)  // R_RISCV_PCREL_LO12_I/S
</pre></div>

<p>Also, the pseudo la instruction with PIC has similar behavior.
</p>
</dd>
<dt><code class="code">%tprel_add(<var class="var">symbol</var>)</code></dt>
<dd><p>This is used purely to associate the R_RISCV_TPREL_ADD relocation for
TLS relaxation.  This one is only valid as the fourth operand to the normally
3 operand add instruction.
</p>
</dd>
<dt><code class="code">%tprel_lo(<var class="var">symbol</var>)</code></dt>
<dd><p>The low 12 bits of relative address between tp and <var class="var">symbol</var>.
</p>
</dd>
<dt><code class="code">%tprel_hi(<var class="var">symbol</var>)</code></dt>
<dd><p>The high 20 bits of relative address between tp and <var class="var">symbol</var>.  This is
usually used with the %tprel_lo and %tprel_add modifiers to access the thread
local variable <var class="var">symbol</var> in TLS Local Exec.
</p>
<div class="example smallexample">
<pre class="example-preformatted">	lui        a5, %tprel_hi(<var class="var">symbol</var>)          // R_RISCV_TPREL_HI20
	add        a5, a5, tp, %tprel_add(<var class="var">symbol</var>) // R_RISCV_TPREL_ADD
	load/store t0, %tprel_lo(<var class="var">symbol</var>)(a5)      // R_RISCV_TPREL_LO12_I/S
</pre></div>

</dd>
<dt><code class="code">%tls_ie_pcrel_hi(<var class="var">symbol</var>)</code></dt>
<dd><p>The high 20 bits of relative address between pc and GOT entry.  It is
usually used with the %pcrel_lo modifier to access the thread local
variable <var class="var">symbol</var> in TLS Initial Exec.
</p>
<div class="example smallexample">
<pre class="example-preformatted">	la.tls.ie  a5, <var class="var">symbol</var>
	add        a5, a5, tp
	load/store t0, 0(a5)
</pre></div>

<p>The pseudo la.tls.ie instruction can be expended to
</p>
<div class="example smallexample">
<pre class="example-preformatted"><var class="var">label</var>:
	auipc a5, %tls_ie_pcrel_hi(<var class="var">symbol</var>) // R_RISCV_TLS_GOT_HI20
	load  a5, %pcrel_lo(<var class="var">label</var>)(a5)     // R_RISCV_PCREL_LO12_I
</pre></div>

</dd>
<dt><code class="code">%tls_gd_pcrel_hi(<var class="var">symbol</var>)</code></dt>
<dd><p>The high 20 bits of relative address between pc and GOT entry.  It is
usually used with the %pcrel_lo modifier to access the thread local variable
<var class="var">symbol</var> in TLS Global Dynamic.
</p>
<div class="example smallexample">
<pre class="example-preformatted">	la.tls.gd  a0, <var class="var">symbol</var>
	call       __tls_get_addr@plt
	mv         a5, a0
	load/store t0, 0(a5)
</pre></div>

<p>The pseudo la.tls.gd instruction can be expended to
</p>
<div class="example smallexample">
<pre class="example-preformatted"><var class="var">label</var>:
	auipc a0, %tls_gd_pcrel_hi(<var class="var">symbol</var>) // R_RISCV_TLS_GD_HI20
	addi  a0, a0, %pcrel_lo(<var class="var">label</var>)     // R_RISCV_PCREL_LO12_I
</pre></div>

</dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="RISC_002dV_002dFloating_002dPoint.html">RISC-V Floating Point</a>, Previous: <a href="RISC_002dV_002dDirectives.html">RISC-V Directives</a>, Up: <a href="RISC_002dV_002dDependent.html">RISC-V Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
