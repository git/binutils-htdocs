<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Sparc-Constants (Using as)</title>

<meta name="description" content="Sparc-Constants (Using as)">
<meta name="keywords" content="Sparc-Constants (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Sparc_002dSyntax.html" rel="up" title="Sparc-Syntax">
<link href="Sparc_002dRelocs.html" rel="next" title="Sparc-Relocs">
<link href="Sparc_002dRegs.html" rel="prev" title="Sparc-Regs">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Sparc_002dConstants">
<div class="nav-panel">
<p>
Next: <a href="Sparc_002dRelocs.html" accesskey="n" rel="next">Relocations</a>, Previous: <a href="Sparc_002dRegs.html" accesskey="p" rel="prev">Register Names</a>, Up: <a href="Sparc_002dSyntax.html" accesskey="u" rel="up">Sparc Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection subsection-level-set-subsubsection" id="Constants-2"><span>9.44.3.3 Constants<a class="copiable-link" href="#Constants-2"> &para;</a></span></h4>
<a class="index-entry-id" id="index-Sparc-constants"></a>
<a class="index-entry-id" id="index-constants_002c-Sparc"></a>

<p>Several Sparc instructions take an immediate operand field for
which mnemonic names exist.  Two such examples are &lsquo;<samp class="samp">membar</samp>&rsquo;
and &lsquo;<samp class="samp">prefetch</samp>&rsquo;.  Another example are the set of V9
memory access instruction that allow specification of an
address space identifier.
</p>
<p>The &lsquo;<samp class="samp">membar</samp>&rsquo; instruction specifies a memory barrier that is
the defined by the operand which is a bitmask.  The supported
mask mnemonics are:
</p>
<ul class="itemize mark-bullet">
<li>&lsquo;<samp class="samp">#Sync</samp>&rsquo; requests that all operations (including nonmemory
reference operations) appearing prior to the <code class="code">membar</code> must have
been performed and the effects of any exceptions become visible before
any instructions after the <code class="code">membar</code> may be initiated.  This
corresponds to <code class="code">membar</code> cmask field bit 2.

</li><li>&lsquo;<samp class="samp">#MemIssue</samp>&rsquo; requests that all memory reference operations
appearing prior to the <code class="code">membar</code> must have been performed before
any memory operation after the <code class="code">membar</code> may be initiated.  This
corresponds to <code class="code">membar</code> cmask field bit 1.

</li><li>&lsquo;<samp class="samp">#Lookaside</samp>&rsquo; requests that a store appearing prior to the
<code class="code">membar</code> must complete before any load following the
<code class="code">membar</code> referencing the same address can be initiated.  This
corresponds to <code class="code">membar</code> cmask field bit 0.

</li><li>&lsquo;<samp class="samp">#StoreStore</samp>&rsquo; defines that the effects of all stores appearing
prior to the <code class="code">membar</code> instruction must be visible to all
processors before the effect of any stores following the
<code class="code">membar</code>.  Equivalent to the deprecated <code class="code">stbar</code> instruction.
This corresponds to <code class="code">membar</code> mmask field bit 3.

</li><li>&lsquo;<samp class="samp">#LoadStore</samp>&rsquo; defines all loads appearing prior to the
<code class="code">membar</code> instruction must have been performed before the effect
of any stores following the <code class="code">membar</code> is visible to any other
processor.  This corresponds to <code class="code">membar</code> mmask field bit 2.

</li><li>&lsquo;<samp class="samp">#StoreLoad</samp>&rsquo; defines that the effects of all stores appearing
prior to the <code class="code">membar</code> instruction must be visible to all
processors before loads following the <code class="code">membar</code> may be performed.
This corresponds to <code class="code">membar</code> mmask field bit 1.

</li><li>&lsquo;<samp class="samp">#LoadLoad</samp>&rsquo; defines that all loads appearing prior to the
<code class="code">membar</code> instruction must have been performed before any loads
following the <code class="code">membar</code> may be performed.  This corresponds to
<code class="code">membar</code> mmask field bit 0.

</li></ul>

<p>These values can be ored together, for example:
</p>
<div class="example">
<pre class="example-preformatted">membar #Sync
membar #StoreLoad | #LoadLoad
membar #StoreLoad | #StoreStore
</pre></div>

<p>The <code class="code">prefetch</code> and <code class="code">prefetcha</code> instructions take a prefetch
function code.  The following prefetch function code constant
mnemonics are available:
</p>
<ul class="itemize mark-bullet">
<li>&lsquo;<samp class="samp">#n_reads</samp>&rsquo; requests a prefetch for several reads, and corresponds
to a prefetch function code of 0.

<p>&lsquo;<samp class="samp">#one_read</samp>&rsquo; requests a prefetch for one read, and corresponds
to a prefetch function code of 1.
</p>
<p>&lsquo;<samp class="samp">#n_writes</samp>&rsquo; requests a prefetch for several writes (and possibly
reads), and corresponds to a prefetch function code of 2.
</p>
<p>&lsquo;<samp class="samp">#one_write</samp>&rsquo; requests a prefetch for one write, and corresponds
to a prefetch function code of 3.
</p>
<p>&lsquo;<samp class="samp">#page</samp>&rsquo; requests a prefetch page, and corresponds to a prefetch
function code of 4.
</p>
<p>&lsquo;<samp class="samp">#invalidate</samp>&rsquo; requests a prefetch invalidate, and corresponds to
a prefetch function code of 16.
</p>
<p>&lsquo;<samp class="samp">#unified</samp>&rsquo; requests a prefetch to the nearest unified cache, and
corresponds to a prefetch function code of 17.
</p>
<p>&lsquo;<samp class="samp">#n_reads_strong</samp>&rsquo; requests a strong prefetch for several reads,
and corresponds to a prefetch function code of 20.
</p>
<p>&lsquo;<samp class="samp">#one_read_strong</samp>&rsquo; requests a strong prefetch for one read,
and corresponds to a prefetch function code of 21.
</p>
<p>&lsquo;<samp class="samp">#n_writes_strong</samp>&rsquo; requests a strong prefetch for several writes,
and corresponds to a prefetch function code of 22.
</p>
<p>&lsquo;<samp class="samp">#one_write_strong</samp>&rsquo; requests a strong prefetch for one write,
and corresponds to a prefetch function code of 23.
</p>
<p>Onle one prefetch code may be specified.  Here are some examples:
</p>
<div class="example">
<pre class="example-preformatted">prefetch  [%l0 + %l2], #one_read
prefetch  [%g2 + 8], #n_writes
prefetcha [%g1] 0x8, #unified
prefetcha [%o0 + 0x10] %asi, #n_reads
</pre></div>

<p>The actual behavior of a given prefetch function code is processor
specific.  If a processor does not implement a given prefetch
function code, it will treat the prefetch instruction as a nop.
</p>
<p>For instructions that accept an immediate address space identifier,
<code class="code">as</code> provides many mnemonics corresponding to
V9 defined as well as UltraSPARC and Niagara extended values.
For example, &lsquo;<samp class="samp">#ASI_P</samp>&rsquo; and &lsquo;<samp class="samp">#ASI_BLK_INIT_QUAD_LDD_AIUS</samp>&rsquo;.
See the V9 and processor specific manuals for details.
</p>
</li></ul>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Sparc_002dRelocs.html">Relocations</a>, Previous: <a href="Sparc_002dRegs.html">Register Names</a>, Up: <a href="Sparc_002dSyntax.html">Sparc Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
