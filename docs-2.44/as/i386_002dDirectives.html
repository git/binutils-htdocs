<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>i386-Directives (Using as)</title>

<meta name="description" content="i386-Directives (Using as)">
<meta name="keywords" content="i386-Directives (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="i386_002dDependent.html" rel="up" title="i386-Dependent">
<link href="i386_002dSyntax.html" rel="next" title="i386-Syntax">
<link href="i386_002dOptions.html" rel="prev" title="i386-Options">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="i386_002dDirectives">
<div class="nav-panel">
<p>
Next: <a href="i386_002dSyntax.html" accesskey="n" rel="next">i386 Syntactical Considerations</a>, Previous: <a href="i386_002dOptions.html" accesskey="p" rel="prev">Options</a>, Up: <a href="i386_002dDependent.html" accesskey="u" rel="up">80386 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="x86-specific-Directives"><span>9.16.2 x86 specific Directives<a class="copiable-link" href="#x86-specific-Directives"> &para;</a></span></h4>

<a class="index-entry-id" id="index-machine-directives_002c-x86"></a>
<a class="index-entry-id" id="index-x86-machine-directives"></a>
<dl class="table">
<dt><a id="index-lcomm-directive_002c-COFF"></a><span><code class="code">.lcomm <var class="var">symbol</var> , <var class="var">length</var>[, <var class="var">alignment</var>]</code><a class="copiable-link" href="#index-lcomm-directive_002c-COFF"> &para;</a></span></dt>
<dd><p>Reserve <var class="var">length</var> (an absolute expression) bytes for a local common
denoted by <var class="var">symbol</var>.  The section and value of <var class="var">symbol</var> are
those of the new local common.  The addresses are allocated in the bss
section, so that at run-time the bytes start off zeroed.  Since
<var class="var">symbol</var> is not declared global, it is normally not visible to
<code class="code">ld</code>.  The optional third parameter, <var class="var">alignment</var>,
specifies the desired alignment of the symbol in the bss section.
</p>
<p>This directive is only available for COFF based x86 targets.
</p>
</dd>
<dt><a id="index-largecomm-directive_002c-ELF"></a><span><code class="code">.largecomm <var class="var">symbol</var> , <var class="var">length</var>[, <var class="var">alignment</var>]</code><a class="copiable-link" href="#index-largecomm-directive_002c-ELF"> &para;</a></span></dt>
<dd><p>This directive behaves in the same way as the <code class="code">comm</code> directive
except that the data is placed into the <var class="var">.lbss</var> section instead of
the <var class="var">.bss</var> section <a class="ref" href="Comm.html"><code class="code">.comm <var class="var">symbol</var> , <var class="var">length</var> </code></a>.
</p>
<p>The directive is intended to be used for data which requires a large
amount of space, and it is only available for ELF based x86_64
targets.
</p>
</dd>
<dt><a id="index-value-directive"></a><span><code class="code">.value <var class="var">expression</var> [, <var class="var">expression</var>]</code><a class="copiable-link" href="#index-value-directive"> &para;</a></span></dt>
<dd><p>This directive behaves in the same way as the <code class="code">.short</code> directive,
taking a series of comma separated expressions and storing them as
two-byte wide values into the current section.
</p>
</dd>
<dt><a id="index-insn-directive"></a><span><code class="code">.insn [<var class="var">prefix</var>[,...]] [<var class="var">encoding</var>] <var class="var">major-opcode</var>[<code class="code">+r</code>|<code class="code">/<var class="var">extension</var></code>] [,<var class="var">operand</var>[,...]]</code><a class="copiable-link" href="#index-insn-directive"> &para;</a></span></dt>
<dd><p>This directive allows composing instructions which <code class="code">as</code>
may not know about yet, or which it has no way of expressing (which
can be the case for certain alternative encodings).  It assumes certain
basic structure in how operands are encoded, and it also only
recognizes - with a few extensions as per below - operands otherwise
valid for instructions.  Therefore there is no guarantee that
everything can be expressed (e.g. the original Intel Xeon Phi&rsquo;s MVEX
encodings cannot be expressed).
</p>
<ul class="itemize mark-bullet">
<li><var class="var">prefix</var> expresses one or more opcode prefixes in the usual way.
Legacy encoding prefixes altering meaning (0x66, 0xF2, 0xF3) may be
specified as high byte of &lt;major-opcode&gt; (perhaps already including an
encoding space prefix).  Note that there can only be one such prefix.
Segment overrides are better specified in the respective memory
operand, as long as there is one.

</li><li><var class="var">encoding</var> is used to specify VEX, XOP, or EVEX encodings. The
syntax tries to resemble that used in documentation:
<ul class="itemize mark-bullet">
<li><code class="code">VEX</code>[<code class="code">.<var class="var">len</var></code>][<code class="code">.<var class="var">prefix</var></code>][<code class="code">.<var class="var">space</var></code>][<code class="code">.<var class="var">w</var></code>]
</li><li><code class="code">EVEX</code>[<code class="code">.<var class="var">len</var></code>][<code class="code">.<var class="var">prefix</var></code>][<code class="code">.<var class="var">space</var></code>][<code class="code">.<var class="var">w</var></code>]
</li><li><code class="code">XOP</code><var class="var">space</var>[<code class="code">.<var class="var">len</var></code>][<code class="code">.<var class="var">prefix</var></code>][<code class="code">.<var class="var">w</var></code>]
</li></ul>

<p>Here
</p><ul class="itemize mark-bullet">
<li><var class="var">len</var> can be <code class="code">LIG</code>, <code class="code">128</code>, <code class="code">256</code>, or (EVEX
only) <code class="code">512</code> as well as <code class="code">L0</code> / <code class="code">L1</code> for VEX / XOP and
<code class="code">L0</code>...<code class="code">L3</code> for EVEX
</li><li><var class="var">prefix</var> can be <code class="code">NP</code>, <code class="code">66</code>, <code class="code">F3</code>, or <code class="code">F2</code>
</li><li><var class="var">space</var> can be
<ul class="itemize mark-bullet">
<li><code class="code">0f</code>, <code class="code">0f38</code>, <code class="code">0f3a</code>, or <code class="code">M0</code>...<code class="code">M31</code>
for VEX
</li><li><code class="code">08</code>...<code class="code">1f</code> for XOP
</li><li><code class="code">0f</code>, <code class="code">0f38</code>, <code class="code">0f3a</code>, or <code class="code">M0</code>...<code class="code">M15</code>
for EVEX
</li></ul>
</li><li><var class="var">w</var> can be <code class="code">WIG</code>, <code class="code">W0</code>, or <code class="code">W1</code>
</li></ul>

<p>Defaults:
</p><ul class="itemize mark-bullet">
<li>Omitted <var class="var">len</var> means &quot;infer from operand size&quot; if there is at
least one sized vector operand, or <code class="code">LIG</code> otherwise. (Obviously
<var class="var">len</var> has to be omitted when there&rsquo;s EVEX rounding control
specified later in the operands.)
</li><li>Omitted <var class="var">prefix</var> means <code class="code">NP</code>.
</li><li>Omitted <var class="var">space</var> (VEX/EVEX only) implies encoding space is
taken from <var class="var">major-opcode</var>.
</li><li>Omitted <var class="var">w</var> means &quot;infer from GPR operand size&quot; in 64-bit
code if there is at least one GPR(-like) operand, or <code class="code">WIG</code>
otherwise.
</li></ul>

</li><li><var class="var">major-opcode</var> is an absolute expression specifying the instruction
opcode.  Legacy encoding prefixes altering encoding space (0x0f,
0x0f38, 0x0f3a) have to be specified as high byte(s) here.
&quot;Degenerate&quot; ModR/M bytes, as present in e.g. certain FPU opcodes or
sub-spaces like that of major opcode 0x0f01, generally want encoding as
immediate operand (such opcodes wouldn&rsquo;t normally have non-immediate
operands); in some cases it may be possible to also encode these as low
byte of the major opcode, but there are potential ambiguities.  Also
note that after stripping encoding prefixes, the residual has to fit in
two bytes (16 bits).  <code class="code">+r</code> can be suffixed to the major opcode
expression to specify register-only encoding forms not using a ModR/M
byte.  <code class="code">/<var class="var">extension</var></code> can alternatively be suffixed to the
major opcode expression to specify an extension opcode, encoded in bits
3-5 of the ModR/M byte.

</li><li><var class="var">operand</var> is an instruction operand expressed the usual way.
Register operands are primarily used to express register numbers as
encoded in ModR/M byte and REX/VEX/XOP/EVEX prefixes.  In certain
cases the register type (really: size) is also used to derive other
encoding attributes, if these aren&rsquo;t specified explicitly.  Note that
there is no consistency checking among operands, so entirely bogus
mixes of operands are possible.  Note further that only operands
actually encoded in the instruction should be specified.  Operands like
&lsquo;<samp class="samp">%cl</samp>&rsquo; in shift/rotate instructions have to be omitted, or else
they&rsquo;ll be encoded as an ordinary (register) operand.  Operand order
may also not match that of the actual instruction (see below).
</li></ul>

<p>Encoding of operands: While for a memory operand (of which there can be
only one) it is clear how to encode it in the resulting ModR/M byte,
register operands are encoded strictly in this order (operand counts do
not include immediate ones in the enumeration below, and if there was an
extension opcode specified it counts as a register operand; VEX.vvvv
is meant to cover XOP and EVEX as well):
</p>
<ul class="itemize mark-bullet">
<li>VEX.vvvv for 1-register-operand VEX/XOP/EVEX insns,
</li><li>ModR/M.rm, ModR/M.reg for 2-operand insns,
</li><li>ModR/M.rm, VEX.vvvv, ModR/M.reg for 3-operand insns, and
</li><li>Imm{4,5}, ModR/M.rm, VEX.vvvv, ModR/M.reg for 4-operand insns,
</li></ul>

<p>obviously with the ModR/M.rm slot skipped when there is a memory
operand, and obviously with the ModR/M.reg slot skipped when there is
an extension opcode.  For Intel syntax of course the opposite order
applies.  With <code class="code">+r</code> (and hence no ModR/M) there can only be a
single register operand for legacy encodings.  VEX and alike can have
two register operands, where the second (first in Intel syntax) would
go into VEX.vvvv.
</p>
<p>Immediate operands (including immediate-like displacements, i.e. when
not part of ModR/M addressing) are emitted in the order specified,
regardless of AT&amp;T or Intel syntax.  Since it may not be possible to
infer the size of such immediates, they can be suffixed by
<code class="code">{:s<var class="var">n</var>}</code> or <code class="code">{:u<var class="var">n</var>}</code>, representing signed /
unsigned immediates of the given number of bits respectively.  When
emitting such operands, the number of bits will be rounded up to the
smallest suitable of 8, 16, 32, or 64.  Immediates wider than 32 bits
are permitted in 64-bit code only.
</p>
<p>For EVEX encoding memory operands with a displacement need to know
Disp8 scaling size in order to use an 8-bit displacement.  For many
instructions this can be inferred from the types of other operands
specified.  In Intel syntax &lsquo;<samp class="samp">DWORD PTR</samp>&rsquo; and alike can be used to
specify the respective size.  In AT&amp;T syntax the memory operands can
be suffixed by <code class="code">{:d<var class="var">n</var>}</code> to specify the size (in bytes).
This can be combined with an embedded broadcast specifier:
&lsquo;<samp class="samp">8(%eax){1to8:d8}</samp>&rsquo;.
</p>
</dd>
<dt><a id="index-noopt-directive"></a><span><code class="code">.noopt</code><a class="copiable-link" href="#index-noopt-directive"> &para;</a></span></dt>
<dd><p>Disable instruction size optimization.
</p>

</dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="i386_002dSyntax.html">i386 Syntactical Considerations</a>, Previous: <a href="i386_002dOptions.html">Options</a>, Up: <a href="i386_002dDependent.html">80386 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
