<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Sparc-Opts (Using as)</title>

<meta name="description" content="Sparc-Opts (Using as)">
<meta name="keywords" content="Sparc-Opts (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Sparc_002dDependent.html" rel="up" title="Sparc-Dependent">
<link href="Sparc_002dAligned_002dData.html" rel="next" title="Sparc-Aligned-Data">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Sparc_002dOpts">
<div class="nav-panel">
<p>
Next: <a href="Sparc_002dAligned_002dData.html" accesskey="n" rel="next">Enforcing aligned data</a>, Up: <a href="Sparc_002dDependent.html" accesskey="u" rel="up">SPARC Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="Options-24"><span>9.44.1 Options<a class="copiable-link" href="#Options-24"> &para;</a></span></h4>

<a class="index-entry-id" id="index-options-for-SPARC"></a>
<a class="index-entry-id" id="index-SPARC-options"></a>
<a class="index-entry-id" id="index-architectures_002c-SPARC"></a>
<a class="index-entry-id" id="index-SPARC-architectures"></a>
<p>The SPARC chip family includes several successive versions, using the same
core instruction set, but including a few additional instructions at
each version.  There are exceptions to this however.  For details on what
instructions each variant supports, please see the chip&rsquo;s architecture
reference manual.
</p>
<p>By default, <code class="code">as</code> assumes the core instruction set (SPARC
v6), but &ldquo;bumps&rdquo; the architecture level as needed: it switches to
successively higher architectures as it encounters instructions that
only exist in the higher levels.
</p>
<p>If not configured for SPARC v9 (<code class="code">sparc64-*-*</code>) GAS will not bump
past sparclite by default, an option must be passed to enable the
v9 instructions.
</p>
<p>GAS treats sparclite as being compatible with v8, unless an architecture
is explicitly requested.  SPARC v9 is always incompatible with sparclite.
</p>

<dl class="table">
<dt><a class="index-entry-id" id="index-_002dAv7"></a>
<a class="index-entry-id" id="index-_002dAv8"></a>
<a class="index-entry-id" id="index-_002dAleon"></a>
<a class="index-entry-id" id="index-_002dAsparclet"></a>
<a class="index-entry-id" id="index-_002dAsparclite"></a>
<a class="index-entry-id" id="index-_002dAv9"></a>
<a class="index-entry-id" id="index-_002dAv9a"></a>
<a class="index-entry-id" id="index-_002dAv9b"></a>
<a class="index-entry-id" id="index-_002dAv9c"></a>
<a class="index-entry-id" id="index-_002dAv9d"></a>
<a class="index-entry-id" id="index-_002dAv9e"></a>
<a class="index-entry-id" id="index-_002dAv9v"></a>
<a class="index-entry-id" id="index-_002dAv9m"></a>
<a class="index-entry-id" id="index-_002dAsparc"></a>
<a class="index-entry-id" id="index-_002dAsparcvis"></a>
<a class="index-entry-id" id="index-_002dAsparcvis2"></a>
<a class="index-entry-id" id="index-_002dAsparcfmaf"></a>
<a class="index-entry-id" id="index-_002dAsparcima"></a>
<a class="index-entry-id" id="index-_002dAsparcvis3"></a>
<a class="index-entry-id" id="index-_002dAsparcvis3r"></a>
<a id="index-_002dAv6"></a><span><code class="code">-Av6 | -Av7 | -Av8 | -Aleon | -Asparclet | -Asparclite</code><a class="copiable-link" href="#index-_002dAv6"> &para;</a></span></dt>
<dt><code class="code">-Av8plus | -Av8plusa | -Av8plusb | -Av8plusc | -Av8plusd |</code></dt>
<dt><code class="code">-Av8plusv | -Av8plusm | -Av8plusm8</code></dt>
<dt><code class="code">-Av9 | -Av9a | -Av9b | -Av9c | -Av9d | -Av9e | -Av9v | -Av9m | -Av9m8</code></dt>
<dt><code class="code">-Asparc | -Asparcvis | -Asparcvis2 | -Asparcfmaf | -Asparcima</code></dt>
<dt><code class="code">-Asparcvis3 | -Asparcvis3r | -Asparc5 | -Asparc6</code></dt>
<dd><p>Use one of the &lsquo;<samp class="samp">-A</samp>&rsquo; options to select one of the SPARC
architectures explicitly.  If you select an architecture explicitly,
<code class="code">as</code> reports a fatal error if it encounters an instruction
or feature requiring an incompatible or higher level.
</p>
<p>&lsquo;<samp class="samp">-Av8plus</samp>&rsquo;, &lsquo;<samp class="samp">-Av8plusa</samp>&rsquo;, &lsquo;<samp class="samp">-Av8plusb</samp>&rsquo;, &lsquo;<samp class="samp">-Av8plusc</samp>&rsquo;,
&lsquo;<samp class="samp">-Av8plusd</samp>&rsquo;, and &lsquo;<samp class="samp">-Av8plusv</samp>&rsquo; select a 32 bit environment.
</p>
<p>&lsquo;<samp class="samp">-Av9</samp>&rsquo;, &lsquo;<samp class="samp">-Av9a</samp>&rsquo;, &lsquo;<samp class="samp">-Av9b</samp>&rsquo;, &lsquo;<samp class="samp">-Av9c</samp>&rsquo;, &lsquo;<samp class="samp">-Av9d</samp>&rsquo;,
&lsquo;<samp class="samp">-Av9e</samp>&rsquo;, &lsquo;<samp class="samp">-Av9v</samp>&rsquo; and &lsquo;<samp class="samp">-Av9m</samp>&rsquo; select a 64 bit
environment and are not available unless GAS is explicitly configured
with 64 bit environment support.
</p>
<p>&lsquo;<samp class="samp">-Av8plusa</samp>&rsquo; and &lsquo;<samp class="samp">-Av9a</samp>&rsquo; enable the SPARC V9 instruction set with
UltraSPARC VIS 1.0 extensions.
</p>
<p>&lsquo;<samp class="samp">-Av8plusb</samp>&rsquo; and &lsquo;<samp class="samp">-Av9b</samp>&rsquo; enable the UltraSPARC VIS 2.0 instructions,
as well as the instructions enabled by &lsquo;<samp class="samp">-Av8plusa</samp>&rsquo; and &lsquo;<samp class="samp">-Av9a</samp>&rsquo;.
</p>
<p>&lsquo;<samp class="samp">-Av8plusc</samp>&rsquo; and &lsquo;<samp class="samp">-Av9c</samp>&rsquo; enable the UltraSPARC Niagara instructions,
as well as the instructions enabled by &lsquo;<samp class="samp">-Av8plusb</samp>&rsquo; and &lsquo;<samp class="samp">-Av9b</samp>&rsquo;.
</p>
<p>&lsquo;<samp class="samp">-Av8plusd</samp>&rsquo; and &lsquo;<samp class="samp">-Av9d</samp>&rsquo; enable the floating point fused
multiply-add, VIS 3.0, and HPC extension instructions, as well as the
instructions enabled by &lsquo;<samp class="samp">-Av8plusc</samp>&rsquo; and &lsquo;<samp class="samp">-Av9c</samp>&rsquo;.
</p>
<p>&lsquo;<samp class="samp">-Av8pluse</samp>&rsquo; and &lsquo;<samp class="samp">-Av9e</samp>&rsquo; enable the cryptographic
instructions, as well as the instructions enabled by &lsquo;<samp class="samp">-Av8plusd</samp>&rsquo;
and &lsquo;<samp class="samp">-Av9d</samp>&rsquo;.
</p>
<p>&lsquo;<samp class="samp">-Av8plusv</samp>&rsquo; and &lsquo;<samp class="samp">-Av9v</samp>&rsquo; enable floating point unfused
multiply-add, and integer multiply-add, as well as the instructions
enabled by &lsquo;<samp class="samp">-Av8pluse</samp>&rsquo; and &lsquo;<samp class="samp">-Av9e</samp>&rsquo;.
</p>
<p>&lsquo;<samp class="samp">-Av8plusm</samp>&rsquo; and &lsquo;<samp class="samp">-Av9m</samp>&rsquo; enable the VIS 4.0, subtract extended,
xmpmul, xmontmul and xmontsqr instructions, as well as the instructions
enabled by &lsquo;<samp class="samp">-Av8plusv</samp>&rsquo; and &lsquo;<samp class="samp">-Av9v</samp>&rsquo;.
</p>
<p>&lsquo;<samp class="samp">-Av8plusm8</samp>&rsquo; and &lsquo;<samp class="samp">-Av9m8</samp>&rsquo; enable the instructions introduced
in the Oracle SPARC Architecture 2017 and the M8 processor, as
well as the instructions enabled by &lsquo;<samp class="samp">-Av8plusm</samp>&rsquo; and &lsquo;<samp class="samp">-Av9m</samp>&rsquo;.
</p>
<p>&lsquo;<samp class="samp">-Asparc</samp>&rsquo; specifies a v9 environment.  It is equivalent to
&lsquo;<samp class="samp">-Av9</samp>&rsquo; if the word size is 64-bit, and &lsquo;<samp class="samp">-Av8plus</samp>&rsquo; otherwise.
</p>
<p>&lsquo;<samp class="samp">-Asparcvis</samp>&rsquo; specifies a v9a environment.  It is equivalent to
&lsquo;<samp class="samp">-Av9a</samp>&rsquo; if the word size is 64-bit, and &lsquo;<samp class="samp">-Av8plusa</samp>&rsquo; otherwise.
</p>
<p>&lsquo;<samp class="samp">-Asparcvis2</samp>&rsquo; specifies a v9b environment.  It is equivalent to
&lsquo;<samp class="samp">-Av9b</samp>&rsquo; if the word size is 64-bit, and &lsquo;<samp class="samp">-Av8plusb</samp>&rsquo; otherwise.
</p>
<p>&lsquo;<samp class="samp">-Asparcfmaf</samp>&rsquo; specifies a v9b environment with the floating point
fused multiply-add instructions enabled.
</p>
<p>&lsquo;<samp class="samp">-Asparcima</samp>&rsquo; specifies a v9b environment with the integer
multiply-add instructions enabled.
</p>
<p>&lsquo;<samp class="samp">-Asparcvis3</samp>&rsquo; specifies a v9b environment with the VIS 3.0,
HPC , and floating point fused multiply-add instructions enabled.
</p>
<p>&lsquo;<samp class="samp">-Asparcvis3r</samp>&rsquo; specifies a v9b environment with the VIS 3.0, HPC,
and floating point unfused multiply-add instructions enabled.
</p>
<p>&lsquo;<samp class="samp">-Asparc5</samp>&rsquo; is equivalent to &lsquo;<samp class="samp">-Av9m</samp>&rsquo;.
</p>
<p>&lsquo;<samp class="samp">-Asparc6</samp>&rsquo; is equivalent to &lsquo;<samp class="samp">-Av9m8</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">-xarch=v8plus | -xarch=v8plusa | -xarch=v8plusb | -xarch=v8plusc</code></dt>
<dt><code class="code">-xarch=v8plusd | -xarch=v8plusv | -xarch=v8plusm |</code></dt>
<dt><code class="code">-xarch=v8plusm8 | -xarch=v9 | -xarch=v9a | -xarch=v9b</code></dt>
<dt><code class="code">-xarch=v9c | -xarch=v9d | -xarch=v9e | -xarch=v9v</code></dt>
<dt><code class="code">-xarch=v9m | -xarch=v9m8</code></dt>
<dt><code class="code">-xarch=sparc | -xarch=sparcvis | -xarch=sparcvis2</code></dt>
<dt><code class="code">-xarch=sparcfmaf | -xarch=sparcima | -xarch=sparcvis3</code></dt>
<dt><code class="code">-xarch=sparcvis3r | -xarch=sparc5 | -xarch=sparc6</code></dt>
<dd><p>For compatibility with the SunOS v9 assembler.  These options are
equivalent to -Av8plus, -Av8plusa, -Av8plusb, -Av8plusc, -Av8plusd,
-Av8plusv, -Av8plusm, -Av8plusm8, -Av9, -Av9a, -Av9b, -Av9c, -Av9d,
-Av9e, -Av9v, -Av9m, -Av9m8, -Asparc, -Asparcvis, -Asparcvis2,
-Asparcfmaf, -Asparcima, -Asparcvis3, -Asparcvis3r, -Asparc5 and
-Asparc6 respectively.
</p>
</dd>
<dt><code class="code">-bump</code></dt>
<dd><p>Warn whenever it is necessary to switch to another level.
If an architecture level is explicitly requested, GAS will not issue
warnings until that level is reached, and will then bump the level
as required (except between incompatible levels).
</p>
</dd>
<dt><code class="code">-32 | -64</code></dt>
<dd><p>Select the word size, either 32 bits or 64 bits.
These options are only available with the ELF object file format,
and require that the necessary BFD support has been included.
</p>
</dd>
<dt><code class="code">--dcti-couples-detect</code></dt>
<dd><p>Warn if a DCTI (delayed control transfer instruction) couple is found
when generating code for a variant of the SPARC architecture in which
the execution of the couple is unpredictable, or very slow.  This is
disabled by default.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Sparc_002dAligned_002dData.html">Enforcing aligned data</a>, Up: <a href="Sparc_002dDependent.html">SPARC Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
