<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>MIPS Options (Using as)</title>

<meta name="description" content="MIPS Options (Using as)">
<meta name="keywords" content="MIPS Options (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="MIPS_002dDependent.html" rel="up" title="MIPS-Dependent">
<link href="MIPS-Macros.html" rel="next" title="MIPS Macros">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="MIPS-Options">
<div class="nav-panel">
<p>
Next: <a href="MIPS-Macros.html" accesskey="n" rel="next">High-level assembly macros</a>, Up: <a href="MIPS_002dDependent.html" accesskey="u" rel="up">MIPS Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="Assembler-options"><span>9.28.1 Assembler options<a class="copiable-link" href="#Assembler-options"> &para;</a></span></h4>

<p>The MIPS configurations of <small class="sc">GNU</small> <code class="code">as</code> support these
special options:
</p>
<dl class="table">
<dt><a id="index-_002dG-option-_0028MIPS_0029"></a><span><code class="code">-G <var class="var">num</var></code><a class="copiable-link" href="#index-_002dG-option-_0028MIPS_0029"> &para;</a></span></dt>
<dd><p>Set the &ldquo;small data&rdquo; limit to <var class="var">n</var> bytes.  The default limit is 8 bytes.
See <a class="xref" href="MIPS-Small-Data.html">Controlling the use of small data accesses</a>.
</p>
</dd>
<dt><a class="index-entry-id" id="index-_002dEL-option-_0028MIPS_0029"></a>
<a class="index-entry-id" id="index-MIPS-big_002dendian-output"></a>
<a class="index-entry-id" id="index-MIPS-little_002dendian-output"></a>
<a class="index-entry-id" id="index-big_002dendian-output_002c-MIPS"></a>
<a class="index-entry-id" id="index-little_002dendian-output_002c-MIPS"></a>
<a id="index-_002dEB-option-_0028MIPS_0029"></a><span><code class="code">-EB</code><a class="copiable-link" href="#index-_002dEB-option-_0028MIPS_0029"> &para;</a></span></dt>
<dt><code class="code">-EL</code></dt>
<dd><p>Any MIPS configuration of <code class="code">as</code> can select big-endian or
little-endian output at run time (unlike the other <small class="sc">GNU</small> development
tools, which must be configured for one or the other).  Use &lsquo;<samp class="samp">-EB</samp>&rsquo;
to select big-endian output, and &lsquo;<samp class="samp">-EL</samp>&rsquo; for little-endian.
</p>
</dd>
<dt><a class="index-entry-id" id="index-_002dKPIC-option_002c-MIPS"></a>
<a id="index-PIC-selection_002c-MIPS"></a><span><code class="code">-KPIC</code><a class="copiable-link" href="#index-PIC-selection_002c-MIPS"> &para;</a></span></dt>
<dd><p>Generate SVR4-style PIC.  This option tells the assembler to generate
SVR4-style position-independent macro expansions.  It also tells the
assembler to mark the output file as PIC.
</p>
</dd>
<dt><a id="index-_002dmvxworks_002dpic-option_002c-MIPS"></a><span><code class="code">-mvxworks-pic</code><a class="copiable-link" href="#index-_002dmvxworks_002dpic-option_002c-MIPS"> &para;</a></span></dt>
<dd><p>Generate VxWorks PIC.  This option tells the assembler to generate
VxWorks-style position-independent macro expansions.
</p>
</dd>
<dt><a id="index-MIPS-architecture-options"></a><span><code class="code">-mips1</code><a class="copiable-link" href="#index-MIPS-architecture-options"> &para;</a></span></dt>
<dt><code class="code">-mips2</code></dt>
<dt><code class="code">-mips3</code></dt>
<dt><code class="code">-mips4</code></dt>
<dt><code class="code">-mips5</code></dt>
<dt><code class="code">-mips32</code></dt>
<dt><code class="code">-mips32r2</code></dt>
<dt><code class="code">-mips32r3</code></dt>
<dt><code class="code">-mips32r5</code></dt>
<dt><code class="code">-mips32r6</code></dt>
<dt><code class="code">-mips64</code></dt>
<dt><code class="code">-mips64r2</code></dt>
<dt><code class="code">-mips64r3</code></dt>
<dt><code class="code">-mips64r5</code></dt>
<dt><code class="code">-mips64r6</code></dt>
<dd><p>Generate code for a particular MIPS Instruction Set Architecture level.
&lsquo;<samp class="samp">-mips1</samp>&rsquo; corresponds to the R2000 and R3000 processors,
&lsquo;<samp class="samp">-mips2</samp>&rsquo; to the R6000 processor, &lsquo;<samp class="samp">-mips3</samp>&rsquo; to the
R4000 processor, and &lsquo;<samp class="samp">-mips4</samp>&rsquo; to the R8000 and R10000 processors.
&lsquo;<samp class="samp">-mips5</samp>&rsquo;, &lsquo;<samp class="samp">-mips32</samp>&rsquo;, &lsquo;<samp class="samp">-mips32r2</samp>&rsquo;, &lsquo;<samp class="samp">-mips32r3</samp>&rsquo;,
&lsquo;<samp class="samp">-mips32r5</samp>&rsquo;, &lsquo;<samp class="samp">-mips32r6</samp>&rsquo;, &lsquo;<samp class="samp">-mips64</samp>&rsquo;, &lsquo;<samp class="samp">-mips64r2</samp>&rsquo;,
&lsquo;<samp class="samp">-mips64r3</samp>&rsquo;, &lsquo;<samp class="samp">-mips64r5</samp>&rsquo;, and &lsquo;<samp class="samp">-mips64r6</samp>&rsquo; correspond to
generic MIPS V, MIPS32, MIPS32 Release 2, MIPS32 Release 3, MIPS32
Release 5, MIPS32 Release 6, MIPS64, and MIPS64 Release 2, MIPS64
Release 3, MIPS64 Release 5, and MIPS64 Release 6 ISA processors,
respectively.  You can also switch instruction sets during the assembly;
see <a class="ref" href="MIPS-ISA.html">Directives to override the ISA level</a>.
</p>
</dd>
<dt><code class="code">-mgp32</code></dt>
<dt><code class="code">-mfp32</code></dt>
<dd><p>Some macros have different expansions for 32-bit and 64-bit registers.
The register sizes are normally inferred from the ISA and ABI, but these
flags force a certain group of registers to be treated as 32 bits wide at
all times.  &lsquo;<samp class="samp">-mgp32</samp>&rsquo; controls the size of general-purpose registers
and &lsquo;<samp class="samp">-mfp32</samp>&rsquo; controls the size of floating-point registers.
</p>
<p>The <code class="code">.set gp=32</code> and <code class="code">.set fp=32</code> directives allow the size
of registers to be changed for parts of an object. The default value is
restored by <code class="code">.set gp=default</code> and <code class="code">.set fp=default</code>.
</p>
<p>On some MIPS variants there is a 32-bit mode flag; when this flag is
set, 64-bit instructions generate a trap.  Also, some 32-bit OSes only
save the 32-bit registers on a context switch, so it is essential never
to use the 64-bit registers.
</p>
</dd>
<dt><code class="code">-mgp64</code></dt>
<dt><code class="code">-mfp64</code></dt>
<dd><p>Assume that 64-bit registers are available.  This is provided in the
interests of symmetry with &lsquo;<samp class="samp">-mgp32</samp>&rsquo; and &lsquo;<samp class="samp">-mfp32</samp>&rsquo;.
</p>
<p>The <code class="code">.set gp=64</code> and <code class="code">.set fp=64</code> directives allow the size
of registers to be changed for parts of an object. The default value is
restored by <code class="code">.set gp=default</code> and <code class="code">.set fp=default</code>.
</p>
</dd>
<dt><code class="code">-mfpxx</code></dt>
<dd><p>Make no assumptions about whether 32-bit or 64-bit floating-point
registers are available. This is provided to support having modules
compatible with either &lsquo;<samp class="samp">-mfp32</samp>&rsquo; or &lsquo;<samp class="samp">-mfp64</samp>&rsquo;. This option can
only be used with MIPS II and above.
</p>
<p>The <code class="code">.set fp=xx</code> directive allows a part of an object to be marked
as not making assumptions about 32-bit or 64-bit FP registers.  The
default value is restored by <code class="code">.set fp=default</code>.
</p>
</dd>
<dt><code class="code">-modd-spreg</code></dt>
<dt><code class="code">-mno-odd-spreg</code></dt>
<dd><p>Enable use of floating-point operations on odd-numbered single-precision
registers when supported by the ISA.  &lsquo;<samp class="samp">-mfpxx</samp>&rsquo; implies
&lsquo;<samp class="samp">-mno-odd-spreg</samp>&rsquo;, otherwise the default is &lsquo;<samp class="samp">-modd-spreg</samp>&rsquo;
</p>
</dd>
<dt><code class="code">-mips16</code></dt>
<dt><code class="code">-no-mips16</code></dt>
<dd><p>Generate code for the MIPS 16 processor.  This is equivalent to putting
<code class="code">.module mips16</code> at the start of the assembly file.  &lsquo;<samp class="samp">-no-mips16</samp>&rsquo;
turns off this option.
</p>
</dd>
<dt><code class="code">-mmips16e2</code></dt>
<dt><code class="code">-mno-mips16e2</code></dt>
<dd><p>Enable the use of MIPS16e2 instructions in MIPS16 mode.  This is equivalent
to putting <code class="code">.module mips16e2</code> at the start of the assembly file.
&lsquo;<samp class="samp">-mno-mips16e2</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mmicromips</code></dt>
<dt><code class="code">-mno-micromips</code></dt>
<dd><p>Generate code for the microMIPS processor.  This is equivalent to putting
<code class="code">.module micromips</code> at the start of the assembly file.
&lsquo;<samp class="samp">-mno-micromips</samp>&rsquo; turns off this option.  This is equivalent to putting
<code class="code">.module nomicromips</code> at the start of the assembly file.
</p>
</dd>
<dt><code class="code">-msmartmips</code></dt>
<dt><code class="code">-mno-smartmips</code></dt>
<dd><p>Enables the SmartMIPS extensions to the MIPS32 instruction set, which
provides a number of new instructions which target smartcard and
cryptographic applications.  This is equivalent to putting
<code class="code">.module smartmips</code> at the start of the assembly file.
&lsquo;<samp class="samp">-mno-smartmips</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mips3d</code></dt>
<dt><code class="code">-no-mips3d</code></dt>
<dd><p>Generate code for the MIPS-3D Application Specific Extension.
This tells the assembler to accept MIPS-3D instructions.
&lsquo;<samp class="samp">-no-mips3d</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mdmx</code></dt>
<dt><code class="code">-no-mdmx</code></dt>
<dd><p>Generate code for the MDMX Application Specific Extension.
This tells the assembler to accept MDMX instructions.
&lsquo;<samp class="samp">-no-mdmx</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mdsp</code></dt>
<dt><code class="code">-mno-dsp</code></dt>
<dd><p>Generate code for the DSP Release 1 Application Specific Extension.
This tells the assembler to accept DSP Release 1 instructions.
&lsquo;<samp class="samp">-mno-dsp</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mdspr2</code></dt>
<dt><code class="code">-mno-dspr2</code></dt>
<dd><p>Generate code for the DSP Release 2 Application Specific Extension.
This option implies &lsquo;<samp class="samp">-mdsp</samp>&rsquo;.
This tells the assembler to accept DSP Release 2 instructions.
&lsquo;<samp class="samp">-mno-dspr2</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mdspr3</code></dt>
<dt><code class="code">-mno-dspr3</code></dt>
<dd><p>Generate code for the DSP Release 3 Application Specific Extension.
This option implies &lsquo;<samp class="samp">-mdsp</samp>&rsquo; and &lsquo;<samp class="samp">-mdspr2</samp>&rsquo;.
This tells the assembler to accept DSP Release 3 instructions.
&lsquo;<samp class="samp">-mno-dspr3</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mmt</code></dt>
<dt><code class="code">-mno-mt</code></dt>
<dd><p>Generate code for the MT Application Specific Extension.
This tells the assembler to accept MT instructions.
&lsquo;<samp class="samp">-mno-mt</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mmcu</code></dt>
<dt><code class="code">-mno-mcu</code></dt>
<dd><p>Generate code for the MCU Application Specific Extension.
This tells the assembler to accept MCU instructions.
&lsquo;<samp class="samp">-mno-mcu</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mmsa</code></dt>
<dt><code class="code">-mno-msa</code></dt>
<dd><p>Generate code for the MIPS SIMD Architecture Extension.
This tells the assembler to accept MSA instructions.
&lsquo;<samp class="samp">-mno-msa</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mxpa</code></dt>
<dt><code class="code">-mno-xpa</code></dt>
<dd><p>Generate code for the MIPS eXtended Physical Address (XPA) Extension.
This tells the assembler to accept XPA instructions.
&lsquo;<samp class="samp">-mno-xpa</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mvirt</code></dt>
<dt><code class="code">-mno-virt</code></dt>
<dd><p>Generate code for the Virtualization Application Specific Extension.
This tells the assembler to accept Virtualization instructions.
&lsquo;<samp class="samp">-mno-virt</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mcrc</code></dt>
<dt><code class="code">-mno-crc</code></dt>
<dd><p>Generate code for the cyclic redundancy check (CRC) Application Specific
Extension.  This tells the assembler to accept CRC instructions.
&lsquo;<samp class="samp">-mno-crc</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mginv</code></dt>
<dt><code class="code">-mno-ginv</code></dt>
<dd><p>Generate code for the Global INValidate (GINV) Application Specific
Extension.  This tells the assembler to accept GINV instructions.
&lsquo;<samp class="samp">-mno-ginv</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mloongson-mmi</code></dt>
<dt><code class="code">-mno-loongson-mmi</code></dt>
<dd><p>Generate code for the Loongson MultiMedia extensions Instructions (MMI)
Application Specific Extension.  This tells the assembler to accept MMI
instructions.
&lsquo;<samp class="samp">-mno-loongson-mmi</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mloongson-cam</code></dt>
<dt><code class="code">-mno-loongson-cam</code></dt>
<dd><p>Generate code for the Loongson Content Address Memory (CAM)
Application Specific Extension.  This tells the assembler to accept CAM
instructions.
&lsquo;<samp class="samp">-mno-loongson-cam</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mloongson-ext</code></dt>
<dt><code class="code">-mno-loongson-ext</code></dt>
<dd><p>Generate code for the Loongson EXTensions (EXT) instructions
Application Specific Extension.  This tells the assembler to accept EXT
instructions.
&lsquo;<samp class="samp">-mno-loongson-ext</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-mloongson-ext2</code></dt>
<dt><code class="code">-mno-loongson-ext2</code></dt>
<dd><p>Generate code for the Loongson EXTensions R2 (EXT2) instructions
Application Specific Extension.  This tells the assembler to accept EXT2
instructions.
&lsquo;<samp class="samp">-mno-loongson-ext2</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-minsn32</code></dt>
<dt><code class="code">-mno-insn32</code></dt>
<dd><p>Only use 32-bit instruction encodings when generating code for the
microMIPS processor.  This option inhibits the use of any 16-bit
instructions.  This is equivalent to putting <code class="code">.set insn32</code> at
the start of the assembly file.  &lsquo;<samp class="samp">-mno-insn32</samp>&rsquo; turns off this
option.  This is equivalent to putting <code class="code">.set noinsn32</code> at the
start of the assembly file.  By default &lsquo;<samp class="samp">-mno-insn32</samp>&rsquo; is
selected, allowing all instructions to be used.
</p>
</dd>
<dt><code class="code">-mfix7000</code></dt>
<dt><code class="code">-mno-fix7000</code></dt>
<dd><p>Cause nops to be inserted if the read of the destination register
of an mfhi or mflo instruction occurs in the following two instructions.
</p>
</dd>
<dt><code class="code">-mfix-rm7000</code></dt>
<dt><code class="code">-mno-fix-rm7000</code></dt>
<dd><p>Cause nops to be inserted if a dmult or dmultu instruction is
followed by a load instruction.
</p>
</dd>
<dt><code class="code">-mfix-loongson2f-jump</code></dt>
<dt><code class="code">-mno-fix-loongson2f-jump</code></dt>
<dd><p>Eliminate instruction fetch from outside 256M region to work around the
Loongson2F &lsquo;<samp class="samp">jump</samp>&rsquo; instructions.  Without it, under extreme cases,
the kernel may crash.  The issue has been solved in latest processor
batches, but this fix has no side effect to them.
</p>
</dd>
<dt><code class="code">-mfix-loongson2f-nop</code></dt>
<dt><code class="code">-mno-fix-loongson2f-nop</code></dt>
<dd><p>Replace nops by <code class="code">or at,at,zero</code> to work around the Loongson2F
&lsquo;<samp class="samp">nop</samp>&rsquo; errata.  Without it, under extreme cases, the CPU might
deadlock.  The issue has been solved in later Loongson2F batches, but
this fix has no side effect to them.
</p>
</dd>
<dt><code class="code">-mfix-loongson3-llsc</code></dt>
<dt><code class="code">-mno-fix-loongson3-llsc</code></dt>
<dd><p>Insert &lsquo;<samp class="samp">sync</samp>&rsquo; before &lsquo;<samp class="samp">ll</samp>&rsquo; and &lsquo;<samp class="samp">lld</samp>&rsquo; to work around
Loongson3 LLSC errata.  Without it, under extrame cases, the CPU might
deadlock. The default can be controlled by the
<samp class="option">--enable-mips-fix-loongson3-llsc=[yes|no]</samp> configure option.
</p>
</dd>
<dt><code class="code">-mfix-vr4120</code></dt>
<dt><code class="code">-mno-fix-vr4120</code></dt>
<dd><p>Insert nops to work around certain VR4120 errata.  This option is
intended to be used on GCC-generated code: it is not designed to catch
all problems in hand-written assembler code.
</p>
</dd>
<dt><code class="code">-mfix-vr4130</code></dt>
<dt><code class="code">-mno-fix-vr4130</code></dt>
<dd><p>Insert nops to work around the VR4130 &lsquo;<samp class="samp">mflo</samp>&rsquo;/&lsquo;<samp class="samp">mfhi</samp>&rsquo; errata.
</p>
</dd>
<dt><code class="code">-mfix-24k</code></dt>
<dt><code class="code">-mno-fix-24k</code></dt>
<dd><p>Insert nops to work around the 24K &lsquo;<samp class="samp">eret</samp>&rsquo;/&lsquo;<samp class="samp">deret</samp>&rsquo; errata.
</p>
</dd>
<dt><code class="code">-mfix-cn63xxp1</code></dt>
<dt><code class="code">-mno-fix-cn63xxp1</code></dt>
<dd><p>Replace <code class="code">pref</code> hints 0 - 4 and 6 - 24 with hint 28 to work around
certain CN63XXP1 errata.
</p>
</dd>
<dt><code class="code">-mfix-r5900</code></dt>
<dt><code class="code">-mno-fix-r5900</code></dt>
<dd><p>Do not attempt to schedule the preceding instruction into the delay slot
of a branch instruction placed at the end of a short loop of six
instructions or fewer and always schedule a <code class="code">nop</code> instruction there
instead.  The short loop bug under certain conditions causes loops to
execute only once or twice, due to a hardware bug in the R5900 chip.
</p>
</dd>
<dt><code class="code">-m4010</code></dt>
<dt><code class="code">-no-m4010</code></dt>
<dd><p>Generate code for the LSI R4010 chip.  This tells the assembler to
accept the R4010-specific instructions (&lsquo;<samp class="samp">addciu</samp>&rsquo;, &lsquo;<samp class="samp">ffc</samp>&rsquo;,
etc.), and to not schedule &lsquo;<samp class="samp">nop</samp>&rsquo; instructions around accesses to
the &lsquo;<samp class="samp">HI</samp>&rsquo; and &lsquo;<samp class="samp">LO</samp>&rsquo; registers.  &lsquo;<samp class="samp">-no-m4010</samp>&rsquo; turns off this
option.
</p>
</dd>
<dt><code class="code">-m4650</code></dt>
<dt><code class="code">-no-m4650</code></dt>
<dd><p>Generate code for the MIPS R4650 chip.  This tells the assembler to accept
the &lsquo;<samp class="samp">mad</samp>&rsquo; and &lsquo;<samp class="samp">madu</samp>&rsquo; instruction, and to not schedule &lsquo;<samp class="samp">nop</samp>&rsquo;
instructions around accesses to the &lsquo;<samp class="samp">HI</samp>&rsquo; and &lsquo;<samp class="samp">LO</samp>&rsquo; registers.
&lsquo;<samp class="samp">-no-m4650</samp>&rsquo; turns off this option.
</p>
</dd>
<dt><code class="code">-m3900</code></dt>
<dt><code class="code">-no-m3900</code></dt>
<dt><code class="code">-m4100</code></dt>
<dt><code class="code">-no-m4100</code></dt>
<dd><p>For each option &lsquo;<samp class="samp">-m<var class="var">nnnn</var></samp>&rsquo;, generate code for the MIPS
R<var class="var">nnnn</var> chip.  This tells the assembler to accept instructions
specific to that chip, and to schedule for that chip&rsquo;s hazards.
</p>
</dd>
<dt><code class="code">-march=<var class="var">cpu</var></code></dt>
<dd><p>Generate code for a particular MIPS CPU.  It is exactly equivalent to
&lsquo;<samp class="samp">-m<var class="var">cpu</var></samp>&rsquo;, except that there are more value of <var class="var">cpu</var>
understood.  Valid <var class="var">cpu</var> value are:
</p>
<blockquote class="quotation">
<p>2000,
3000,
3900,
4000,
4010,
4100,
4111,
vr4120,
vr4130,
vr4181,
4300,
4400,
4600,
4650,
5000,
rm5200,
rm5230,
rm5231,
rm5261,
rm5721,
vr5400,
vr5500,
6000,
rm7000,
8000,
rm9000,
10000,
12000,
14000,
16000,
4kc,
4km,
4kp,
4ksc,
4kec,
4kem,
4kep,
4ksd,
m4k,
m4kp,
m14k,
m14kc,
m14ke,
m14kec,
24kc,
24kf2_1,
24kf,
24kf1_1,
24kec,
24kef2_1,
24kef,
24kef1_1,
34kc,
34kf2_1,
34kf,
34kf1_1,
34kn,
74kc,
74kf2_1,
74kf,
74kf1_1,
74kf3_2,
1004kc,
1004kf2_1,
1004kf,
1004kf1_1,
interaptiv,
interaptiv-mr2,
m5100,
m5101,
p5600,
5kc,
5kf,
20kc,
25kf,
sb1,
sb1a,
i6400,
i6500,
p6600,
loongson2e,
loongson2f,
gs464,
gs464e,
gs264e,
octeon,
octeon+,
octeon2,
octeon3,
xlr,
xlp
</p></blockquote>

<p>For compatibility reasons, &lsquo;<samp class="samp"><var class="var">n</var>x</samp>&rsquo; and &lsquo;<samp class="samp"><var class="var">b</var>fx</samp>&rsquo; are
accepted as synonyms for &lsquo;<samp class="samp"><var class="var">n</var>f1_1</samp>&rsquo;.  These values are
deprecated.
</p>
<p>In addition the special name &lsquo;<samp class="samp">from-abi</samp>&rsquo; can be used, in which
case the assembler will select an architecture suitable for whichever
ABI has been selected, either via the <samp class="option">-mabi=</samp> command line
option or the built in default.
</p>
</dd>
<dt><code class="code">-mtune=<var class="var">cpu</var></code></dt>
<dd><p>Schedule and tune for a particular MIPS CPU.  Valid <var class="var">cpu</var> values are
identical to &lsquo;<samp class="samp">-march=<var class="var">cpu</var></samp>&rsquo;.
</p>
</dd>
<dt><code class="code">-mabi=<var class="var">abi</var></code></dt>
<dd><p>Record which ABI the source code uses.  The recognized arguments
are: &lsquo;<samp class="samp">32</samp>&rsquo;, &lsquo;<samp class="samp">n32</samp>&rsquo;, &lsquo;<samp class="samp">o64</samp>&rsquo;, &lsquo;<samp class="samp">64</samp>&rsquo; and &lsquo;<samp class="samp">eabi</samp>&rsquo;.
</p>
</dd>
<dt><a class="index-entry-id" id="index-_002dmno_002dsym32"></a>
<a id="index-_002dmsym32"></a><span><code class="code">-msym32</code><a class="copiable-link" href="#index-_002dmsym32"> &para;</a></span></dt>
<dt><code class="code">-mno-sym32</code></dt>
<dd><p>Equivalent to adding <code class="code">.set sym32</code> or <code class="code">.set nosym32</code> to
the beginning of the assembler input.  See <a class="xref" href="MIPS-Symbol-Sizes.html">Directives to override the size of symbols</a>.
</p>
</dd>
<dt><a id="index-_002dnocpp-ignored-_0028MIPS_0029"></a><span><code class="code">-nocpp</code><a class="copiable-link" href="#index-_002dnocpp-ignored-_0028MIPS_0029"> &para;</a></span></dt>
<dd><p>This option is ignored.  It is accepted for command-line compatibility with
other assemblers, which use it to turn off C style preprocessing.  With
<small class="sc">GNU</small> <code class="code">as</code>, there is no need for &lsquo;<samp class="samp">-nocpp</samp>&rsquo;, because the
<small class="sc">GNU</small> assembler itself never runs the C preprocessor.
</p>
</dd>
<dt><code class="code">-msoft-float</code></dt>
<dt><code class="code">-mhard-float</code></dt>
<dd><p>Disable or enable floating-point instructions.  Note that by default
floating-point instructions are always allowed even with CPU targets
that don&rsquo;t have support for these instructions.
</p>
</dd>
<dt><code class="code">-msingle-float</code></dt>
<dt><code class="code">-mdouble-float</code></dt>
<dd><p>Disable or enable double-precision floating-point operations.  Note
that by default double-precision floating-point operations are always
allowed even with CPU targets that don&rsquo;t have support for these
operations.
</p>
</dd>
<dt><code class="code">--construct-floats</code></dt>
<dt><code class="code">--no-construct-floats</code></dt>
<dd><p>The <code class="code">--no-construct-floats</code> option disables the construction of
double width floating point constants by loading the two halves of the
value into the two single width floating point registers that make up
the double width register.  This feature is useful if the processor
support the FR bit in its status  register, and this bit is known (by
the programmer) to be set.  This bit prevents the aliasing of the double
width register by the single width registers.
</p>
<p>By default <code class="code">--construct-floats</code> is selected, allowing construction
of these floating point constants.
</p>
</dd>
<dt><code class="code">--relax-branch</code></dt>
<dt><code class="code">--no-relax-branch</code></dt>
<dd><p>The &lsquo;<samp class="samp">--relax-branch</samp>&rsquo; option enables the relaxation of out-of-range
branches.  Any branches whose target cannot be reached directly are
converted to a small instruction sequence including an inverse-condition
branch to the physically next instruction, and a jump to the original
target is inserted between the two instructions.  In PIC code the jump
will involve further instructions for address calculation.
</p>
<p>The <code class="code">BC1ANY2F</code>, <code class="code">BC1ANY2T</code>, <code class="code">BC1ANY4F</code>, <code class="code">BC1ANY4T</code>,
<code class="code">BPOSGE32</code> and <code class="code">BPOSGE64</code> instructions are excluded from
relaxation, because they have no complementing counterparts.  They could
be relaxed with the use of a longer sequence involving another branch,
however this has not been implemented and if their target turns out of
reach, they produce an error even if branch relaxation is enabled.
</p>
<p>Also no MIPS16 branches are ever relaxed.
</p>
<p>By default &lsquo;<samp class="samp">--no-relax-branch</samp>&rsquo; is selected, causing any out-of-range
branches to produce an error.
</p>
</dd>
<dt><code class="code">-mignore-branch-isa</code></dt>
<dt><code class="code">-mno-ignore-branch-isa</code></dt>
<dd><p>Ignore branch checks for invalid transitions between ISA modes.
</p>
<p>The semantics of branches does not provide for an ISA mode switch, so in
most cases the ISA mode a branch has been encoded for has to be the same
as the ISA mode of the branch&rsquo;s target label.  If the ISA modes do not
match, then such a branch, if taken, will cause the ISA mode to remain
unchanged and instructions that follow will be executed in the wrong ISA
mode causing the program to misbehave or crash.
</p>
<p>In the case of the <code class="code">BAL</code> instruction it may be possible to relax
it to an equivalent <code class="code">JALX</code> instruction so that the ISA mode is
switched at the run time as required.  For other branches no relaxation
is possible and therefore GAS has checks implemented that verify in
branch assembly that the two ISA modes match, and report an error
otherwise so that the problem with code can be diagnosed at the assembly
time rather than at the run time.
</p>
<p>However some assembly code, including generated code produced by some
versions of GCC, may incorrectly include branches to data labels, which
appear to require a mode switch but are either dead or immediately
followed by valid instructions encoded for the same ISA the branch has
been encoded for.  While not strictly correct at the source level such
code will execute as intended, so to help with these cases
&lsquo;<samp class="samp">-mignore-branch-isa</samp>&rsquo; is supported which disables ISA mode checks
for branches.
</p>
<p>By default &lsquo;<samp class="samp">-mno-ignore-branch-isa</samp>&rsquo; is selected, causing any invalid
branch requiring a transition between ISA modes to produce an error.
</p>
</dd>
<dt><a id="index-_002dmnan_003d-command_002dline-option_002c-MIPS"></a><span><code class="code">-mnan=<var class="var">encoding</var></code><a class="copiable-link" href="#index-_002dmnan_003d-command_002dline-option_002c-MIPS"> &para;</a></span></dt>
<dd><p>This option indicates whether the source code uses the IEEE 2008
NaN encoding (<samp class="option">-mnan=2008</samp>) or the original MIPS encoding
(<samp class="option">-mnan=legacy</samp>).  It is equivalent to adding a <code class="code">.nan</code>
directive to the beginning of the source file.  See <a class="xref" href="MIPS-NaN-Encodings.html">Directives to record which NaN encoding is being used</a>.
</p>
<p><samp class="option">-mnan=legacy</samp> is the default if no <samp class="option">-mnan</samp> option or
<code class="code">.nan</code> directive is used.
</p>
</dd>
<dt><code class="code">--trap</code></dt>
<dt><code class="code">--no-break</code></dt>
<dd><p><code class="code">as</code> automatically macro expands certain division and
multiplication instructions to check for overflow and division by zero.
This option causes <code class="code">as</code> to generate code to take a trap
exception rather than a break exception when an error is detected and the
ISA selected for assembly at the originating place in source code permits
the use of trap instructions.  The trap instructions are only supported at
Instruction Set Architecture level 2 and higher.
</p>
</dd>
<dt><code class="code">--break</code></dt>
<dt><code class="code">--no-trap</code></dt>
<dd><p>Generate code to take a break exception rather than a trap exception when an
error is detected.  This is the default.
</p>
</dd>
<dt><code class="code">-mpdr</code></dt>
<dt><code class="code">-mno-pdr</code></dt>
<dd><p>Control generation of <code class="code">.pdr</code> sections.  Off by default on IRIX, on
elsewhere.
</p>
</dd>
<dt><code class="code">-mshared</code></dt>
<dt><code class="code">-mno-shared</code></dt>
<dd><p>When generating code using the Unix calling conventions (selected by
&lsquo;<samp class="samp">-KPIC</samp>&rsquo; or &lsquo;<samp class="samp">-mcall_shared</samp>&rsquo;), gas will normally generate code
which can go into a shared library.  The &lsquo;<samp class="samp">-mno-shared</samp>&rsquo; option
tells gas to generate code which uses the calling convention, but can
not go into a shared library.  The resulting code is slightly more
efficient.  This option only affects the handling of the
&lsquo;<samp class="samp">.cpload</samp>&rsquo; and &lsquo;<samp class="samp">.cpsetup</samp>&rsquo; pseudo-ops.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="MIPS-Macros.html">High-level assembly macros</a>, Up: <a href="MIPS_002dDependent.html">MIPS Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
