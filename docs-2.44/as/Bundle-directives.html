<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Bundle directives (Using as)</title>

<meta name="description" content="Bundle directives (Using as)">
<meta name="keywords" content="Bundle directives (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Pseudo-Ops.html" rel="up" title="Pseudo Ops">
<link href="Byte.html" rel="next" title="Byte">
<link href="Bss.html" rel="prev" title="Bss">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Bundle-directives">
<div class="nav-panel">
<p>
Next: <a href="Byte.html" accesskey="n" rel="next"><code class="code">.byte <var class="var">expressions</var></code></a>, Previous: <a href="Bss.html" accesskey="p" rel="prev"><code class="code">.bss <var class="var">subsection</var></code></a>, Up: <a href="Pseudo-Ops.html" accesskey="u" rel="up">Assembler Directives</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Bundle-directives-1"><span>7.11 Bundle directives<a class="copiable-link" href="#Bundle-directives-1"> &para;</a></span></h3>
<ul class="mini-toc">
<li><a href="#g_t_002ebundle_005falign_005fmode-abs_002dexpr" accesskey="1"><code class="code">.bundle_align_mode <var class="var">abs-expr</var></code></a></li>
<li><a href="#g_t_002ebundle_005flock-and-_002ebundle_005funlock" accesskey="2"><code class="code">.bundle_lock</code> and <code class="code">.bundle_unlock</code></a></li>
</ul>
<div class="subsection-level-extent" id="g_t_002ebundle_005falign_005fmode-abs_002dexpr">
<h4 class="subsection"><span>7.11.1 <code class="code">.bundle_align_mode <var class="var">abs-expr</var></code><a class="copiable-link" href="#g_t_002ebundle_005falign_005fmode-abs_002dexpr"> &para;</a></span></h4>
<a class="index-entry-id" id="index-bundle_005falign_005fmode-directive"></a>
<a class="index-entry-id" id="index-bundle"></a>
<a class="index-entry-id" id="index-instruction-bundle"></a>
<a class="index-entry-id" id="index-aligned-instruction-bundle"></a>
<p><code class="code">.bundle_align_mode</code> enables or disables <em class="dfn">aligned instruction
bundle</em> mode.  In this mode, sequences of adjacent instructions are grouped
into fixed-sized <em class="dfn">bundles</em>.  If the argument is zero, this mode is
disabled (which is the default state).  If the argument it not zero, it
gives the size of an instruction bundle as a power of two (as for the
<code class="code">.p2align</code> directive, see <a class="pxref" href="P2align.html"><code class="code">.p2align[wl] [<var class="var">abs-expr</var>[, <var class="var">abs-expr</var>[, <var class="var">abs-expr</var>]]]</code></a>).
</p>
<p>For some targets, it&rsquo;s an ABI requirement that no instruction may span a
certain aligned boundary.  A <em class="dfn">bundle</em> is simply a sequence of
instructions that starts on an aligned boundary.  For example, if
<var class="var">abs-expr</var> is <code class="code">5</code> then the bundle size is 32, so each aligned
chunk of 32 bytes is a bundle.  When aligned instruction bundle mode is in
effect, no single instruction may span a boundary between bundles.  If an
instruction would start too close to the end of a bundle for the length of
that particular instruction to fit within the bundle, then the space at the
end of that bundle is filled with no-op instructions so the instruction
starts in the next bundle.  As a corollary, it&rsquo;s an error if any single
instruction&rsquo;s encoding is longer than the bundle size.
</p>
</div>
<div class="subsection-level-extent" id="g_t_002ebundle_005flock-and-_002ebundle_005funlock">
<h4 class="subsection"><span>7.11.2 <code class="code">.bundle_lock</code> and <code class="code">.bundle_unlock</code><a class="copiable-link" href="#g_t_002ebundle_005flock-and-_002ebundle_005funlock"> &para;</a></span></h4>
<a class="index-entry-id" id="index-bundle_005flock-directive"></a>
<a class="index-entry-id" id="index-bundle_005funlock-directive"></a>
<p>The <code class="code">.bundle_lock</code> and directive <code class="code">.bundle_unlock</code> directives
allow explicit control over instruction bundle padding.  These directives
are only valid when <code class="code">.bundle_align_mode</code> has been used to enable
aligned instruction bundle mode.  It&rsquo;s an error if they appear when
<code class="code">.bundle_align_mode</code> has not been used at all, or when the last
directive was <code class="code">.bundle_align_mode&nbsp;0</code><!-- /@w -->.
</p>
<a class="index-entry-id" id="index-bundle_002dlocked"></a>
<p>For some targets, it&rsquo;s an ABI requirement that certain instructions may
appear only as part of specified permissible sequences of multiple
instructions, all within the same bundle.  A pair of <code class="code">.bundle_lock</code>
and <code class="code">.bundle_unlock</code> directives define a <em class="dfn">bundle-locked</em>
instruction sequence.  For purposes of aligned instruction bundle mode, a
sequence starting with <code class="code">.bundle_lock</code> and ending with
<code class="code">.bundle_unlock</code> is treated as a single instruction.  That is, the
entire sequence must fit into a single bundle and may not span a bundle
boundary.  If necessary, no-op instructions will be inserted before the
first instruction of the sequence so that the whole sequence starts on an
aligned bundle boundary.  It&rsquo;s an error if the sequence is longer than the
bundle size.
</p>
<p>For convenience when using <code class="code">.bundle_lock</code> and <code class="code">.bundle_unlock</code>
inside assembler macros (see <a class="pxref" href="Macro.html"><code class="code">.macro</code></a>), bundle-locked sequences may be
nested.  That is, a second <code class="code">.bundle_lock</code> directive before the next
<code class="code">.bundle_unlock</code> directive has no effect except that it must be
matched by another closing <code class="code">.bundle_unlock</code> so that there is the
same number of <code class="code">.bundle_lock</code> and <code class="code">.bundle_unlock</code> directives.
</p>
</div>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Byte.html"><code class="code">.byte <var class="var">expressions</var></code></a>, Previous: <a href="Bss.html"><code class="code">.bss <var class="var">subsection</var></code></a>, Up: <a href="Pseudo-Ops.html">Assembler Directives</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
