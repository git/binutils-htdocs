<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2025 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Type (Using as)</title>

<meta name="description" content="Type (Using as)">
<meta name="keywords" content="Type (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Pseudo-Ops.html" rel="up" title="Pseudo Ops">
<link href="Uleb128.html" rel="next" title="Uleb128">
<link href="Tls_005fcommon.html" rel="prev" title="Tls_common">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Type">
<div class="nav-panel">
<p>
Next: <a href="Uleb128.html" accesskey="n" rel="next"><code class="code">.uleb128 <var class="var">expressions</var></code></a>, Previous: <a href="Tls_005fcommon.html" accesskey="p" rel="prev"><code class="code">.tls_common <var class="var">symbol</var>, <var class="var">length</var>[, <var class="var">alignment</var>]</code></a>, Up: <a href="Pseudo-Ops.html" accesskey="u" rel="up">Assembler Directives</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="g_t_002etype"><span>7.103 <code class="code">.type</code><a class="copiable-link" href="#g_t_002etype"> &para;</a></span></h3>

<p>This directive is used to set the type of a symbol.
</p>
<h4 class="subheading" id="COFF-Version-2"><span>COFF Version<a class="copiable-link" href="#COFF-Version-2"> &para;</a></span></h4>

<a class="index-entry-id" id="index-COFF-symbol-type"></a>
<a class="index-entry-id" id="index-symbol-type_002c-COFF"></a>
<a class="index-entry-id" id="index-type-directive-_0028COFF-version_0029"></a>
<p>For COFF targets, this directive is permitted only within
<code class="code">.def</code>/<code class="code">.endef</code> pairs.  It is used like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">.type <var class="var">int</var>
</pre></div>

<p>This records the integer <var class="var">int</var> as the type attribute of a symbol table
entry.
</p>

<h4 class="subheading" id="ELF-Version-2"><span>ELF Version<a class="copiable-link" href="#ELF-Version-2"> &para;</a></span></h4>

<a class="index-entry-id" id="index-ELF-symbol-type"></a>
<a class="index-entry-id" id="index-symbol-type_002c-ELF"></a>
<a class="index-entry-id" id="index-type-directive-_0028ELF-version_0029"></a>
<p>For ELF targets, the <code class="code">.type</code> directive is used like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">.type <var class="var">name</var> , <var class="var">type description</var>
</pre></div>

<p>This sets the type of symbol <var class="var">name</var> to be either a
function symbol or an object symbol.  There are five different syntaxes
supported for the <var class="var">type description</var> field, in order to provide
compatibility with various other assemblers.
</p>
<p>Because some of the characters used in these syntaxes (such as &lsquo;<samp class="samp">@</samp>&rsquo; and
&lsquo;<samp class="samp">#</samp>&rsquo;) are comment characters for some architectures, some of the syntaxes
below do not work on all architectures.  The first variant will be accepted by
the GNU assembler on all architectures so that variant should be used for
maximum portability, if you do not need to assemble your code with other
assemblers.
</p>
<p>The syntaxes supported are:
</p>
<div class="example smallexample">
<pre class="example-preformatted">  .type &lt;name&gt; STT_&lt;TYPE_IN_UPPER_CASE&gt;
  .type &lt;name&gt;,#&lt;type&gt;
  .type &lt;name&gt;,@&lt;type&gt;
  .type &lt;name&gt;,%&lt;type&gt;
  .type &lt;name&gt;,&quot;&lt;type&gt;&quot;
</pre></div>

<p>The types supported are:
</p>
<dl class="table">
<dt><code class="code">STT_FUNC</code></dt>
<dt><code class="code">function</code></dt>
<dd><p>Mark the symbol as being a function name.
</p>
</dd>
<dt><code class="code">STT_GNU_IFUNC</code></dt>
<dt><code class="code">gnu_indirect_function</code></dt>
<dd><p>Mark the symbol as an indirect function when evaluated during reloc
processing.  (This is only supported on assemblers targeting GNU systems).
</p>
</dd>
<dt><code class="code">STT_OBJECT</code></dt>
<dt><code class="code">object</code></dt>
<dd><p>Mark the symbol as being a data object.
</p>
</dd>
<dt><code class="code">STT_TLS</code></dt>
<dt><code class="code">tls_object</code></dt>
<dd><p>Mark the symbol as being a thread-local data object.
</p>
</dd>
<dt><code class="code">STT_COMMON</code></dt>
<dt><code class="code">common</code></dt>
<dd><p>Mark the symbol as being a common data object.
</p>
</dd>
<dt><code class="code">STT_NOTYPE</code></dt>
<dt><code class="code">notype</code></dt>
<dd><p>Does not mark the symbol in any way.  It is supported just for completeness.
</p>
</dd>
<dt><code class="code">gnu_unique_object</code></dt>
<dd><p>Marks the symbol as being a globally unique data object.  The dynamic linker
will make sure that in the entire process there is just one symbol with this
name and type in use.  (This is only supported on assemblers targeting GNU
systems).
</p>
</dd>
</dl>

<p>Changing between incompatible types other than from/to STT_NOTYPE will
result in a diagnostic.  An intermediate change to STT_NOTYPE will silence
this.
</p>
<p>Note: Some targets support extra types in addition to those listed above.
</p>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Uleb128.html"><code class="code">.uleb128 <var class="var">expressions</var></code></a>, Previous: <a href="Tls_005fcommon.html"><code class="code">.tls_common <var class="var">symbol</var>, <var class="var">length</var>[, <var class="var">alignment</var>]</code></a>, Up: <a href="Pseudo-Ops.html">Assembler Directives</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
