<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.40.

Copyright (C) 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Format Commands (LD)</title>

<meta name="description" content="Format Commands (LD)">
<meta name="keywords" content="Format Commands (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Simple-Commands.html" rel="up" title="Simple Commands">
<link href="REGION_005fALIAS.html" rel="next" title="REGION_ALIAS">
<link href="File-Commands.html" rel="prev" title="File Commands">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="subsection" id="Format-Commands">
<div class="header">
<p>
Next: <a href="REGION_005fALIAS.html" accesskey="n" rel="next">Assign alias names to memory regions</a>, Previous: <a href="File-Commands.html" accesskey="p" rel="prev">Commands Dealing with Files</a>, Up: <a href="Simple-Commands.html" accesskey="u" rel="up">Simple Linker Script Commands</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Commands-Dealing-with-Object-File-Formats"></span><h4 class="subsection">3.4.3 Commands Dealing with Object File Formats</h4>
<p>A couple of linker script commands deal with object file formats.
</p>
<dl compact="compact">
<dt id='index-OUTPUT_005fFORMAT_0028bfdname_0029'><span><code>OUTPUT_FORMAT(<var>bfdname</var>)</code><a href='#index-OUTPUT_005fFORMAT_0028bfdname_0029' class='copiable-anchor'> &para;</a></span></dt>
<dt><span><code>OUTPUT_FORMAT(<var>default</var>, <var>big</var>, <var>little</var>)</code></span></dt>
<dd><span id="index-output-file-format-in-linker-script"></span>
<p>The <code>OUTPUT_FORMAT</code> command names the BFD format to use for the
output file (see <a href="BFD.html">BFD</a>).  Using <code>OUTPUT_FORMAT(<var>bfdname</var>)</code> is
exactly like using &lsquo;<samp>--oformat <var>bfdname</var></samp>&rsquo; on the command line
(see <a href="Options.html">Command-line Options</a>).  If both are used, the command
line option takes precedence.
</p>
<p>You can use <code>OUTPUT_FORMAT</code> with three arguments to use different
formats based on the &lsquo;<samp>-EB</samp>&rsquo; and &lsquo;<samp>-EL</samp>&rsquo; command-line options.
This permits the linker script to set the output format based on the
desired endianness.
</p>
<p>If neither &lsquo;<samp>-EB</samp>&rsquo; nor &lsquo;<samp>-EL</samp>&rsquo; are used, then the output format
will be the first argument, <var>default</var>.  If &lsquo;<samp>-EB</samp>&rsquo; is used, the
output format will be the second argument, <var>big</var>.  If &lsquo;<samp>-EL</samp>&rsquo; is
used, the output format will be the third argument, <var>little</var>.
</p>
<p>For example, the default linker script for the MIPS ELF target uses this
command:
</p><div class="example">
<pre class="example">OUTPUT_FORMAT(elf32-bigmips, elf32-bigmips, elf32-littlemips)
</pre></div>
<p>This says that the default format for the output file is
&lsquo;<samp>elf32-bigmips</samp>&rsquo;, but if the user uses the &lsquo;<samp>-EL</samp>&rsquo; command-line
option, the output file will be created in the &lsquo;<samp>elf32-littlemips</samp>&rsquo;
format.
</p>
</dd>
<dt id='index-TARGET_0028bfdname_0029'><span><code>TARGET(<var>bfdname</var>)</code><a href='#index-TARGET_0028bfdname_0029' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-input-file-format-in-linker-script"></span>
<p>The <code>TARGET</code> command names the BFD format to use when reading input
files.  It affects subsequent <code>INPUT</code> and <code>GROUP</code> commands.
This command is like using &lsquo;<samp>-b <var>bfdname</var></samp>&rsquo; on the command line
(see <a href="Options.html">Command-line Options</a>).  If the <code>TARGET</code> command
is used but <code>OUTPUT_FORMAT</code> is not, then the last <code>TARGET</code>
command is also used to set the format for the output file.  See <a href="BFD.html">BFD</a>.
</p></dd>
</dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="REGION_005fALIAS.html">Assign alias names to memory regions</a>, Previous: <a href="File-Commands.html">Commands Dealing with Files</a>, Up: <a href="Simple-Commands.html">Simple Linker Script Commands</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
