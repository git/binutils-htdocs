<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.40.

Copyright (C) 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Output Section Type (LD)</title>

<meta name="description" content="Output Section Type (LD)">
<meta name="keywords" content="Output Section Type (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Output-Section-Attributes.html" rel="up" title="Output Section Attributes">
<link href="Output-Section-LMA.html" rel="next" title="Output Section LMA">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection" id="Output-Section-Type">
<div class="header">
<p>
Next: <a href="Output-Section-LMA.html" accesskey="n" rel="next">Output Section LMA</a>, Up: <a href="Output-Section-Attributes.html" accesskey="u" rel="up">Output Section Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Output-Section-Type-1"></span><h4 class="subsubsection">3.6.8.1 Output Section Type</h4>
<p>Each output section may have a type.  The type is a keyword in
parentheses.  The following types are defined:
</p>
<dl compact="compact">
<dt><span><code>NOLOAD</code></span></dt>
<dd><p>The section should be marked as not loadable, so that it will not be
loaded into memory when the program is run.
</p>
</dd>
<dt><span><code>READONLY</code></span></dt>
<dd><p>The section should be marked as read-only.
</p>
</dd>
<dt><span><code>DSECT</code></span></dt>
<dt><span><code>COPY</code></span></dt>
<dt><span><code>INFO</code></span></dt>
<dt><span><code>OVERLAY</code></span></dt>
<dd><p>These type names are supported for backward compatibility, and are
rarely used.  They all have the same effect: the section should be
marked as not allocatable, so that no memory is allocated for the
section when the program is run.
</p>
</dd>
<dt><span><code>TYPE = <var>type</var></code></span></dt>
<dd><p>Set the section type to the integer <var>type</var>. When generating an ELF
output file, type names <code>SHT_PROGBITS</code>, <code>SHT_STRTAB</code>,
<code>SHT_NOTE</code>, <code>SHT_NOBITS</code>, <code>SHT_INIT_ARRAY</code>,
<code>SHT_FINI_ARRAY</code>, and <code>SHT_PREINIT_ARRAY</code> are also allowed
for <var>type</var>.  It is the user&rsquo;s responsibility to ensure that any
special requirements of the section type are met.
</p>
<p>Note - the TYPE only is used if some or all of the contents of the
section do not have an implicit type of their own.  So for example:
</p><div class="example">
<pre class="example">  .foo . TYPE = SHT_PROGBITS { *(.bar) }
</pre></div>
<p>will set the type of section &lsquo;<samp>.foo</samp>&rsquo; to the type of the section
&lsquo;<samp>.bar</samp>&rsquo; in the input files, which may not be the SHT_PROGBITS
type.  Whereas:
</p><div class="example">
<pre class="example">  .foo . TYPE = SHT_PROGBITS { BYTE(1) }
</pre></div>
<p>will set the type of &lsquo;<samp>.foo</samp>&rsquo; to SHT_PROGBBITS.  If it is necessary
to override the type of incoming sections and force the output section
type then an extra piece of untyped data will be needed:
</p><div class="example">
<pre class="example">  .foo . TYPE = SHT_PROGBITS { BYTE(1); *(.bar) }
</pre></div>

</dd>
<dt><span><code>READONLY ( TYPE = <var>type</var> )</code></span></dt>
<dd><p>This form of the syntax combines the <var>READONLY</var> type with the
type specified by <var>type</var>.
</p>
</dd>
</dl>

<span id="index-NOLOAD"></span>
<span id="index-prevent-unnecessary-loading"></span>
<span id="index-loading_002c-preventing"></span>
<p>The linker normally sets the attributes of an output section based on
the input sections which map into it.  You can override this by using
the section type.  For example, in the script sample below, the
&lsquo;<samp>ROM</samp>&rsquo; section is addressed at memory location &lsquo;<samp>0</samp>&rsquo; and does not
need to be loaded when the program is run.
</p><div class="example">
<pre class="example">SECTIONS {
  ROM 0 (NOLOAD) : { &hellip; }
  &hellip;
}
</pre></div>

</div>
<hr>
<div class="header">
<p>
Next: <a href="Output-Section-LMA.html">Output Section LMA</a>, Up: <a href="Output-Section-Attributes.html">Output Section Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
