<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright (C) 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>RISC-V-Directives (Using as)</title>

<meta name="description" content="RISC-V-Directives (Using as)">
<meta name="keywords" content="RISC-V-Directives (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="RISC_002dV_002dDependent.html" rel="up" title="RISC-V-Dependent">
<link href="RISC_002dV_002dModifiers.html" rel="next" title="RISC-V-Modifiers">
<link href="RISC_002dV_002dOptions.html" rel="prev" title="RISC-V-Options">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="section" id="RISC_002dV_002dDirectives">
<div class="header">
<p>
Next: <a href="RISC_002dV_002dModifiers.html" accesskey="n" rel="next">RISC-V Assembler Modifiers</a>, Previous: <a href="RISC_002dV_002dOptions.html" accesskey="p" rel="prev">RISC-V Options</a>, Up: <a href="RISC_002dV_002dDependent.html" accesskey="u" rel="up">RISC-V Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="RISC_002dV-Directives"></span><h4 class="subsection">9.38.2 RISC-V Directives</h4>
<span id="index-machine-directives_002c-RISC_002dV"></span>
<span id="index-RISC_002dV-machine-directives"></span>

<p>The following table lists all available RISC-V specific directives.
</p>
<dl compact="compact">
<dd>
<span id="index-align-directive-1"></span>
</dd>
<dt><span><code>.align <var>size-log-2</var></code></span></dt>
<dd><p>Align to the given boundary, with the size given as log2 the number of bytes to
align to.
</p>
<span id="index-Data-directives"></span>
</dd>
<dt><span><code>.half <var>value</var></code></span></dt>
<dt><span><code>.word <var>value</var></code></span></dt>
<dt><span><code>.dword <var>value</var></code></span></dt>
<dd><p>Emits a half-word, word, or double-word value at the current position.
</p>
<span id="index-DTP_002drelative-data-directives"></span>
</dd>
<dt><span><code>.dtprelword <var>value</var></code></span></dt>
<dt><span><code>.dtpreldword <var>value</var></code></span></dt>
<dd><p>Emits a DTP-relative word (or double-word) at the current position.  This is
meant to be used by the compiler in shared libraries for DWARF debug info for
thread local variables.
</p>
<span id="index-BSS-directive"></span>
</dd>
<dt><span><code>.bss</code></span></dt>
<dd><p>Sets the current section to the BSS section.
</p>
<span id="index-LEB128-directives"></span>
</dd>
<dt><span><code>.uleb128 <var>value</var></code></span></dt>
<dt><span><code>.sleb128 <var>value</var></code></span></dt>
<dd><p>Emits a signed or unsigned LEB128 value at the current position.  This only
accepts constant expressions, because symbol addresses can change with
relaxation, and we don&rsquo;t support relocations to modify LEB128 values at link
time.
</p>
<span id="index-Option-directive"></span>
<span id="index-option-directive"></span>
</dd>
<dt><span><code>.option <var>argument</var></code></span></dt>
<dd><p>Modifies RISC-V specific assembler options inline with the assembly code.
This is used when particular instruction sequences must be assembled with a
specific set of options.  For example, since we relax addressing sequences to
shorter GP-relative sequences when possible the initial load of GP must not be
relaxed and should be emitted as something like
</p>
<div class="example">
<pre class="example">	.option push
	.option norelax
	la gp, __global_pointer$
	.option pop
</pre></div>

<p>in order to produce after linker relaxation the expected
</p>
<div class="example">
<pre class="example">	auipc gp, %pcrel_hi(__global_pointer$)
	addi gp, gp, %pcrel_lo(__global_pointer$)
</pre></div>

<p>instead of just
</p>
<div class="example">
<pre class="example">	addi gp, gp, 0
</pre></div>

<p>It&rsquo;s not expected that options are changed in this manner during regular use,
but there are a handful of esoteric cases like the one above where users need
to disable particular features of the assembler for particular code sequences.
The complete list of option arguments is shown below:
</p>
<dl compact="compact">
<dt><span><code>push</code></span></dt>
<dt><span><code>pop</code></span></dt>
<dd><p>Pushes or pops the current option stack.  These should be used whenever
changing an option in line with assembly code in order to ensure the user&rsquo;s
command-line options are respected for the bulk of the file being assembled.
</p>
</dd>
<dt><span><code>rvc</code></span></dt>
<dt><span><code>norvc</code></span></dt>
<dd><p>Enables or disables the generation of compressed instructions.  Instructions
are opportunistically compressed by the RISC-V assembler when possible, but
sometimes this behavior is not desirable, especially when handling alignments.
</p>
</dd>
<dt><span><code>pic</code></span></dt>
<dt><span><code>nopic</code></span></dt>
<dd><p>Enables or disables position-independent code generation.  Unless you really
know what you&rsquo;re doing, this should only be at the top of a file.
</p>
</dd>
<dt><span><code>relax</code></span></dt>
<dt><span><code>norelax</code></span></dt>
<dd><p>Enables or disables relaxation.  The RISC-V assembler and linker
opportunistically relax some code sequences, but sometimes this behavior is not
desirable.
</p>
</dd>
<dt><span><code>csr-check</code></span></dt>
<dt><span><code>no-csr-check</code></span></dt>
<dd><p>Enables or disables the CSR checking.
</p>
</dd>
<dt><span><code>arch, <var>+extension[version]</var> [,...,<var>+extension_n[version_n]</var>]</code></span></dt>
<dt><span><code>arch, <var>-extension</var> [,...,<var>-extension_n</var>]</code></span></dt>
<dt><span><code>arch, <var>=ISA</var></code></span></dt>
<dd><p>Enables or disables the extensions for specific code region.  For example,
&lsquo;<samp>.option arch, +m2p0</samp>&rsquo; means add m extension with version 2.0, and
&lsquo;<samp>.option arch, -f, -d</samp>&rsquo; means remove extensions, f and d, from the
architecture string.  Note that, &lsquo;<samp>.option arch, +c, -c</samp>&rsquo; have the same
behavior as &lsquo;<samp>.option rvc, norvc</samp>&rsquo;.  However, they are also undesirable
sometimes.  Besides, &lsquo;<samp>.option arch, -i</samp>&rsquo; is illegal, since we cannot
remove the base i extension anytime.  If you want to reset the whole ISA
string, you can also use &lsquo;<samp>.option arch, =rv32imac</samp>&rsquo; to overwrite the
previous settings.
</p></dd>
</dl>

<span id="index-INSN-directives"></span>
</dd>
<dt><span><code>.insn <var>type</var>, <var>operand</var> [,...,<var>operand_n</var>]</code></span></dt>
<dt><span><code>.insn <var>insn_length</var>, <var>value</var></code></span></dt>
<dt><span><code>.insn <var>value</var></code></span></dt>
<dd><p>This directive permits the numeric representation of an instructions
and makes the assembler insert the operands according to one of the
instruction formats for &lsquo;<samp>.insn</samp>&rsquo; (<a href="RISC_002dV_002dFormats.html">RISC-V Instruction Formats</a>).
For example, the instruction &lsquo;<samp>add a0, a1, a2</samp>&rsquo; could be written as
&lsquo;<samp>.insn r 0x33, 0, 0, a0, a1, a2</samp>&rsquo;.  But in fact, the instruction
formats are difficult to use for some users, so most of them are using
&lsquo;<samp>.word</samp>&rsquo; to encode the instruction directly, rather than using
&lsquo;<samp>.insn</samp>&rsquo;.  It is fine for now, but will be wrong when the mapping
symbols are supported, since &lsquo;<samp>.word</samp>&rsquo; will not be shown as an
instruction, it should be shown as data.  Therefore, we also support
two more formats of the &lsquo;<samp>.insn</samp>&rsquo;, the instruction &lsquo;<samp>add a0, a1, a2</samp>&rsquo;
could also be written as &lsquo;<samp>.insn 0x4, 0xc58533</samp>&rsquo; or &lsquo;<samp>.insn 0xc58533</samp>&rsquo;.
When the <var>insn_length</var> is set, then assembler will check if the
<var>value</var> is a valid <var>insn_length</var> bytes instruction.
</p>
<span id="index-_002eattribute-directive_002c-RISC_002dV"></span>
</dd>
<dt><span><code>.attribute <var>tag</var>, <var>value</var></code></span></dt>
<dd><p>Set the object attribute <var>tag</var> to <var>value</var>.
</p>
<p>The <var>tag</var> is either an attribute number, or one of the following:
<code>Tag_RISCV_arch</code>, <code>Tag_RISCV_stack_align</code>,
<code>Tag_RISCV_unaligned_access</code>, <code>Tag_RISCV_priv_spec</code>,
<code>Tag_RISCV_priv_spec_minor</code>, <code>Tag_RISCV_priv_spec_revision</code>.
</p>
</dd>
</dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="RISC_002dV_002dModifiers.html">RISC-V Assembler Modifiers</a>, Previous: <a href="RISC_002dV_002dOptions.html">RISC-V Options</a>, Up: <a href="RISC_002dV_002dDependent.html">RISC-V Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
