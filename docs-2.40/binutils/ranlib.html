<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Copyright (C) 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>ranlib (GNU Binary Utilities)</title>

<meta name="description" content="ranlib (GNU Binary Utilities)">
<meta name="keywords" content="ranlib (GNU Binary Utilities)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Binutils-Index.html" rel="index" title="Binutils Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="size.html" rel="next" title="size">
<link href="objdump.html" rel="prev" title="objdump">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="chapter" id="ranlib">
<div class="header">
<p>
Next: <a href="size.html" accesskey="n" rel="next">size</a>, Previous: <a href="objdump.html" accesskey="p" rel="prev">objdump</a>, Up: <a href="index.html" accesskey="u" rel="up">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="ranlib-1"></span><h2 class="chapter">5 ranlib</h2>

<span id="index-ranlib-1"></span>
<span id="index-archive-contents"></span>
<span id="index-symbol-index-1"></span>


<div class="example">
<pre class="example">ranlib [<samp>--plugin</samp> <var>name</var>] [<samp>-DhHvVt</samp>] <var>archive</var>
</pre></div>


<p><code>ranlib</code> generates an index to the contents of an archive and
stores it in the archive.  The index lists each symbol defined by a
member of an archive that is a relocatable object file.
</p>
<p>You may use &lsquo;<samp>nm -s</samp>&rsquo; or &lsquo;<samp>nm --print-armap</samp>&rsquo; to list this index.
</p>
<p>An archive with such an index speeds up linking to the library and
allows routines in the library to call each other without regard to
their placement in the archive.
</p>
<p>The <small>GNU</small> <code>ranlib</code> program is another form of <small>GNU</small> <code>ar</code>; running
<code>ranlib</code> is completely equivalent to executing &lsquo;<samp>ar -s</samp>&rsquo;.
See <a href="ar.html">ar</a>.
</p>


<dl compact="compact">
<dt><span><code>-h</code></span></dt>
<dt><span><code>-H</code></span></dt>
<dt><span><code>--help</code></span></dt>
<dd><p>Show usage information for <code>ranlib</code>.
</p>
</dd>
<dt><span><code>-v</code></span></dt>
<dt><span><code>-V</code></span></dt>
<dt><span><code>--version</code></span></dt>
<dd><p>Show the version number of <code>ranlib</code>.
</p>
</dd>
<dt id='index-deterministic-archives-4'><span><code>-D</code><a href='#index-deterministic-archives-4' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-_002d_002denable_002ddeterministic_002darchives-4"></span>
<p>Operate in <em>deterministic</em> mode.  The symbol map archive member&rsquo;s
header will show zero for the UID, GID, and timestamp.  When this
option is used, multiple runs will produce identical output files.
</p>
<p>If <samp>binutils</samp> was configured with
<samp>--enable-deterministic-archives</samp>, then this mode is on by
default.  It can be disabled with the &lsquo;<samp>-U</samp>&rsquo; option, described
below.
</p>
</dd>
<dt><span><code>-t</code></span></dt>
<dd><p>Update the timestamp of the symbol map of an archive.
</p>
</dd>
<dt id='index-deterministic-archives-5'><span><code>-U</code><a href='#index-deterministic-archives-5' class='copiable-anchor'> &para;</a></span></dt>
<dd><span id="index-_002d_002denable_002ddeterministic_002darchives-5"></span>
<p>Do <em>not</em> operate in <em>deterministic</em> mode.  This is the
inverse of the &lsquo;<samp>-D</samp>&rsquo; option, above: the archive index will get
actual UID, GID, timestamp, and file mode values.
</p>
<p>If <samp>binutils</samp> was configured <em>without</em>
<samp>--enable-deterministic-archives</samp>, then this mode is on by
default.
</p>
</dd>
</dl>



</div>
<hr>
<div class="header">
<p>
Next: <a href="size.html">size</a>, Previous: <a href="objdump.html">objdump</a>, Up: <a href="index.html">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
