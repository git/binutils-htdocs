<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Copyright (C) 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>windmc (GNU Binary Utilities)</title>

<meta name="description" content="windmc (GNU Binary Utilities)">
<meta name="keywords" content="windmc (GNU Binary Utilities)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Binutils-Index.html" rel="index" title="Binutils Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="windres.html" rel="next" title="windres">
<link href="addr2line.html" rel="prev" title="addr2line">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="chapter" id="windmc">
<div class="header">
<p>
Next: <a href="windres.html" accesskey="n" rel="next">windres</a>, Previous: <a href="addr2line.html" accesskey="p" rel="prev">addr2line</a>, Up: <a href="index.html" accesskey="u" rel="up">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="windmc-1"></span><h2 class="chapter">11 windmc</h2>

<p><code>windmc</code> may be used to generator Windows message resources.
</p>
<blockquote>
<p><em>Warning:</em> <code>windmc</code> is not always built as part of the binary
utilities, since it is only useful for Windows targets.
</p></blockquote>


<div class="example">
<pre class="example">windmc [options] input-file
</pre></div>


<p><code>windmc</code> reads message definitions from an input file (.mc) and
translate them into a set of output files.  The output files may be of
four kinds:
</p>
<dl compact="compact">
<dt><span><code>h</code></span></dt>
<dd><p>A C header file containing the message definitions.
</p>
</dd>
<dt><span><code>rc</code></span></dt>
<dd><p>A resource file compilable by the <code>windres</code> tool.
</p>
</dd>
<dt><span><code>bin</code></span></dt>
<dd><p>One or more binary files containing the resource data for a specific
message language.
</p>
</dd>
<dt><span><code>dbg</code></span></dt>
<dd><p>A C include file that maps message id&rsquo;s to their symbolic name.
</p></dd>
</dl>

<p>The exact description of these different formats is available in
documentation from Microsoft.
</p>
<p>When <code>windmc</code> converts from the <code>mc</code> format to the <code>bin</code>
format, <code>rc</code>, <code>h</code>, and optional <code>dbg</code> it is acting like the
Windows Message Compiler.
</p>


<dl compact="compact">
<dt><span><code>-a</code></span></dt>
<dt><span><code>--ascii_in</code></span></dt>
<dd><p>Specifies that the input file specified is ASCII. This is the default
behaviour.
</p>
</dd>
<dt><span><code>-A</code></span></dt>
<dt><span><code>--ascii_out</code></span></dt>
<dd><p>Specifies that messages in the output <code>bin</code> files should be in ASCII
format.
</p>
</dd>
<dt><span><code>-b</code></span></dt>
<dt><span><code>--binprefix</code></span></dt>
<dd><p>Specifies that <code>bin</code> filenames should have to be prefixed by the
basename of the source file.
</p>
</dd>
<dt><span><code>-c</code></span></dt>
<dt><span><code>--customflag</code></span></dt>
<dd><p>Sets the customer bit in all message id&rsquo;s.
</p>
</dd>
<dt><span><code>-C <var>codepage</var></code></span></dt>
<dt><span><code>--codepage_in <var>codepage</var></code></span></dt>
<dd><p>Sets the default codepage to be used to convert input file to UTF16. The
default is ocdepage 1252.
</p>
</dd>
<dt><span><code>-d</code></span></dt>
<dt><span><code>--decimal_values</code></span></dt>
<dd><p>Outputs the constants in the header file in decimal. Default is using
hexadecimal output.
</p>
</dd>
<dt><span><code>-e <var>ext</var></code></span></dt>
<dt><span><code>--extension <var>ext</var></code></span></dt>
<dd><p>The extension for the header file. The default is .h extension.
</p>
</dd>
<dt><span><code>-F <var>target</var></code></span></dt>
<dt><span><code>--target <var>target</var></code></span></dt>
<dd><p>Specify the BFD format to use for a bin file as output.  This
is a BFD target name; you can use the <samp>--help</samp> option to see a list
of supported targets.  Normally <code>windmc</code> will use the default
format, which is the first one listed by the <samp>--help</samp> option.
<a href="Target-Selection.html">Target Selection</a>.
</p>
</dd>
<dt><span><code>-h <var>path</var></code></span></dt>
<dt><span><code>--headerdir <var>path</var></code></span></dt>
<dd><p>The target directory of the generated header file. The default is the
current directory.
</p>
</dd>
<dt><span><code>-H</code></span></dt>
<dt><span><code>--help</code></span></dt>
<dd><p>Displays a list of command-line options and then exits.
</p>
</dd>
<dt><span><code>-m <var>characters</var></code></span></dt>
<dt><span><code>--maxlength <var>characters</var></code></span></dt>
<dd><p>Instructs <code>windmc</code> to generate a warning if the length
of any message exceeds the number specified.
</p>
</dd>
<dt><span><code>-n</code></span></dt>
<dt><span><code>--nullterminate</code></span></dt>
<dd><p>Terminate message text in <code>bin</code> files by zero. By default they are
terminated by CR/LF.
</p>
</dd>
<dt><span><code>-o</code></span></dt>
<dt><span><code>--hresult_use</code></span></dt>
<dd><p>Not yet implemented. Instructs <code>windmc</code> to generate an OLE2 header
file, using HRESULT definitions. Status codes are used if the flag is not
specified.
</p>
</dd>
<dt><span><code>-O <var>codepage</var></code></span></dt>
<dt><span><code>--codepage_out <var>codepage</var></code></span></dt>
<dd><p>Sets the default codepage to be used to output text files. The default
is ocdepage 1252.
</p>
</dd>
<dt><span><code>-r <var>path</var></code></span></dt>
<dt><span><code>--rcdir <var>path</var></code></span></dt>
<dd><p>The target directory for the generated <code>rc</code> script and the generated
<code>bin</code> files that the resource compiler script includes. The default
is the current directory.
</p>
</dd>
<dt><span><code>-u</code></span></dt>
<dt><span><code>--unicode_in</code></span></dt>
<dd><p>Specifies that the input file is UTF16.
</p>
</dd>
<dt><span><code>-U</code></span></dt>
<dt><span><code>--unicode_out</code></span></dt>
<dd><p>Specifies that messages in the output <code>bin</code> file should be in UTF16
format. This is the default behaviour.
</p>
</dd>
<dt><span><code>-v</code></span></dt>
<dt><span><code>--verbose</code></span></dt>
<dd><p>Enable verbose mode.
</p>
</dd>
<dt><span><code>-V</code></span></dt>
<dt><span><code>--version</code></span></dt>
<dd><p>Prints the version number for <code>windmc</code>.
</p>
</dd>
<dt><span><code>-x <var>path</var></code></span></dt>
<dt><span><code>--xdgb <var>path</var></code></span></dt>
<dd><p>The path of the <code>dbg</code> C include file that maps message id&rsquo;s to the
symbolic name. No such file is generated without specifying the switch.
</p></dd>
</dl>



</div>
<hr>
<div class="header">
<p>
Next: <a href="windres.html">windres</a>, Previous: <a href="addr2line.html">addr2line</a>, Up: <a href="index.html">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
