<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.8, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the gprof profiler of the GNU system.

Copyright (C) 1988-2022 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Output Options (GNU gprof)</title>

<meta name="description" content="Output Options (GNU gprof)">
<meta name="keywords" content="Output Options (GNU gprof)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Invoking.html" rel="up" title="Invoking">
<link href="Analysis-Options.html" rel="next" title="Analysis Options">
<style type="text/css">
<!--
a.copiable-anchor {visibility: hidden; text-decoration: none; line-height: 0em}
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
span:hover a.copiable-anchor {visibility: visible}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<div class="section" id="Output-Options">
<div class="header">
<p>
Next: <a href="Analysis-Options.html" accesskey="n" rel="next">Analysis Options</a>, Up: <a href="Invoking.html" accesskey="u" rel="up"><code>gprof</code> Command Summary</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<span id="Output-Options-1"></span><h3 class="section">4.1 Output Options</h3>

<p>These options specify which of several output formats
<code>gprof</code> should produce.
</p>
<p>Many of these options take an optional <em>symspec</em> to specify
functions to be included or excluded.  These options can be
specified multiple times, with different symspecs, to include
or exclude sets of symbols.  See <a href="Symspecs.html">Symspecs</a>.
</p>
<p>Specifying any of these options overrides the default (&lsquo;<samp>-p -q</samp>&rsquo;),
which prints a flat profile and call graph analysis
for all functions.
</p>
<dl compact="compact">
<dt><span><code>-A[<var>symspec</var>]</code></span></dt>
<dt><span><code>--annotated-source[=<var>symspec</var>]</code></span></dt>
<dd><p>The &lsquo;<samp>-A</samp>&rsquo; option causes <code>gprof</code> to print annotated source code.
If <var>symspec</var> is specified, print output only for matching symbols.
See <a href="Annotated-Source.html">The Annotated Source Listing</a>.
</p>
</dd>
<dt><span><code>-b</code></span></dt>
<dt><span><code>--brief</code></span></dt>
<dd><p>If the &lsquo;<samp>-b</samp>&rsquo; option is given, <code>gprof</code> doesn&rsquo;t print the
verbose blurbs that try to explain the meaning of all of the fields in
the tables.  This is useful if you intend to print out the output, or
are tired of seeing the blurbs.
</p>
</dd>
<dt><span><code>-C[<var>symspec</var>]</code></span></dt>
<dt><span><code>--exec-counts[=<var>symspec</var>]</code></span></dt>
<dd><p>The &lsquo;<samp>-C</samp>&rsquo; option causes <code>gprof</code> to
print a tally of functions and the number of times each was called.
If <var>symspec</var> is specified, print tally only for matching symbols.
</p>
<p>If the profile data file contains basic-block count records, specifying
the &lsquo;<samp>-l</samp>&rsquo; option, along with &lsquo;<samp>-C</samp>&rsquo;, will cause basic-block
execution counts to be tallied and displayed.
</p>
</dd>
<dt><span><code>-i</code></span></dt>
<dt><span><code>--file-info</code></span></dt>
<dd><p>The &lsquo;<samp>-i</samp>&rsquo; option causes <code>gprof</code> to display summary information
about the profile data file(s) and then exit.  The number of histogram,
call graph, and basic-block count records is displayed.
</p>
</dd>
<dt><span><code>-I <var>dirs</var></code></span></dt>
<dt><span><code>--directory-path=<var>dirs</var></code></span></dt>
<dd><p>The &lsquo;<samp>-I</samp>&rsquo; option specifies a list of search directories in
which to find source files.  Environment variable <var>GPROF_PATH</var>
can also be used to convey this information.
Used mostly for annotated source output.
</p>
</dd>
<dt><span><code>-J[<var>symspec</var>]</code></span></dt>
<dt><span><code>--no-annotated-source[=<var>symspec</var>]</code></span></dt>
<dd><p>The &lsquo;<samp>-J</samp>&rsquo; option causes <code>gprof</code> not to
print annotated source code.
If <var>symspec</var> is specified, <code>gprof</code> prints annotated source,
but excludes matching symbols.
</p>
</dd>
<dt><span><code>-L</code></span></dt>
<dt><span><code>--print-path</code></span></dt>
<dd><p>Normally, source filenames are printed with the path
component suppressed.  The &lsquo;<samp>-L</samp>&rsquo; option causes <code>gprof</code>
to print the full pathname of
source filenames, which is determined
from symbolic debugging information in the image file
and is relative to the directory in which the compiler
was invoked.
</p>
</dd>
<dt><span><code>-p[<var>symspec</var>]</code></span></dt>
<dt><span><code>--flat-profile[=<var>symspec</var>]</code></span></dt>
<dd><p>The &lsquo;<samp>-p</samp>&rsquo; option causes <code>gprof</code> to print a flat profile.
If <var>symspec</var> is specified, print flat profile only for matching symbols.
See <a href="Flat-Profile.html">The Flat Profile</a>.
</p>
</dd>
<dt><span><code>-P[<var>symspec</var>]</code></span></dt>
<dt><span><code>--no-flat-profile[=<var>symspec</var>]</code></span></dt>
<dd><p>The &lsquo;<samp>-P</samp>&rsquo; option causes <code>gprof</code> to suppress printing a flat profile.
If <var>symspec</var> is specified, <code>gprof</code> prints a flat profile,
but excludes matching symbols.
</p>
</dd>
<dt><span><code>-q[<var>symspec</var>]</code></span></dt>
<dt><span><code>--graph[=<var>symspec</var>]</code></span></dt>
<dd><p>The &lsquo;<samp>-q</samp>&rsquo; option causes <code>gprof</code> to print the call graph analysis.
If <var>symspec</var> is specified, print call graph only for matching symbols
and their children.
See <a href="Call-Graph.html">The Call Graph</a>.
</p>
</dd>
<dt><span><code>-Q[<var>symspec</var>]</code></span></dt>
<dt><span><code>--no-graph[=<var>symspec</var>]</code></span></dt>
<dd><p>The &lsquo;<samp>-Q</samp>&rsquo; option causes <code>gprof</code> to suppress printing the
call graph.
If <var>symspec</var> is specified, <code>gprof</code> prints a call graph,
but excludes matching symbols.
</p>
</dd>
<dt><span><code>-t</code></span></dt>
<dt><span><code>--table-length=<var>num</var></code></span></dt>
<dd><p>The &lsquo;<samp>-t</samp>&rsquo; option causes the <var>num</var> most active source lines in
each source file to be listed when source annotation is enabled.  The
default is 10.
</p>
</dd>
<dt><span><code>-y</code></span></dt>
<dt><span><code>--separate-files</code></span></dt>
<dd><p>This option affects annotated source output only.
Normally, <code>gprof</code> prints annotated source files
to standard-output.  If this option is specified,
annotated source for a file named <samp>path/<var>filename</var></samp>
is generated in the file <samp><var>filename</var>-ann</samp>.  If the underlying
file system would truncate <samp><var>filename</var>-ann</samp> so that it
overwrites the original <samp><var>filename</var></samp>, <code>gprof</code> generates
annotated source in the file <samp><var>filename</var>.ann</samp> instead (if the
original file name has an extension, that extension is <em>replaced</em>
with <samp>.ann</samp>).
</p>
</dd>
<dt><span><code>-Z[<var>symspec</var>]</code></span></dt>
<dt><span><code>--no-exec-counts[=<var>symspec</var>]</code></span></dt>
<dd><p>The &lsquo;<samp>-Z</samp>&rsquo; option causes <code>gprof</code> not to
print a tally of functions and the number of times each was called.
If <var>symspec</var> is specified, print tally, but exclude matching symbols.
</p>
</dd>
<dt><span><code>-r</code></span></dt>
<dt><span><code>--function-ordering</code></span></dt>
<dd><p>The &lsquo;<samp>--function-ordering</samp>&rsquo; option causes <code>gprof</code> to print a
suggested function ordering for the program based on profiling data.
This option suggests an ordering which may improve paging, tlb and
cache behavior for the program on systems which support arbitrary
ordering of functions in an executable.
</p>
<p>The exact details of how to force the linker to place functions
in a particular order is system dependent and out of the scope of this
manual.
</p>
</dd>
<dt><span><code>-R <var>map_file</var></code></span></dt>
<dt><span><code>--file-ordering <var>map_file</var></code></span></dt>
<dd><p>The &lsquo;<samp>--file-ordering</samp>&rsquo; option causes <code>gprof</code> to print a
suggested .o link line ordering for the program based on profiling data.
This option suggests an ordering which may improve paging, tlb and
cache behavior for the program on systems which do not support arbitrary
ordering of functions in an executable.
</p>
<p>Use of the &lsquo;<samp>-a</samp>&rsquo; argument is highly recommended with this option.
</p>
<p>The <var>map_file</var> argument is a pathname to a file which provides
function name to object file mappings.  The format of the file is similar to
the output of the program <code>nm</code>.
</p>
<div class="example">
<pre class="example">c-parse.o:00000000 T yyparse
c-parse.o:00000004 C yyerrflag
c-lang.o:00000000 T maybe_objc_method_name
c-lang.o:00000000 T print_lang_statistics
c-lang.o:00000000 T recognize_objc_keyword
c-decl.o:00000000 T print_lang_identifier
c-decl.o:00000000 T print_lang_type
&hellip;

</pre></div>

<p>To create a <var>map_file</var> with <small>GNU</small> <code>nm</code>, type a command like
<kbd>nm --extern-only --defined-only -v --print-file-name program-name</kbd>.
</p>
</dd>
<dt><span><code>-T</code></span></dt>
<dt><span><code>--traditional</code></span></dt>
<dd><p>The &lsquo;<samp>-T</samp>&rsquo; option causes <code>gprof</code> to print its output in
&ldquo;traditional&rdquo; BSD style.
</p>
</dd>
<dt><span><code>-w <var>width</var></code></span></dt>
<dt><span><code>--width=<var>width</var></code></span></dt>
<dd><p>Sets width of output lines to <var>width</var>.
Currently only used when printing the function index at the bottom
of the call graph.
</p>
</dd>
<dt><span><code>-x</code></span></dt>
<dt><span><code>--all-lines</code></span></dt>
<dd><p>This option affects annotated source output only.
By default, only the lines at the beginning of a basic-block
are annotated.  If this option is specified, every line in
a basic-block is annotated by repeating the annotation for the
first line.  This behavior is similar to <code>tcov</code>&rsquo;s &lsquo;<samp>-a</samp>&rsquo;.
</p>
</dd>
<dt><span><code>--demangle[=<var>style</var>]</code></span></dt>
<dt><span><code>--no-demangle</code></span></dt>
<dd><p>These options control whether C++ symbol names should be demangled when
printing output.  The default is to demangle symbols.  The
<code>--no-demangle</code> option may be used to turn off demangling. Different
compilers have different mangling styles.  The optional demangling style
argument can be used to choose an appropriate demangling style for your
compiler.
</p></dd>
</dl>

</div>
<hr>
<div class="header">
<p>
Next: <a href="Analysis-Options.html">Analysis Options</a>, Up: <a href="Invoking.html"><code>gprof</code> Command Summary</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
