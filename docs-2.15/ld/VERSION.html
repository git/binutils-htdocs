<html lang="en">
<head>
<title>Untitled</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name="description" content="Untitled">
<meta name="generator" content="makeinfo 4.6">
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
--></style>
</head>
<body>
<div class="node">
<p>
Node:&nbsp;<a name="VERSION">VERSION</a>,
Next:&nbsp;<a rel="next" accesskey="n" href="Expressions.html#Expressions">Expressions</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="PHDRS.html#PHDRS">PHDRS</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="Scripts.html#Scripts">Scripts</a>
<hr><br>
</div>

<h3 class="section">VERSION Command</h3>

<p>The linker supports symbol versions when using ELF.  Symbol versions are
only useful when using shared libraries.  The dynamic linker can use
symbol versions to select a specific version of a function when it runs
a program that may have been linked against an earlier version of the
shared library.

   <p>You can include a version script directly in the main linker script, or
you can supply the version script as an implicit linker script.  You can
also use the <code>--version-script</code> linker option.

   <p>The syntax of the <code>VERSION</code> command is simply
<pre class="smallexample">     VERSION { version-script-commands }
     </pre>

   <p>The format of the version script commands is identical to that used by
Sun's linker in Solaris 2.5.  The version script defines a tree of
version nodes.  You specify the node names and interdependencies in the
version script.  You can specify which symbols are bound to which
version nodes, and you can reduce a specified set of symbols to local
scope so that they are not globally visible outside of the shared
library.

   <p>The easiest way to demonstrate the version script language is with a few
examples.

<pre class="smallexample">     VERS_1.1 {
     	 global:
     		 foo1;
     	 local:
     		 old*;
     		 original*;
     		 new*;
     };
     
     VERS_1.2 {
     		 foo2;
     } VERS_1.1;
     
     VERS_2.0 {
     		 bar1; bar2;
     } VERS_1.2;
     </pre>

   <p>This example version script defines three version nodes.  The first
version node defined is <code>VERS_1.1</code>; it has no other dependencies. 
The script binds the symbol <code>foo1</code> to <code>VERS_1.1</code>.  It reduces
a number of symbols to local scope so that they are not visible outside
of the shared library; this is done using wildcard patterns, so that any
symbol whose name begins with <code>old</code>, <code>original</code>, or <code>new</code>
is matched.  The wildcard patterns available are the same as those used
in the shell when matching filenames (also known as "globbing").

   <p>Next, the version script defines node <code>VERS_1.2</code>.  This node
depends upon <code>VERS_1.1</code>.  The script binds the symbol <code>foo2</code>
to the version node <code>VERS_1.2</code>.

   <p>Finally, the version script defines node <code>VERS_2.0</code>.  This node
depends upon <code>VERS_1.2</code>.  The scripts binds the symbols <code>bar1</code>
and <code>bar2</code> are bound to the version node <code>VERS_2.0</code>.

   <p>When the linker finds a symbol defined in a library which is not
specifically bound to a version node, it will effectively bind it to an
unspecified base version of the library.  You can bind all otherwise
unspecified symbols to a given version node by using <code>global: *;</code>
somewhere in the version script.

   <p>The names of the version nodes have no specific meaning other than what
they might suggest to the person reading them.  The <code>2.0</code> version
could just as well have appeared in between <code>1.1</code> and <code>1.2</code>. 
However, this would be a confusing way to write a version script.

   <p>Node name can be omited, provided it is the only version node
in the version script.  Such version script doesn't assign any versions to
symbols, only selects which symbols will be globally visible out and which
won't.

<pre class="smallexample">     { global: foo; bar; local: *; };
     </pre>

   <p>When you link an application against a shared library that has versioned
symbols, the application itself knows which version of each symbol it
requires, and it also knows which version nodes it needs from each
shared library it is linked against.  Thus at runtime, the dynamic
loader can make a quick check to make sure that the libraries you have
linked against do in fact supply all of the version nodes that the
application will need to resolve all of the dynamic symbols.  In this
way it is possible for the dynamic linker to know with certainty that
all external symbols that it needs will be resolvable without having to
search for each symbol reference.

   <p>The symbol versioning is in effect a much more sophisticated way of
doing minor version checking that SunOS does.  The fundamental problem
that is being addressed here is that typically references to external
functions are bound on an as-needed basis, and are not all bound when
the application starts up.  If a shared library is out of date, a
required interface may be missing; when the application tries to use
that interface, it may suddenly and unexpectedly fail.  With symbol
versioning, the user will get a warning when they start their program if
the libraries being used with the application are too old.

   <p>There are several GNU extensions to Sun's versioning approach.  The
first of these is the ability to bind a symbol to a version node in the
source file where the symbol is defined instead of in the versioning
script.  This was done mainly to reduce the burden on the library
maintainer.  You can do this by putting something like:
<pre class="smallexample">     __asm__(".symver original_foo,foo@VERS_1.1");
     </pre>

<p>in the C source file.  This renames the function <code>original_foo</code> to
be an alias for <code>foo</code> bound to the version node <code>VERS_1.1</code>. 
The <code>local:</code> directive can be used to prevent the symbol
<code>original_foo</code> from being exported. A <code>.symver</code> directive
takes precedence over a version script.

   <p>The second GNU extension is to allow multiple versions of the same
function to appear in a given shared library.  In this way you can make
an incompatible change to an interface without increasing the major
version number of the shared library, while still allowing applications
linked against the old interface to continue to function.

   <p>To do this, you must use multiple <code>.symver</code> directives in the
source file.  Here is an example:

<pre class="smallexample">     __asm__(".symver original_foo,foo@");
     __asm__(".symver old_foo,foo@VERS_1.1");
     __asm__(".symver old_foo1,foo@VERS_1.2");
     __asm__(".symver new_foo,foo@@VERS_2.0");
     </pre>

   <p>In this example, <code>foo@</code> represents the symbol <code>foo</code> bound to the
unspecified base version of the symbol.  The source file that contains this
example would define 4 C functions: <code>original_foo</code>, <code>old_foo</code>,
<code>old_foo1</code>, and <code>new_foo</code>.

   <p>When you have multiple definitions of a given symbol, there needs to be
some way to specify a default version to which external references to
this symbol will be bound.  You can do this with the
<code>foo@@VERS_2.0</code> type of <code>.symver</code> directive.  You can only
declare one version of a symbol as the default in this manner; otherwise
you would effectively have multiple definitions of the same symbol.

   <p>If you wish to bind a reference to a specific version of the symbol
within the shared library, you can use the aliases of convenience
(i.e., <code>old_foo</code>), or you can use the <code>.symver</code> directive to
specifically bind to an external version of the function in question.

   <p>You can also specify the language in the version script:

<pre class="smallexample">     VERSION extern "lang" { version-script-commands }
     </pre>

   <p>The supported <code>lang</code>s are <code>C</code>, <code>C++</code>, and <code>Java</code>. 
The linker will iterate over the list of symbols at the link time and
demangle them according to <code>lang</code> before matching them to the
patterns specified in <code>version-script-commands</code>.

   </body></html>

