<p>
Node:<a name="MIPS%20Opts">MIPS Opts</a>,
Next:<a rel=next href="MIPS-Object.html#MIPS%20Object">MIPS Object</a>,
Up:<a rel=up href="MIPS-Dependent.html#MIPS-Dependent">MIPS-Dependent</a>
<hr><br>

<h4>Assembler options</h3>

<p>The <small>MIPS</small> configurations of <small>GNU</small> <code>as</code> support these
special options:

<dl>
<dt><code>-G <var>num</var></code>
<dd>This option sets the largest size of an object that can be referenced
implicitly with the <code>gp</code> register.  It is only accepted for targets
that use <small>ECOFF</small> format.  The default value is 8.

<br><dt><code>-EB</code>
<dt><code>-EL</code>
<dd>Any <small>MIPS</small> configuration of <code>as</code> can select big-endian or
little-endian output at run time (unlike the other <small>GNU</small> development
tools, which must be configured for one or the other).  Use <code>-EB</code>
to select big-endian output, and <code>-EL</code> for little-endian.

<br><dt><code>-mips1</code>
<dt><code>-mips2</code>
<dt><code>-mips3</code>
<dt><code>-mips4</code>
<dt><code>-mips5</code>
<dt><code>-mips32</code>
<dt><code>-mips64</code>
<dd>Generate code for a particular MIPS Instruction Set Architecture level. 
<code>-mips1</code> corresponds to the <small>R2000</small> and <small>R3000</small> processors,
<code>-mips2</code> to the <small>R6000</small> processor, <code>-mips3</code> to the
<small>R4000</small> processor, and <code>-mips4</code> to the <small>R8000</small> and
<small>R10000</small> processors.  <code>-mips5</code>, <code>-mips32</code>, and
<code>-mips64</code> correspond to generic <small>MIPS V</small>, <small>MIPS32</small>, and
<small>MIPS64</small> ISA processors, respectively.  You can also switch
instruction sets during the assembly; see <a href="MIPS-ISA.html#MIPS%20ISA">Directives to override the ISA level</a>.

<br><dt><code>-mgp32</code>
<dt><code>-mfp32</code>
<dd>Some macros have different expansions for 32-bit and 64-bit registers. 
The register sizes are normally inferred from the ISA and ABI, but these
flags force a certain group of registers to be treated as 32 bits wide at
all times.  <code>-mgp32</code> controls the size of general-purpose registers
and <code>-mfp32</code> controls the size of floating-point registers.

<p>On some MIPS variants there is a 32-bit mode flag; when this flag is
set, 64-bit instructions generate a trap.  Also, some 32-bit OSes only
save the 32-bit registers on a context switch, so it is essential never
to use the 64-bit registers.

<br><dt><code>-mgp64</code>
<dd>Assume that 64-bit general purpose registers are available.  This is
provided in the interests of symmetry with -gp32.

<br><dt><code>-mips16</code>
<dt><code>-no-mips16</code>
<dd>Generate code for the MIPS 16 processor.  This is equivalent to putting
<code>.set mips16</code> at the start of the assembly file.  <code>-no-mips16</code>
turns off this option.

<br><dt><code>-mfix7000</code>
<dt><code>-no-mfix7000</code>
<dd>Cause nops to be inserted if the read of the destination register
of an mfhi or mflo instruction occurs in the following two instructions.

<br><dt><code>-m4010</code>
<dt><code>-no-m4010</code>
<dd>Generate code for the LSI <small>R4010</small> chip.  This tells the assembler to
accept the <small>R4010</small> specific instructions (<code>addciu</code>, <code>ffc</code>,
etc.), and to not schedule <code>nop</code> instructions around accesses to
the <code>HI</code> and <code>LO</code> registers.  <code>-no-m4010</code> turns off this
option.

<br><dt><code>-m4650</code>
<dt><code>-no-m4650</code>
<dd>Generate code for the MIPS <small>R4650</small> chip.  This tells the assembler to accept
the <code>mad</code> and <code>madu</code> instruction, and to not schedule <code>nop</code>
instructions around accesses to the <code>HI</code> and <code>LO</code> registers. 
<code>-no-m4650</code> turns off this option.

<dt><code>-m3900</code>
<dt><code>-no-m3900</code>
<dt><code>-m4100</code>
<dt><code>-no-m4100</code>
<dd>For each option <code>-m<var>nnnn</var></code>, generate code for the MIPS
<small>R<VAR>NNNN</VAR></small> chip.  This tells the assembler to accept instructions
specific to that chip, and to schedule for that chip's hazards.

<br><dt><code>-march=<var>cpu</var></code>
<dd>Generate code for a particular MIPS cpu.  It is exactly equivalent to
<code>-m<var>cpu</var></code>, except that there are more value of <var>cpu</var>
understood.  Valid <var>cpu</var> value are:

<blockquote>
2000,
3000,
3900,
4000,
4010,
4100,
4111,
4300,
4400,
4600,
4650,
5000,
rm5200,
rm5230,
rm5231,
rm5261,
rm5721,
6000,
rm7000,
8000,
10000,
12000,
mips32-4k,
sb1
</blockquote>

<br><dt><code>-mtune=<var>cpu</var></code>
<dd>Schedule and tune for a particular MIPS cpu.  Valid <var>cpu</var> values are
identical to <code>-march=<var>cpu</var></code>.

<br><dt><code>-mcpu=<var>cpu</var></code>
<dd>Generate code and schedule for a particular MIPS cpu.  This is exactly
equivalent to <code>-march=<var>cpu</var></code> and <code>-mtune=<var>cpu</var></code>.  Valid
<var>cpu</var> values are identical to <code>-march=<var>cpu</var></code>. 
Use of this option is discouraged.

<br><dt><code>-nocpp</code>
<dd>This option is ignored.  It is accepted for command-line compatibility with
other assemblers, which use it to turn off C style preprocessing.  With
<small>GNU</small> <code>as</code>, there is no need for <code>-nocpp</code>, because the
<small>GNU</small> assembler itself never runs the C preprocessor.

<br><dt><code>--construct-floats</code>
<dt><code>--no-construct-floats</code>
<dd>The <code>--no-construct-floats</code> option disables the construction of
double width floating point constants by loading the two halves of the
value into the two single width floating point registers that make up
the double width register.  This feature is useful if the processor
support the FR bit in its status  register, and this bit is known (by
the programmer) to be set.  This bit prevents the aliasing of the double
width register by the single width registers.

<p>By default <code>--construct-floats</code> is selected, allowing construction
of these floating point constants.

<br><dt><code>--trap</code>
<dt><code>--no-break</code>
<dd><code>as</code> automatically macro expands certain division and
multiplication instructions to check for overflow and division by zero.  This
option causes <code>as</code> to generate code to take a trap exception
rather than a break exception when an error is detected.  The trap instructions
are only supported at Instruction Set Architecture level 2 and higher.

<br><dt><code>--break</code>
<dt><code>--no-trap</code>
<dd>Generate code to take a break exception rather than a trap exception when an
error is detected.  This is the default.

<br><dt><code>-n</code>
<dd>When this option is used, <code>as</code> will issue a warning every
time it generates a nop instruction from a macro. 
</dl>

</body></html>

