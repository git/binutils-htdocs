<p>
Node:<a name="MMIX-Pseudos">MMIX-Pseudos</a>,
Previous:<a rel=previous href="MMIX-Regs.html#MMIX-Regs">MMIX-Regs</a>,
Up:<a rel=up href="MMIX-Syntax.html#MMIX-Syntax">MMIX-Syntax</a>
<hr><br>

<h5>Assembler Directives</h4>

<dl>
<dt><code>LOC</code>
<dd>

<p><a name="MMIX-loc"></a>
The <code>LOC</code> directive sets the current location to the value of the
operand field, which may include changing sections.  If the operand is a
constant, the section is set to either <code>.data</code> if the value is
<code>0x2000000000000000</code> or larger, else it is set to <code>.text</code>. 
Within a section, the current location may only be changed to
monotonically higher addresses.  A LOC expression must be a previously
defined symbol or a "pure" constant.

<p>An example, which sets the label <var>prev</var> to the current location, and
updates the current location to eight bytes forward:
<br><pre>prev LOC @+8
</pre>

<p>When a LOC has a constant as its operand, a symbol
<code>__.MMIX.start..text</code> or <code>__.MMIX.start..data</code> is defined
depending on the address as mentioned above.  Each such symbol is
interpreted as special by the linker, locating the section at that
address.  Note that if multiple files are linked, the first object file
with that section will be mapped to that address (not necessarily the file
with the LOC definition).

<br><dt><code>LOCAL</code>
<dd>

<p><a name="MMIX-local"></a>
Example:
<br><pre> LOCAL external_symbol
 LOCAL 42
 .local asymbol
</pre>

<p>This directive-operation generates a link-time assertion that the operand
does not correspond to a global register.  The operand is an expression
that at link-time resolves to a register symbol or a number.  A number is
treated as the register having that number.  There is one restriction on
the use of this directive: the pseudo-directive must be placed in a
section with contents, code or data.

<br><dt><code>IS</code>
<dd>

<p><a name="MMIX-is"></a>
The <code>IS</code> directive:
<br><pre>asymbol IS an_expression
</pre>
sets the symbol <code>asymbol</code> to <code>an_expression</code>.  A symbol may not
be set more than once using this directive.  Local labels may be set using
this directive, for example:
<br><pre>5H IS @+4
</pre>

<br><dt><code>GREG</code>
<dd>

<p><a name="MMIX-greg"></a>
This directive reserves a global register, gives it an initial value and
optionally gives it a symbolic name.  Some examples:

<br><pre>areg GREG
breg GREG data_value
     GREG data_buffer
     .greg creg, another_data_value
</pre>

<p>The symbolic register name can be used in place of a (non-special)
register.  If a value isn't provided, it defaults to zero.  Unless the
option <code>--no-merge-gregs</code> is specified, non-zero registers allocated
with this directive may be eliminated by <code>as</code>; another
register with the same value used in its place. 
Any of the instructions
<code>CSWAP</code>,
<code>GO</code>,
<code>LDA</code>,
<code>LDBU</code>,
<code>LDB</code>,
<code>LDHT</code>,
<code>LDOU</code>,
<code>LDO</code>,
<code>LDSF</code>,
<code>LDTU</code>,
<code>LDT</code>,
<code>LDUNC</code>,
<code>LDVTS</code>,
<code>LDWU</code>,
<code>LDW</code>,
<code>PREGO</code>,
<code>PRELD</code>,
<code>PREST</code>,
<code>PUSHGO</code>,
<code>STBU</code>,
<code>STB</code>,
<code>STCO</code>,
<code>STHT</code>,
<code>STOU</code>,
<code>STSF</code>,
<code>STTU</code>,
<code>STT</code>,
<code>STUNC</code>,
<code>SYNCD</code>,
<code>SYNCID</code>,
can have a value nearby <a name="GREG-base"></a>an initial value in place of its
second and third operands.  Here, "nearby" is defined as within the
range 0<small>...</small>255 from the initial value of such an allocated register.

<br><pre>buffer1 BYTE 0,0,0,0,0
buffer2 BYTE 0,0,0,0,0
 <small>...</small>
 GREG buffer1
 LDOU $42,buffer2
</pre>
In the example above, the <code>Y</code> field of the <code>LDOUI</code> instruction
(LDOU with a constant Z) will be replaced with the global register
allocated for <code>buffer1</code>, and the <code>Z</code> field will have the value
5, the offset from <code>buffer1</code> to <code>buffer2</code>.  The result is
equivalent to this code:
<br><pre>buffer1 BYTE 0,0,0,0,0
buffer2 BYTE 0,0,0,0,0
 <small>...</small>
tmpreg GREG buffer1
 LDOU $42,tmpreg,(buffer2-buffer1)
</pre>

<p>Global registers allocated with this directive are allocated in order
higher-to-lower within a file.  Other than that, the exact order of
register allocation and elimination is undefined.  For example, the order
is undefined when more than one file with such directives are linked
together.  With the options <code>-x</code> and <code>--linker-allocated-gregs</code>,
<code>GREG</code> directives for two-operand cases like the one mentioned above
can be omitted.  Sufficient global registers will then be allocated by the
linker.

<br><dt><code>BYTE</code>
<dd>

<p><a name="MMIX-byte"></a>
The <code>BYTE</code> directive takes a series of operands separated by a comma. 
If an operand is a string (see <a href="Strings.html#Strings">Strings</a>), each character of that string
is emitted as a byte.  Other operands must be constant expressions without
forward references, in the range 0<small>...</small>255.  If you need operands having
expressions with forward references, use <code>.byte</code> (see <a href="Byte.html#Byte">Byte</a>).  An
operand can be omitted, defaulting to a zero value.

<br><dt><code>WYDE</code>
<dt><code>TETRA</code>
<dt><code>OCTA</code>
<dd>

<p><a name="MMIX-constants"></a>
The directives <code>WYDE</code>, <code>TETRA</code> and <code>OCTA</code> emit constants of
two, four and eight bytes size respectively.  Before anything else happens
for the directive, the current location is aligned to the respective
constant-size bondary.  If a label is defined at the beginning of the
line, its value will be that after the alignment.  A single operand can be
omitted, defaulting to a zero value emitted for the directive.  Operands
can be expressed as strings (see <a href="Strings.html#Strings">Strings</a>), in which case each
character in the string is emitted as a separate constant of the size
indicated by the directive.

<br><dt><code>PREFIX</code>
<dd>

<p><a name="MMIX-prefix"></a>
The <code>PREFIX</code> directive sets a symbol name prefix to be prepended to
all symbols (except local symbols, see <a href="MMIX-Symbols.html#MMIX-Symbols">MMIX-Symbols</a>), that are not
prefixed with <code>:</code>, until the next <code>PREFIX</code> directive.  Such
prefixes accumulate.  For example,
<br><pre> PREFIX a
 PREFIX b
c IS 0
</pre>
defines a symbol <code>abc</code> with the value 0.

<br><dt><code>BSPEC</code>
<dt><code>ESPEC</code>
<dd>

<p><a name="MMIX-spec"></a>
A pair of <code>BSPEC</code> and <code>ESPEC</code> directives delimit a section of
special contents (without specified semantics).  Example:
<br><pre> BSPEC 42
 TETRA 1,2,3
 ESPEC
</pre>
The single operand to <code>BSPEC</code> must be number in the range
0<small>...</small>255.  The <code>BSPEC</code> number 80 is used by the GNU binutils
implementation. 
</dl>

</body></html>

