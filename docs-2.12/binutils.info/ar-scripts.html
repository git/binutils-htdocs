<p>
Node:<a name="ar%20scripts">ar scripts</a>,
Previous:<a rel=previous href="ar-cmdline.html#ar%20cmdline">ar cmdline</a>,
Up:<a rel=up href="ar.html#ar">ar</a>
<hr><br>

<h3>Controlling <code>ar</code> with a script</h2>

<br><pre>ar -M [ &lt;<var>script</var> ]
</pre>

<p>If you use the single command-line option <code>-M</code> with <code>ar</code>, you
can control its operation with a rudimentary command language.  This
form of <code>ar</code> operates interactively if standard input is coming
directly from a terminal.  During interactive use, <code>ar</code> prompts for
input (the prompt is <code>AR &gt;</code>), and continues executing even after
errors.  If you redirect standard input to a script file, no prompts are
issued, and <code>ar</code> abandons execution (with a nonzero exit code)
on any error.

<p>The <code>ar</code> command language is <em>not</em> designed to be equivalent
to the command-line options; in fact, it provides somewhat less control
over archives.  The only purpose of the command language is to ease the
transition to <small>GNU</small> <code>ar</code> for developers who already have scripts
written for the MRI "librarian" program.

<p>The syntax for the <code>ar</code> command language is straightforward:
<ul>
<li>commands are recognized in upper or lower case; for example, <code>LIST</code>
is the same as <code>list</code>.  In the following descriptions, commands are
shown in upper case for clarity.

<li>a single command may appear on each line; it is the first word on the
line.

<li>empty lines are allowed, and have no effect.

<li>comments are allowed; text after either of the characters <code>*</code>
or <code>;</code> is ignored.

<li>Whenever you use a list of names as part of the argument to an <code>ar</code>
command, you can separate the individual names with either commas or
blanks.  Commas are shown in the explanations below, for clarity.

<li><code>+</code> is used as a line continuation character; if <code>+</code> appears
at the end of a line, the text on the following line is considered part
of the current command. 
</ul>

<p>Here are the commands you can use in <code>ar</code> scripts, or when using
<code>ar</code> interactively.  Three of them have special significance:

<p><code>OPEN</code> or <code>CREATE</code> specify a <dfn>current archive</dfn>, which is
a temporary file required for most of the other commands.

<p><code>SAVE</code> commits the changes so far specified by the script.  Prior
to <code>SAVE</code>, commands affect only the temporary copy of the current
archive.

<dl>
<dt><code>ADDLIB <var>archive</var></code>
<dt><code>ADDLIB <var>archive</var> (<var>module</var>, <var>module</var>, <small>...</small> <var>module</var>)</code>
<dd>Add all the contents of <var>archive</var> (or, if specified, each named
<var>module</var> from <var>archive</var>) to the current archive.

<p>Requires prior use of <code>OPEN</code> or <code>CREATE</code>.

<br><dt><code>ADDMOD <var>member</var>, <var>member</var>, <small>...</small> <var>member</var></code>
<dd>Add each named <var>member</var> as a module in the current archive.

<p>Requires prior use of <code>OPEN</code> or <code>CREATE</code>.

<br><dt><code>CLEAR</code>
<dd>Discard the contents of the current archive, canceling the effect of
any operations since the last <code>SAVE</code>.  May be executed (with no
effect) even if  no current archive is specified.

<br><dt><code>CREATE <var>archive</var></code>
<dd>Creates an archive, and makes it the current archive (required for many
other commands).  The new archive is created with a temporary name; it
is not actually saved as <var>archive</var> until you use <code>SAVE</code>. 
You can overwrite existing archives; similarly, the contents of any
existing file named <var>archive</var> will not be destroyed until <code>SAVE</code>.

<br><dt><code>DELETE <var>module</var>, <var>module</var>, <small>...</small> <var>module</var></code>
<dd>Delete each listed <var>module</var> from the current archive; equivalent to
<code>ar -d <var>archive</var> <var>module</var> <small>...</small> <var>module</var></code>.

<p>Requires prior use of <code>OPEN</code> or <code>CREATE</code>.

<br><dt><code>DIRECTORY <var>archive</var> (<var>module</var>, <small>...</small> <var>module</var>)</code>
<dt><code>DIRECTORY <var>archive</var> (<var>module</var>, <small>...</small> <var>module</var>) <var>outputfile</var></code>
<dd>List each named <var>module</var> present in <var>archive</var>.  The separate
command <code>VERBOSE</code> specifies the form of the output: when verbose
output is off, output is like that of <code>ar -t <var>archive</var>
<var>module</var><small>...</small></code>.  When verbose output is on, the listing is like
<code>ar -tv <var>archive</var> <var>module</var><small>...</small></code>.

<p>Output normally goes to the standard output stream; however, if you
specify <var>outputfile</var> as a final argument, <code>ar</code> directs the
output to that file.

<br><dt><code>END</code>
<dd>Exit from <code>ar</code>, with a <code>0</code> exit code to indicate successful
completion.  This command does not save the output file; if you have
changed the current archive since the last <code>SAVE</code> command, those
changes are lost.

<br><dt><code>EXTRACT <var>module</var>, <var>module</var>, <small>...</small> <var>module</var></code>
<dd>Extract each named <var>module</var> from the current archive, writing them
into the current directory as separate files.  Equivalent to <code>ar -x
<var>archive</var> <var>module</var><small>...</small></code>.

<p>Requires prior use of <code>OPEN</code> or <code>CREATE</code>.

<br><dt><code>LIST</code>
<dd>Display full contents of the current archive, in "verbose" style
regardless of the state of <code>VERBOSE</code>.  The effect is like <code>ar
tv <var>archive</var></code>.  (This single command is a <small>GNU</small> <code>ar</code>
enhancement, rather than present for MRI compatibility.)

<p>Requires prior use of <code>OPEN</code> or <code>CREATE</code>.

<br><dt><code>OPEN <var>archive</var></code>
<dd>Opens an existing archive for use as the current archive (required for
many other commands).  Any changes as the result of subsequent commands
will not actually affect <var>archive</var> until you next use <code>SAVE</code>.

<br><dt><code>REPLACE <var>module</var>, <var>module</var>, <small>...</small> <var>module</var></code>
<dd>In the current archive, replace each existing <var>module</var> (named in
the <code>REPLACE</code> arguments) from files in the current working directory. 
To execute this command without errors, both the file, and the module in
the current archive, must exist.

<p>Requires prior use of <code>OPEN</code> or <code>CREATE</code>.

<br><dt><code>VERBOSE</code>
<dd>Toggle an internal flag governing the output from <code>DIRECTORY</code>. 
When the flag is on, <code>DIRECTORY</code> output matches output from
<code>ar -tv </code><small>...</small>.

<br><dt><code>SAVE</code>
<dd>Commit your changes to the current archive, and actually save it as a
file with the name specified in the last <code>CREATE</code> or <code>OPEN</code>
command.

<p>Requires prior use of <code>OPEN</code> or <code>CREATE</code>.

</dl>

</body></html>

