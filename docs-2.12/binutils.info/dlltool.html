<p>
Node:<a name="dlltool">dlltool</a>,
Next:<a rel=next href="Selecting-The-Target-System.html#Selecting%20The%20Target%20System">Selecting The Target System</a>,
Previous:<a rel=previous href="windres.html#windres">windres</a>,
Up:<a rel=up href="index.html#Top">Top</a>
<hr><br>

<h2>Create files needed to build and use DLLs</h1>

<p><code>dlltool</code> may be used to create the files needed to build and use
dynamic link libraries (DLLs).

<blockquote>
<em>Warning:</em> <code>dlltool</code> is not always built as part of the binary
utilities, since it is only useful for those targets which support DLLs. 
</blockquote>

<br><pre>dlltool [<code>-d</code>|<code>--input-def</code> <var>def-file-name</var>]
        [<code>-b</code>|<code>--base-file</code> <var>base-file-name</var>]
        [<code>-e</code>|<code>--output-exp</code> <var>exports-file-name</var>]
        [<code>-z</code>|<code>--output-def</code> <var>def-file-name</var>]
        [<code>-l</code>|<code>--output-lib</code> <var>library-file-name</var>]
        [<code>--export-all-symbols</code>] [<code>--no-export-all-symbols</code>]
        [<code>--exclude-symbols</code> <var>list</var>]
        [<code>--no-default-excludes</code>]
        [<code>-S</code>|<code>--as</code> <var>path-to-assembler</var>] [<code>-f</code>|<code>--as-flags</code> <var>options</var>]
        [<code>-D</code>|<code>--dllname</code> <var>name</var>] [<code>-m</code>|<code>--machine</code> <var>machine</var>]
        [<code>-a</code>|<code>--add-indirect</code>] [<code>-U</code>|<code>--add-underscore</code>] [<code>-k</code>|<code>--kill-at</code>]
        [<code>-A</code>|<code>--add-stdcall-alias</code>]
        [<code>-x</code>|<code>--no-idata4</code>] [<code>-c</code>|<code>--no-idata5</code>] [<code>-i</code>|<code>--interwork</code>]
        [<code>-n</code>|<code>--nodelete</code>] [<code>-v</code>|<code>--verbose</code>]
        [<code>-h</code>|<code>--help</code>] [<code>-V</code>|<code>--version</code>]
        [object-file <small>...</small>]
</pre>

<p><code>dlltool</code> reads its inputs, which can come from the <code>-d</code> and
<code>-b</code> options as well as object files specified on the command
line.  It then processes these inputs and if the <code>-e</code> option has
been specified it creates a exports file.  If the <code>-l</code> option
has been specified it creates a library file and if the <code>-z</code> option
has been specified it creates a def file.  Any or all of the <code>-e</code>,
<code>-l</code> and <code>-z</code> options can be present in one invocation of
dlltool.

<p>When creating a DLL, along with the source for the DLL, it is necessary
to have three other files.  <code>dlltool</code> can help with the creation of
these files.

<p>The first file is a <code>.def</code> file which specifies which functions are
exported from the DLL, which functions the DLL imports, and so on.  This
is a text file and can be created by hand, or <code>dlltool</code> can be used
to create it using the <code>-z</code> option.  In this case <code>dlltool</code>
will scan the object files specified on its command line looking for
those functions which have been specially marked as being exported and
put entries for them in the .def file it creates.

<p>In order to mark a function as being exported from a DLL, it needs to
have an <code>-export:&lt;name_of_function&gt;</code> entry in the <code>.drectve</code>
section of the object file.  This can be done in C by using the
asm() operator:

<br><pre>  asm (".section .drectve");
  asm (".ascii \"-export:my_func\"");

  int my_func (void) { <small>...</small> }
</pre>

<p>The second file needed for DLL creation is an exports file.  This file
is linked with the object files that make up the body of the DLL and it
handles the interface between the DLL and the outside world.  This is a
binary file and it can be created by giving the <code>-e</code> option to
<code>dlltool</code> when it is creating or reading in a .def file.

<p>The third file needed for DLL creation is the library file that programs
will link with in order to access the functions in the DLL.  This file
can be created by giving the <code>-l</code> option to dlltool when it
is creating or reading in a .def file.

<p><code>dlltool</code> builds the library file by hand, but it builds the
exports file by creating temporary files containing assembler statements
and then assembling these.  The <code>-S</code> command line option can be
used to specify the path to the assembler that dlltool will use,
and the <code>-f</code> option can be used to pass specific flags to that
assembler.  The <code>-n</code> can be used to prevent dlltool from deleting
these temporary assembler files when it is done, and if <code>-n</code> is
specified twice then this will prevent dlltool from deleting the
temporary object files it used to build the library.

<p>Here is an example of creating a DLL from a source file <code>dll.c</code> and
also creating a program (from an object file called <code>program.o</code>)
that uses that DLL:

<br><pre>  gcc -c dll.c
  dlltool -e exports.o -l dll.lib dll.o
  gcc dll.o exports.o -o dll.dll
  gcc program.o dll.lib -o program
</pre>

<p>The command line options have the following meanings:

<dl>

<br><dt><code>-d <var>filename</var></code>
<dt><code>--input-def <var>filename</var></code>
<dd>Specifies the name of a .def file to be read in and processed.

<br><dt><code>-b <var>filename</var></code>
<dt><code>--base-file <var>filename</var></code>
<dd>Specifies the name of a base file to be read in and processed.  The
contents of this file will be added to the relocation section in the
exports file generated by dlltool.

<br><dt><code>-e <var>filename</var></code>
<dt><code>--output-exp <var>filename</var></code>
<dd>Specifies the name of the export file to be created by dlltool.

<br><dt><code>-z <var>filename</var></code>
<dt><code>--output-def <var>filename</var></code>
<dd>Specifies the name of the .def file to be created by dlltool.

<br><dt><code>-l <var>filename</var></code>
<dt><code>--output-lib <var>filename</var></code>
<dd>Specifies the name of the library file to be created by dlltool.

<br><dt><code>--export-all-symbols</code>
<dd>Treat all global and weak defined symbols found in the input object
files as symbols to be exported.  There is a small list of symbols which
are not exported by default; see the <code>--no-default-excludes</code>
option.  You may add to the list of symbols to not export by using the
<code>--exclude-symbols</code> option.

<br><dt><code>--no-export-all-symbols</code>
<dd>Only export symbols explicitly listed in an input .def file or in
<code>.drectve</code> sections in the input object files.  This is the default
behaviour.  The <code>.drectve</code> sections are created by <code>dllexport</code>
attributes in the source code.

<br><dt><code>--exclude-symbols <var>list</var></code>
<dd>Do not export the symbols in <var>list</var>.  This is a list of symbol names
separated by comma or colon characters.  The symbol names should not
contain a leading underscore.  This is only meaningful when
<code>--export-all-symbols</code> is used.

<br><dt><code>--no-default-excludes</code>
<dd>When <code>--export-all-symbols</code> is used, it will by default avoid
exporting certain special symbols.  The current list of symbols to avoid
exporting is <code>DllMain@12</code>, <code>DllEntryPoint@0</code>,
<code>impure_ptr</code>.  You may use the <code>--no-default-excludes</code> option
to go ahead and export these special symbols.  This is only meaningful
when <code>--export-all-symbols</code> is used.

<br><dt><code>-S <var>path</var></code>
<dt><code>--as <var>path</var></code>
<dd>Specifies the path, including the filename, of the assembler to be used
to create the exports file.

<br><dt><code>-f <var>options</var></code>
<dt><code>--as-flags <var>options</var></code>
<dd>Specifies any specific command line options to be passed to the
assembler when building the exports file.  This option will work even if
the <code>-S</code> option is not used.  This option only takes one argument,
and if it occurs more than once on the command line, then later
occurrences will override earlier occurrences.  So if it is necessary to
pass multiple options to the assembler they should be enclosed in
double quotes.

<br><dt><code>-D <var>name</var></code>
<dt><code>--dll-name <var>name</var></code>
<dd>Specifies the name to be stored in the .def file as the name of the DLL
when the <code>-e</code> option is used.  If this option is not present, then
the filename given to the <code>-e</code> option will be used as the name of
the DLL.

<br><dt><code>-m <var>machine</var></code>
<dt><code>-machine <var>machine</var></code>
<dd>Specifies the type of machine for which the library file should be
built.  <code>dlltool</code> has a built in default type, depending upon how
it was created, but this option can be used to override that.  This is
normally only useful when creating DLLs for an ARM processor, when the
contents of the DLL are actually encode using Thumb instructions.

<br><dt><code>-a</code>
<dt><code>--add-indirect</code>
<dd>Specifies that when <code>dlltool</code> is creating the exports file it
should add a section which allows the exported functions to be
referenced without using the import library.  Whatever the hell that
means!

<br><dt><code>-U</code>
<dt><code>--add-underscore</code>
<dd>Specifies that when <code>dlltool</code> is creating the exports file it
should prepend an underscore to the names of the exported functions.

<br><dt><code>-k</code>
<dt><code>--kill-at</code>
<dd>Specifies that when <code>dlltool</code> is creating the exports file it
should not append the string <code>@ &lt;number&gt;</code>.  These numbers are
called ordinal numbers and they represent another way of accessing the
function in a DLL, other than by name.

<br><dt><code>-A</code>
<dt><code>--add-stdcall-alias</code>
<dd>Specifies that when <code>dlltool</code> is creating the exports file it
should add aliases for stdcall symbols without <code>@ &lt;number&gt;</code>
in addition to the symbols with <code>@ &lt;number&gt;</code>.

<br><dt><code>-x</code>
<dt><code>--no-idata4</code>
<dd>Specifies that when <code>dlltool</code> is creating the exports and library
files it should omit the <code>.idata4</code> section.  This is for compatibility
with certain operating systems.

<br><dt><code>-c</code>
<dt><code>--no-idata5</code>
<dd>Specifies that when <code>dlltool</code> is creating the exports and library
files it should omit the <code>.idata5</code> section.  This is for compatibility
with certain operating systems.

<br><dt><code>-i</code>
<dt><code>--interwork</code>
<dd>Specifies that <code>dlltool</code> should mark the objects in the library
file and exports file that it produces as supporting interworking
between ARM and Thumb code.

<br><dt><code>-n</code>
<dt><code>--nodelete</code>
<dd>Makes <code>dlltool</code> preserve the temporary assembler files it used to
create the exports file.  If this option is repeated then dlltool will
also preserve the temporary object files it uses to create the library
file.

<br><dt><code>-v</code>
<dt><code>--verbose</code>
<dd>Make dlltool describe what it is doing.

<br><dt><code>-h</code>
<dt><code>--help</code>
<dd>Displays a list of command line options and then exits.

<br><dt><code>-V</code>
<dt><code>--version</code>
<dd>Displays dlltool's version number and then exits.

</dl>

</body></html>

