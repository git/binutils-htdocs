<p>
Node:<a name="ar%20cmdline">ar cmdline</a>,
Next:<a rel=next href="ar-scripts.html#ar%20scripts">ar scripts</a>,
Up:<a rel=up href="ar.html#ar">ar</a>
<hr><br>

<h3>Controlling <code>ar</code> on the command line</h2>

<br><pre>ar [<code>-X32_64</code>] [<code>-</code>]<var>p</var>[<var>mod</var> [<var>relpos</var>] [<var>count</var>]] <var>archive</var> [<var>member</var><small>...</small>]
</pre>

<p>When you use <code>ar</code> in the Unix style, <code>ar</code> insists on at least two
arguments to execute: one keyletter specifying the <em>operation</em>
(optionally accompanied by other keyletters specifying
<em>modifiers</em>), and the archive name to act on.

<p>Most operations can also accept further <var>member</var> arguments,
specifying particular files to operate on.

<p><small>GNU</small> <code>ar</code> allows you to mix the operation code <var>p</var> and modifier
flags <var>mod</var> in any order, within the first command-line argument.

<p>If you wish, you may begin the first command-line argument with a
dash.

<p>The <var>p</var> keyletter specifies what operation to execute; it may be
any of the following, but you must specify only one of them:

<dl>
<dt><code>d</code>
<dd><em>Delete</em> modules from the archive.  Specify the names of modules to
be deleted as <var>member</var><small>...</small>; the archive is untouched if you
specify no files to delete.

<p>If you specify the <code>v</code> modifier, <code>ar</code> lists each module
as it is deleted.

<br><dt><code>m</code>
<dd>Use this operation to <em>move</em> members in an archive.

<p>The ordering of members in an archive can make a difference in how
programs are linked using the library, if a symbol is defined in more
than one member.

<p>If no modifiers are used with <code>m</code>, any members you name in the
<var>member</var> arguments are moved to the <em>end</em> of the archive;
you can use the <code>a</code>, <code>b</code>, or <code>i</code> modifiers to move them to a
specified place instead.

<br><dt><code>p</code>
<dd><em>Print</em> the specified members of the archive, to the standard
output file.  If the <code>v</code> modifier is specified, show the member
name before copying its contents to standard output.

<p>If you specify no <var>member</var> arguments, all the files in the archive are
printed.

<br><dt><code>q</code>
<dd><em>Quick append</em>; Historically, add the files <var>member</var><small>...</small> to the end of
<var>archive</var>, without checking for replacement.

<p>The modifiers <code>a</code>, <code>b</code>, and <code>i</code> do <em>not</em> affect this
operation; new members are always placed at the end of the archive.

<p>The modifier <code>v</code> makes <code>ar</code> list each file as it is appended.

<p>Since the point of this operation is speed, the archive's symbol table
index is not updated, even if it already existed; you can use <code>ar s</code> or
<code>ranlib</code> explicitly to update the symbol table index.

<p>However, too many different systems assume quick append rebuilds the
index, so GNU ar implements <code>q</code> as a synonym for <code>r</code>.

<br><dt><code>r</code>
<dd>Insert the files <var>member</var><small>...</small> into <var>archive</var> (with
<em>replacement</em>). This operation differs from <code>q</code> in that any
previously existing members are deleted if their names match those being
added.

<p>If one of the files named in <var>member</var><small>...</small> does not exist, <code>ar</code>
displays an error message, and leaves undisturbed any existing members
of the archive matching that name.

<p>By default, new members are added at the end of the file; but you may
use one of the modifiers <code>a</code>, <code>b</code>, or <code>i</code> to request
placement relative to some existing member.

<p>The modifier <code>v</code> used with this operation elicits a line of
output for each file inserted, along with one of the letters <code>a</code> or
<code>r</code> to indicate whether the file was appended (no old member
deleted) or replaced.

<br><dt><code>t</code>
<dd>Display a <em>table</em> listing the contents of <var>archive</var>, or those
of the files listed in <var>member</var><small>...</small> that are present in the
archive.  Normally only the member name is shown; if you also want to
see the modes (permissions), timestamp, owner, group, and size, you can
request that by also specifying the <code>v</code> modifier.

<p>If you do not specify a <var>member</var>, all files in the archive
are listed.

<p>If there is more than one file with the same name (say, <code>fie</code>) in
an archive (say <code>b.a</code>), <code>ar t b.a fie</code> lists only the
first instance; to see them all, you must ask for a complete
listing--in our example, <code>ar t b.a</code>.

<br><dt><code>x</code>
<dd><em>Extract</em> members (named <var>member</var>) from the archive.  You can
use the <code>v</code> modifier with this operation, to request that
<code>ar</code> list each name as it extracts it.

<p>If you do not specify a <var>member</var>, all files in the archive
are extracted.

</dl>

<p>A number of modifiers (<var>mod</var>) may immediately follow the <var>p</var>
keyletter, to specify variations on an operation's behavior:

<dl>
<dt><code>a</code>
<dd>Add new files <em>after</em> an existing member of the
archive.  If you use the modifier <code>a</code>, the name of an existing archive
member must be present as the <var>relpos</var> argument, before the
<var>archive</var> specification.

<br><dt><code>b</code>
<dd>Add new files <em>before</em> an existing member of the
archive.  If you use the modifier <code>b</code>, the name of an existing archive
member must be present as the <var>relpos</var> argument, before the
<var>archive</var> specification.  (same as <code>i</code>).

<br><dt><code>c</code>
<dd><em>Create</em> the archive.  The specified <var>archive</var> is always
created if it did not exist, when you request an update.  But a warning is
issued unless you specify in advance that you expect to create it, by
using this modifier.

<br><dt><code>f</code>
<dd>Truncate names in the archive.  <small>GNU</small> <code>ar</code> will normally permit file
names of any length.  This will cause it to create archives which are
not compatible with the native <code>ar</code> program on some systems.  If
this is a concern, the <code>f</code> modifier may be used to truncate file
names when putting them in the archive.

<br><dt><code>i</code>
<dd>Insert new files <em>before</em> an existing member of the
archive.  If you use the modifier <code>i</code>, the name of an existing archive
member must be present as the <var>relpos</var> argument, before the
<var>archive</var> specification.  (same as <code>b</code>).

<br><dt><code>l</code>
<dd>This modifier is accepted but not used.

<br><dt><code>N</code>
<dd>Uses the <var>count</var> parameter.  This is used if there are multiple
entries in the archive with the same name.  Extract or delete instance
<var>count</var> of the given name from the archive.

<br><dt><code>o</code>
<dd>Preserve the <em>original</em> dates of members when extracting them.  If
you do not specify this modifier, files extracted from the archive
are stamped with the time of extraction.

<br><dt><code>P</code>
<dd>Use the full path name when matching names in the archive.  <small>GNU</small>
<code>ar</code> can not create an archive with a full path name (such archives
are not POSIX complaint), but other archive creators can.  This option
will cause <small>GNU</small> <code>ar</code> to match file names using a complete path
name, which can be convenient when extracting a single file from an
archive created by another tool.

<br><dt><code>s</code>
<dd>Write an object-file index into the archive, or update an existing one,
even if no other change is made to the archive.  You may use this modifier
flag either with any operation, or alone.  Running <code>ar s</code> on an
archive is equivalent to running <code>ranlib</code> on it.

<br><dt><code>S</code>
<dd>Do not generate an archive symbol table.  This can speed up building a
large library in several steps.  The resulting archive can not be used
with the linker.  In order to build a symbol table, you must omit the
<code>S</code> modifier on the last execution of <code>ar</code>, or you must run
<code>ranlib</code> on the archive.

<br><dt><code>u</code>
<dd>Normally, <code>ar r</code><small>...</small> inserts all files
listed into the archive.  If you would like to insert <em>only</em> those
of the files you list that are newer than existing members of the same
names, use this modifier.  The <code>u</code> modifier is allowed only for the
operation <code>r</code> (replace).  In particular, the combination <code>qu</code> is
not allowed, since checking the timestamps would lose any speed
advantage from the operation <code>q</code>.

<br><dt><code>v</code>
<dd>This modifier requests the <em>verbose</em> version of an operation.  Many
operations display additional information, such as filenames processed,
when the modifier <code>v</code> is appended.

<br><dt><code>V</code>
<dd>This modifier shows the version number of <code>ar</code>. 
</dl>

<p><code>ar</code> ignores an initial option spelt <code>-X32_64</code>, for
compatibility with AIX.  The behaviour produced by this option is the
default for GNU <code>ar</code>.  <code>ar</code> does not support any of the other
<code>-X</code> options; in particular, it does not support <code>-X32</code>
which is the default for AIX <code>ar</code>.

</body></html>

