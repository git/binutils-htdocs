<p>
Node:<a name="Output%20Options">Output Options</a>,
Next:<a rel=next href="Analysis-Options.html#Analysis%20Options">Analysis Options</a>,
Up:<a rel=up href="Invoking.html#Invoking">Invoking</a>
<hr><br>

<h3>Output Options</h2>

<p>These options specify which of several output formats
<code>gprof</code> should produce.

<p>Many of these options take an optional <dfn>symspec</dfn> to specify
functions to be included or excluded.  These options can be
specified multiple times, with different symspecs, to include
or exclude sets of symbols.  See <a href="Symspecs.html#Symspecs">Symspecs</a>.

<p>Specifying any of these options overrides the default (<code>-p -q</code>),
which prints a flat profile and call graph analysis
for all functions.

<dl>

<br><dt><code>-A[<var>symspec</var>]</code>
<dt><code>--annotated-source[=<var>symspec</var>]</code>
<dd>The <code>-A</code> option causes <code>gprof</code> to print annotated source code. 
If <var>symspec</var> is specified, print output only for matching symbols. 
See <a href="Annotated-Source.html#Annotated%20Source">Annotated Source</a>.

<br><dt><code>-b</code>
<dt><code>--brief</code>
<dd>If the <code>-b</code> option is given, <code>gprof</code> doesn't print the
verbose blurbs that try to explain the meaning of all of the fields in
the tables.  This is useful if you intend to print out the output, or
are tired of seeing the blurbs.

<br><dt><code>-C[<var>symspec</var>]</code>
<dt><code>--exec-counts[=<var>symspec</var>]</code>
<dd>The <code>-C</code> option causes <code>gprof</code> to
print a tally of functions and the number of times each was called. 
If <var>symspec</var> is specified, print tally only for matching symbols.

<p>If the profile data file contains basic-block count records, specifying
the <code>-l</code> option, along with <code>-C</code>, will cause basic-block
execution counts to be tallied and displayed.

<br><dt><code>-i</code>
<dt><code>--file-info</code>
<dd>The <code>-i</code> option causes <code>gprof</code> to display summary information
about the profile data file(s) and then exit.  The number of histogram,
call graph, and basic-block count records is displayed.

<br><dt><code>-I <var>dirs</var></code>
<dt><code>--directory-path=<var>dirs</var></code>
<dd>The <code>-I</code> option specifies a list of search directories in
which to find source files.  Environment variable <var>GPROF_PATH</var>
can also be used to convey this information. 
Used mostly for annotated source output.

<br><dt><code>-J[<var>symspec</var>]</code>
<dt><code>--no-annotated-source[=<var>symspec</var>]</code>
<dd>The <code>-J</code> option causes <code>gprof</code> not to
print annotated source code. 
If <var>symspec</var> is specified, <code>gprof</code> prints annotated source,
but excludes matching symbols.

<br><dt><code>-L</code>
<dt><code>--print-path</code>
<dd>Normally, source filenames are printed with the path
component suppressed.  The <code>-L</code> option causes <code>gprof</code>
to print the full pathname of
source filenames, which is determined
from symbolic debugging information in the image file
and is relative to the directory in which the compiler
was invoked.

<br><dt><code>-p[<var>symspec</var>]</code>
<dt><code>--flat-profile[=<var>symspec</var>]</code>
<dd>The <code>-p</code> option causes <code>gprof</code> to print a flat profile. 
If <var>symspec</var> is specified, print flat profile only for matching symbols. 
See <a href="Flat-Profile.html#Flat%20Profile">Flat Profile</a>.

<br><dt><code>-P[<var>symspec</var>]</code>
<dt><code>--no-flat-profile[=<var>symspec</var>]</code>
<dd>The <code>-P</code> option causes <code>gprof</code> to suppress printing a flat profile. 
If <var>symspec</var> is specified, <code>gprof</code> prints a flat profile,
but excludes matching symbols.

<br><dt><code>-q[<var>symspec</var>]</code>
<dt><code>--graph[=<var>symspec</var>]</code>
<dd>The <code>-q</code> option causes <code>gprof</code> to print the call graph analysis. 
If <var>symspec</var> is specified, print call graph only for matching symbols
and their children. 
See <a href="Call-Graph.html#Call%20Graph">Call Graph</a>.

<br><dt><code>-Q[<var>symspec</var>]</code>
<dt><code>--no-graph[=<var>symspec</var>]</code>
<dd>The <code>-Q</code> option causes <code>gprof</code> to suppress printing the
call graph. 
If <var>symspec</var> is specified, <code>gprof</code> prints a call graph,
but excludes matching symbols.

<br><dt><code>-y</code>
<dt><code>--separate-files</code>
<dd>This option affects annotated source output only. 
Normally, <code>gprof</code> prints annotated source files
to standard-output.  If this option is specified,
annotated source for a file named <code>path/<var>filename</var></code>
is generated in the file <code><var>filename</var>-ann</code>.  If the underlying
filesystem would truncate <code><var>filename</var>-ann</code> so that it
overwrites the original <code><var>filename</var></code>, <code>gprof</code> generates
annotated source in the file <code><var>filename</var>.ann</code> instead (if the
original file name has an extension, that extension is <em>replaced</em>
with <code>.ann</code>).

<br><dt><code>-Z[<var>symspec</var>]</code>
<dt><code>--no-exec-counts[=<var>symspec</var>]</code>
<dd>The <code>-Z</code> option causes <code>gprof</code> not to
print a tally of functions and the number of times each was called. 
If <var>symspec</var> is specified, print tally, but exclude matching symbols.

<br><dt><code>--function-ordering</code>
<dd>The <code>--function-ordering</code> option causes <code>gprof</code> to print a
suggested function ordering for the program based on profiling data. 
This option suggests an ordering which may improve paging, tlb and
cache behavior for the program on systems which support arbitrary
ordering of functions in an executable.

<p>The exact details of how to force the linker to place functions
in a particular order is system dependent and out of the scope of this
manual.

<br><dt><code>--file-ordering <var>map_file</var></code>
<dd>The <code>--file-ordering</code> option causes <code>gprof</code> to print a
suggested .o link line ordering for the program based on profiling data. 
This option suggests an ordering which may improve paging, tlb and
cache behavior for the program on systems which do not support arbitrary
ordering of functions in an executable.

<p>Use of the <code>-a</code> argument is highly recommended with this option.

<p>The <var>map_file</var> argument is a pathname to a file which provides
function name to object file mappings.  The format of the file is similar to
the output of the program <code>nm</code>.

<br><pre>c-parse.o:00000000 T yyparse
c-parse.o:00000004 C yyerrflag
c-lang.o:00000000 T maybe_objc_method_name
c-lang.o:00000000 T print_lang_statistics
c-lang.o:00000000 T recognize_objc_keyword
c-decl.o:00000000 T print_lang_identifier
c-decl.o:00000000 T print_lang_type
<small>...</small>
</pre>

<p>To create a <var>map_file</var> with <small>GNU</small> <code>nm</code>, type a command like
<kbd>nm --extern-only --defined-only -v --print-file-name program-name</kbd>.

<br><dt><code>-T</code>
<dt><code>--traditional</code>
<dd>The <code>-T</code> option causes <code>gprof</code> to print its output in
"traditional" BSD style.

<br><dt><code>-w <var>width</var></code>
<dt><code>--width=<var>width</var></code>
<dd>Sets width of output lines to <var>width</var>. 
Currently only used when printing the function index at the bottom
of the call graph.

<br><dt><code>-x</code>
<dt><code>--all-lines</code>
<dd>This option affects annotated source output only. 
By default, only the lines at the beginning of a basic-block
are annotated.  If this option is specified, every line in
a basic-block is annotated by repeating the annotation for the
first line.  This behavior is similar to <code>tcov</code>'s <code>-a</code>.

<br><dt><code>--demangle[=<var>style</var>]</code>
<dt><code>--no-demangle</code>
<dd>These options control whether C++ symbol names should be demangled when
printing output.  The default is to demangle symbols.  The
<code>--no-demangle</code> option may be used to turn off demangling. Different
compilers have different mangling styles.  The optional demangling style
argument can be used to choose an appropriate demangling style for your
compiler. 
</dl>

</body></html>

