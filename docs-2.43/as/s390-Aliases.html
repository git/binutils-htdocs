<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>s390 Aliases (Using as)</title>

<meta name="description" content="s390 Aliases (Using as)">
<meta name="keywords" content="s390 Aliases (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="s390-Syntax.html" rel="up" title="s390 Syntax">
<link href="s390-Operand-Modifier.html" rel="next" title="s390 Operand Modifier">
<link href="s390-Formats.html" rel="prev" title="s390 Formats">
<style type="text/css">
<!--
div.display {margin-left: 3.2em}
pre.display-preformatted {font-family: inherit}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="s390-Aliases">
<div class="nav-panel">
<p>
Next: <a href="s390-Operand-Modifier.html" accesskey="n" rel="next">Instruction Operand Modifier</a>, Previous: <a href="s390-Formats.html" accesskey="p" rel="prev">Instruction Formats</a>, Up: <a href="s390-Syntax.html" accesskey="u" rel="up">Instruction syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection subsection-level-set-subsubsection" id="Instruction-Aliases">9.42.3.5 Instruction Aliases</h4>
<a class="index-entry-id" id="index-instruction-aliases_002c-s390"></a>
<a class="index-entry-id" id="index-s390-instruction-aliases"></a>

<p>A specific bit pattern can have multiple mnemonics, for example
the bit pattern &lsquo;<samp class="samp">0xa7000000</samp>&rsquo; has the mnemonics &lsquo;<samp class="samp">tmh</samp>&rsquo; and
&lsquo;<samp class="samp">tmlh</samp>&rsquo;. In addition, there are a number of mnemonics recognized by
<code class="code">as</code> that are not present in the Principles of Operation.
These are the short forms of the branch instructions, where the condition
code mask operand is encoded in the mnemonic. This is relevant for the
branch instructions, the compare and branch instructions, and the
compare and trap instructions.
</p>
<p>For the branch instructions there are 20 condition code strings that can
be used as part of the mnemonic in place of a mask operand in the instruction
format:
</p>
<div class="display">
<table class="multitable">
<thead><tr><th width="30%"><pre class="display-preformatted">instruction</pre></th><th width="30%"><pre class="display-preformatted">short form</pre></th></tr></thead>
<tbody><tr><td width="30%"><pre class="display-preformatted">bcr   M1,R2</pre></td><td width="30%"><pre class="display-preformatted">b&lt;m&gt;r  R2</pre></td></tr>
<tr><td width="30%"><pre class="display-preformatted">bc    M1,D2(X2,B2)</pre></td><td width="30%"><pre class="display-preformatted">b&lt;m&gt;   D2(X2,B2)</pre></td></tr>
<tr><td width="30%"><pre class="display-preformatted">brc   M1,I2</pre></td><td width="30%"><pre class="display-preformatted">j&lt;m&gt;   I2</pre></td></tr>
<tr><td width="30%"><pre class="display-preformatted">brcl  M1,I2</pre></td><td width="30%"><pre class="display-preformatted">jg&lt;m&gt;  I2</pre></td></tr>
</tbody>
</table>
</div>

<p>In the mnemonic for a branch instruction the condition code string &lt;m&gt;
can be any of the following:
</p>
<div class="display">
<table class="multitable">
<tbody><tr><td><pre class="display-preformatted">o</pre></td><td><pre class="display-preformatted">jump on overflow / if ones</pre></td></tr>
<tr><td><pre class="display-preformatted">h</pre></td><td><pre class="display-preformatted">jump on A high</pre></td></tr>
<tr><td><pre class="display-preformatted">p</pre></td><td><pre class="display-preformatted">jump on plus</pre></td></tr>
<tr><td><pre class="display-preformatted">nle</pre></td><td><pre class="display-preformatted">jump on not low or equal</pre></td></tr>
<tr><td><pre class="display-preformatted">l</pre></td><td><pre class="display-preformatted">jump on A low</pre></td></tr>
<tr><td><pre class="display-preformatted">m</pre></td><td><pre class="display-preformatted">jump on minus</pre></td></tr>
<tr><td><pre class="display-preformatted">nhe</pre></td><td><pre class="display-preformatted">jump on not high or equal</pre></td></tr>
<tr><td><pre class="display-preformatted">lh</pre></td><td><pre class="display-preformatted">jump on low or high</pre></td></tr>
<tr><td><pre class="display-preformatted">ne</pre></td><td><pre class="display-preformatted">jump on A not equal B</pre></td></tr>
<tr><td><pre class="display-preformatted">nz</pre></td><td><pre class="display-preformatted">jump on not zero / if not zeros</pre></td></tr>
<tr><td><pre class="display-preformatted">e</pre></td><td><pre class="display-preformatted">jump on A equal B</pre></td></tr>
<tr><td><pre class="display-preformatted">z</pre></td><td><pre class="display-preformatted">jump on zero / if zeroes</pre></td></tr>
<tr><td><pre class="display-preformatted">nlh</pre></td><td><pre class="display-preformatted">jump on not low or high</pre></td></tr>
<tr><td><pre class="display-preformatted">he</pre></td><td><pre class="display-preformatted">jump on high or equal</pre></td></tr>
<tr><td><pre class="display-preformatted">nl</pre></td><td><pre class="display-preformatted">jump on A not low</pre></td></tr>
<tr><td><pre class="display-preformatted">nm</pre></td><td><pre class="display-preformatted">jump on not minus / if not mixed</pre></td></tr>
<tr><td><pre class="display-preformatted">le</pre></td><td><pre class="display-preformatted">jump on low or equal</pre></td></tr>
<tr><td><pre class="display-preformatted">nh</pre></td><td><pre class="display-preformatted">jump on A not high</pre></td></tr>
<tr><td><pre class="display-preformatted">np</pre></td><td><pre class="display-preformatted">jump on not plus</pre></td></tr>
<tr><td><pre class="display-preformatted">no</pre></td><td><pre class="display-preformatted">jump on not overflow / if not ones</pre></td></tr>
</tbody>
</table>
</div>

<p>For the compare and branch, and compare and trap instructions there
are 12 condition code strings that can be used as part of the mnemonic in
place of a mask operand in the instruction format:
</p>
<div class="display">
<table class="multitable">
<thead><tr><th width="40%"><pre class="display-preformatted">instruction</pre></th><th width="40%"><pre class="display-preformatted">short form</pre></th></tr></thead>
<tbody><tr><td width="40%"><pre class="display-preformatted">crb    R1,R2,M3,D4(B4)</pre></td><td width="40%"><pre class="display-preformatted">crb&lt;m&gt;    R1,R2,D4(B4)</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">cgrb   R1,R2,M3,D4(B4)</pre></td><td width="40%"><pre class="display-preformatted">cgrb&lt;m&gt;   R1,R2,D4(B4)</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">crj    R1,R2,M3,I4</pre></td><td width="40%"><pre class="display-preformatted">crj&lt;m&gt;    R1,R2,I4</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">cgrj   R1,R2,M3,I4</pre></td><td width="40%"><pre class="display-preformatted">cgrj&lt;m&gt;   R1,R2,I4</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">cib    R1,I2,M3,D4(B4)</pre></td><td width="40%"><pre class="display-preformatted">cib&lt;m&gt;    R1,I2,D4(B4)</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">cgib   R1,I2,M3,D4(B4)</pre></td><td width="40%"><pre class="display-preformatted">cgib&lt;m&gt;   R1,I2,D4(B4)</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">cij    R1,I2,M3,I4</pre></td><td width="40%"><pre class="display-preformatted">cij&lt;m&gt;    R1,I2,I4</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">cgij   R1,I2,M3,I4</pre></td><td width="40%"><pre class="display-preformatted">cgij&lt;m&gt;   R1,I2,I4</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">crt    R1,R2,M3</pre></td><td width="40%"><pre class="display-preformatted">crt&lt;m&gt;    R1,R2</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">cgrt   R1,R2,M3</pre></td><td width="40%"><pre class="display-preformatted">cgrt&lt;m&gt;   R1,R2</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">cit    R1,I2,M3</pre></td><td width="40%"><pre class="display-preformatted">cit&lt;m&gt;    R1,I2</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">cgit   R1,I2,M3</pre></td><td width="40%"><pre class="display-preformatted">cgit&lt;m&gt;   R1,I2</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clrb   R1,R2,M3,D4(B4)</pre></td><td width="40%"><pre class="display-preformatted">clrb&lt;m&gt;   R1,R2,D4(B4)</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clgrb  R1,R2,M3,D4(B4)</pre></td><td width="40%"><pre class="display-preformatted">clgrb&lt;m&gt;  R1,R2,D4(B4)</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clrj   R1,R2,M3,I4</pre></td><td width="40%"><pre class="display-preformatted">clrj&lt;m&gt;   R1,R2,I4</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clgrj  R1,R2,M3,I4</pre></td><td width="40%"><pre class="display-preformatted">clgrj&lt;m&gt;  R1,R2,I4</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clib   R1,I2,M3,D4(B4)</pre></td><td width="40%"><pre class="display-preformatted">clib&lt;m&gt;   R1,I2,D4(B4)</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clgib  R1,I2,M3,D4(B4)</pre></td><td width="40%"><pre class="display-preformatted">clgib&lt;m&gt;  R1,I2,D4(B4)</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clij   R1,I2,M3,I4</pre></td><td width="40%"><pre class="display-preformatted">clij&lt;m&gt;   R1,I2,I4</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clgij  R1,I2,M3,I4</pre></td><td width="40%"><pre class="display-preformatted">clgij&lt;m&gt;  R1,I2,I4</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clrt   R1,R2,M3</pre></td><td width="40%"><pre class="display-preformatted">clrt&lt;m&gt;   R1,R2</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clgrt  R1,R2,M3</pre></td><td width="40%"><pre class="display-preformatted">clgrt&lt;m&gt;  R1,R2</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clfit  R1,I2,M3</pre></td><td width="40%"><pre class="display-preformatted">clfit&lt;m&gt;  R1,I2</pre></td></tr>
<tr><td width="40%"><pre class="display-preformatted">clgit  R1,I2,M3</pre></td><td width="40%"><pre class="display-preformatted">clgit&lt;m&gt;  R1,I2</pre></td></tr>
</tbody>
</table>
</div>

<p>In the mnemonic for a compare and branch and compare and trap instruction
the condition code string &lt;m&gt; can be any of the following:
</p>
<div class="display">
<table class="multitable">
<tbody><tr><td><pre class="display-preformatted">h</pre></td><td><pre class="display-preformatted">jump on A high</pre></td></tr>
<tr><td><pre class="display-preformatted">nle</pre></td><td><pre class="display-preformatted">jump on not low or equal</pre></td></tr>
<tr><td><pre class="display-preformatted">l</pre></td><td><pre class="display-preformatted">jump on A low</pre></td></tr>
<tr><td><pre class="display-preformatted">nhe</pre></td><td><pre class="display-preformatted">jump on not high or equal</pre></td></tr>
<tr><td><pre class="display-preformatted">ne</pre></td><td><pre class="display-preformatted">jump on A not equal B</pre></td></tr>
<tr><td><pre class="display-preformatted">lh</pre></td><td><pre class="display-preformatted">jump on low or high</pre></td></tr>
<tr><td><pre class="display-preformatted">e</pre></td><td><pre class="display-preformatted">jump on A equal B</pre></td></tr>
<tr><td><pre class="display-preformatted">nlh</pre></td><td><pre class="display-preformatted">jump on not low or high</pre></td></tr>
<tr><td><pre class="display-preformatted">nl</pre></td><td><pre class="display-preformatted">jump on A not low</pre></td></tr>
<tr><td><pre class="display-preformatted">he</pre></td><td><pre class="display-preformatted">jump on high or equal</pre></td></tr>
<tr><td><pre class="display-preformatted">nh</pre></td><td><pre class="display-preformatted">jump on A not high</pre></td></tr>
<tr><td><pre class="display-preformatted">le</pre></td><td><pre class="display-preformatted">jump on low or equal</pre></td></tr>
</tbody>
</table>
</div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="s390-Operand-Modifier.html">Instruction Operand Modifier</a>, Previous: <a href="s390-Formats.html">Instruction Formats</a>, Up: <a href="s390-Syntax.html">Instruction syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
