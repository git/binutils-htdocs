<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Symbol Names (Using as)</title>

<meta name="description" content="Symbol Names (Using as)">
<meta name="keywords" content="Symbol Names (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Symbols.html" rel="up" title="Symbols">
<link href="Dot.html" rel="next" title="Dot">
<link href="Setting-Symbols.html" rel="prev" title="Setting Symbols">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
kbd.kbd {font-style: oblique}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Symbol-Names">
<div class="nav-panel">
<p>
Next: <a href="Dot.html" accesskey="n" rel="next">The Special Dot Symbol</a>, Previous: <a href="Setting-Symbols.html" accesskey="p" rel="prev">Giving Symbols Other Values</a>, Up: <a href="Symbols.html" accesskey="u" rel="up">Symbols</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Symbol-Names-1">5.3 Symbol Names</h3>

<a class="index-entry-id" id="index-symbol-names"></a>
<a class="index-entry-id" id="index-names_002c-symbol"></a>
<p>Symbol names begin with a letter or with one of &lsquo;<samp class="samp">._</samp>&rsquo;.  On most
machines, you can also use <code class="code">$</code> in symbol names; exceptions are
noted in <a class="ref" href="Machine-Dependencies.html">Machine Dependent Features</a>.  That character may be followed by any
string of digits, letters, dollar signs (unless otherwise noted for a
particular target machine), and underscores.  These restrictions do not
apply when quoting symbol names by &lsquo;<samp class="samp">&quot;</samp>&rsquo;, which is permitted for most
targets.  Escaping characters in quoted symbol names with &lsquo;<samp class="samp">\</samp>&rsquo; generally
extends only to &lsquo;<samp class="samp">\</samp>&rsquo; itself and &lsquo;<samp class="samp">&quot;</samp>&rsquo;, at the time of writing.
</p>
<p>Case of letters is significant: <code class="code">foo</code> is a different symbol name
than <code class="code">Foo</code>.
</p>
<p>Symbol names do not start with a digit.  An exception to this rule is made for
Local Labels.  See below.
</p>
<p>Multibyte characters are supported, but note that the setting of the
<samp class="option">multibyte-handling</samp> option might prevent their use.
To generate a symbol name containing
multibyte characters enclose it within double quotes and use escape codes. cf
See <a class="xref" href="Strings.html">Strings</a>.  Generating a multibyte symbol name from a label is not
currently supported.
</p>
<p>Since multibyte symbol names are unusual, and could possibly be used
maliciously, <code class="command">as</code> provides a command line option
(<samp class="option">--multibyte-handling=warn-sym-only</samp>) which can be used to generate a
warning message whenever a symbol name containing multibyte characters is defined.
</p>
<p>Each symbol has exactly one name.  Each name in an assembly language program
refers to exactly one symbol.  You may use that symbol name any number of times
in a program.
</p>
<h4 class="subheading" id="Local-Symbol-Names">Local Symbol Names</h4>

<a class="index-entry-id" id="index-local-symbol-names"></a>
<a class="index-entry-id" id="index-symbol-names_002c-local"></a>
<p>A local symbol is any symbol beginning with certain local label prefixes.
By default, the local label prefix is &lsquo;<samp class="samp">.L</samp>&rsquo; for ELF systems or
&lsquo;<samp class="samp">L</samp>&rsquo; for traditional a.out systems, but each target may have its own
set of local label prefixes.
On the HPPA local symbols begin with &lsquo;<samp class="samp">L$</samp>&rsquo;.
</p>
<p>Local symbols are defined and used within the assembler, but they are
normally not saved in object files.  Thus, they are not visible when debugging.
You may use the &lsquo;<samp class="samp">-L</samp>&rsquo; option (see <a class="pxref" href="L.html">Include Local Symbols</a>)
to retain the local symbols in the object files.
</p>
<h4 class="subheading" id="Local-Labels-1">Local Labels</h4>

<a class="index-entry-id" id="index-local-labels"></a>
<a class="index-entry-id" id="index-temporary-symbol-names"></a>
<a class="index-entry-id" id="index-symbol-names_002c-temporary"></a>
<p>Local labels are different from local symbols.  Local labels help compilers and
programmers use names temporarily.  They create symbols which are guaranteed to
be unique over the entire scope of the input source code and which can be
referred to by a simple notation.  To define a local label, write a label of
the form &lsquo;<samp class="samp"><b class="b">N</b>:</samp>&rsquo; (where <b class="b">N</b> represents any non-negative integer).
To refer to the most recent previous definition of that label write
&lsquo;<samp class="samp"><b class="b">N</b>b</samp>&rsquo;, using the same number as when you defined the label.  To refer
to the next definition of a local label, write &lsquo;<samp class="samp"><b class="b">N</b>f</samp>&rsquo;.  The &lsquo;<samp class="samp">b</samp>&rsquo;
stands for &ldquo;backwards&rdquo; and the &lsquo;<samp class="samp">f</samp>&rsquo; stands for &ldquo;forwards&rdquo;.
</p>
<p>There is no restriction on how you can use these labels, and you can reuse them
too.  So that it is possible to repeatedly define the same local label (using
the same number &lsquo;<samp class="samp"><b class="b">N</b></samp>&rsquo;), although you can only refer to the most recently
defined local label of that number (for a backwards reference) or the next
definition of a specific local label for a forward reference.  It is also worth
noting that the first 10 local labels (&lsquo;<samp class="samp"><b class="b">0:</b></samp>&rsquo;&hellip;&lsquo;<samp class="samp"><b class="b">9:</b></samp>&rsquo;) are
implemented in a slightly more efficient manner than the others.
</p>
<p>Here is an example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">1:        branch 1f
2:        branch 1b
1:        branch 2f
2:        branch 1b
</pre></div>

<p>Which is the equivalent of:
</p>
<div class="example smallexample">
<pre class="example-preformatted">label_1:  branch label_3
label_2:  branch label_1
label_3:  branch label_4
label_4:  branch label_3
</pre></div>

<p>Local label names are only a notational device.  They are immediately
transformed into more conventional symbol names before the assembler uses them.
The symbol names are stored in the symbol table, appear in error messages, and
are optionally emitted to the object file.  The names are constructed using
these parts:
</p>
<dl class="table">
<dt><code class="code"><em class="emph">local label prefix</em></code></dt>
<dd><p>All local symbols begin with the system-specific local label prefix.
Normally both <code class="command">as</code> and <code class="code">ld</code> forget symbols
that start with the local label prefix.  These labels are
used for symbols you are never intended to see.  If you use the
&lsquo;<samp class="samp">-L</samp>&rsquo; option then <code class="command">as</code> retains these symbols in the
object file. If you also instruct <code class="code">ld</code> to retain these symbols,
you may use them in debugging.
</p>
</dd>
<dt><code class="code"><var class="var">number</var></code></dt>
<dd><p>This is the number that was used in the local label definition.  So if the
label is written &lsquo;<samp class="samp">55:</samp>&rsquo; then the number is &lsquo;<samp class="samp">55</samp>&rsquo;.
</p>
</dd>
<dt><code class="code"><kbd class="kbd">C-B</kbd></code></dt>
<dd><p>This unusual character is included so you do not accidentally invent a symbol
of the same name.  The character has ASCII value of &lsquo;<samp class="samp">\002</samp>&rsquo; (control-B).
</p>
</dd>
<dt><code class="code"><em class="emph">ordinal number</em></code></dt>
<dd><p>This is a serial number to keep the labels distinct.  The first definition of
&lsquo;<samp class="samp">0:</samp>&rsquo; gets the number &lsquo;<samp class="samp">1</samp>&rsquo;.  The 15th definition of &lsquo;<samp class="samp">0:</samp>&rsquo; gets the
number &lsquo;<samp class="samp">15</samp>&rsquo;, and so on.  Likewise the first definition of &lsquo;<samp class="samp">1:</samp>&rsquo; gets
the number &lsquo;<samp class="samp">1</samp>&rsquo; and its 15th definition gets &lsquo;<samp class="samp">15</samp>&rsquo; as well.
</p></dd>
</dl>

<p>So for example, the first <code class="code">1:</code> may be named <code class="code">.L1<kbd class="kbd">C-B</kbd>1</code>, and
the 44th <code class="code">3:</code> may be named <code class="code">.L3<kbd class="kbd">C-B</kbd>44</code>.
</p>
<h4 class="subheading" id="Dollar-Local-Labels">Dollar Local Labels</h4>
<a class="index-entry-id" id="index-dollar-local-symbols"></a>

<p>On some targets <code class="code">as</code> also supports an even more local form of
local labels called dollar labels.  These labels go out of scope (i.e., they
become undefined) as soon as a non-local label is defined.  Thus they remain
valid for only a small region of the input source code.  Normal local labels,
by contrast, remain in scope for the entire file, or until they are redefined
by another occurrence of the same local label.
</p>
<p>Dollar labels are defined in exactly the same way as ordinary local labels,
except that they have a dollar sign suffix to their numeric value, e.g.,
&lsquo;<samp class="samp"><b class="b">55$:</b></samp>&rsquo;.
</p>
<p>They can also be distinguished from ordinary local labels by their transformed
names which use ASCII character &lsquo;<samp class="samp">\001</samp>&rsquo; (control-A) as the magic character
to distinguish them from ordinary labels.  For example, the fifth definition of
&lsquo;<samp class="samp">6$</samp>&rsquo; may be named &lsquo;<samp class="samp">.L6<kbd class="kbd">C-A</kbd>5</samp>&rsquo;.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Dot.html">The Special Dot Symbol</a>, Previous: <a href="Setting-Symbols.html">Giving Symbols Other Values</a>, Up: <a href="Symbols.html">Symbols</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
