<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Alpha Directives (Using as)</title>

<meta name="description" content="Alpha Directives (Using as)">
<meta name="keywords" content="Alpha Directives (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Alpha_002dDependent.html" rel="up" title="Alpha-Dependent">
<link href="Alpha-Opcodes.html" rel="next" title="Alpha Opcodes">
<link href="Alpha-Floating-Point.html" rel="prev" title="Alpha Floating Point">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Alpha-Directives">
<div class="nav-panel">
<p>
Next: <a href="Alpha-Opcodes.html" accesskey="n" rel="next">Opcodes</a>, Previous: <a href="Alpha-Floating-Point.html" accesskey="p" rel="prev">Floating Point</a>, Up: <a href="Alpha_002dDependent.html" accesskey="u" rel="up">Alpha Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="Alpha-Assembler-Directives">9.2.5 Alpha Assembler Directives</h4>

<p><code class="command">as</code> for the Alpha supports many additional directives for
compatibility with the native assembler.  This section describes them only
briefly.
</p>
<a class="index-entry-id" id="index-Alpha_002donly-directives"></a>
<p>These are the additional directives in <code class="code">as</code> for the Alpha:
</p>
<dl class="table">
<dt><code class="code">.arch <var class="var">cpu</var></code></dt>
<dd><p>Specifies the target processor.  This is equivalent to the
<samp class="option">-m<var class="var">cpu</var></samp> command-line option.  See <a class="xref" href="Alpha-Options.html">Options</a>,
for a list of values for <var class="var">cpu</var>.
</p>
</dd>
<dt><code class="code">.ent <var class="var">function</var>[, <var class="var">n</var>]</code></dt>
<dd><p>Mark the beginning of <var class="var">function</var>.  An optional number may follow for
compatibility with the OSF/1 assembler, but is ignored.  When generating
<code class="code">.mdebug</code> information, this will create a procedure descriptor for
the function.  In ELF, it will mark the symbol as a function a-la the
generic <code class="code">.type</code> directive.
</p>
</dd>
<dt><code class="code">.end <var class="var">function</var></code></dt>
<dd><p>Mark the end of <var class="var">function</var>.  In ELF, it will set the size of the symbol
a-la the generic <code class="code">.size</code> directive.
</p>
</dd>
<dt><code class="code">.mask <var class="var">mask</var>, <var class="var">offset</var></code></dt>
<dd><p>Indicate which of the integer registers are saved in the current
function&rsquo;s stack frame.  <var class="var">mask</var> is interpreted a bit mask in which
bit <var class="var">n</var> set indicates that register <var class="var">n</var> is saved.  The registers
are saved in a block located <var class="var">offset</var> bytes from the <em class="dfn">canonical
frame address</em> (CFA) which is the value of the stack pointer on entry to
the function.  The registers are saved sequentially, except that the
return address register (normally <code class="code">$26</code>) is saved first.
</p>
<p>This and the other directives that describe the stack frame are
currently only used when generating <code class="code">.mdebug</code> information.  They
may in the future be used to generate DWARF2 <code class="code">.debug_frame</code> unwind
information for hand written assembly.
</p>
</dd>
<dt><code class="code">.fmask <var class="var">mask</var>, <var class="var">offset</var></code></dt>
<dd><p>Indicate which of the floating-point registers are saved in the current
stack frame.  The <var class="var">mask</var> and <var class="var">offset</var> parameters are interpreted
as with <code class="code">.mask</code>.
</p>
</dd>
<dt><code class="code">.frame <var class="var">framereg</var>, <var class="var">frameoffset</var>, <var class="var">retreg</var>[, <var class="var">argoffset</var>]</code></dt>
<dd><p>Describes the shape of the stack frame.  The frame pointer in use is
<var class="var">framereg</var>; normally this is either <code class="code">$fp</code> or <code class="code">$sp</code>.  The
frame pointer is <var class="var">frameoffset</var> bytes below the CFA.  The return
address is initially located in <var class="var">retreg</var> until it is saved as
indicated in <code class="code">.mask</code>.  For compatibility with OSF/1 an optional
<var class="var">argoffset</var> parameter is accepted and ignored.  It is believed to
indicate the offset from the CFA to the saved argument registers.
</p>
</dd>
<dt><code class="code">.prologue <var class="var">n</var></code></dt>
<dd><p>Indicate that the stack frame is set up and all registers have been
spilled.  The argument <var class="var">n</var> indicates whether and how the function
uses the incoming <em class="dfn">procedure vector</em> (the address of the called
function) in <code class="code">$27</code>.  0 indicates that <code class="code">$27</code> is not used; 1
indicates that the first two instructions of the function use <code class="code">$27</code>
to perform a load of the GP register; 2 indicates that <code class="code">$27</code> is
used in some non-standard way and so the linker cannot elide the load of
the procedure vector during relaxation.
</p>
</dd>
<dt><code class="code">.usepv <var class="var">function</var>, <var class="var">which</var></code></dt>
<dd><p>Used to indicate the use of the <code class="code">$27</code> register, similar to
<code class="code">.prologue</code>, but without the other semantics of needing to
be inside an open <code class="code">.ent</code>/<code class="code">.end</code> block.
</p>
<p>The <var class="var">which</var> argument should be either <code class="code">no</code>, indicating that
<code class="code">$27</code> is not used, or <code class="code">std</code>, indicating that the first two
instructions of the function perform a GP load.
</p>
<p>One might use this directive instead of <code class="code">.prologue</code> if you are
also using dwarf2 CFI directives.
</p>
</dd>
<dt><code class="code">.gprel32 <var class="var">expression</var></code></dt>
<dd><p>Computes the difference between the address in <var class="var">expression</var> and the
GP for the current object file, and stores it in 4 bytes.  In addition
to being smaller than a full 8 byte address, this also does not require
a dynamic relocation when used in a shared library.
</p>
</dd>
<dt><code class="code">.t_floating <var class="var">expression</var></code></dt>
<dd><p>Stores <var class="var">expression</var> as an <small class="sc">IEEE</small> double precision value.
</p>
</dd>
<dt><code class="code">.s_floating <var class="var">expression</var></code></dt>
<dd><p>Stores <var class="var">expression</var> as an <small class="sc">IEEE</small> single precision value.
</p>
</dd>
<dt><code class="code">.f_floating <var class="var">expression</var></code></dt>
<dd><p>Stores <var class="var">expression</var> as a VAX F format value.
</p>
</dd>
<dt><code class="code">.g_floating <var class="var">expression</var></code></dt>
<dd><p>Stores <var class="var">expression</var> as a VAX G format value.
</p>
</dd>
<dt><code class="code">.d_floating <var class="var">expression</var></code></dt>
<dd><p>Stores <var class="var">expression</var> as a VAX D format value.
</p>
</dd>
<dt><code class="code">.set <var class="var">feature</var></code></dt>
<dd><p>Enables or disables various assembler features.  Using the positive
name of the feature enables while using &lsquo;<samp class="samp">no<var class="var">feature</var></samp>&rsquo; disables.
</p>
<dl class="table">
<dt><code class="code">at</code></dt>
<dd><p>Indicates that macro expansions may clobber the <em class="dfn">assembler
temporary</em> (<code class="code">$at</code> or <code class="code">$28</code>) register.  Some macros may not be
expanded without this and will generate an error message if <code class="code">noat</code>
is in effect.  When <code class="code">at</code> is in effect, a warning will be generated
if <code class="code">$at</code> is used by the programmer.
</p>
</dd>
<dt><code class="code">macro</code></dt>
<dd><p>Enables the expansion of macro instructions.  Note that variants of real
instructions, such as <code class="code">br label</code> vs <code class="code">br $31,label</code> are
considered alternate forms and not macros.
</p>
</dd>
<dt><code class="code">move</code></dt>
<dt><code class="code">reorder</code></dt>
<dt><code class="code">volatile</code></dt>
<dd><p>These control whether and how the assembler may re-order instructions.
Accepted for compatibility with the OSF/1 assembler, but <code class="command">as</code>
does not do instruction scheduling, so these features are ignored.
</p></dd>
</dl>
</dd>
</dl>

<p>The following directives are recognized for compatibility with the OSF/1
assembler but are ignored.
</p>
<div class="example">
<pre class="example-preformatted">.proc           .aproc
.reguse         .livereg
.option         .aent
.ugen           .eflag
.alias          .noalias
</pre></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Alpha-Opcodes.html">Opcodes</a>, Previous: <a href="Alpha-Floating-Point.html">Floating Point</a>, Up: <a href="Alpha_002dDependent.html">Alpha Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
