<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>s390 Operand Modifier (Using as)</title>

<meta name="description" content="s390 Operand Modifier (Using as)">
<meta name="keywords" content="s390 Operand Modifier (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="s390-Syntax.html" rel="up" title="s390 Syntax">
<link href="s390-Instruction-Marker.html" rel="next" title="s390 Instruction Marker">
<link href="s390-Aliases.html" rel="prev" title="s390 Aliases">


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="s390-Operand-Modifier">
<div class="nav-panel">
<p>
Next: <a href="s390-Instruction-Marker.html" accesskey="n" rel="next">Instruction Marker</a>, Previous: <a href="s390-Aliases.html" accesskey="p" rel="prev">Instruction Aliases</a>, Up: <a href="s390-Syntax.html" accesskey="u" rel="up">Instruction syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection subsection-level-set-subsubsection" id="Instruction-Operand-Modifier">9.42.3.6 Instruction Operand Modifier</h4>
<a class="index-entry-id" id="index-instruction-operand-modifier_002c-s390"></a>
<a class="index-entry-id" id="index-s390-instruction-operand-modifier"></a>

<p>If a symbol modifier is attached to a symbol in an expression for an
instruction operand field, the symbol term is replaced with a reference
to an object in the global offset table (GOT) or the procedure linkage
table (PLT). The following expressions are allowed:
&lsquo;<samp class="samp">symbol@modifier + constant</samp>&rsquo;,
&lsquo;<samp class="samp">symbol@modifier + label + constant</samp>&rsquo;, and
&lsquo;<samp class="samp">symbol@modifier - label + constant</samp>&rsquo;.
The term &lsquo;<samp class="samp">symbol</samp>&rsquo; is the symbol that will be entered into the GOT or
PLT, &lsquo;<samp class="samp">label</samp>&rsquo; is a local label, and &lsquo;<samp class="samp">constant</samp>&rsquo; is an arbitrary
expression that the assembler can evaluate to a constant value.
</p>
<p>The term &lsquo;<samp class="samp">(symbol + constant1)@modifier +/- label + constant2</samp>&rsquo;
is also accepted but a warning message is printed and the term is
converted to &lsquo;<samp class="samp">symbol@modifier +/- label + constant1 + constant2</samp>&rsquo;.
</p>
<dl class="table">
<dt><code class="code">@got</code></dt>
<dt><code class="code">@got12</code></dt>
<dd><p>The @got modifier can be used for displacement fields, 16-bit immediate
fields and 32-bit pc-relative immediate fields. The @got12 modifier is
synonym to @got. The symbol is added to the GOT. For displacement
fields and 16-bit immediate fields the symbol term is replaced with
the offset from the start of the GOT to the GOT slot for the symbol.
For a 32-bit pc-relative field the pc-relative offset to the GOT
slot from the current instruction address is used.
</p></dd>
<dt><code class="code">@gotent</code></dt>
<dd><p>The @gotent modifier can be used for 32-bit pc-relative immediate fields.
The symbol is added to the GOT and the symbol term is replaced with
the pc-relative offset from the current instruction to the GOT slot for the
symbol.
</p></dd>
<dt><code class="code">@gotoff</code></dt>
<dd><p>The @gotoff modifier can be used for 16-bit immediate fields. The symbol
term is replaced with the offset from the start of the GOT to the
address of the symbol.
</p></dd>
<dt><code class="code">@gotplt</code></dt>
<dd><p>The @gotplt modifier can be used for displacement fields, 16-bit immediate
fields, and 32-bit pc-relative immediate fields. A procedure linkage
table entry is generated for the symbol and a jump slot for the symbol
is added to the GOT. For displacement fields and 16-bit immediate
fields the symbol term is replaced with the offset from the start of the
GOT to the jump slot for the symbol. For a 32-bit pc-relative field
the pc-relative offset to the jump slot from the current instruction
address is used.
</p></dd>
<dt><code class="code">@plt</code></dt>
<dd><p>The @plt modifier can be used for 16-bit and 32-bit pc-relative immediate
fields. A procedure linkage table entry is generated for the symbol.
The symbol term is replaced with the relative offset from the current
instruction to the PLT entry for the symbol.
</p></dd>
<dt><code class="code">@pltoff</code></dt>
<dd><p>The @pltoff modifier can be used for 16-bit immediate fields. The symbol
term is replaced with the offset from the start of the PLT to the address
of the symbol.
</p></dd>
<dt><code class="code">@gotntpoff</code></dt>
<dd><p>The @gotntpoff modifier can be used for displacement fields. The symbol
is added to the static TLS block and the negated offset to the symbol
in the static TLS block is added to the GOT. The symbol term is replaced
with the offset to the GOT slot from the start of the GOT.
</p></dd>
<dt><code class="code">@indntpoff</code></dt>
<dd><p>The @indntpoff modifier can be used for 32-bit pc-relative immediate
fields. The symbol is added to the static TLS block and the negated offset
to the symbol in the static TLS block is added to the GOT. The symbol term
is replaced with the pc-relative offset to the GOT slot from the current
instruction address.
</p></dd>
</dl>

<p>For more information about the thread local storage modifiers
&lsquo;<samp class="samp">gotntpoff</samp>&rsquo; and &lsquo;<samp class="samp">indntpoff</samp>&rsquo; see the ELF extension documentation
&lsquo;<samp class="samp">ELF Handling For Thread-Local Storage</samp>&rsquo;.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="s390-Instruction-Marker.html">Instruction Marker</a>, Previous: <a href="s390-Aliases.html">Instruction Aliases</a>, Up: <a href="s390-Syntax.html">Instruction syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
