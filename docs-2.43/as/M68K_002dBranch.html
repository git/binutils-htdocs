<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>M68K-Branch (Using as)</title>

<meta name="description" content="M68K-Branch (Using as)">
<meta name="keywords" content="M68K-Branch (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="M68K_002dopcodes.html" rel="up" title="M68K-opcodes">
<link href="M68K_002dChars.html" rel="next" title="M68K-Chars">
<style type="text/css">
<!--
div.center {text-align:center}
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="M68K_002dBranch">
<div class="nav-panel">
<p>
Next: <a href="M68K_002dChars.html" accesskey="n" rel="next">Special Characters</a>, Up: <a href="M68K_002dopcodes.html" accesskey="u" rel="up">Opcodes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection subsection-level-set-subsubsection" id="Branch-Improvement">9.23.6.1 Branch Improvement</h4>

<a class="index-entry-id" id="index-pseudo_002dopcodes_002c-M680x0"></a>
<a class="index-entry-id" id="index-M680x0-pseudo_002dopcodes"></a>
<a class="index-entry-id" id="index-branch-improvement_002c-M680x0"></a>
<a class="index-entry-id" id="index-M680x0-branch-improvement"></a>
<p>Certain pseudo opcodes are permitted for branch instructions.
They expand to the shortest branch instruction that reach the
target.  Generally these mnemonics are made by substituting &lsquo;<samp class="samp">j</samp>&rsquo; for
&lsquo;<samp class="samp">b</samp>&rsquo; at the start of a Motorola mnemonic.
</p>
<p>The following table summarizes the pseudo-operations.  A <code class="code">*</code> flags
cases that are more fully described after the table:
</p>
<div class="example smallexample">
<pre class="example-preformatted">          Displacement
          +------------------------------------------------------------
          |                68020           68000/10, not PC-relative OK
Pseudo-Op |BYTE    WORD    LONG            ABSOLUTE LONG JUMP    **
          +------------------------------------------------------------
     jbsr |bsrs    bsrw    bsrl            jsr
      jra |bras    braw    bral            jmp
*     jXX |bXXs    bXXw    bXXl            bNXs;jmp
*    dbXX | N/A    dbXXw   dbXX;bras;bral  dbXX;bras;jmp
     fjXX | N/A    fbXXw   fbXXl            N/A

XX: condition
NX: negative of condition XX

</pre></div>
<div class="center"><code class="code">*</code>&mdash;see full description below
</div><div class="center"><code class="code">**</code>&mdash;this expansion mode is disallowed by &lsquo;<samp class="samp">--pcrel</samp>&rsquo;
</div>
<dl class="table">
<dt><code class="code">jbsr</code></dt>
<dt><code class="code">jra</code></dt>
<dd><p>These are the simplest jump pseudo-operations; they always map to one
particular machine instruction, depending on the displacement to the
branch target.  This instruction will be a byte or word branch is that
is sufficient.  Otherwise, a long branch will be emitted if available.
If no long branches are available and the &lsquo;<samp class="samp">--pcrel</samp>&rsquo; option is not
given, an absolute long jump will be emitted instead.  If no long
branches are available, the &lsquo;<samp class="samp">--pcrel</samp>&rsquo; option is given, and a word
branch cannot reach the target, an error message is generated.
</p>
<p>In addition to standard branch operands, <code class="code">as</code> allows these
pseudo-operations to have all operands that are allowed for jsr and jmp,
substituting these instructions if the operand given is not valid for a
branch instruction.
</p>
</dd>
<dt><code class="code">j<var class="var">XX</var></code></dt>
<dd><p>Here, &lsquo;<samp class="samp">j<var class="var">XX</var></samp>&rsquo; stands for an entire family of pseudo-operations,
where <var class="var">XX</var> is a conditional branch or condition-code test.  The full
list of pseudo-ops in this family is:
</p><div class="example smallexample">
<pre class="example-preformatted"> jhi   jls   jcc   jcs   jne   jeq   jvc
 jvs   jpl   jmi   jge   jlt   jgt   jle
</pre></div>

<p>Usually, each of these pseudo-operations expands to a single branch
instruction.  However, if a word branch is not sufficient, no long branches
are available, and the &lsquo;<samp class="samp">--pcrel</samp>&rsquo; option is not given, <code class="code">as</code>
issues a longer code fragment in terms of <var class="var">NX</var>, the opposite condition
to <var class="var">XX</var>.  For example, under these conditions:
</p><div class="example smallexample">
<pre class="example-preformatted">    j<var class="var">XX</var> foo
</pre></div>
<p>gives
</p><div class="example smallexample">
<pre class="example-preformatted">     b<var class="var">NX</var>s oof
     jmp foo
 oof:
</pre></div>

</dd>
<dt><code class="code">db<var class="var">XX</var></code></dt>
<dd><p>The full family of pseudo-operations covered here is
</p><div class="example smallexample">
<pre class="example-preformatted"> dbhi   dbls   dbcc   dbcs   dbne   dbeq   dbvc
 dbvs   dbpl   dbmi   dbge   dblt   dbgt   dble
 dbf    dbra   dbt
</pre></div>

<p>Motorola &lsquo;<samp class="samp">db<var class="var">XX</var></samp>&rsquo; instructions allow word displacements only.  When
a word displacement is sufficient, each of these pseudo-operations expands
to the corresponding Motorola instruction.  When a word displacement is not
sufficient and long branches are available, when the source reads
&lsquo;<samp class="samp">db<var class="var">XX</var> foo</samp>&rsquo;, <code class="code">as</code> emits
</p><div class="example smallexample">
<pre class="example-preformatted">     db<var class="var">XX</var> oo1
     bras oo2
 oo1:bral foo
 oo2:
</pre></div>

<p>If, however, long branches are not available and the &lsquo;<samp class="samp">--pcrel</samp>&rsquo; option is
not given, <code class="code">as</code> emits
</p><div class="example smallexample">
<pre class="example-preformatted">     db<var class="var">XX</var> oo1
     bras oo2
 oo1:jmp foo
 oo2:
</pre></div>

</dd>
<dt><code class="code">fj<var class="var">XX</var></code></dt>
<dd><p>This family includes
</p><div class="example smallexample">
<pre class="example-preformatted"> fjne   fjeq   fjge   fjlt   fjgt   fjle   fjf
 fjt    fjgl   fjgle  fjnge  fjngl  fjngle fjngt
 fjnle  fjnlt  fjoge  fjogl  fjogt  fjole  fjolt
 fjor   fjseq  fjsf   fjsne  fjst   fjueq  fjuge
 fjugt  fjule  fjult  fjun
</pre></div>

<p>Each of these pseudo-operations always expands to a single Motorola
coprocessor branch instruction, word or long.  All Motorola coprocessor
branch instructions allow both word and long displacements.
</p>
</dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="M68K_002dChars.html">Special Characters</a>, Up: <a href="M68K_002dopcodes.html">Opcodes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
