<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>s390 Formats (Using as)</title>

<meta name="description" content="s390 Formats (Using as)">
<meta name="keywords" content="s390 Formats (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="s390-Syntax.html" rel="up" title="s390 Syntax">
<link href="s390-Aliases.html" rel="next" title="s390 Aliases">
<link href="s390-Operands.html" rel="prev" title="s390 Operands">
<style type="text/css">
<!--
div.display {margin-left: 3.2em}
pre.display-preformatted {font-family: inherit}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="s390-Formats">
<div class="nav-panel">
<p>
Next: <a href="s390-Aliases.html" accesskey="n" rel="next">Instruction Aliases</a>, Previous: <a href="s390-Operands.html" accesskey="p" rel="prev">Instruction Operands</a>, Up: <a href="s390-Syntax.html" accesskey="u" rel="up">Instruction syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection subsection-level-set-subsubsection" id="Instruction-Formats">9.42.3.4 Instruction Formats</h4>
<a class="index-entry-id" id="index-instruction-formats_002c-s390"></a>
<a class="index-entry-id" id="index-s390-instruction-formats"></a>

<p>The Principles of Operation manuals lists 35 instruction formats where
some of the formats have multiple variants. For the &lsquo;<samp class="samp">.insn</samp>&rsquo;
pseudo directive the assembler recognizes some of the formats.
Typically, the most general variant of the instruction format is used
by the &lsquo;<samp class="samp">.insn</samp>&rsquo; directive.
</p>
<p>The following table lists the abbreviations used in the table of
instruction formats:
</p>
<div class="display">
<table class="multitable">
<tbody><tr><td><pre class="display-preformatted">OpCode / OpCd</pre></td><td><pre class="display-preformatted">Part of the op code.</pre></td></tr>
<tr><td><pre class="display-preformatted">Bx</pre></td><td><pre class="display-preformatted">Base register number for operand x.</pre></td></tr>
<tr><td><pre class="display-preformatted">Dx</pre></td><td><pre class="display-preformatted">Displacement for operand x.</pre></td></tr>
<tr><td><pre class="display-preformatted">DLx</pre></td><td><pre class="display-preformatted">Displacement lower 12 bits for operand x.</pre></td></tr>
<tr><td><pre class="display-preformatted">DHx</pre></td><td><pre class="display-preformatted">Displacement higher 8-bits for operand x.</pre></td></tr>
<tr><td><pre class="display-preformatted">Rx</pre></td><td><pre class="display-preformatted">Register number for operand x.</pre></td></tr>
<tr><td><pre class="display-preformatted">Xx</pre></td><td><pre class="display-preformatted">Index register number for operand x.</pre></td></tr>
<tr><td><pre class="display-preformatted">Ix</pre></td><td><pre class="display-preformatted">Signed immediate for operand x.</pre></td></tr>
<tr><td><pre class="display-preformatted">Ux</pre></td><td><pre class="display-preformatted">Unsigned immediate for operand x.</pre></td></tr>
</tbody>
</table>
</div>

<p>An instruction is two, four, or six bytes in length and must be aligned
on a 2 byte boundary. The first two bits of the instruction specify the
length of the instruction, 00 indicates a two byte instruction, 01 and 10
indicates a four byte instruction, and 11 indicates a six byte instruction.
</p>
<p>The following table lists the s390 instruction formats that are available
with the &lsquo;<samp class="samp">.insn</samp>&rsquo; pseudo directive:
</p>
<dl class="table">
<dt><code class="code">E format</code></dt>
<dd><pre class="verbatim">+-------------+
|    OpCode   |
+-------------+
0            15
</pre>
</dd>
<dt><code class="code">RI format: &lt;insn&gt; R1,I2</code></dt>
<dd><pre class="verbatim">+--------+----+----+------------------+
| OpCode | R1 |OpCd|        I2        |
+--------+----+----+------------------+
0        8    12   16                31
</pre>
</dd>
<dt><code class="code">RIE format: &lt;insn&gt; R1,R3,I2</code></dt>
<dd><pre class="verbatim">+--------+----+----+------------------+--------+--------+
| OpCode | R1 | R3 |        I2        |////////| OpCode |
+--------+----+----+------------------+--------+--------+
0        8    12   16                 32       40      47
</pre>
</dd>
<dt><code class="code">RIL format: &lt;insn&gt; R1,I2</code></dt>
<dd><pre class="verbatim">+--------+----+----+------------------------------------+
| OpCode | R1 |OpCd|                  I2                |
+--------+----+----+------------------------------------+
0        8    12   16                                  47
</pre>
</dd>
<dt><code class="code">RILU format: &lt;insn&gt; R1,U2</code></dt>
<dd><pre class="verbatim">+--------+----+----+------------------------------------+
| OpCode | R1 |OpCd|                  U2                |
+--------+----+----+------------------------------------+
0        8    12   16                                  47
</pre>
</dd>
<dt><code class="code">RIS format: &lt;insn&gt; R1,I2,M3,D4(B4)</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+--------+--------+
| OpCode | R1 | M3 | B4 |     D4      |   I2   | Opcode |
+--------+----+----+----+-------------+--------+--------+
0        8    12   16   20            32       36      47
</pre>
</dd>
<dt><code class="code">RR format: &lt;insn&gt; R1,R2</code></dt>
<dd><pre class="verbatim">+--------+----+----+
| OpCode | R1 | R2 |
+--------+----+----+
0        8    12  15
</pre>
</dd>
<dt><code class="code">RRE format: &lt;insn&gt; R1,R2</code></dt>
<dd><pre class="verbatim">+------------------+--------+----+----+
|      OpCode      |////////| R1 | R2 |
+------------------+--------+----+----+
0                  16       24   28  31
</pre>
</dd>
<dt><code class="code">RRF format: &lt;insn&gt; R1,R2,R3,M4</code></dt>
<dd><pre class="verbatim">+------------------+----+----+----+----+
|      OpCode      | R3 | M4 | R1 | R2 |
+------------------+----+----+----+----+
0                  16   20   24   28  31
</pre>
</dd>
<dt><code class="code">RRS format: &lt;insn&gt; R1,R2,M3,D4(B4)</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+----+----+--------+
| OpCode | R1 | R3 | B4 |     D4      | M3 |////| OpCode |
+--------+----+----+----+-------------+----+----+--------+
0        8    12   16   20            32   36   40      47
</pre>
</dd>
<dt><code class="code">RS format: &lt;insn&gt; R1,R3,D2(B2)</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+
| OpCode | R1 | R3 | B2 |     D2      |
+--------+----+----+----+-------------+
0        8    12   16   20           31
</pre>
</dd>
<dt><code class="code">RSE format: &lt;insn&gt; R1,R3,D2(B2)</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+--------+--------+
| OpCode | R1 | R3 | B2 |     D2      |////////| OpCode |
+--------+----+----+----+-------------+--------+--------+
0        8    12   16   20            32       40      47
</pre>
</dd>
<dt><code class="code">RSI format: &lt;insn&gt; R1,R3,I2</code></dt>
<dd><pre class="verbatim">+--------+----+----+------------------------------------+
| OpCode | R1 | R3 |                  I2                |
+--------+----+----+------------------------------------+
0        8    12   16                                  47
</pre>
</dd>
<dt><code class="code">RSY format: &lt;insn&gt; R1,R3,D2(B2)</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+--------+--------+
| OpCode | R1 | R3 | B2 |    DL2      |  DH2   | OpCode |
+--------+----+----+----+-------------+--------+--------+
0        8    12   16   20            32       40      47
</pre>
</dd>
<dt><code class="code">RX format: &lt;insn&gt; R1,D2(X2,B2)</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+
| OpCode | R1 | X2 | B2 |     D2      |
+--------+----+----+----+-------------+
0        8    12   16   20           31
</pre>
</dd>
<dt><code class="code">RXE format: &lt;insn&gt; R1,D2(X2,B2)</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+--------+--------+
| OpCode | R1 | X2 | B2 |     D2      |////////| OpCode |
+--------+----+----+----+-------------+--------+--------+
0        8    12   16   20            32       40      47
</pre>
</dd>
<dt><code class="code">RXF format: &lt;insn&gt; R1,R3,D2(X2,B2)</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+----+---+--------+
| OpCode | R3 | X2 | B2 |     D2      | R1 |///| OpCode |
+--------+----+----+----+-------------+----+---+--------+
0        8    12   16   20            32   36  40      47
</pre>
</dd>
<dt><code class="code">RXY format: &lt;insn&gt; R1,D2(X2,B2)</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+--------+--------+
| OpCode | R1 | X2 | B2 |     DL2     |   DH2  | OpCode |
+--------+----+----+----+-------------+--------+--------+
0        8    12   16   20            32   36   40      47
</pre>
</dd>
<dt><code class="code">S format: &lt;insn&gt; D2(B2)</code></dt>
<dd><pre class="verbatim">+------------------+----+-------------+
|      OpCode      | B2 |     D2      |
+------------------+----+-------------+
0                  16   20           31
</pre>
</dd>
<dt><code class="code">SI format: &lt;insn&gt; D1(B1),I2</code></dt>
<dd><pre class="verbatim">+--------+---------+----+-------------+
| OpCode |   I2    | B1 |     D1      |
+--------+---------+----+-------------+
0        8         16   20           31
</pre>
</dd>
<dt><code class="code">SIY format: &lt;insn&gt; D1(B1),U2</code></dt>
<dd><pre class="verbatim">+--------+---------+----+-------------+--------+--------+
| OpCode |   I2    | B1 |     DL1     |  DH1   | OpCode |
+--------+---------+----+-------------+--------+--------+
0        8         16   20            32   36   40      47
</pre>
</dd>
<dt><code class="code">SIL format: &lt;insn&gt; D1(B1),I2</code></dt>
<dd><pre class="verbatim">+------------------+----+-------------+-----------------+
|      OpCode      | B1 |      D1     |       I2        |
+------------------+----+-------------+-----------------+
0                  16   20            32               47
</pre>
</dd>
<dt><code class="code">SS format: &lt;insn&gt; D1(R1,B1),D2(B3),R3</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+----+------------+
| OpCode | R1 | R3 | B1 |     D1      | B2 |     D2     |
+--------+----+----+----+-------------+----+------------+
0        8    12   16   20            32   36          47
</pre>
</dd>
<dt><code class="code">SSE format: &lt;insn&gt; D1(B1),D2(B2)</code></dt>
<dd><pre class="verbatim">+------------------+----+-------------+----+------------+
|      OpCode      | B1 |     D1      | B2 |     D2     |
+------------------+----+-------------+----+------------+
0        8    12   16   20            32   36           47
</pre>
</dd>
<dt><code class="code">SSF format: &lt;insn&gt; D1(B1),D2(B2),R3</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+----+------------+
| OpCode | R3 |OpCd| B1 |     D1      | B2 |     D2     |
+--------+----+----+----+-------------+----+------------+
0        8    12   16   20            32   36           47
</pre>
</dd>
<dt><code class="code">VRV format: &lt;insn&gt; V1,D2(V2,B2),M3</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+----+------------+
| OpCode | V1 | V2 | B2 |     D2      | M3 |   Opcode   |
+--------+----+----+----+-------------+----+------------+
0        8    12   16   20            32   36           47
</pre>
</dd>
<dt><code class="code">VRI format: &lt;insn&gt; V1,V2,I3,M4,M5</code></dt>
<dd><pre class="verbatim">+--------+----+----+-------------+----+----+------------+
| OpCode | V1 | V2 |     I3      | M5 | M4 |   Opcode   |
+--------+----+----+-------------+----+----+------------+
0        8    12   16            28   32   36           47
</pre>
</dd>
<dt><code class="code">VRX format: &lt;insn&gt; V1,D2(R2,B2),M3</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+----+------------+
| OpCode | V1 | R2 | B2 |     D2      | M3 |   Opcode   |
+--------+----+----+----+-------------+----+------------+
0        8    12   16   20            32   36           47
</pre>
</dd>
<dt><code class="code">VRS format: &lt;insn&gt; R1,V3,D2(B2),M4</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+-------------+----+------------+
| OpCode | R1 | V3 | B2 |     D2      | M4 |   Opcode   |
+--------+----+----+----+-------------+----+------------+
0        8    12   16   20            32   36           47
</pre>
</dd>
<dt><code class="code">VRR format: &lt;insn&gt; V1,V2,V3,M4,M5,M6</code></dt>
<dd><pre class="verbatim">+--------+----+----+----+---+----+----+----+------------+
| OpCode | V1 | V2 | V3 |///| M6 | M5 | M4 |   Opcode   |
+--------+----+----+----+---+----+----+----+------------+
0        8    12   16       24   28   32   36           47
</pre>
</dd>
<dt><code class="code">VSI format: &lt;insn&gt; V1,D2(B2),I3</code></dt>
<dd><pre class="verbatim">+--------+---------+----+-------------+----+------------+
| OpCode |   I3    | B2 |     D2      | V1 |   Opcode   |
+--------+---------+----+-------------+----+------------+
0        8         16   20            32   36           47
</pre>
</dd>
</dl>

<p>For the complete list of all instruction format variants see the
Principles of Operation manuals.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="s390-Aliases.html">Instruction Aliases</a>, Previous: <a href="s390-Operands.html">Instruction Operands</a>, Up: <a href="s390-Syntax.html">Instruction syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
