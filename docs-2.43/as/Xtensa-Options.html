<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Xtensa Options (Using as)</title>

<meta name="description" content="Xtensa Options (Using as)">
<meta name="keywords" content="Xtensa Options (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Xtensa_002dDependent.html" rel="up" title="Xtensa-Dependent">
<link href="Xtensa-Syntax.html" rel="next" title="Xtensa Syntax">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Xtensa-Options">
<div class="nav-panel">
<p>
Next: <a href="Xtensa-Syntax.html" accesskey="n" rel="next">Assembler Syntax</a>, Up: <a href="Xtensa_002dDependent.html" accesskey="u" rel="up">Xtensa Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="Command_002dline-Options-2">9.56.1 Command-line Options</h4>

<dl class="table">
<dt id='index-_002d_002dtext_002dsection_002dliterals'><span><code class="code">--text-section-literals | --no-text-section-literals</code><a class="copiable-link" href='#index-_002d_002dtext_002dsection_002dliterals'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-_002d_002dno_002dtext_002dsection_002dliterals"></a>
<p>Control the treatment of literal pools.  The default is
&lsquo;<samp class="samp">--no-text-section-literals</samp>&rsquo;, which places literals in
separate sections in the output file.  This allows the literal pool to be
placed in a data RAM/ROM.  With &lsquo;<samp class="samp">--text-section-literals</samp>&rsquo;, the
literals are interspersed in the text section in order to keep them as
close as possible to their references.  This may be necessary for large
assembly files, where the literals would otherwise be out of range of the
<code class="code">L32R</code> instructions in the text section.  Literals are grouped into
pools following <code class="code">.literal_position</code> directives or preceding
<code class="code">ENTRY</code> instructions.  These options only affect literals referenced
via PC-relative <code class="code">L32R</code> instructions; literals for absolute mode
<code class="code">L32R</code> instructions are handled separately.
See <a class="xref" href="Literal-Directive.html">literal</a>.
</p>
</dd>
<dt id='index-_002d_002dauto_002dlitpools'><span><code class="code">--auto-litpools | --no-auto-litpools</code><a class="copiable-link" href='#index-_002d_002dauto_002dlitpools'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-_002d_002dno_002dauto_002dlitpools"></a>
<p>Control the treatment of literal pools.  The default is
&lsquo;<samp class="samp">--no-auto-litpools</samp>&rsquo;, which in the absence of
&lsquo;<samp class="samp">--text-section-literals</samp>&rsquo; places literals in separate sections
in the output file.  This allows the literal pool to be placed in a data
RAM/ROM.  With &lsquo;<samp class="samp">--auto-litpools</samp>&rsquo;, the literals are interspersed
in the text section in order to keep them as close as possible to their
references, explicit <code class="code">.literal_position</code> directives are not
required.  This may be necessary for very large functions, where single
literal pool at the beginning of the function may not be reachable by
<code class="code">L32R</code> instructions at the end.  These options only affect
literals referenced via PC-relative <code class="code">L32R</code> instructions; literals
for absolute mode <code class="code">L32R</code> instructions are handled separately.
When used together with &lsquo;<samp class="samp">--text-section-literals</samp>&rsquo;,
&lsquo;<samp class="samp">--auto-litpools</samp>&rsquo; takes precedence.
See <a class="xref" href="Literal-Directive.html">literal</a>.
</p>
</dd>
<dt id='index-_002d_002dabsolute_002dliterals'><span><code class="code">--absolute-literals | --no-absolute-literals</code><a class="copiable-link" href='#index-_002d_002dabsolute_002dliterals'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-_002d_002dno_002dabsolute_002dliterals"></a>
<p>Indicate to the assembler whether <code class="code">L32R</code> instructions use absolute
or PC-relative addressing.  If the processor includes the absolute
addressing option, the default is to use absolute <code class="code">L32R</code>
relocations.  Otherwise, only the PC-relative <code class="code">L32R</code> relocations
can be used.
</p>
</dd>
<dt id='index-_002d_002dtarget_002dalign'><span><code class="code">--target-align | --no-target-align</code><a class="copiable-link" href='#index-_002d_002dtarget_002dalign'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-_002d_002dno_002dtarget_002dalign"></a>
<p>Enable or disable automatic alignment to reduce branch penalties at some
expense in code size.  See <a class="xref" href="Xtensa-Automatic-Alignment.html">Automatic
Instruction Alignment</a>.  This optimization is enabled by default.  Note
that the assembler will always align instructions like <code class="code">LOOP</code> that
have fixed alignment requirements.
</p>
</dd>
<dt id='index-_002d_002dlongcalls'><span><code class="code">--longcalls | --no-longcalls</code><a class="copiable-link" href='#index-_002d_002dlongcalls'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-_002d_002dno_002dlongcalls"></a>
<p>Enable or disable transformation of call instructions to allow calls
across a greater range of addresses.  See <a class="xref" href="Xtensa-Call-Relaxation.html">Function Call Relaxation</a>.  This option should be used when call
targets can potentially be out of range.  It may degrade both code size
and performance, but the linker can generally optimize away the
unnecessary overhead when a call ends up within range.  The default is
&lsquo;<samp class="samp">--no-longcalls</samp>&rsquo;.
</p>
</dd>
<dt id='index-_002d_002dtransform'><span><code class="code">--transform | --no-transform</code><a class="copiable-link" href='#index-_002d_002dtransform'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-_002d_002dno_002dtransform"></a>
<p>Enable or disable all assembler transformations of Xtensa instructions,
including both relaxation and optimization.  The default is
&lsquo;<samp class="samp">--transform</samp>&rsquo;; &lsquo;<samp class="samp">--no-transform</samp>&rsquo; should only be used in the
rare cases when the instructions must be exactly as specified in the
assembly source.  Using &lsquo;<samp class="samp">--no-transform</samp>&rsquo; causes out of range
instruction operands to be errors.
</p>
</dd>
<dt id='index-_002d_002drename_002dsection'><span><code class="code">--rename-section <var class="var">oldname</var>=<var class="var">newname</var></code><a class="copiable-link" href='#index-_002d_002drename_002dsection'> &para;</a></span></dt>
<dd><p>Rename the <var class="var">oldname</var> section to <var class="var">newname</var>.  This option can be used
multiple times to rename multiple sections.
</p>
</dd>
<dt id='index-_002d_002dtrampolines'><span><code class="code">--trampolines | --no-trampolines</code><a class="copiable-link" href='#index-_002d_002dtrampolines'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-_002d_002dno_002dtrampolines"></a>
<p>Enable or disable transformation of jump instructions to allow jumps
across a greater range of addresses.  See <a class="xref" href="Xtensa-Jump-Relaxation.html">Jump Trampolines</a>.  This option should be used when jump targets can
potentially be out of range.  In the absence of such jumps this option
does not affect code size or performance.  The default is
&lsquo;<samp class="samp">--trampolines</samp>&rsquo;.
</p>
</dd>
<dt id='index-_002d_002dabi_002dwindowed'><span><code class="code">--abi-windowed | --abi-call0</code><a class="copiable-link" href='#index-_002d_002dabi_002dwindowed'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-_002d_002dabi_002dcall0"></a>
<p>Choose ABI tag written to the <code class="code">.xtensa.info</code> section.  ABI tag
indicates ABI of the assembly code.  A warning is issued by the linker
on an attempt to link object files with inconsistent ABI tags.
Default ABI is chosen by the Xtensa core configuration.
</p></dd>
</dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Xtensa-Syntax.html">Assembler Syntax</a>, Up: <a href="Xtensa_002dDependent.html">Xtensa Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
