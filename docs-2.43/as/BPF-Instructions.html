<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>BPF Instructions (Using as)</title>

<meta name="description" content="BPF Instructions (Using as)">
<meta name="keywords" content="BPF Instructions (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="BPF_002dDependent.html" rel="up" title="BPF-Dependent">
<link href="BPF-Directives.html" rel="prev" title="BPF Directives">


</head>

<body lang="en">
<div class="subsection-level-extent" id="BPF-Instructions">
<div class="nav-panel">
<p>
Previous: <a href="BPF-Directives.html" accesskey="p" rel="prev">BPF Directives</a>, Up: <a href="BPF_002dDependent.html" accesskey="u" rel="up">BPF Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="BPF-Instructions-1">9.7.5 BPF Instructions</h4>

<a class="index-entry-id" id="index-BPF-opcodes"></a>
<a class="index-entry-id" id="index-opcodes-for-BPF"></a>
<p>In the instruction descriptions below the following field descriptors
are used:
</p>
<dl class="table">
<dt><code class="code">rd</code></dt>
<dd><p>Destination general-purpose register whose role is to be the
destination of an operation.
</p></dd>
<dt><code class="code">rs</code></dt>
<dd><p>Source general-purpose register whose role is to be the source of an
operation.
</p></dd>
<dt><code class="code">disp16</code></dt>
<dd><p>16-bit signed PC-relative offset, measured in number of 64-bit words,
minus one.
</p></dd>
<dt><code class="code">disp32</code></dt>
<dd><p>32-bit signed PC-relative offset, measured in number of 64-bit words,
minus one.
</p></dd>
<dt><code class="code">offset16</code></dt>
<dd><p>Signed 16-bit immediate representing an offset in bytes.
</p></dd>
<dt><code class="code">disp16</code></dt>
<dd><p>Signed 16-bit immediate representing a displacement to a target,
measured in number of 64-bit words <em class="emph">minus one</em>.
</p></dd>
<dt><code class="code">disp32</code></dt>
<dd><p>Signed 32-bit immediate representing a displacement to a target,
measured in number of 64-bit words <em class="emph">minus one</em>.
</p></dd>
<dt><code class="code">imm32</code></dt>
<dd><p>Signed 32-bit immediate.
</p></dd>
<dt><code class="code">imm64</code></dt>
<dd><p>Signed 64-bit immediate.
</p></dd>
</dl>

<p>Note that the assembler allows to express the value for an immediate
using any numerical literal whose two&rsquo;s complement encoding fits in
the immediate field.  For example, <code class="code">-2</code>, <code class="code">0xfffffffe</code> and
<code class="code">4294967294</code> all denote the same encoded 32-bit immediate, whose
value may be then interpreted by different instructions as either as a
negative or a positive number.
</p>
<ul class="mini-toc">
<li><a href="#Arithmetic-instructions" accesskey="1">Arithmetic instructions</a></li>
<li><a href="#g_t32_002dbit-arithmetic-instructions" accesskey="2">32-bit arithmetic instructions</a></li>
<li><a href="#Endianness-conversion-instructions" accesskey="3">Endianness conversion instructions</a></li>
<li><a href="#Byte-swap-instructions" accesskey="4">Byte swap instructions</a></li>
<li><a href="#g_t64_002dbit-load-and-pseudo-maps" accesskey="5">64-bit load and pseudo maps</a></li>
<li><a href="#Load-instructions-for-socket-filters" accesskey="6">Load instructions for socket filters</a></li>
<li><a href="#Generic-load_002fstore-instructions" accesskey="7">Generic load/store instructions</a></li>
<li><a href="#Jump-instructions" accesskey="8">Jump instructions</a></li>
<li><a href="#g_t32_002dbit-jump-instructions" accesskey="9">32-bit jump instructions</a></li>
<li><a href="#Atomic-instructions">Atomic instructions</a></li>
<li><a href="#g_t32_002dbit-atomic-instructions">32-bit atomic instructions</a></li>
</ul>
<div class="subsubsection-level-extent" id="Arithmetic-instructions">
<h4 class="subsubsection subsection-level-set-subsubsection">9.7.5.1 Arithmetic instructions</h4>

<p>The destination register in these instructions act like an
accumulator.
</p>
<p>Note that in pseudoc syntax these instructions should use <code class="code">r</code>
registers.
</p>
<dl class="table">
<dt><code class="code">add rd, rs</code></dt>
<dt><code class="code">add rd, imm32</code></dt>
<dt><code class="code">rd += rs</code></dt>
<dt><code class="code">rd += imm32</code></dt>
<dd><p>64-bit arithmetic addition.
</p>
</dd>
<dt><code class="code">sub rd, rs</code></dt>
<dt><code class="code">sub rd, rs</code></dt>
<dt><code class="code">rd -= rs</code></dt>
<dt><code class="code">rd -= imm32</code></dt>
<dd><p>64-bit arithmetic subtraction.
</p>
</dd>
<dt><code class="code">mul rd, rs</code></dt>
<dt><code class="code">mul rd, imm32</code></dt>
<dt><code class="code">rd *= rs</code></dt>
<dt><code class="code">rd *= imm32</code></dt>
<dd><p>64-bit arithmetic multiplication.
</p>
</dd>
<dt><code class="code">div rd, rs</code></dt>
<dt><code class="code">div rd, imm32</code></dt>
<dt><code class="code">rd /= rs</code></dt>
<dt><code class="code">rd /= imm32</code></dt>
<dd><p>64-bit arithmetic integer division.
</p>
</dd>
<dt><code class="code">mod rd, rs</code></dt>
<dt><code class="code">mod rd, imm32</code></dt>
<dt><code class="code">rd %= rs</code></dt>
<dt><code class="code">rd %= imm32</code></dt>
<dd><p>64-bit integer remainder.
</p>
</dd>
<dt><code class="code">and rd, rs</code></dt>
<dt><code class="code">and rd, imm32</code></dt>
<dt><code class="code">rd &amp;= rs</code></dt>
<dt><code class="code">rd &amp;= imm32</code></dt>
<dd><p>64-bit bit-wise &ldquo;and&rdquo; operation.
</p>
</dd>
<dt><code class="code">or rd, rs</code></dt>
<dt><code class="code">or rd, imm32</code></dt>
<dt><code class="code">rd |= rs</code></dt>
<dt><code class="code">rd |= imm32</code></dt>
<dd><p>64-bit bit-wise &ldquo;or&rdquo; operation.
</p>
</dd>
<dt><code class="code">xor rd, imm32</code></dt>
<dt><code class="code">xor rd, rs</code></dt>
<dt><code class="code">rd ^= rs</code></dt>
<dt><code class="code">rd ^= imm32</code></dt>
<dd><p>64-bit bit-wise exclusive-or operation.
</p>
</dd>
<dt><code class="code">lsh rd, rs</code></dt>
<dt><code class="code">ldh rd, imm32</code></dt>
<dt><code class="code">rd &lt;&lt;= rs</code></dt>
<dt><code class="code">rd &lt;&lt;= imm32</code></dt>
<dd><p>64-bit left shift, by <code class="code">rs</code> or <code class="code">imm32</code> bits.
</p>
</dd>
<dt><code class="code">rsh %d, %s</code></dt>
<dt><code class="code">rsh rd, imm32</code></dt>
<dt><code class="code">rd &gt;&gt;= rs</code></dt>
<dt><code class="code">rd &gt;&gt;= imm32</code></dt>
<dd><p>64-bit right logical shift, by <code class="code">rs</code> or <code class="code">imm32</code> bits.
</p>
</dd>
<dt><code class="code">arsh rd, rs</code></dt>
<dt><code class="code">arsh rd, imm32</code></dt>
<dt><code class="code">rd s&gt;&gt;= rs</code></dt>
<dt><code class="code">rd s&gt;&gt;= imm32</code></dt>
<dd><p>64-bit right arithmetic shift, by <code class="code">rs</code> or <code class="code">imm32</code> bits.
</p>
</dd>
<dt><code class="code">neg rd</code></dt>
<dt><code class="code">rd = - rd</code></dt>
<dd><p>64-bit arithmetic negation.
</p>
</dd>
<dt><code class="code">mov rd, rs</code></dt>
<dt><code class="code">mov rd, imm32</code></dt>
<dt><code class="code">rd = rs</code></dt>
<dt><code class="code">rd = imm32</code></dt>
<dd><p>Move the 64-bit value of <code class="code">rs</code> in <code class="code">rd</code>, or load <code class="code">imm32</code>
in <code class="code">rd</code>.
</p>
</dd>
<dt><code class="code">movs rd, rs, 8</code></dt>
<dt><code class="code">rd = (s8) rs</code></dt>
<dd><p>Move the sign-extended 8-bit value in <code class="code">rs</code> to <code class="code">rd</code>.
</p>
</dd>
<dt><code class="code">movs rd, rs, 16</code></dt>
<dt><code class="code">rd = (s16) rs</code></dt>
<dd><p>Move the sign-extended 16-bit value in <code class="code">rs</code> to <code class="code">rd</code>.
</p>
</dd>
<dt><code class="code">movs rd, rs, 32</code></dt>
<dt><code class="code">rd = (s32) rs</code></dt>
<dd><p>Move the sign-extended 32-bit value in <code class="code">rs</code> to <code class="code">rd</code>.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="g_t32_002dbit-arithmetic-instructions">
<h4 class="subsubsection subsection-level-set-subsubsection">9.7.5.2 32-bit arithmetic instructions</h4>

<p>The destination register in these instructions act as an accumulator.
</p>
<p>Note that in pseudoc syntax these instructions should use <code class="code">w</code>
registers.  It is not allowed to mix <code class="code">w</code> and <code class="code">r</code> registers
in the same instruction.
</p>
<dl class="table">
<dt><code class="code">add32 rd, rs</code></dt>
<dt><code class="code">add32 rd, imm32</code></dt>
<dt><code class="code">rd += rs</code></dt>
<dt><code class="code">rd += imm32</code></dt>
<dd><p>32-bit arithmetic addition.
</p>
</dd>
<dt><code class="code">sub32 rd, rs</code></dt>
<dt><code class="code">sub32 rd, imm32</code></dt>
<dt><code class="code">rd -= rs</code></dt>
<dt><code class="code">rd += imm32</code></dt>
<dd><p>32-bit arithmetic subtraction.
</p>
</dd>
<dt><code class="code">mul32 rd, rs</code></dt>
<dt><code class="code">mul32 rd, imm32</code></dt>
<dt><code class="code">rd *= rs</code></dt>
<dt><code class="code">rd *= imm32</code></dt>
<dd><p>32-bit arithmetic multiplication.
</p>
</dd>
<dt><code class="code">div32 rd, rs</code></dt>
<dt><code class="code">div32 rd, imm32</code></dt>
<dt><code class="code">rd /= rs</code></dt>
<dt><code class="code">rd /= imm32</code></dt>
<dd><p>32-bit arithmetic integer division.
</p>
</dd>
<dt><code class="code">mod32 rd, rs</code></dt>
<dt><code class="code">mod32 rd, imm32</code></dt>
<dt><code class="code">rd %= rs</code></dt>
<dt><code class="code">rd %= imm32</code></dt>
<dd><p>32-bit integer remainder.
</p>
</dd>
<dt><code class="code">and32 rd, rs</code></dt>
<dt><code class="code">and32 rd, imm32</code></dt>
<dt><code class="code">rd &amp;= rs</code></dt>
<dt><code class="code">rd &amp;= imm32</code></dt>
<dd><p>32-bit bit-wise &ldquo;and&rdquo; operation.
</p>
</dd>
<dt><code class="code">or32 rd, rs</code></dt>
<dt><code class="code">or32 rd, imm32</code></dt>
<dt><code class="code">rd |= rs</code></dt>
<dt><code class="code">rd |= imm32</code></dt>
<dd><p>32-bit bit-wise &ldquo;or&rdquo; operation.
</p>
</dd>
<dt><code class="code">xor32 rd, rs</code></dt>
<dt><code class="code">xor32 rd, imm32</code></dt>
<dt><code class="code">rd ^= rs</code></dt>
<dt><code class="code">rd ^= imm32</code></dt>
<dd><p>32-bit bit-wise exclusive-or operation.
</p>
</dd>
<dt><code class="code">lsh32 rd, rs</code></dt>
<dt><code class="code">lsh32 rd, imm32</code></dt>
<dt><code class="code">rd &lt;&lt;= rs</code></dt>
<dt><code class="code">rd &lt;&lt;= imm32</code></dt>
<dd><p>32-bit left shift, by <code class="code">rs</code> or <code class="code">imm32</code> bits.
</p>
</dd>
<dt><code class="code">rsh32 rd, rs</code></dt>
<dt><code class="code">rsh32 rd, imm32</code></dt>
<dt><code class="code">rd &gt;&gt;= rs</code></dt>
<dt><code class="code">rd &gt;&gt;= imm32</code></dt>
<dd><p>32-bit right logical shift, by <code class="code">rs</code> or <code class="code">imm32</code> bits.
</p>
</dd>
<dt><code class="code">arsh32 rd, rs</code></dt>
<dt><code class="code">arsh32 rd, imm32</code></dt>
<dt><code class="code">rd s&gt;&gt;= rs</code></dt>
<dt><code class="code">rd s&gt;&gt;= imm32</code></dt>
<dd><p>32-bit right arithmetic shift, by <code class="code">rs</code> or <code class="code">imm32</code> bits.
</p>
</dd>
<dt><code class="code">neg32 rd</code></dt>
<dt><code class="code">rd = - rd</code></dt>
<dd><p>32-bit arithmetic negation.
</p>
</dd>
<dt><code class="code">mov32 rd, rs</code></dt>
<dt><code class="code">mov32 rd, imm32</code></dt>
<dt><code class="code">rd = rs</code></dt>
<dt><code class="code">rd = imm32</code></dt>
<dd><p>Move the 32-bit value of <code class="code">rs</code> in <code class="code">rd</code>, or load <code class="code">imm32</code>
in <code class="code">rd</code>.
</p>
</dd>
<dt><code class="code">mov32s rd, rs, 8</code></dt>
<dt><code class="code">rd = (s8) rs</code></dt>
<dd><p>Move the sign-extended 8-bit value in <code class="code">rs</code> to <code class="code">rd</code>.
</p>
</dd>
<dt><code class="code">mov32s rd, rs, 16</code></dt>
<dt><code class="code">rd = (s16) rs</code></dt>
<dd><p>Move the sign-extended 16-bit value in <code class="code">rs</code> to <code class="code">rd</code>.
</p>
</dd>
<dt><code class="code">mov32s rd, rs, 32</code></dt>
<dt><code class="code">rd = (s32) rs</code></dt>
<dd><p>Move the sign-extended 32-bit value in <code class="code">rs</code> to <code class="code">rd</code>.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="Endianness-conversion-instructions">
<h4 class="subsubsection subsection-level-set-subsubsection">9.7.5.3 Endianness conversion instructions</h4>

<dl class="table">
<dt><code class="code">endle rd, 16</code></dt>
<dt><code class="code">endle rd, 32</code></dt>
<dt><code class="code">endle rd, 64</code></dt>
<dt><code class="code">rd = le16 rd</code></dt>
<dt><code class="code">rd = le32 rd</code></dt>
<dt><code class="code">rd = le64 rd</code></dt>
<dd><p>Convert the 16-bit, 32-bit or 64-bit value in <code class="code">rd</code> to
little-endian and store it back in <code class="code">rd</code>.
</p></dd>
<dt><code class="code">endbe %d, 16</code></dt>
<dt><code class="code">endbe %d, 32</code></dt>
<dt><code class="code">endbe %d, 64</code></dt>
<dt><code class="code">rd = be16 rd</code></dt>
<dt><code class="code">rd = be32 rd</code></dt>
<dt><code class="code">rd = be64 rd</code></dt>
<dd><p>Convert the 16-bit, 32-bit or 64-bit value in <code class="code">rd</code> to big-endian
and store it back in <code class="code">rd</code>.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="Byte-swap-instructions">
<h4 class="subsubsection subsection-level-set-subsubsection">9.7.5.4 Byte swap instructions</h4>

<dl class="table">
<dt><code class="code">bswap rd, 16</code></dt>
<dt><code class="code">rd = bswap16 rd</code></dt>
<dd><p>Swap the least-significant 16-bit word in <code class="code">rd</code> with the
most-significant 16-bit word.
</p>
</dd>
<dt><code class="code">bswap rd, 32</code></dt>
<dt><code class="code">rd = bswap32 rd</code></dt>
<dd><p>Swap the least-significant 32-bit word in <code class="code">rd</code> with the
most-significant 32-bit word.
</p>
</dd>
<dt><code class="code">bswap rd, 64</code></dt>
<dt><code class="code">rd = bswap64 rd</code></dt>
<dd><p>Swap the least-significant 64-bit word in <code class="code">rd</code> with the
most-significant 64-bit word.
</p></dd>
</dl>


</div>
<div class="subsubsection-level-extent" id="g_t64_002dbit-load-and-pseudo-maps">
<h4 class="subsubsection subsection-level-set-subsubsection">9.7.5.5 64-bit load and pseudo maps</h4>

<dl class="table">
<dt><code class="code">lddw rd, imm64</code></dt>
<dt><code class="code">rd = imm64 ll</code></dt>
<dd><p>Load the given signed 64-bit immediate to the destination register
<code class="code">rd</code>.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="Load-instructions-for-socket-filters">
<h4 class="subsubsection subsection-level-set-subsubsection">9.7.5.6 Load instructions for socket filters</h4>

<p>The following instructions are intended to be used in socket filters,
and are therefore not general-purpose: they make assumptions on the
contents of several registers.  See the file
<samp class="file">Documentation/networking/filter.txt</samp> in the Linux kernel source
tree for more information.
</p>
<p>Absolute loads:
</p>
<dl class="table">
<dt><code class="code">ldabsw imm32</code></dt>
<dt><code class="code">r0 = *(u32 *) skb[imm32]</code></dt>
<dd><p>Absolute 32-bit load.
</p>
</dd>
<dt><code class="code">ldabsh imm32</code></dt>
<dt><code class="code">r0 = *(u16 *) skb[imm32]</code></dt>
<dd><p>Absolute 16-bit load.
</p>
</dd>
<dt><code class="code">ldabsb imm32</code></dt>
<dt><code class="code">r0 = *(u8 *) skb[imm32]</code></dt>
<dd><p>Absolute 8-bit load.
</p></dd>
</dl>

<p>Indirect loads:
</p>
<dl class="table">
<dt><code class="code">ldindw rs, imm32</code></dt>
<dt><code class="code">r0 = *(u32 *) skb[rs + imm32]</code></dt>
<dd><p>Indirect 32-bit load.
</p>
</dd>
<dt><code class="code">ldindh rs, imm32</code></dt>
<dt><code class="code">r0 = *(u16 *) skb[rs + imm32]</code></dt>
<dd><p>Indirect 16-bit load.
</p>
</dd>
<dt><code class="code">ldindb %s, imm32</code></dt>
<dt><code class="code">r0 = *(u8 *) skb[rs + imm32]</code></dt>
<dd><p>Indirect 8-bit load.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="Generic-load_002fstore-instructions">
<h4 class="subsubsection subsection-level-set-subsubsection">9.7.5.7 Generic load/store instructions</h4>

<p>General-purpose load and store instructions are provided for several
word sizes.
</p>
<p>Load to register instructions:
</p>
<dl class="table">
<dt><code class="code">ldxdw rd, [rs + offset16]</code></dt>
<dt><code class="code">rd = *(u64 *) (rs + offset16)</code></dt>
<dd><p>Generic 64-bit load.
</p>
</dd>
<dt><code class="code">ldxw rd, [rs + offset16]</code></dt>
<dt><code class="code">rd = *(u32 *) (rs + offset16)</code></dt>
<dd><p>Generic 32-bit load.
</p>
</dd>
<dt><code class="code">ldxh rd, [rs + offset16]</code></dt>
<dt><code class="code">rd = *(u16 *) (rs + offset16)</code></dt>
<dd><p>Generic 16-bit load.
</p>
</dd>
<dt><code class="code">ldxb rd, [rs + offset16]</code></dt>
<dt><code class="code">rd = *(u8 *) (rs + offset16)</code></dt>
<dd><p>Generic 8-bit load.
</p></dd>
</dl>

<p>Signed load to register instructions:
</p>
<dl class="table">
<dt><code class="code">ldxsdw rd, [rs + offset16]</code></dt>
<dt><code class="code">rd = *(s64 *) (rs + offset16)</code></dt>
<dd><p>Generic 64-bit signed load.
</p>
</dd>
<dt><code class="code">ldxsw rd, [rs + offset16]</code></dt>
<dt><code class="code">rd = *(s32 *) (rs + offset16)</code></dt>
<dd><p>Generic 32-bit signed load.
</p>
</dd>
<dt><code class="code">ldxsh rd, [rs + offset16]</code></dt>
<dt><code class="code">rd = *(s16 *) (rs + offset16)</code></dt>
<dd><p>Generic 16-bit signed load.
</p>
</dd>
<dt><code class="code">ldxsb rd, [rs + offset16]</code></dt>
<dt><code class="code">rd = *(s8 *) (rs + offset16)</code></dt>
<dd><p>Generic 8-bit signed load.
</p></dd>
</dl>

<p>Store from register instructions:
</p>
<dl class="table">
<dt><code class="code">stxdw [rd + offset16], %s</code></dt>
<dt><code class="code">*(u64 *) (rd + offset16)</code></dt>
<dd><p>Generic 64-bit store.
</p>
</dd>
<dt><code class="code">stxw [rd + offset16], %s</code></dt>
<dt><code class="code">*(u32 *) (rd + offset16)</code></dt>
<dd><p>Generic 32-bit store.
</p>
</dd>
<dt><code class="code">stxh [rd + offset16], %s</code></dt>
<dt><code class="code">*(u16 *) (rd + offset16)</code></dt>
<dd><p>Generic 16-bit store.
</p>
</dd>
<dt><code class="code">stxb [rd + offset16], %s</code></dt>
<dt><code class="code">*(u8 *) (rd + offset16)</code></dt>
<dd><p>Generic 8-bit store.
</p></dd>
</dl>

<p>Store from immediates instructions:
</p>
<dl class="table">
<dt><code class="code">stdw [rd + offset16], imm32</code></dt>
<dt><code class="code">*(u64 *) (rd + offset16) = imm32</code></dt>
<dd><p>Store immediate as 64-bit.
</p>
</dd>
<dt><code class="code">stw [rd + offset16], imm32</code></dt>
<dt><code class="code">*(u32 *) (rd + offset16) = imm32</code></dt>
<dd><p>Store immediate as 32-bit.
</p>
</dd>
<dt><code class="code">sth [rd + offset16], imm32</code></dt>
<dt><code class="code">*(u16 *) (rd + offset16) = imm32</code></dt>
<dd><p>Store immediate as 16-bit.
</p>
</dd>
<dt><code class="code">stb [rd + offset16], imm32</code></dt>
<dt><code class="code">*(u8 *) (rd + offset16) = imm32</code></dt>
<dd><p>Store immediate as 8-bit.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="Jump-instructions">
<h4 class="subsubsection subsection-level-set-subsubsection">9.7.5.8 Jump instructions</h4>

<p>eBPF provides the following compare-and-jump instructions, which
compare the values of the two given registers, or the values of a
register and an immediate, and perform a branch in case the comparison
holds true.
</p>
<dl class="table">
<dt><code class="code">ja disp16</code></dt>
<dt><code class="code">goto disp16</code></dt>
<dd><p>Jump-always.
</p>
</dd>
<dt><code class="code">jal disp32</code></dt>
<dt><code class="code">gotol disp32</code></dt>
<dd><p>Jump-always, long range.
</p>
</dd>
<dt><code class="code">jeq rd, rs, disp16</code></dt>
<dt><code class="code">jeq rd, imm32, disp16</code></dt>
<dt><code class="code">if rd == rs goto disp16</code></dt>
<dt><code class="code">if rd == imm32 goto disp16</code></dt>
<dd><p>Jump if equal, unsigned.
</p>
</dd>
<dt><code class="code">jgt rd, rs, disp16</code></dt>
<dt><code class="code">jgt rd, imm32, disp16</code></dt>
<dt><code class="code">if rd &gt; rs goto disp16</code></dt>
<dt><code class="code">if rd &gt; imm32 goto disp16</code></dt>
<dd><p>Jump if greater, unsigned.
</p>
</dd>
<dt><code class="code">jge rd, rs, disp16</code></dt>
<dt><code class="code">jge rd, imm32, disp16</code></dt>
<dt><code class="code">if rd &gt;= rs goto disp16</code></dt>
<dt><code class="code">if rd &gt;= imm32 goto disp16</code></dt>
<dd><p>Jump if greater or equal.
</p>
</dd>
<dt><code class="code">jlt rd, rs, disp16</code></dt>
<dt><code class="code">jlt rd, imm32, disp16</code></dt>
<dt><code class="code">if rd &lt; rs goto disp16</code></dt>
<dt><code class="code">if rd &lt; imm32 goto disp16</code></dt>
<dd><p>Jump if lesser.
</p>
</dd>
<dt><code class="code">jle rd , rs, disp16</code></dt>
<dt><code class="code">jle rd, imm32, disp16</code></dt>
<dt><code class="code">if rd &lt;= rs goto disp16</code></dt>
<dt><code class="code">if rd &lt;= imm32 goto disp16</code></dt>
<dd><p>Jump if lesser or equal.
</p>
</dd>
<dt><code class="code">jset rd, rs, disp16</code></dt>
<dt><code class="code">jset rd, imm32, disp16</code></dt>
<dt><code class="code">if rd &amp; rs goto disp16</code></dt>
<dt><code class="code">if rd &amp; imm32 goto disp16</code></dt>
<dd><p>Jump if signed equal.
</p>
</dd>
<dt><code class="code">jne rd, rs, disp16</code></dt>
<dt><code class="code">jne rd, imm32, disp16</code></dt>
<dt><code class="code">if rd != rs goto disp16</code></dt>
<dt><code class="code">if rd != imm32 goto disp16</code></dt>
<dd><p>Jump if not equal.
</p>
</dd>
<dt><code class="code">jsgt rd, rs, disp16</code></dt>
<dt><code class="code">jsgt rd, imm32, disp16</code></dt>
<dt><code class="code">if rd s&gt; rs goto disp16</code></dt>
<dt><code class="code">if rd s&gt; imm32 goto disp16</code></dt>
<dd><p>Jump if signed greater.
</p>
</dd>
<dt><code class="code">jsge rd, rs, disp16</code></dt>
<dt><code class="code">jsge rd, imm32, disp16</code></dt>
<dt><code class="code">if rd s&gt;= rd goto disp16</code></dt>
<dt><code class="code">if rd s&gt;= imm32 goto disp16</code></dt>
<dd><p>Jump if signed greater or equal.
</p>
</dd>
<dt><code class="code">jslt rd, rs, disp16</code></dt>
<dt><code class="code">jslt rd, imm32, disp16</code></dt>
<dt><code class="code">if rd s&lt; rs goto disp16</code></dt>
<dt><code class="code">if rd s&lt; imm32 goto disp16</code></dt>
<dd><p>Jump if signed lesser.
</p>
</dd>
<dt><code class="code">jsle rd, rs, disp16</code></dt>
<dt><code class="code">jsle rd, imm32, disp16</code></dt>
<dt><code class="code">if rd s&lt;= rs goto disp16</code></dt>
<dt><code class="code">if rd s&lt;= imm32 goto disp16</code></dt>
<dd><p>Jump if signed lesser or equal.
</p></dd>
</dl>

<p>A call instruction is provided in order to perform calls to other eBPF
functions, or to external kernel helpers:
</p>
<dl class="table">
<dt><code class="code">call disp32</code></dt>
<dt><code class="code">call imm32</code></dt>
<dd><p>Jump and link to the offset <em class="emph">disp32</em>, or to the kernel helper
function identified by <em class="emph">imm32</em>.
</p></dd>
</dl>

<p>Finally:
</p>
<dl class="table">
<dt><code class="code">exit</code></dt>
<dd><p>Terminate the eBPF program.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="g_t32_002dbit-jump-instructions">
<h4 class="subsubsection subsection-level-set-subsubsection">9.7.5.9 32-bit jump instructions</h4>

<p>eBPF provides the following compare-and-jump instructions, which
compare the 32-bit values of the two given registers, or the values of
a register and an immediate, and perform a branch in case the
comparison holds true.
</p>
<p>These instructions are only available in BPF v3 or later.
</p>
<dl class="table">
<dt><code class="code">jeq32 rd, rs, disp16</code></dt>
<dt><code class="code">jeq32 rd, imm32, disp16</code></dt>
<dt><code class="code">if rd == rs goto disp16</code></dt>
<dt><code class="code">if rd == imm32 goto disp16</code></dt>
<dd><p>Jump if equal, unsigned.
</p>
</dd>
<dt><code class="code">jgt32 rd, rs, disp16</code></dt>
<dt><code class="code">jgt32 rd, imm32, disp16</code></dt>
<dt><code class="code">if rd &gt; rs goto disp16</code></dt>
<dt><code class="code">if rd &gt; imm32 goto disp16</code></dt>
<dd><p>Jump if greater, unsigned.
</p>
</dd>
<dt><code class="code">jge32 rd, rs, disp16</code></dt>
<dt><code class="code">jge32 rd, imm32, disp16</code></dt>
<dt><code class="code">if rd &gt;= rs goto disp16</code></dt>
<dt><code class="code">if rd &gt;= imm32 goto disp16</code></dt>
<dd><p>Jump if greater or equal.
</p>
</dd>
<dt><code class="code">jlt32 rd, rs, disp16</code></dt>
<dt><code class="code">jlt32 rd, imm32, disp16</code></dt>
<dt><code class="code">if rd &lt; rs goto disp16</code></dt>
<dt><code class="code">if rd &lt; imm32 goto disp16</code></dt>
<dd><p>Jump if lesser.
</p>
</dd>
<dt><code class="code">jle32 rd , rs, disp16</code></dt>
<dt><code class="code">jle32 rd, imm32, disp16</code></dt>
<dt><code class="code">if rd &lt;= rs goto disp16</code></dt>
<dt><code class="code">if rd &lt;= imm32 goto disp16</code></dt>
<dd><p>Jump if lesser or equal.
</p>
</dd>
<dt><code class="code">jset32 rd, rs, disp16</code></dt>
<dt><code class="code">jset32 rd, imm32, disp16</code></dt>
<dt><code class="code">if rd &amp; rs goto disp16</code></dt>
<dt><code class="code">if rd &amp; imm32 goto disp16</code></dt>
<dd><p>Jump if signed equal.
</p>
</dd>
<dt><code class="code">jne32 rd, rs, disp16</code></dt>
<dt><code class="code">jne32 rd, imm32, disp16</code></dt>
<dt><code class="code">if rd != rs goto disp16</code></dt>
<dt><code class="code">if rd != imm32 goto disp16</code></dt>
<dd><p>Jump if not equal.
</p>
</dd>
<dt><code class="code">jsgt32 rd, rs, disp16</code></dt>
<dt><code class="code">jsgt32 rd, imm32, disp16</code></dt>
<dt><code class="code">if rd s&gt; rs goto disp16</code></dt>
<dt><code class="code">if rd s&gt; imm32 goto disp16</code></dt>
<dd><p>Jump if signed greater.
</p>
</dd>
<dt><code class="code">jsge32 rd, rs, disp16</code></dt>
<dt><code class="code">jsge32 rd, imm32, disp16</code></dt>
<dt><code class="code">if rd s&gt;= rd goto disp16</code></dt>
<dt><code class="code">if rd s&gt;= imm32 goto disp16</code></dt>
<dd><p>Jump if signed greater or equal.
</p>
</dd>
<dt><code class="code">jslt32 rd, rs, disp16</code></dt>
<dt><code class="code">jslt32 rd, imm32, disp16</code></dt>
<dt><code class="code">if rd s&lt; rs goto disp16</code></dt>
<dt><code class="code">if rd s&lt; imm32 goto disp16</code></dt>
<dd><p>Jump if signed lesser.
</p>
</dd>
<dt><code class="code">jsle32 rd, rs, disp16</code></dt>
<dt><code class="code">jsle32 rd, imm32, disp16</code></dt>
<dt><code class="code">if rd s&lt;= rs goto disp16</code></dt>
<dt><code class="code">if rd s&lt;= imm32 goto disp16</code></dt>
<dd><p>Jump if signed lesser or equal.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="Atomic-instructions">
<h4 class="subsubsection subsection-level-set-subsubsection">9.7.5.10 Atomic instructions</h4>

<p>Atomic exchange instructions are provided in two flavors: one for
compare-and-swap, one for unconditional exchange.
</p>
<dl class="table">
<dt><code class="code">acmp [rd + offset16], rs</code></dt>
<dt><code class="code">r0 = cmpxchg_64 (rd + offset16, r0, rs)</code></dt>
<dd><p>Atomic compare-and-swap.  Compares value in <code class="code">r0</code> to value
addressed by <code class="code">rd + offset16</code>.  On match, the value addressed by
<code class="code">rd + offset16</code> is replaced with the value in <code class="code">rs</code>.
Regardless, the value that was at <code class="code">rd + offset16</code> is
zero-extended and loaded into <code class="code">r0</code>.
</p>
</dd>
<dt><code class="code">axchg [rd + offset16], rs</code></dt>
<dt><code class="code">rs = xchg_64 (rd + offset16, rs)</code></dt>
<dd><p>Atomic exchange.  Atomically exchanges the value in <code class="code">rs</code> with
the value addressed by <code class="code">rd + offset16</code>.
</p></dd>
</dl>

<p>The following instructions provide atomic arithmetic operations.
</p>
<dl class="table">
<dt><code class="code">aadd [rd + offset16], rs</code></dt>
<dt><code class="code">lock *(u64 *)(rd + offset16) = rs</code></dt>
<dd><p>Atomic add instruction.
</p>
</dd>
<dt><code class="code">aor [rd + offset16], rs</code></dt>
<dt><code class="code">lock *(u64 *) (rd + offset16) |= rs</code></dt>
<dd><p>Atomic or instruction.
</p>
</dd>
<dt><code class="code">aand [rd + offset16], rs</code></dt>
<dt><code class="code">lock *(u64 *) (rd + offset16) &amp;= rs</code></dt>
<dd><p>Atomic and instruction.
</p>
</dd>
<dt><code class="code">axor [rd + offset16], rs</code></dt>
<dt><code class="code">lock *(u64 *) (rd + offset16) ^= rs</code></dt>
<dd><p>Atomic xor instruction.
</p></dd>
</dl>

<p>The following variants perform fetching before the atomic operation.
</p>
<dl class="table">
<dt><code class="code">afadd [rd + offset16], rs</code></dt>
<dt><code class="code">rs = atomic_fetch_add ((u64 *)(rd + offset16), rs)</code></dt>
<dd><p>Atomic fetch-and-add instruction.
</p>
</dd>
<dt><code class="code">afor [rd + offset16], rs</code></dt>
<dt><code class="code">rs = atomic_fetch_or ((u64 *)(rd + offset16), rs)</code></dt>
<dd><p>Atomic fetch-and-or instruction.
</p>
</dd>
<dt><code class="code">afand [rd + offset16], rs</code></dt>
<dt><code class="code">rs = atomic_fetch_and ((u64 *)(rd + offset16), rs)</code></dt>
<dd><p>Atomic fetch-and-and instruction.
</p>
</dd>
<dt><code class="code">afxor [rd + offset16], rs</code></dt>
<dt><code class="code">rs = atomic_fetch_xor ((u64 *)(rd + offset16), rs)</code></dt>
<dd><p>Atomic fetch-and-or instruction.
</p></dd>
</dl>

<p>The above instructions were introduced in the V3 of the BPF
instruction set.  The following instruction is supported for backwards
compatibility:
</p>
<dl class="table">
<dt><code class="code">xadddw [rd + offset16], rs</code></dt>
<dd><p>Alias to <code class="code">aadd</code>.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="g_t32_002dbit-atomic-instructions">
<h4 class="subsubsection subsection-level-set-subsubsection">9.7.5.11 32-bit atomic instructions</h4>

<p>32-bit atomic exchange instructions are provided in two flavors: one
for compare-and-swap, one for unconditional exchange.
</p>
<dl class="table">
<dt><code class="code">acmp32 [rd + offset16], rs</code></dt>
<dt><code class="code">w0 = cmpxchg32_32 (rd + offset16, w0, ws)</code></dt>
<dd><p>Atomic compare-and-swap.  Compares value in <code class="code">w0</code> to value
addressed by <code class="code">rd + offset16</code>.  On match, the value addressed by
<code class="code">rd + offset16</code> is replaced with the value in <code class="code">ws</code>.
Regardless, the value that was at <code class="code">rd + offset16</code> is
zero-extended and loaded into <code class="code">w0</code>.
</p>
</dd>
<dt><code class="code">axchg [rd + offset16], rs</code></dt>
<dt><code class="code">ws = xchg32_32 (rd + offset16, ws)</code></dt>
<dd><p>Atomic exchange.  Atomically exchanges the value in <code class="code">ws</code> with
the value addressed by <code class="code">rd + offset16</code>.
</p></dd>
</dl>

<p>The following instructions provide 32-bit atomic arithmetic operations.
</p>
<dl class="table">
<dt><code class="code">aadd32 [rd + offset16], rs</code></dt>
<dt><code class="code">lock *(u32 *)(rd + offset16) = rs</code></dt>
<dd><p>Atomic add instruction.
</p>
</dd>
<dt><code class="code">aor32 [rd + offset16], rs</code></dt>
<dt><code class="code">lock *(u32 *) (rd + offset16) |= rs</code></dt>
<dd><p>Atomic or instruction.
</p>
</dd>
<dt><code class="code">aand32 [rd + offset16], rs</code></dt>
<dt><code class="code">lock *(u32 *) (rd + offset16) &amp;= rs</code></dt>
<dd><p>Atomic and instruction.
</p>
</dd>
<dt><code class="code">axor32 [rd + offset16], rs</code></dt>
<dt><code class="code">lock *(u32 *) (rd + offset16) ^= rs</code></dt>
<dd><p>Atomic xor instruction
</p></dd>
</dl>

<p>The following variants perform fetching before the atomic operation.
</p>
<dl class="table">
<dt><code class="code">afadd32 [dr + offset16], rs</code></dt>
<dt><code class="code">ws = atomic_fetch_add ((u32 *)(rd + offset16), ws)</code></dt>
<dd><p>Atomic fetch-and-add instruction.
</p>
</dd>
<dt><code class="code">afor32 [dr + offset16], rs</code></dt>
<dt><code class="code">ws = atomic_fetch_or ((u32 *)(rd + offset16), ws)</code></dt>
<dd><p>Atomic fetch-and-or instruction.
</p>
</dd>
<dt><code class="code">afand32 [dr + offset16], rs</code></dt>
<dt><code class="code">ws = atomic_fetch_and ((u32 *)(rd + offset16), ws)</code></dt>
<dd><p>Atomic fetch-and-and instruction.
</p>
</dd>
<dt><code class="code">afxor32 [dr + offset16], rs</code></dt>
<dt><code class="code">ws = atomic_fetch_xor ((u32 *)(rd + offset16), ws)</code></dt>
<dd><p>Atomic fetch-and-or instruction
</p></dd>
</dl>

<p>The above instructions were introduced in the V3 of the BPF
instruction set.  The following instruction is supported for backwards
compatibility:
</p>
<dl class="table">
<dt><code class="code">xaddw [rd + offset16], rs</code></dt>
<dd><p>Alias to <code class="code">aadd32</code>.
</p></dd>
</dl>


</div>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="BPF-Directives.html">BPF Directives</a>, Up: <a href="BPF_002dDependent.html">BPF Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
