<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Z80 Opcodes (Using as)</title>

<meta name="description" content="Z80 Opcodes (Using as)">
<meta name="keywords" content="Z80 Opcodes (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Z80_002dDependent.html" rel="up" title="Z80-Dependent">
<link href="Z80-Directives.html" rel="prev" title="Z80 Directives">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Z80-Opcodes">
<div class="nav-panel">
<p>
Previous: <a href="Z80-Directives.html" accesskey="p" rel="prev">Z80 Assembler Directives</a>, Up: <a href="Z80_002dDependent.html" accesskey="u" rel="up">Z80 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="Opcodes-21">9.57.5 Opcodes</h4>
<p>In line with common practice, Z80 mnemonics are used for the Z80,
Z80N, Z180, eZ80, Ascii R800 and the GameBoy Z80.
</p>
<p>In many instructions it is possible to use one of the half index
registers (&lsquo;<samp class="samp">ixl</samp>&rsquo;,&lsquo;<samp class="samp">ixh</samp>&rsquo;,&lsquo;<samp class="samp">iyl</samp>&rsquo;,&lsquo;<samp class="samp">iyh</samp>&rsquo;) in stead of an
8-bit general purpose register. This yields instructions that are
documented on the eZ80 and the R800, undocumented on the Z80 and
unsupported on the Z180.
Similarly <code class="code">in f,(c)</code> is documented on the R800, undocumented on
the Z80 and unsupported on the Z180 and the eZ80.
</p>
<p>The assembler also supports the following undocumented Z80-instructions,
that have not been adopted in any other instruction set:
</p><dl class="table">
<dt><code class="code">out (c),0</code></dt>
<dd><p>Sends zero to the port pointed to by register <code class="code">C</code>.
</p>
</dd>
<dt><code class="code">sli <var class="var">m</var></code></dt>
<dd><p>Equivalent to <code class="code"><var class="var">m</var> = (<var class="var">m</var>&lt;&lt;1)+1</code>, the operand <var class="var">m</var> can
be any operand that is valid for &lsquo;<samp class="samp">sla</samp>&rsquo;. One can use &lsquo;<samp class="samp">sll</samp>&rsquo; as a
synonym for &lsquo;<samp class="samp">sli</samp>&rsquo;.
</p>
</dd>
<dt><code class="code"><var class="var">op</var> (ix+<var class="var">d</var>), <var class="var">r</var></code></dt>
<dd><p>This is equivalent to
</p>
<div class="example">
<pre class="example-preformatted">ld <var class="var">r</var>, (ix+<var class="var">d</var>)
<var class="var">op</var> <var class="var">r</var>
ld (ix+<var class="var">d</var>), <var class="var">r</var>
</pre></div>

<p>The operation &lsquo;<samp class="samp"><var class="var">op</var></samp>&rsquo; may be any of &lsquo;<samp class="samp">res <var class="var">b</var>,</samp>&rsquo;,
&lsquo;<samp class="samp">set <var class="var">b</var>,</samp>&rsquo;, &lsquo;<samp class="samp">rl</samp>&rsquo;, &lsquo;<samp class="samp">rlc</samp>&rsquo;, &lsquo;<samp class="samp">rr</samp>&rsquo;, &lsquo;<samp class="samp">rrc</samp>&rsquo;,
&lsquo;<samp class="samp">sla</samp>&rsquo;, &lsquo;<samp class="samp">sli</samp>&rsquo;, &lsquo;<samp class="samp">sra</samp>&rsquo; and &lsquo;<samp class="samp">srl</samp>&rsquo;, and the register
&lsquo;<samp class="samp"><var class="var">r</var></samp>&rsquo; may be any of &lsquo;<samp class="samp">a</samp>&rsquo;, &lsquo;<samp class="samp">b</samp>&rsquo;, &lsquo;<samp class="samp">c</samp>&rsquo;, &lsquo;<samp class="samp">d</samp>&rsquo;,
&lsquo;<samp class="samp">e</samp>&rsquo;, &lsquo;<samp class="samp">h</samp>&rsquo; and &lsquo;<samp class="samp">l</samp>&rsquo;.
</p>
</dd>
<dt><code class="code"><var class="var">op</var> (iy+<var class="var">d</var>), <var class="var">r</var></code></dt>
<dd><p>As above, but with &lsquo;<samp class="samp">iy</samp>&rsquo; instead of &lsquo;<samp class="samp">ix</samp>&rsquo;.
</p></dd>
</dl>

<p>The web site at <a class="uref" href="http://www.z80.info">http://www.z80.info</a> is a good starting place to
find more information on programming the Z80.
</p>
<p>You may enable or disable any of these instructions for any target CPU
even this instruction is not supported by any real CPU of this type.
Useful for custom CPU cores.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Z80-Directives.html">Z80 Assembler Directives</a>, Up: <a href="Z80_002dDependent.html">Z80 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
