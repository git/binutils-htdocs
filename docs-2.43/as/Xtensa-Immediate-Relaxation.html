<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Xtensa Immediate Relaxation (Using as)</title>

<meta name="description" content="Xtensa Immediate Relaxation (Using as)">
<meta name="keywords" content="Xtensa Immediate Relaxation (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Xtensa-Relaxation.html" rel="up" title="Xtensa Relaxation">
<link href="Xtensa-Jump-Relaxation.html" rel="prev" title="Xtensa Jump Relaxation">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Xtensa-Immediate-Relaxation">
<div class="nav-panel">
<p>
Previous: <a href="Xtensa-Jump-Relaxation.html" accesskey="p" rel="prev">Jump Relaxation</a>, Up: <a href="Xtensa-Relaxation.html" accesskey="u" rel="up">Xtensa Relaxation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection subsection-level-set-subsubsection" id="Other-Immediate-Field-Relaxation">9.56.4.4 Other Immediate Field Relaxation</h4>
<a class="index-entry-id" id="index-immediate-fields_002c-relaxation"></a>
<a class="index-entry-id" id="index-relaxation-of-immediate-fields"></a>

<p>The assembler normally performs the following other relaxations.  They
can be disabled by using underscore prefixes (see <a class="pxref" href="Xtensa-Opcodes.html">Opcode Names</a>), the &lsquo;<samp class="samp">--no-transform</samp>&rsquo; command-line option
(see <a class="pxref" href="Xtensa-Options.html">Command-line Options</a>), or the
<code class="code">no-transform</code> directive (see <a class="pxref" href="Transform-Directive.html">transform</a>).
</p>
<a class="index-entry-id" id="index-MOVI-instructions_002c-relaxation"></a>
<a class="index-entry-id" id="index-relaxation-of-MOVI-instructions"></a>
<p>The <code class="code">MOVI</code> machine instruction can only materialize values in the
range from -2048 to 2047.  Values outside this range are best
materialized with <code class="code">L32R</code> instructions.  Thus:
</p>
<div class="example smallexample">
<pre class="example-preformatted">    movi a0, 100000
</pre></div>

<p>is assembled into the following machine code:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">    .literal .L1, 100000
    l32r a0, .L1
</pre></div></div>

<a class="index-entry-id" id="index-L8UI-instructions_002c-relaxation"></a>
<a class="index-entry-id" id="index-L16SI-instructions_002c-relaxation"></a>
<a class="index-entry-id" id="index-L16UI-instructions_002c-relaxation"></a>
<a class="index-entry-id" id="index-L32I-instructions_002c-relaxation"></a>
<a class="index-entry-id" id="index-relaxation-of-L8UI-instructions"></a>
<a class="index-entry-id" id="index-relaxation-of-L16SI-instructions"></a>
<a class="index-entry-id" id="index-relaxation-of-L16UI-instructions"></a>
<a class="index-entry-id" id="index-relaxation-of-L32I-instructions"></a>
<p>The <code class="code">L8UI</code> machine instruction can only be used with immediate
offsets in the range from 0 to 255. The <code class="code">L16SI</code> and <code class="code">L16UI</code>
machine instructions can only be used with offsets from 0 to 510.  The
<code class="code">L32I</code> machine instruction can only be used with offsets from 0 to
1020.  A load offset outside these ranges can be materialized with
an <code class="code">L32R</code> instruction if the destination register of the load
is different than the source address register.  For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">    l32i a1, a0, 2040
</pre></div>

<p>is translated to:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">    .literal .L1, 2040
    l32r a1, .L1
</pre></div><div class="group"><pre class="example-preformatted">    add a1, a0, a1
    l32i a1, a1, 0
</pre></div></div>

<p>If the load destination and source address register are the same, an
out-of-range offset causes an error.
</p>
<a class="index-entry-id" id="index-ADDI-instructions_002c-relaxation"></a>
<a class="index-entry-id" id="index-relaxation-of-ADDI-instructions"></a>
<p>The Xtensa <code class="code">ADDI</code> instruction only allows immediate operands in the
range from -128 to 127.  There are a number of alternate instruction
sequences for the <code class="code">ADDI</code> operation.  First, if the
immediate is 0, the <code class="code">ADDI</code> will be turned into a <code class="code">MOV.N</code>
instruction (or the equivalent <code class="code">OR</code> instruction if the code density
option is not available).  If the <code class="code">ADDI</code> immediate is outside of
the range -128 to 127, but inside the range -32896 to 32639, an
<code class="code">ADDMI</code> instruction or <code class="code">ADDMI</code>/<code class="code">ADDI</code> sequence will be
used.  Finally, if the immediate is outside of this range and a free
register is available, an <code class="code">L32R</code>/<code class="code">ADD</code> sequence will be used
with a literal allocated from the literal pool.
</p>
<p>For example:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">    addi    a5, a6, 0
    addi    a5, a6, 512
</pre></div><div class="group"><pre class="example-preformatted">    addi    a5, a6, 513
    addi    a5, a6, 50000
</pre></div></div>

<p>is assembled into the following:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">    .literal .L1, 50000
    mov.n   a5, a6
</pre></div><pre class="example-preformatted">    addmi   a5, a6, 0x200
    addmi   a5, a6, 0x200
    addi    a5, a5, 1
</pre><div class="group"><pre class="example-preformatted">    l32r    a5, .L1
    add     a5, a6, a5
</pre></div></div>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Xtensa-Jump-Relaxation.html">Jump Relaxation</a>, Up: <a href="Xtensa-Relaxation.html">Xtensa Relaxation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
