<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>i386-Prefixes (Using as)</title>

<meta name="description" content="i386-Prefixes (Using as)">
<meta name="keywords" content="i386-Prefixes (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="i386_002dDependent.html" rel="up" title="i386-Dependent">
<link href="i386_002dMemory.html" rel="next" title="i386-Memory">
<link href="i386_002dRegs.html" rel="prev" title="i386-Regs">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="i386_002dPrefixes">
<div class="nav-panel">
<p>
Next: <a href="i386_002dMemory.html" accesskey="n" rel="next">Memory References</a>, Previous: <a href="i386_002dRegs.html" accesskey="p" rel="prev">Register Naming</a>, Up: <a href="i386_002dDependent.html" accesskey="u" rel="up">80386 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="Instruction-Prefixes">9.16.6 Instruction Prefixes</h4>

<a class="index-entry-id" id="index-i386-instruction-prefixes"></a>
<a class="index-entry-id" id="index-instruction-prefixes_002c-i386"></a>
<a class="index-entry-id" id="index-prefixes_002c-i386"></a>
<p>Instruction prefixes are used to modify the following instruction.  They
are used to repeat string instructions, to provide section overrides, to
perform bus lock operations, and to change operand and address sizes.
(Most instructions that normally operate on 32-bit operands will use
16-bit operands if the instruction has an &ldquo;operand size&rdquo; prefix.)
Instruction prefixes are best written on the same line as the instruction
they act upon. For example, the &lsquo;<samp class="samp">scas</samp>&rsquo; (scan string) instruction is
repeated with:
</p>
<div class="example smallexample">
<pre class="example-preformatted">        repne scas %es:(%edi),%al
</pre></div>

<p>You may also place prefixes on the lines immediately preceding the
instruction, but this circumvents checks that <code class="code">as</code> does
with prefixes, and will not work with all prefixes.
</p>
<p>Here is a list of instruction prefixes:
</p>
<a class="index-entry-id" id="index-section-override-prefixes_002c-i386"></a>
<ul class="itemize mark-bullet">
<li>Section override prefixes &lsquo;<samp class="samp">cs</samp>&rsquo;, &lsquo;<samp class="samp">ds</samp>&rsquo;, &lsquo;<samp class="samp">ss</samp>&rsquo;, &lsquo;<samp class="samp">es</samp>&rsquo;,
&lsquo;<samp class="samp">fs</samp>&rsquo;, &lsquo;<samp class="samp">gs</samp>&rsquo;.  These are automatically added by specifying
using the <var class="var">section</var>:<var class="var">memory-operand</var> form for memory references.

</li><li><a class="index-entry-id" id="index-size-prefixes_002c-i386"></a>
Operand/Address size prefixes &lsquo;<samp class="samp">data16</samp>&rsquo; and &lsquo;<samp class="samp">addr16</samp>&rsquo;
change 32-bit operands/addresses into 16-bit operands/addresses,
while &lsquo;<samp class="samp">data32</samp>&rsquo; and &lsquo;<samp class="samp">addr32</samp>&rsquo; change 16-bit ones (in a
<code class="code">.code16</code> section) into 32-bit operands/addresses.  These prefixes
<em class="emph">must</em> appear on the same line of code as the instruction they
modify. For example, in a 16-bit <code class="code">.code16</code> section, you might
write:

<div class="example smallexample">
<pre class="example-preformatted">        addr32 jmpl *(%ebx)
</pre></div>

</li><li><a class="index-entry-id" id="index-bus-lock-prefixes_002c-i386"></a>
<a class="index-entry-id" id="index-inhibiting-interrupts_002c-i386"></a>
The bus lock prefix &lsquo;<samp class="samp">lock</samp>&rsquo; inhibits interrupts during execution of
the instruction it precedes.  (This is only valid with certain
instructions; see a 80386 manual for details).

</li><li><a class="index-entry-id" id="index-coprocessor-wait_002c-i386"></a>
The wait for coprocessor prefix &lsquo;<samp class="samp">wait</samp>&rsquo; waits for the coprocessor to
complete the current instruction.  This should never be needed for the
80386/80387 combination.

</li><li><a class="index-entry-id" id="index-repeat-prefixes_002c-i386"></a>
The &lsquo;<samp class="samp">rep</samp>&rsquo;, &lsquo;<samp class="samp">repe</samp>&rsquo;, and &lsquo;<samp class="samp">repne</samp>&rsquo; prefixes are added
to string instructions to make them repeat &lsquo;<samp class="samp">%ecx</samp>&rsquo; times (&lsquo;<samp class="samp">%cx</samp>&rsquo;
times if the current address size is 16-bits).
</li><li><a class="index-entry-id" id="index-REX-prefixes_002c-i386"></a>
The &lsquo;<samp class="samp">rex</samp>&rsquo; family of prefixes is used by x86-64 to encode
extensions to i386 instruction set.  The &lsquo;<samp class="samp">rex</samp>&rsquo; prefix has four
bits &mdash; an operand size overwrite (<code class="code">64</code>) used to change operand size
from 32-bit to 64-bit and X, Y and Z extensions bits used to extend the
register set.

<p>You may write the &lsquo;<samp class="samp">rex</samp>&rsquo; prefixes directly. The &lsquo;<samp class="samp">rex64xyz</samp>&rsquo;
instruction emits &lsquo;<samp class="samp">rex</samp>&rsquo; prefix with all the bits set.  By omitting
the <code class="code">64</code>, <code class="code">x</code>, <code class="code">y</code> or <code class="code">z</code> you may write other
prefixes as well.  Normally, there is no need to write the prefixes
explicitly, since gas will automatically generate them based on the
instruction operands.
</p></li></ul>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="i386_002dMemory.html">Memory References</a>, Previous: <a href="i386_002dRegs.html">Register Naming</a>, Up: <a href="i386_002dDependent.html">80386 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
