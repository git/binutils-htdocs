<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>TILEPro Modifiers (Using as)</title>

<meta name="description" content="TILEPro Modifiers (Using as)">
<meta name="keywords" content="TILEPro Modifiers (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="TILEPro-Syntax.html" rel="up" title="TILEPro Syntax">
<link href="TILEPro-Registers.html" rel="prev" title="TILEPro Registers">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="TILEPro-Modifiers">
<div class="nav-panel">
<p>
Previous: <a href="TILEPro-Registers.html" accesskey="p" rel="prev">Register Names</a>, Up: <a href="TILEPro-Syntax.html" accesskey="u" rel="up">Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection subsection-level-set-subsubsection" id="Symbolic-Operand-Modifiers-5">9.49.2.3 Symbolic Operand Modifiers</h4>
<a class="index-entry-id" id="index-TILEPro-modifiers"></a>
<a class="index-entry-id" id="index-symbol-modifiers_002c-TILEPro"></a>

<p>The assembler supports several modifiers when using symbol addresses
in TILEPro instruction operands.  The general syntax is the following:
</p>
<div class="example smallexample">
<pre class="example-preformatted">modifier(symbol)
</pre></div>

<p>The following modifiers are supported:
</p>
<dl class="table">
<dt><code class="code">lo16</code></dt>
<dd>
<p>This modifier is used to load the low 16 bits of the symbol&rsquo;s address,
sign-extended to a 32-bit value (sign-extension allows it to be
range-checked against signed 16 bit immediate operands without
complaint).
</p>
</dd>
<dt><code class="code">hi16</code></dt>
<dd>
<p>This modifier is used to load the high 16 bits of the symbol&rsquo;s
address, also sign-extended to a 32-bit value.
</p>
</dd>
<dt><code class="code">ha16</code></dt>
<dd>
<p><code class="code">ha16(N)</code> is identical to <code class="code">hi16(N)</code>, except if
<code class="code">lo16(N)</code> is negative it adds one to the <code class="code">hi16(N)</code>
value. This way <code class="code">lo16</code> and <code class="code">ha16</code> can be added to create any
32-bit value using <code class="code">auli</code>.  For example, here is how you move an
arbitrary 32-bit address into r3:
</p>
<div class="example smallexample">
<pre class="example-preformatted">moveli r3, lo16(sym)
auli r3, r3, ha16(sym)
</pre></div>

</dd>
<dt><code class="code">got</code></dt>
<dd>
<p>This modifier is used to load the offset of the GOT entry
corresponding to the symbol.
</p>
</dd>
<dt><code class="code">got_lo16</code></dt>
<dd>
<p>This modifier is used to load the sign-extended low 16 bits of the
offset of the GOT entry corresponding to the symbol.
</p>
</dd>
<dt><code class="code">got_hi16</code></dt>
<dd>
<p>This modifier is used to load the sign-extended high 16 bits of the
offset of the GOT entry corresponding to the symbol.
</p>
</dd>
<dt><code class="code">got_ha16</code></dt>
<dd>
<p>This modifier is like <code class="code">got_hi16</code>, but it adds one if
<code class="code">got_lo16</code> of the input value is negative.
</p>
</dd>
<dt><code class="code">plt</code></dt>
<dd>
<p>This modifier is used for function symbols.  It causes a
<em class="emph">procedure linkage table</em>, an array of code stubs, to be created
at the time the shared object is created or linked against, together
with a global offset table entry.  The value is a pc-relative offset
to the corresponding stub code in the procedure linkage table.  This
arrangement causes the run-time symbol resolver to be called to look
up and set the value of the symbol the first time the function is
called (at latest; depending environment variables).  It is only safe
to leave the symbol unresolved this way if all references are function
calls.
</p>
</dd>
<dt><code class="code">tls_gd</code></dt>
<dd>
<p>This modifier is used to load the offset of the GOT entry of the
symbol&rsquo;s TLS descriptor, to be used for general-dynamic TLS accesses.
</p>
</dd>
<dt><code class="code">tls_gd_lo16</code></dt>
<dd>
<p>This modifier is used to load the sign-extended low 16 bits of the
offset of the GOT entry of the symbol&rsquo;s TLS descriptor, to be used for
general dynamic TLS accesses.
</p>
</dd>
<dt><code class="code">tls_gd_hi16</code></dt>
<dd>
<p>This modifier is used to load the sign-extended high 16 bits of the
offset of the GOT entry of the symbol&rsquo;s TLS descriptor, to be used for
general dynamic TLS accesses.
</p>
</dd>
<dt><code class="code">tls_gd_ha16</code></dt>
<dd>
<p>This modifier is like <code class="code">tls_gd_hi16</code>, but it adds one to the value
if <code class="code">tls_gd_lo16</code> of the input value is negative.
</p>
</dd>
<dt><code class="code">tls_ie</code></dt>
<dd>
<p>This modifier is used to load the offset of the GOT entry containing
the offset of the symbol&rsquo;s address from the TCB, to be used for
initial-exec TLS accesses.
</p>
</dd>
<dt><code class="code">tls_ie_lo16</code></dt>
<dd>
<p>This modifier is used to load the low 16 bits of the offset of the GOT
entry containing the offset of the symbol&rsquo;s address from the TCB, to
be used for initial-exec TLS accesses.
</p>
</dd>
<dt><code class="code">tls_ie_hi16</code></dt>
<dd>
<p>This modifier is used to load the high 16 bits of the offset of the
GOT entry containing the offset of the symbol&rsquo;s address from the TCB,
to be used for initial-exec TLS accesses.
</p>
</dd>
<dt><code class="code">tls_ie_ha16</code></dt>
<dd>
<p>This modifier is like <code class="code">tls_ie_hi16</code>, but it adds one to the value
if <code class="code">tls_ie_lo16</code> of the input value is negative.
</p>
</dd>
<dt><code class="code">tls_le</code></dt>
<dd>
<p>This modifier is used to load the offset of the symbol&rsquo;s address from
the TCB, to be used for local-exec TLS accesses.
</p>
</dd>
<dt><code class="code">tls_le_lo16</code></dt>
<dd>
<p>This modifier is used to load the low 16 bits of the offset of the
symbol&rsquo;s address from the TCB, to be used for local-exec TLS accesses.
</p>
</dd>
<dt><code class="code">tls_le_hi16</code></dt>
<dd>
<p>This modifier is used to load the high 16 bits of the offset of the
symbol&rsquo;s address from the TCB, to be used for local-exec TLS accesses.
</p>
</dd>
<dt><code class="code">tls_le_ha16</code></dt>
<dd>
<p>This modifier is like <code class="code">tls_le_hi16</code>, but it adds one to the value
if <code class="code">tls_le_lo16</code> of the input value is negative.
</p>
</dd>
<dt><code class="code">tls_gd_call</code></dt>
<dd>
<p>This modifier is used to tag an instruction as the &ldquo;call&rdquo; part of a
calling sequence for a TLS GD reference of its operand.
</p>
</dd>
<dt><code class="code">tls_gd_add</code></dt>
<dd>
<p>This modifier is used to tag an instruction as the &ldquo;add&rdquo; part of a
calling sequence for a TLS GD reference of its operand.
</p>
</dd>
<dt><code class="code">tls_ie_load</code></dt>
<dd>
<p>This modifier is used to tag an instruction as the &ldquo;load&rdquo; part of a
calling sequence for a TLS IE reference of its operand.
</p>
</dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="TILEPro-Registers.html">Register Names</a>, Up: <a href="TILEPro-Syntax.html">Syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
