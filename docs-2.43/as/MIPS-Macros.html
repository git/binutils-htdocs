<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>MIPS Macros (Using as)</title>

<meta name="description" content="MIPS Macros (Using as)">
<meta name="keywords" content="MIPS Macros (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="MIPS_002dDependent.html" rel="up" title="MIPS-Dependent">
<link href="MIPS-Symbol-Sizes.html" rel="next" title="MIPS Symbol Sizes">
<link href="MIPS-Options.html" rel="prev" title="MIPS Options">


</head>

<body lang="en">
<div class="subsection-level-extent" id="MIPS-Macros">
<div class="nav-panel">
<p>
Next: <a href="MIPS-Symbol-Sizes.html" accesskey="n" rel="next">Directives to override the size of symbols</a>, Previous: <a href="MIPS-Options.html" accesskey="p" rel="prev">Assembler options</a>, Up: <a href="MIPS_002dDependent.html" accesskey="u" rel="up">MIPS Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="High_002dlevel-assembly-macros">9.28.2 High-level assembly macros</h4>

<p>MIPS assemblers have traditionally provided a wider range of
instructions than the MIPS architecture itself.  These extra
instructions are usually referred to as &ldquo;macro&rdquo; instructions
<a class="footnote" id="DOCF2" href="#FOOT2"><sup>2</sup></a>.
</p>
<p>Some MIPS macro instructions extend an underlying architectural instruction
while others are entirely new.  An example of the former type is <code class="code">and</code>,
which allows the third operand to be either a register or an arbitrary
immediate value.  Examples of the latter type include <code class="code">bgt</code>, which
branches to the third operand when the first operand is greater than
the second operand, and <code class="code">ulh</code>, which implements an unaligned
2-byte load.
</p>
<p>One of the most common extensions provided by macros is to expand
memory offsets to the full address range (32 or 64 bits) and to allow
symbolic offsets such as &lsquo;<samp class="samp">my_data + 4</samp>&rsquo; to be used in place of
integer constants.  For example, the architectural instruction
<code class="code">lbu</code> allows only a signed 16-bit offset, whereas the macro
<code class="code">lbu</code> allows code such as &lsquo;<samp class="samp">lbu $4,array+32769($5)</samp>&rsquo;.
The implementation of these symbolic offsets depends on several factors,
such as whether the assembler is generating SVR4-style PIC (selected by
<samp class="option">-KPIC</samp>, see <a class="pxref" href="MIPS-Options.html">Assembler options</a>), the size of symbols
(see <a class="pxref" href="MIPS-Symbol-Sizes.html">Directives to override the size of symbols</a>),
and the small data limit (see <a class="pxref" href="MIPS-Small-Data.html">Controlling the use
of small data accesses</a>).
</p>
<a class="index-entry-id" id="index-_002eset-macro"></a>
<a class="index-entry-id" id="index-_002eset-nomacro"></a>
<p>Sometimes it is undesirable to have one assembly instruction expand
to several machine instructions.  The directive <code class="code">.set nomacro</code>
tells the assembler to warn when this happens.  <code class="code">.set macro</code>
restores the default behavior.
</p>
<a class="index-entry-id" id="index-at-register_002c-MIPS"></a>
<a class="index-entry-id" id="index-_002eset-at_003dreg"></a>
<p>Some macro instructions need a temporary register to store intermediate
results.  This register is usually <code class="code">$1</code>, also known as <code class="code">$at</code>,
but it can be changed to any core register <var class="var">reg</var> using
<code class="code">.set at=<var class="var">reg</var></code>.  Note that <code class="code">$at</code> always refers
to <code class="code">$1</code> regardless of which register is being used as the
temporary register.
</p>
<a class="index-entry-id" id="index-_002eset-at"></a>
<a class="index-entry-id" id="index-_002eset-noat"></a>
<p>Implicit uses of the temporary register in macros could interfere with
explicit uses in the assembly code.  The assembler therefore warns
whenever it sees an explicit use of the temporary register.  The directive
<code class="code">.set noat</code> silences this warning while <code class="code">.set at</code> restores
the default behavior.  It is safe to use <code class="code">.set noat</code> while
<code class="code">.set nomacro</code> is in effect since single-instruction macros
never need a temporary register.
</p>
<p>Note that while the <small class="sc">GNU</small> assembler provides these macros for compatibility,
it does not make any attempt to optimize them with the surrounding code.
</p>
</div>
<div class="footnotes-segment">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5 class="footnote-body-heading"><a id="FOOT2" href="#DOCF2">(2)</a></h5>
<p>The term &ldquo;macro&rdquo; is somewhat overloaded here, since
these macros have no relation to those defined by <code class="code">.macro</code>,
see <a class="pxref" href="Macro.html"><code class="code">.macro</code></a>.</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="MIPS-Symbol-Sizes.html">Directives to override the size of symbols</a>, Previous: <a href="MIPS-Options.html">Assembler options</a>, Up: <a href="MIPS_002dDependent.html">MIPS Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
