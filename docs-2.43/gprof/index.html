<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the gprof profiler of the GNU system.

Copyright © 1988-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Top (GNU gprof)</title>

<meta name="description" content="Top (GNU gprof)">
<meta name="keywords" content="Top (GNU gprof)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="#Top" rel="start" title="Top">
<link href="#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Introduction.html" rel="next" title="Introduction">
<style type="text/css">
<!--
ul.toc-numbered-mark {list-style: none}
-->
</style>


</head>

<body lang="en">






<div class="top-level-extent" id="Top">
<div class="nav-panel">
<p>
Next: <a href="Introduction.html" accesskey="n" rel="next">Introduction to Profiling</a> &nbsp; [<a href="#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h1 class="top" id="Profiling-a-Program_003a-Where-Does-It-Spend-Its-Time_003f">Profiling a Program: Where Does It Spend Its Time?</h1>

<p>This manual describes the <small class="sc">GNU</small> profiler, <code class="code">gprof</code>, and how you
can use it to determine which parts of a program are taking most of the
execution time.  We assume that you know how to write, compile, and
execute programs.  <small class="sc">GNU</small> <code class="code">gprof</code> was written by Jay Fenlason.
</p>
<p>This manual is for <code class="code">gprof</code>
(GNU Binutils)
version 2.43.
</p>
<p>This document is distributed under the terms of the GNU Free
Documentation License version 1.3.  A copy of the license is included
in the section entitled &ldquo;GNU Free Documentation License&rdquo;.
</p>

<div class="element-contents" id="SEC_Contents">
<h2 class="contents-heading">Table of Contents</h2>

<div class="contents">

<ul class="toc-numbered-mark">
  <li><a id="toc-Introduction-to-Profiling" href="Introduction.html">1 Introduction to Profiling</a></li>
  <li><a id="toc-Compiling-a-Program-for-Profiling" href="Compiling.html">2 Compiling a Program for Profiling</a></li>
  <li><a id="toc-Executing-the-Program" href="Executing.html">3 Executing the Program</a></li>
  <li><a id="toc-gprof-Command-Summary" href="Invoking.html">4 <code class="code">gprof</code> Command Summary</a>
  <ul class="toc-numbered-mark">
    <li><a id="toc-Output-Options-1" href="Output-Options.html">4.1 Output Options</a></li>
    <li><a id="toc-Analysis-Options-1" href="Analysis-Options.html">4.2 Analysis Options</a></li>
    <li><a id="toc-Miscellaneous-Options-1" href="Miscellaneous-Options.html">4.3 Miscellaneous Options</a></li>
    <li><a id="toc-Deprecated-Options-1" href="Deprecated-Options.html">4.4 Deprecated Options</a></li>
    <li><a id="toc-Symspecs-1" href="Symspecs.html">4.5 Symspecs</a></li>
  </ul></li>
  <li><a id="toc-Interpreting-gprof_0027s-Output" href="Output.html">5 Interpreting <code class="code">gprof</code>&rsquo;s Output</a>
  <ul class="toc-numbered-mark">
    <li><a id="toc-The-Flat-Profile" href="Flat-Profile.html">5.1 The Flat Profile</a></li>
    <li><a id="toc-The-Call-Graph" href="Call-Graph.html">5.2 The Call Graph</a>
    <ul class="toc-numbered-mark">
      <li><a id="toc-The-Primary-Line" href="Primary.html">5.2.1 The Primary Line</a></li>
      <li><a id="toc-Lines-for-a-Function_0027s-Callers" href="Callers.html">5.2.2 Lines for a Function&rsquo;s Callers</a></li>
      <li><a id="toc-Lines-for-a-Function_0027s-Subroutines" href="Subroutines.html">5.2.3 Lines for a Function&rsquo;s Subroutines</a></li>
      <li><a id="toc-How-Mutually-Recursive-Functions-Are-Described" href="Cycles.html">5.2.4 How Mutually Recursive Functions Are Described</a></li>
    </ul></li>
    <li><a id="toc-Line_002dby_002dline-Profiling" href="Line_002dby_002dline.html">5.3 Line-by-line Profiling</a></li>
    <li><a id="toc-The-Annotated-Source-Listing" href="Annotated-Source.html">5.4 The Annotated Source Listing</a></li>
  </ul></li>
  <li><a id="toc-Inaccuracy-of-gprof-Output" href="Inaccuracy.html">6 Inaccuracy of <code class="code">gprof</code> Output</a>
  <ul class="toc-numbered-mark">
    <li><a id="toc-Statistical-Sampling-Error" href="Sampling-Error.html">6.1 Statistical Sampling Error</a></li>
    <li><a id="toc-Estimating-children-Times" href="Assumptions.html">6.2 Estimating <code class="code">children</code> Times</a></li>
  </ul></li>
  <li><a id="toc-Answers-to-Common-Questions" href="How-do-I_003f.html">7 Answers to Common Questions</a></li>
  <li><a id="toc-Incompatibilities-with-Unix-gprof" href="Incompatibilities.html">8 Incompatibilities with Unix <code class="code">gprof</code></a></li>
  <li><a id="toc-Details-of-Profiling" href="Details.html">9 Details of Profiling</a>
  <ul class="toc-numbered-mark">
    <li><a id="toc-Implementation-of-Profiling" href="Implementation.html">9.1 Implementation of Profiling</a></li>
    <li><a id="toc-Profiling-Data-File-Format" href="File-Format.html">9.2 Profiling Data File Format</a>
    <ul class="toc-numbered-mark">
      <li><a id="toc-Histogram-Records" href="File-Format.html#Histogram-Records">9.2.1 Histogram Records</a></li>
      <li><a id="toc-Call_002dGraph-Records" href="File-Format.html#Call_002dGraph-Records">9.2.2 Call-Graph Records</a></li>
      <li><a id="toc-Basic_002dBlock-Execution-Count-Records" href="File-Format.html#Basic_002dBlock-Execution-Count-Records">9.2.3 Basic-Block Execution Count Records</a></li>
    </ul></li>
    <li><a id="toc-gprof_0027s-Internal-Operation" href="Internals.html">9.3 <code class="code">gprof</code>&rsquo;s Internal Operation</a></li>
    <li><a id="toc-Debugging-gprof" href="Debugging.html">9.4 Debugging <code class="code">gprof</code></a></li>
  </ul></li>
  <li><a id="toc-GNU-Free-Documentation-License-1" href="GNU-Free-Documentation-License.html">Appendix A GNU Free Documentation License</a></li>
</ul>
</div>
</div>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Introduction.html" accesskey="n" rel="next">Introduction to Profiling</a> &nbsp; [<a href="#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
