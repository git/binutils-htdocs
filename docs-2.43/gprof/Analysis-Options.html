<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the gprof profiler of the GNU system.

Copyright © 1988-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Analysis Options (GNU gprof)</title>

<meta name="description" content="Analysis Options (GNU gprof)">
<meta name="keywords" content="Analysis Options (GNU gprof)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Invoking.html" rel="up" title="Invoking">
<link href="Miscellaneous-Options.html" rel="next" title="Miscellaneous Options">
<link href="Output-Options.html" rel="prev" title="Output Options">


</head>

<body lang="en">
<div class="section-level-extent" id="Analysis-Options">
<div class="nav-panel">
<p>
Next: <a href="Miscellaneous-Options.html" accesskey="n" rel="next">Miscellaneous Options</a>, Previous: <a href="Output-Options.html" accesskey="p" rel="prev">Output Options</a>, Up: <a href="Invoking.html" accesskey="u" rel="up"><code class="code">gprof</code> Command Summary</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h3 class="section" id="Analysis-Options-1">4.2 Analysis Options</h3>

<dl class="table">
<dt><code class="code">-a</code></dt>
<dt><code class="code">--no-static</code></dt>
<dd><p>The &lsquo;<samp class="samp">-a</samp>&rsquo; option causes <code class="code">gprof</code> to suppress the printing of
statically declared (private) functions.  (These are functions whose
names are not listed as global, and which are not visible outside the
file/function/block where they were defined.)  Time spent in these
functions, calls to/from them, etc., will all be attributed to the
function that was loaded directly before it in the executable file.
This option affects both the flat profile and the call graph.
</p>
</dd>
<dt><code class="code">-c</code></dt>
<dt><code class="code">--static-call-graph</code></dt>
<dd><p>The &lsquo;<samp class="samp">-c</samp>&rsquo; option causes the call graph of the program to be
augmented by a heuristic which examines the text space of the object
file and identifies function calls in the binary machine code.
Since normal call graph records are only generated when functions are
entered, this option identifies children that could have been called,
but never were.  Calls to functions that were not compiled with
profiling enabled are also identified, but only if symbol table
entries are present for them.
Calls to dynamic library routines are typically <em class="emph">not</em> found
by this option.
Parents or children identified via this heuristic
are indicated in the call graph with call counts of &lsquo;<samp class="samp">0</samp>&rsquo;.
</p>
</dd>
<dt><code class="code">-D</code></dt>
<dt><code class="code">--ignore-non-functions</code></dt>
<dd><p>The &lsquo;<samp class="samp">-D</samp>&rsquo; option causes <code class="code">gprof</code> to ignore symbols which
are not known to be functions.  This option will give more accurate
profile data on systems where it is supported (Solaris and HPUX for
example).
</p>
</dd>
<dt><code class="code">-k <var class="var">from</var>/<var class="var">to</var></code></dt>
<dd><p>The &lsquo;<samp class="samp">-k</samp>&rsquo; option allows you to delete from the call graph any arcs from
symbols matching symspec <var class="var">from</var> to those matching symspec <var class="var">to</var>.
</p>
</dd>
<dt><code class="code">-l</code></dt>
<dt><code class="code">--line</code></dt>
<dd><p>The &lsquo;<samp class="samp">-l</samp>&rsquo; option enables line-by-line profiling, which causes
histogram hits to be charged to individual source code lines,
instead of functions.  This feature only works with programs compiled
by older versions of the <code class="code">gcc</code> compiler.  Newer versions of
<code class="code">gcc</code> are designed to work with the <code class="code">gcov</code> tool instead.
</p>
<p>If the program was compiled with basic-block counting enabled,
this option will also identify how many times each line of
code was executed.
While line-by-line profiling can help isolate where in a large function
a program is spending its time, it also significantly increases
the running time of <code class="code">gprof</code>, and magnifies statistical
inaccuracies.
See <a class="xref" href="Sampling-Error.html">Statistical Sampling Error</a>.
</p>
</dd>
<dt><code class="code">--inline-file-names</code></dt>
<dd><p>This option causes <code class="code">gprof</code> to print the source file after each
symbol in both the flat profile and the call graph. The full path to the
file is printed if used with the &lsquo;<samp class="samp">-L</samp>&rsquo; option.
</p>
</dd>
<dt><code class="code">-m <var class="var">num</var></code></dt>
<dt><code class="code">--min-count=<var class="var">num</var></code></dt>
<dd><p>This option affects execution count output only.
Symbols that are executed less than <var class="var">num</var> times are suppressed.
</p>
</dd>
<dt><code class="code">-n<var class="var">symspec</var></code></dt>
<dt><code class="code">--time=<var class="var">symspec</var></code></dt>
<dd><p>The &lsquo;<samp class="samp">-n</samp>&rsquo; option causes <code class="code">gprof</code>, in its call graph analysis,
to only propagate times for symbols matching <var class="var">symspec</var>.
</p>
</dd>
<dt><code class="code">-N<var class="var">symspec</var></code></dt>
<dt><code class="code">--no-time=<var class="var">symspec</var></code></dt>
<dd><p>The &lsquo;<samp class="samp">-n</samp>&rsquo; option causes <code class="code">gprof</code>, in its call graph analysis,
not to propagate times for symbols matching <var class="var">symspec</var>.
</p>
</dd>
<dt><code class="code">-S<var class="var">filename</var></code></dt>
<dt><code class="code">--external-symbol-table=<var class="var">filename</var></code></dt>
<dd><p>The &lsquo;<samp class="samp">-S</samp>&rsquo; option causes <code class="code">gprof</code> to read an external symbol table
file, such as <samp class="file">/proc/kallsyms</samp>, rather than read the symbol table
from the given object file (the default is <code class="code">a.out</code>). This is useful
for profiling kernel modules.
</p>
</dd>
<dt><code class="code">-z</code></dt>
<dt><code class="code">--display-unused-functions</code></dt>
<dd><p>If you give the &lsquo;<samp class="samp">-z</samp>&rsquo; option, <code class="code">gprof</code> will mention all
functions in the flat profile, even those that were never called, and
that had no time spent in them.  This is useful in conjunction with the
&lsquo;<samp class="samp">-c</samp>&rsquo; option for discovering which routines were never called.
</p>
</dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Miscellaneous-Options.html">Miscellaneous Options</a>, Previous: <a href="Output-Options.html">Output Options</a>, Up: <a href="Invoking.html"><code class="code">gprof</code> Command Summary</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
