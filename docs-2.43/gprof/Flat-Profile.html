<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the gprof profiler of the GNU system.

Copyright © 1988-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Flat Profile (GNU gprof)</title>

<meta name="description" content="Flat Profile (GNU gprof)">
<meta name="keywords" content="Flat Profile (GNU gprof)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Output.html" rel="up" title="Output">
<link href="Call-Graph.html" rel="next" title="Call Graph">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Flat-Profile">
<div class="nav-panel">
<p>
Next: <a href="Call-Graph.html" accesskey="n" rel="next">The Call Graph</a>, Up: <a href="Output.html" accesskey="u" rel="up">Interpreting <code class="code">gprof</code>&rsquo;s Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>
<hr>
<h3 class="section" id="The-Flat-Profile">5.1 The Flat Profile</h3>
<a class="index-entry-id" id="index-flat-profile"></a>

<p>The <em class="dfn">flat profile</em> shows the total amount of time your program
spent executing each function.  Unless the &lsquo;<samp class="samp">-z</samp>&rsquo; option is given,
functions with no apparent time spent in them, and no apparent calls
to them, are not mentioned.  Note that if a function was not compiled
for profiling, and didn&rsquo;t run long enough to show up on the program
counter histogram, it will be indistinguishable from a function that
was never called.
</p>
<p>This is part of a flat profile for a small program:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls  ms/call  ms/call  name
 33.34      0.02     0.02     7208     0.00     0.00  open
 16.67      0.03     0.01      244     0.04     0.12  offtime
 16.67      0.04     0.01        8     1.25     1.25  memccpy
 16.67      0.05     0.01        7     1.43     1.43  write
 16.67      0.06     0.01                             mcount
  0.00      0.06     0.00      236     0.00     0.00  tzset
  0.00      0.06     0.00      192     0.00     0.00  tolower
  0.00      0.06     0.00       47     0.00     0.00  strlen
  0.00      0.06     0.00       45     0.00     0.00  strchr
  0.00      0.06     0.00        1     0.00    50.00  main
  0.00      0.06     0.00        1     0.00     0.00  memcpy
  0.00      0.06     0.00        1     0.00    10.11  print
  0.00      0.06     0.00        1     0.00     0.00  profil
  0.00      0.06     0.00        1     0.00    50.00  report
&hellip;
</pre></div></div>

<p>The functions are sorted first by decreasing run-time spent in them,
then by decreasing number of calls, then alphabetically by name.  The
functions &lsquo;<samp class="samp">mcount</samp>&rsquo; and &lsquo;<samp class="samp">profil</samp>&rsquo; are part of the profiling
apparatus and appear in every flat profile; their time gives a measure of
the amount of overhead due to profiling.
</p>
<p>Just before the column headers, a statement appears indicating
how much time each sample counted as.
This <em class="dfn">sampling period</em> estimates the margin of error in each of the time
figures.  A time figure that is not much larger than this is not
reliable.  In this example, each sample counted as 0.01 seconds,
suggesting a 100 Hz sampling rate.
The program&rsquo;s total execution time was 0.06
seconds, as indicated by the &lsquo;<samp class="samp">cumulative seconds</samp>&rsquo; field.  Since
each sample counted for 0.01 seconds, this means only six samples
were taken during the run.  Two of the samples occurred while the
program was in the &lsquo;<samp class="samp">open</samp>&rsquo; function, as indicated by the
&lsquo;<samp class="samp">self seconds</samp>&rsquo; field.  Each of the other four samples
occurred one each in &lsquo;<samp class="samp">offtime</samp>&rsquo;, &lsquo;<samp class="samp">memccpy</samp>&rsquo;, &lsquo;<samp class="samp">write</samp>&rsquo;,
and &lsquo;<samp class="samp">mcount</samp>&rsquo;.
Since only six samples were taken, none of these values can
be regarded as particularly reliable.
In another run,
the &lsquo;<samp class="samp">self seconds</samp>&rsquo; field for
&lsquo;<samp class="samp">mcount</samp>&rsquo; might well be &lsquo;<samp class="samp">0.00</samp>&rsquo; or &lsquo;<samp class="samp">0.02</samp>&rsquo;.
See <a class="xref" href="Sampling-Error.html">Statistical Sampling Error</a>,
for a complete discussion.
</p>
<p>The remaining functions in the listing (those whose
&lsquo;<samp class="samp">self seconds</samp>&rsquo; field is &lsquo;<samp class="samp">0.00</samp>&rsquo;) didn&rsquo;t appear
in the histogram samples at all.  However, the call graph
indicated that they were called, so therefore they are listed,
sorted in decreasing order by the &lsquo;<samp class="samp">calls</samp>&rsquo; field.
Clearly some time was spent executing these functions,
but the paucity of histogram samples prevents any
determination of how much time each took.
</p>
<p>Here is what the fields in each line mean:
</p>
<dl class="table">
<dt><code class="code">% time</code></dt>
<dd><p>This is the percentage of the total execution time your program spent
in this function.  These should all add up to 100%.
</p>
</dd>
<dt><code class="code">cumulative seconds</code></dt>
<dd><p>This is the cumulative total number of seconds the computer spent
executing this functions, plus the time spent in all the functions
above this one in this table.
</p>
</dd>
<dt><code class="code">self seconds</code></dt>
<dd><p>This is the number of seconds accounted for by this function alone.
The flat profile listing is sorted first by this number.
</p>
</dd>
<dt><code class="code">calls</code></dt>
<dd><p>This is the total number of times the function was called.  If the
function was never called, or the number of times it was called cannot
be determined (probably because the function was not compiled with
profiling enabled), the <em class="dfn">calls</em> field is blank.
</p>
</dd>
<dt><code class="code">self ms/call</code></dt>
<dd><p>This represents the average number of milliseconds spent in this
function per call, if this function is profiled.  Otherwise, this field
is blank for this function.
</p>
</dd>
<dt><code class="code">total ms/call</code></dt>
<dd><p>This represents the average number of milliseconds spent in this
function and its descendants per call, if this function is profiled.
Otherwise, this field is blank for this function.
This is the only field in the flat profile that uses call graph analysis.
</p>
</dd>
<dt><code class="code">name</code></dt>
<dd><p>This is the name of the function.   The flat profile is sorted by this
field alphabetically after the <em class="dfn">self seconds</em> and <em class="dfn">calls</em>
fields are sorted.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Call-Graph.html">The Call Graph</a>, Up: <a href="Output.html">Interpreting <code class="code">gprof</code>&rsquo;s Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>]</p>
</div>



</body>
</html>
