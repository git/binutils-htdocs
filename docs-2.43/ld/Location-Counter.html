<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.43.

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Location Counter (LD)</title>

<meta name="description" content="Location Counter (LD)">
<meta name="keywords" content="Location Counter (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Expressions.html" rel="up" title="Expressions">
<link href="Operators.html" rel="next" title="Operators">
<link href="Orphan-Sections.html" rel="prev" title="Orphan Sections">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Location-Counter">
<div class="nav-panel">
<p>
Next: <a href="Operators.html" accesskey="n" rel="next">Operators</a>, Previous: <a href="Orphan-Sections.html" accesskey="p" rel="prev">Orphan Sections</a>, Up: <a href="Expressions.html" accesskey="u" rel="up">Expressions in Linker Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="The-Location-Counter">3.10.5 The Location Counter</h4>
<a class="index-entry-id" id="index-_002e"></a>
<a class="index-entry-id" id="index-dot"></a>
<a class="index-entry-id" id="index-location-counter"></a>
<a class="index-entry-id" id="index-current-output-location"></a>
<p>The special linker variable <em class="dfn">dot</em> &lsquo;<samp class="samp">.</samp>&rsquo; always contains the
current output location counter.  Since the <code class="code">.</code> always refers to a
location in an output section, it may only appear in an expression
within a <code class="code">SECTIONS</code> command.  The <code class="code">.</code> symbol may appear
anywhere that an ordinary symbol is allowed in an expression.
</p>
<a class="index-entry-id" id="index-holes"></a>
<p>Assigning a value to <code class="code">.</code> will cause the location counter to be
moved.  This may be used to create holes in the output section.  The
location counter may not be moved backwards inside an output section,
and may not be moved backwards outside of an output section if so
doing creates areas with overlapping LMAs.
</p>
<div class="example smallexample">
<pre class="example-preformatted">SECTIONS
{
  output :
    {
      file1(.text)
      . = . + 1000;
      file2(.text)
      . += 1000;
      file3(.text)
    } = 0x12345678;
}
</pre></div>
<p>In the previous example, the &lsquo;<samp class="samp">.text</samp>&rsquo; section from <samp class="file">file1</samp> is
located at the beginning of the output section &lsquo;<samp class="samp">output</samp>&rsquo;.  It is
followed by a 1000 byte gap.  Then the &lsquo;<samp class="samp">.text</samp>&rsquo; section from
<samp class="file">file2</samp> appears, also with a 1000 byte gap following before the
&lsquo;<samp class="samp">.text</samp>&rsquo; section from <samp class="file">file3</samp>.  The notation &lsquo;<samp class="samp">= 0x12345678</samp>&rsquo;
specifies what data to write in the gaps (see <a class="pxref" href="Output-Section-Fill.html">Output Section Fill</a>).
</p>
<a class="index-entry-id" id="index-dot-inside-sections"></a>
<p>Note: <code class="code">.</code> actually refers to the byte offset from the start of the
current containing object.  Normally this is the <code class="code">SECTIONS</code>
statement, whose start address is 0, hence <code class="code">.</code> can be used as an
absolute address.  If <code class="code">.</code> is used inside a section description
however, it refers to the byte offset from the start of that section,
not an absolute address.  Thus in a script like this:
</p>
<div class="example smallexample">
<pre class="example-preformatted">SECTIONS
{
    . = 0x100
    .text: {
      *(.text)
      . = 0x200
    }
    . = 0x500
    .data: {
      *(.data)
      . += 0x600
    }
}
</pre></div>

<p>The &lsquo;<samp class="samp">.text</samp>&rsquo; section will be assigned a starting address of 0x100
and a size of exactly 0x200 bytes, even if there is not enough data in
the &lsquo;<samp class="samp">.text</samp>&rsquo; input sections to fill this area.  (If there is too
much data, an error will be produced because this would be an attempt to
move <code class="code">.</code> backwards).  The &lsquo;<samp class="samp">.data</samp>&rsquo; section will start at 0x500
and it will have an extra 0x600 bytes worth of space after the end of
the values from the &lsquo;<samp class="samp">.data</samp>&rsquo; input sections and before the end of
the &lsquo;<samp class="samp">.data</samp>&rsquo; output section itself.
</p>
<a class="index-entry-id" id="index-dot-outside-sections"></a>
<p>Setting symbols to the value of the location counter outside of an
output section statement can result in unexpected values if the linker
needs to place orphan sections.  For example, given the following:
</p>
<div class="example smallexample">
<pre class="example-preformatted">SECTIONS
{
    start_of_text = . ;
    .text: { *(.text) }
    end_of_text = . ;

    start_of_data = . ;
    .data: { *(.data) }
    end_of_data = . ;
}
</pre></div>

<p>If the linker needs to place some input section, e.g. <code class="code">.rodata</code>,
not mentioned in the script, it might choose to place that section
between <code class="code">.text</code> and <code class="code">.data</code>.  You might think the linker
should place <code class="code">.rodata</code> on the blank line in the above script, but
blank lines are of no particular significance to the linker.  As well,
the linker doesn&rsquo;t associate the above symbol names with their
sections.  Instead, it assumes that all assignments or other
statements belong to the previous output section, except for the
special case of an assignment to <code class="code">.</code>.  I.e., the linker will
place the orphan <code class="code">.rodata</code> section as if the script was written
as follows:
</p>
<div class="example smallexample">
<pre class="example-preformatted">SECTIONS
{
    start_of_text = . ;
    .text: { *(.text) }
    end_of_text = . ;

    start_of_data = . ;
    .rodata: { *(.rodata) }
    .data: { *(.data) }
    end_of_data = . ;
}
</pre></div>

<p>This may or may not be the script author&rsquo;s intention for the value of
<code class="code">start_of_data</code>.  One way to influence the orphan section
placement is to assign the location counter to itself, as the linker
assumes that an assignment to <code class="code">.</code> is setting the start address of
a following output section and thus should be grouped with that
section.  So you could write:
</p>
<div class="example smallexample">
<pre class="example-preformatted">SECTIONS
{
    start_of_text = . ;
    .text: { *(.text) }
    end_of_text = . ;

    . = . ;
    start_of_data = . ;
    .data: { *(.data) }
    end_of_data = . ;
}
</pre></div>

<p>Now, the orphan <code class="code">.rodata</code> section will be placed between
<code class="code">end_of_text</code> and <code class="code">start_of_data</code>.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Operators.html">Operators</a>, Previous: <a href="Orphan-Sections.html">Orphan Sections</a>, Up: <a href="Expressions.html">Expressions in Linker Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
