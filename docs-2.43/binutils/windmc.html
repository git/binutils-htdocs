<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>windmc (GNU Binary Utilities)</title>

<meta name="description" content="windmc (GNU Binary Utilities)">
<meta name="keywords" content="windmc (GNU Binary Utilities)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Binutils-Index.html" rel="index" title="Binutils Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="windres.html" rel="next" title="windres">
<link href="addr2line.html" rel="prev" title="addr2line">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="chapter-level-extent" id="windmc">
<div class="nav-panel">
<p>
Next: <a href="windres.html" accesskey="n" rel="next">windres</a>, Previous: <a href="addr2line.html" accesskey="p" rel="prev">addr2line</a>, Up: <a href="index.html" accesskey="u" rel="up">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h2 class="chapter" id="windmc-1">11 windmc</h2>

<p><code class="command">windmc</code> may be used to generator Windows message resources.
</p>
<blockquote class="quotation">
<p><em class="emph">Warning:</em> <code class="command">windmc</code> is not always built as part of the binary
utilities, since it is only useful for Windows targets.
</p></blockquote>


<div class="example smallexample">
<pre class="example-preformatted">windmc [options] input-file
</pre></div>


<p><code class="command">windmc</code> reads message definitions from an input file (.mc) and
translate them into a set of output files.  The output files may be of
four kinds:
</p>
<dl class="table">
<dt><code class="code">h</code></dt>
<dd><p>A C header file containing the message definitions.
</p>
</dd>
<dt><code class="code">rc</code></dt>
<dd><p>A resource file compilable by the <code class="command">windres</code> tool.
</p>
</dd>
<dt><code class="code">bin</code></dt>
<dd><p>One or more binary files containing the resource data for a specific
message language.
</p>
</dd>
<dt><code class="code">dbg</code></dt>
<dd><p>A C include file that maps message id&rsquo;s to their symbolic name.
</p></dd>
</dl>

<p>The exact description of these different formats is available in
documentation from Microsoft.
</p>
<p>When <code class="command">windmc</code> converts from the <code class="code">mc</code> format to the <code class="code">bin</code>
format, <code class="code">rc</code>, <code class="code">h</code>, and optional <code class="code">dbg</code> it is acting like the
Windows Message Compiler.
</p>


<dl class="table">
<dt><code class="env">-a</code></dt>
<dt><code class="env">--ascii_in</code></dt>
<dd><p>Specifies that the input file specified is ASCII. This is the default
behaviour.
</p>
</dd>
<dt><code class="env">-A</code></dt>
<dt><code class="env">--ascii_out</code></dt>
<dd><p>Specifies that messages in the output <code class="code">bin</code> files should be in ASCII
format.
</p>
</dd>
<dt><code class="env">-b</code></dt>
<dt><code class="env">--binprefix</code></dt>
<dd><p>Specifies that <code class="code">bin</code> filenames should have to be prefixed by the
basename of the source file.
</p>
</dd>
<dt><code class="env">-c</code></dt>
<dt><code class="env">--customflag</code></dt>
<dd><p>Sets the customer bit in all message id&rsquo;s.
</p>
</dd>
<dt><code class="env">-C <var class="var">codepage</var></code></dt>
<dt><code class="env">--codepage_in <var class="var">codepage</var></code></dt>
<dd><p>Sets the default codepage to be used to convert input file to UTF16. The
default is ocdepage 1252.
</p>
</dd>
<dt><code class="env">-d</code></dt>
<dt><code class="env">--decimal_values</code></dt>
<dd><p>Outputs the constants in the header file in decimal. Default is using
hexadecimal output.
</p>
</dd>
<dt><code class="env">-e <var class="var">ext</var></code></dt>
<dt><code class="env">--extension <var class="var">ext</var></code></dt>
<dd><p>The extension for the header file. The default is .h extension.
</p>
</dd>
<dt><code class="env">-F <var class="var">target</var></code></dt>
<dt><code class="env">--target <var class="var">target</var></code></dt>
<dd><p>Specify the BFD format to use for a bin file as output.  This
is a BFD target name; you can use the <samp class="option">--help</samp> option to see a list
of supported targets.  Normally <code class="command">windmc</code> will use the default
format, which is the first one listed by the <samp class="option">--help</samp> option.
<a class="ref" href="Target-Selection.html">Target Selection</a>.
</p>
</dd>
<dt><code class="env">-h <var class="var">path</var></code></dt>
<dt><code class="env">--headerdir <var class="var">path</var></code></dt>
<dd><p>The target directory of the generated header file. The default is the
current directory.
</p>
</dd>
<dt><code class="env">-H</code></dt>
<dt><code class="env">--help</code></dt>
<dd><p>Displays a list of command-line options and then exits.
</p>
</dd>
<dt><code class="env">-m <var class="var">characters</var></code></dt>
<dt><code class="env">--maxlength <var class="var">characters</var></code></dt>
<dd><p>Instructs <code class="command">windmc</code> to generate a warning if the length
of any message exceeds the number specified.
</p>
</dd>
<dt><code class="env">-n</code></dt>
<dt><code class="env">--nullterminate</code></dt>
<dd><p>Terminate message text in <code class="code">bin</code> files by zero. By default they are
terminated by CR/LF.
</p>
</dd>
<dt><code class="env">-o</code></dt>
<dt><code class="env">--hresult_use</code></dt>
<dd><p>Not yet implemented. Instructs <code class="code">windmc</code> to generate an OLE2 header
file, using HRESULT definitions. Status codes are used if the flag is not
specified.
</p>
</dd>
<dt><code class="env">-O <var class="var">codepage</var></code></dt>
<dt><code class="env">--codepage_out <var class="var">codepage</var></code></dt>
<dd><p>Sets the default codepage to be used to output text files. The default
is ocdepage 1252.
</p>
</dd>
<dt><code class="env">-r <var class="var">path</var></code></dt>
<dt><code class="env">--rcdir <var class="var">path</var></code></dt>
<dd><p>The target directory for the generated <code class="code">rc</code> script and the generated
<code class="code">bin</code> files that the resource compiler script includes. The default
is the current directory.
</p>
</dd>
<dt><code class="env">-u</code></dt>
<dt><code class="env">--unicode_in</code></dt>
<dd><p>Specifies that the input file is UTF16.
</p>
</dd>
<dt><code class="env">-U</code></dt>
<dt><code class="env">--unicode_out</code></dt>
<dd><p>Specifies that messages in the output <code class="code">bin</code> file should be in UTF16
format. This is the default behaviour.
</p>
</dd>
<dt><code class="env">-v</code></dt>
<dt><code class="env">--verbose</code></dt>
<dd><p>Enable verbose mode.
</p>
</dd>
<dt><code class="env">-V</code></dt>
<dt><code class="env">--version</code></dt>
<dd><p>Prints the version number for <code class="command">windmc</code>.
</p>
</dd>
<dt><code class="env">-x <var class="var">path</var></code></dt>
<dt><code class="env">--xdgb <var class="var">path</var></code></dt>
<dd><p>The path of the <code class="code">dbg</code> C include file that maps message id&rsquo;s to the
symbolic name. No such file is generated without specifying the switch.
</p></dd>
</dl>



</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="windres.html">windres</a>, Previous: <a href="addr2line.html">addr2line</a>, Up: <a href="index.html">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
