<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>ar (GNU Binary Utilities)</title>

<meta name="description" content="ar (GNU Binary Utilities)">
<meta name="keywords" content="ar (GNU Binary Utilities)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Binutils-Index.html" rel="index" title="Binutils Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="nm.html" rel="next" title="nm">
<link href="index.html" rel="prev" title="Top">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="chapter-level-extent" id="ar">
<div class="nav-panel">
<p>
Next: <a href="nm.html" accesskey="n" rel="next">nm</a>, Previous: <a href="index.html" accesskey="p" rel="prev">Introduction</a>, Up: <a href="index.html" accesskey="u" rel="up">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h2 class="chapter" id="ar-1">1 ar</h2>

<a class="index-entry-id" id="index-ar"></a>
<a class="index-entry-id" id="index-archives"></a>
<a class="index-entry-id" id="index-collections-of-files"></a>


<div class="example smallexample">
<pre class="example-preformatted">ar [-]<var class="var">p</var>[<var class="var">mod</var>] [<samp class="option">--plugin</samp> <var class="var">name</var>] [<samp class="option">--target</samp> <var class="var">bfdname</var>] [<samp class="option">--output</samp> <var class="var">dirname</var>] [<samp class="option">--record-libdeps</samp> <var class="var">libdeps</var>] [<var class="var">relpos</var>] [<var class="var">count</var>] <var class="var">archive</var> [<var class="var">member</var>&hellip;]
ar -M [ &lt;mri-script ]
</pre></div>


<p>The <small class="sc">GNU</small> <code class="command">ar</code> program creates, modifies, and extracts from
archives.  An <em class="dfn">archive</em> is a single file holding a collection of
other files in a structure that makes it possible to retrieve
the original individual files (called <em class="dfn">members</em> of the archive).
</p>
<p>The original files&rsquo; contents, mode (permissions), timestamp, owner, and
group are preserved in the archive, and can be restored on
extraction.
</p>
<a class="index-entry-id" id="index-name-length"></a>
<p><small class="sc">GNU</small> <code class="command">ar</code> can maintain archives whose members have names of any
length; however, depending on how <code class="command">ar</code> is configured on your
system, a limit on member-name length may be imposed for compatibility
with archive formats maintained with other tools.  If it exists, the
limit is often 15 characters (typical of formats related to a.out) or 16
characters (typical of formats related to coff).
</p>
<a class="index-entry-id" id="index-libraries"></a>
<p><code class="command">ar</code> is considered a binary utility because archives of this sort
are most often used as <em class="dfn">libraries</em> holding commonly needed
subroutines.  Since libraries often will depend on other libraries,
<code class="command">ar</code> can also record the dependencies of a library when the
<samp class="option">--record-libdeps</samp> option is specified.
</p>
<a class="index-entry-id" id="index-symbol-index"></a>
<p><code class="command">ar</code> creates an index to the symbols defined in relocatable
object modules in the archive when you specify the modifier &lsquo;<samp class="samp">s</samp>&rsquo;.
Once created, this index is updated in the archive whenever <code class="command">ar</code>
makes a change to its contents (save for the &lsquo;<samp class="samp">q</samp>&rsquo; update operation).
An archive with such an index speeds up linking to the library, and
allows routines in the library to call each other without regard to
their placement in the archive.
</p>
<p>You may use &lsquo;<samp class="samp">nm -s</samp>&rsquo; or &lsquo;<samp class="samp">nm --print-armap</samp>&rsquo; to list this index
table.  If an archive lacks the table, another form of <code class="command">ar</code> called
<code class="command">ranlib</code> can be used to add just the table.
</p>
<a class="index-entry-id" id="index-thin-archives"></a>
<p><small class="sc">GNU</small> <code class="command">ar</code> can optionally create a <em class="emph">thin</em> archive,
which contains a symbol index and references to the original copies
of the member files of the archive.  This is useful for building
libraries for use within a local build tree, where the relocatable
objects are expected to remain available, and copying the contents of
each object would only waste time and space.
</p>
<p>An archive can either be <em class="emph">thin</em> or it can be normal.  It cannot
be both at the same time.  Once an archive is created its format
cannot be changed without first deleting it and then creating a new
archive in its place.
</p>
<p>Thin archives are also <em class="emph">flattened</em>, so that adding one thin
archive to another thin archive does not nest it, as would happen with
a normal archive.  Instead the elements of the first archive are added
individually to the second archive.
</p>
<p>The paths to the elements of the archive are stored relative to the
archive itself.
</p>
<a class="index-entry-id" id="index-compatibility_002c-ar"></a>
<a class="index-entry-id" id="index-ar-compatibility"></a>
<p><small class="sc">GNU</small> <code class="command">ar</code> is designed to be compatible with two different
facilities.  You can control its activity using command-line options,
like the different varieties of <code class="command">ar</code> on Unix systems; or, if you
specify the single command-line option <samp class="option">-M</samp>, you can control it
with a script supplied via standard input, like the MRI &ldquo;librarian&rdquo;
program.
</p>


<ul class="mini-toc">
<li><a href="ar-cmdline.html" accesskey="1">Controlling <code class="command">ar</code> on the Command Line</a></li>
<li><a href="ar-scripts.html" accesskey="2">Controlling <code class="command">ar</code> with a Script</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="nm.html">nm</a>, Previous: <a href="index.html">Introduction</a>, Up: <a href="index.html">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
