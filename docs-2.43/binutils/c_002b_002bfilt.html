<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>c++filt (GNU Binary Utilities)</title>

<meta name="description" content="c++filt (GNU Binary Utilities)">
<meta name="keywords" content="c++filt (GNU Binary Utilities)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Binutils-Index.html" rel="index" title="Binutils Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="addr2line.html" rel="next" title="addr2line">
<link href="strip.html" rel="prev" title="strip">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
kbd.kbd {font-style: oblique}
-->
</style>


</head>

<body lang="en">
<div class="chapter-level-extent" id="c_002b_002bfilt">
<div class="nav-panel">
<p>
Next: <a href="addr2line.html" accesskey="n" rel="next">addr2line</a>, Previous: <a href="strip.html" accesskey="p" rel="prev">strip</a>, Up: <a href="index.html" accesskey="u" rel="up">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h2 class="chapter" id="c_002b_002bfilt-1">9 c++filt</h2>

<a class="index-entry-id" id="index-c_002b_002bfilt"></a>
<a class="index-entry-id" id="index-demangling-C_002b_002b-symbols"></a>


<div class="example smallexample">
<pre class="example-preformatted">c++filt [<samp class="option">-_</samp>|<samp class="option">--strip-underscore</samp>]
        [<samp class="option">-n</samp>|<samp class="option">--no-strip-underscore</samp>]
        [<samp class="option">-p</samp>|<samp class="option">--no-params</samp>]
        [<samp class="option">-t</samp>|<samp class="option">--types</samp>]
        [<samp class="option">-i</samp>|<samp class="option">--no-verbose</samp>]
        [<samp class="option">-r</samp>|<samp class="option">--no-recurse-limit</samp>]
        [<samp class="option">-R</samp>|<samp class="option">--recurse-limit</samp>]
        [<samp class="option">-s</samp> <var class="var">format</var>|<samp class="option">--format=</samp><var class="var">format</var>]
        [<samp class="option">--help</samp>]  [<samp class="option">--version</samp>]  [<var class="var">symbol</var>&hellip;]
</pre></div>


<a class="index-entry-id" id="index-cxxfilt"></a>
<p>The C++ and Java languages provide function overloading, which means
that you can write many functions with the same name, providing that
each function takes parameters of different types.  In order to be
able to distinguish these similarly named functions C++ and Java
encode them into a low-level assembler name which uniquely identifies
each different version.  This process is known as <em class="dfn">mangling</em>. The
<code class="command">c++filt</code>
<a class="footnote" id="DOCF1" href="#FOOT1"><sup>1</sup></a>
program does the inverse mapping: it decodes (<em class="dfn">demangles</em>) low-level
names into user-level names so that they can be read.
</p>
<p>Every alphanumeric word (consisting of letters, digits, underscores,
dollars, or periods) seen in the input is a potential mangled name.
If the name decodes into a C++ name, the C++ name replaces the
low-level name in the output, otherwise the original word is output.
In this way you can pass an entire assembler source file, containing
mangled names, through <code class="command">c++filt</code> and see the same source file
containing demangled names.
</p>
<p>You can also use <code class="command">c++filt</code> to decipher individual symbols by
passing them on the command line:
</p>
<div class="example">
<pre class="example-preformatted">c++filt <var class="var">symbol</var>
</pre></div>

<p>If no <var class="var">symbol</var> arguments are given, <code class="command">c++filt</code> reads symbol
names from the standard input instead.  All the results are printed on
the standard output.  The difference between reading names from the
command line versus reading names from the standard input is that
command-line arguments are expected to be just mangled names and no
checking is performed to separate them from surrounding text.  Thus
for example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">c++filt -n _Z1fv
</pre></div>

<p>will work and demangle the name to &ldquo;f()&rdquo; whereas:
</p>
<div class="example smallexample">
<pre class="example-preformatted">c++filt -n _Z1fv,
</pre></div>

<p>will not work.  (Note the extra comma at the end of the mangled
name which makes it invalid).  This command however will work:
</p>
<div class="example smallexample">
<pre class="example-preformatted">echo _Z1fv, | c++filt -n
</pre></div>

<p>and will display &ldquo;f(),&rdquo;, i.e., the demangled name followed by a
trailing comma.  This behaviour is because when the names are read
from the standard input it is expected that they might be part of an
assembler source file where there might be extra, extraneous
characters trailing after a mangled name.  For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">    .type   _Z1fv, @function
</pre></div>



<dl class="table">
<dt><code class="env">-_</code></dt>
<dt><code class="env">--strip-underscore</code></dt>
<dd><p>On some systems, both the C and C++ compilers put an underscore in front
of every name.  For example, the C name <code class="code">foo</code> gets the low-level
name <code class="code">_foo</code>.  This option removes the initial underscore.  Whether
<code class="command">c++filt</code> removes the underscore by default is target dependent.
</p>
</dd>
<dt><code class="env">-n</code></dt>
<dt><code class="env">--no-strip-underscore</code></dt>
<dd><p>Do not remove the initial underscore.
</p>
</dd>
<dt><code class="env">-p</code></dt>
<dt><code class="env">--no-params</code></dt>
<dd><p>When demangling the name of a function, do not display the types of
the function&rsquo;s parameters.
</p>
</dd>
<dt><code class="env">-t</code></dt>
<dt><code class="env">--types</code></dt>
<dd><p>Attempt to demangle types as well as function names.  This is disabled
by default since mangled types are normally only used internally in
the compiler, and they can be confused with non-mangled names.  For example,
a function called &ldquo;a&rdquo; treated as a mangled type name would be
demangled to &ldquo;signed char&rdquo;.
</p>
</dd>
<dt><code class="env">-i</code></dt>
<dt><code class="env">--no-verbose</code></dt>
<dd><p>Do not include implementation details (if any) in the demangled
output.
</p>
</dd>
<dt><code class="env">-r</code></dt>
<dt><code class="env">-R</code></dt>
<dt><code class="env">--recurse-limit</code></dt>
<dt><code class="env">--no-recurse-limit</code></dt>
<dt><code class="env">--recursion-limit</code></dt>
<dt><code class="env">--no-recursion-limit</code></dt>
<dd><p>Enables or disables a limit on the amount of recursion performed
whilst demangling strings.  Since the name mangling formats allow for
an infinite level of recursion it is possible to create strings whose
decoding will exhaust the amount of stack space available on the host
machine, triggering a memory fault.  The limit tries to prevent this
from happening by restricting recursion to 2048 levels of nesting.
</p>
<p>The default is for this limit to be enabled, but disabling it may be
necessary in order to demangle truly complicated names.  Note however
that if the recursion limit is disabled then stack exhaustion is
possible and any bug reports about such an event will be rejected.
</p>
<p>The <samp class="option">-r</samp> option is a synonym for the
<samp class="option">--no-recurse-limit</samp> option.  The <samp class="option">-R</samp> option is a
synonym for the <samp class="option">--recurse-limit</samp> option.
</p>
</dd>
<dt><code class="env">-s <var class="var">format</var></code></dt>
<dt><code class="env">--format=<var class="var">format</var></code></dt>
<dd><p><code class="command">c++filt</code> can decode various methods of mangling, used by
different compilers.  The argument to this option selects which
method it uses:
</p>
<dl class="table">
<dt><code class="code">auto</code></dt>
<dd><p>Automatic selection based on executable (the default method)
</p></dd>
<dt><code class="code">gnu</code></dt>
<dd><p>the one used by the <small class="sc">GNU</small> C++ compiler (g++)
</p></dd>
<dt><code class="code">lucid</code></dt>
<dd><p>the one used by the Lucid compiler (lcc)
</p></dd>
<dt><code class="code">arm</code></dt>
<dd><p>the one specified by the C++ Annotated Reference Manual
</p></dd>
<dt><code class="code">hp</code></dt>
<dd><p>the one used by the HP compiler (aCC)
</p></dd>
<dt><code class="code">edg</code></dt>
<dd><p>the one used by the EDG compiler
</p></dd>
<dt><code class="code">gnu-v3</code></dt>
<dd><p>the one used by the <small class="sc">GNU</small> C++ compiler (g++) with the V3 ABI.
</p></dd>
<dt><code class="code">java</code></dt>
<dd><p>the one used by the <small class="sc">GNU</small> Java compiler (gcj)
</p></dd>
<dt><code class="code">gnat</code></dt>
<dd><p>the one used by the <small class="sc">GNU</small> Ada compiler (GNAT).
</p></dd>
</dl>

</dd>
<dt><code class="env">--help</code></dt>
<dd><p>Print a summary of the options to <code class="command">c++filt</code> and exit.
</p>
</dd>
<dt><code class="env">--version</code></dt>
<dd><p>Print the version number of <code class="command">c++filt</code> and exit.
</p></dd>
</dl>



<blockquote class="quotation">
<p><em class="emph">Warning:</em> <code class="command">c++filt</code> is a new utility, and the details of its
user interface are subject to change in future releases.  In particular,
a command-line option may be required in the future to decode a name
passed as an argument on the command line; in other words,
</p>
<div class="example">
<pre class="example-preformatted">c++filt <var class="var">symbol</var>
</pre></div>

<p>may in a future release become
</p>
<div class="example">
<pre class="example-preformatted">c++filt <var class="var">option</var> <var class="var">symbol</var>
</pre></div>
</blockquote>

</div>
<div class="footnotes-segment">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5 class="footnote-body-heading"><a id="FOOT1" href="#DOCF1">(1)</a></h5>
<p>MS-DOS does not allow <kbd class="kbd">+</kbd> characters in file names, so on
MS-DOS this program is named <code class="command">CXXFILT</code>.</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="addr2line.html">addr2line</a>, Previous: <a href="strip.html">strip</a>, Up: <a href="index.html">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
