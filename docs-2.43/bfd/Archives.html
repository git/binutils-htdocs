<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.3, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the BFD library.

Copyright © 1991-2024 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with the
Invariant Sections being "GNU General Public License" and "Funding
Free Software", the Front-Cover texts being (a) (see below), and with
the Back-Cover Texts being (b) (see below).  A copy of the license is
included in the section entitled "GNU Free Documentation License".

(a) The FSF's Front-Cover Text is:

A GNU Manual

(b) The FSF's Back-Cover Text is:

You have freedom to copy and modify this GNU Manual, like GNU
     software.  Copies published by the Free Software Foundation raise
     funds for GNU development. -->
<title>Archives (Untitled Document)</title>

<meta name="description" content="Archives (Untitled Document)">
<meta name="keywords" content="Archives (Untitled Document)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="BFD-Index.html" rel="index" title="BFD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="BFD-front-end.html" rel="up" title="BFD front end">
<link href="Formats.html" rel="next" title="Formats">
<link href="Symbols.html" rel="prev" title="Symbols">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Archives">
<div class="nav-panel">
<p>
Next: <a href="Formats.html" accesskey="n" rel="next">File formats</a>, Previous: <a href="Symbols.html" accesskey="p" rel="prev">Symbols</a>, Up: <a href="BFD-front-end.html" accesskey="u" rel="up">BFD Front End</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="BFD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Archives-1">2.8 Archives</h3>


<p>An archive (or library) is just another BFD.  It has a symbol
table, although there&rsquo;s not much a user program will do with it.
</p>
<p>The big difference between an archive BFD and an ordinary BFD
is that the archive doesn&rsquo;t have sections.  Instead it has a
chain of BFDs that are considered its contents.  These BFDs can
be manipulated like any other.  The BFDs contained in an
archive opened for reading will all be opened for reading.  You
may put either input or output BFDs into an archive opened for
output; they will be handled correctly when the archive is closed.
</p>
<p>Use <code class="code">bfd_openr_next_archived_file</code> to step through
the contents of an archive opened for input.  You don&rsquo;t
have to read the entire archive if you don&rsquo;t want
to!  Read it until you find what you want.
</p>
<p>A BFD returned by <code class="code">bfd_openr_next_archived_file</code> can be
closed manually with <code class="code">bfd_close</code>.  If you do not close it,
then a second iteration through the members of an archive may
return the same BFD.  If you close the archive BFD, then all
the member BFDs will automatically be closed as well.
</p>
<p>Archive contents of output BFDs are chained through the
<code class="code">archive_next</code> pointer in a BFD.  The first one is findable
through the <code class="code">archive_head</code> slot of the archive.  Set it with
<code class="code">bfd_set_archive_head</code> (q.v.).  A given BFD may be in only
one open output archive at a time.
</p>
<p>As expected, the BFD archive code is more general than the
archive code of any given environment.  BFD archives may
contain files of different formats (e.g., a.out and coff) and
even different architectures.  You may even place archives
recursively into archives!
</p>
<p>This can cause unexpected confusion, since some archive
formats are more expressive than others.  For instance, Intel
COFF archives can preserve long filenames; SunOS a.out archives
cannot.  If you move a file from the first to the second
format and back again, the filename may be truncated.
Likewise, different a.out environments have different
conventions as to how they truncate filenames, whether they
preserve directory names in filenames, etc.  When
interoperating with native tools, be sure your files are
homogeneous.
</p>
<p>Beware: most of these formats do not react well to the
presence of spaces in filenames.  We do the best we can, but
can&rsquo;t always handle this case due to restrictions in the format of
archives.  Many Unix utilities are braindead in regards to
spaces and such in filenames anyway, so this shouldn&rsquo;t be much
of a restriction.
</p>
<p>Archives are supported in BFD in <code class="code">archive.c</code>.
</p>
<ul class="mini-toc">
<li><a href="#Archive-functions" accesskey="1">Archive functions</a></li>
</ul>
<div class="subsection-level-extent" id="Archive-functions">
<h4 class="subsection">2.8.1 Archive functions</h4>


<a class="index-entry-id" id="index-bfd_005fget_005fnext_005fmapent"></a>
<ul class="mini-toc">
<li><a href="#bfd_005fget_005fnext_005fmapent" accesskey="1"><code class="code">bfd_get_next_mapent</code></a></li>
<li><a href="#bfd_005fset_005farchive_005fhead" accesskey="2"><code class="code">bfd_set_archive_head</code></a></li>
<li><a href="#bfd_005fopenr_005fnext_005farchived_005ffile" accesskey="3"><code class="code">bfd_openr_next_archived_file</code></a></li>
</ul>
<div class="subsubsection-level-extent" id="bfd_005fget_005fnext_005fmapent">
<h4 class="subsubsection">2.8.1.1 <code class="code">bfd_get_next_mapent</code></h4>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-bfd_005fget_005fnext_005fmapent-1"><span class="category-def">Function: </span><span><code class="def-type">symindex</code> <strong class="def-name">bfd_get_next_mapent</strong> <code class="def-code-arguments">(bfd *abfd, symindex previous, carsym **sym);</code><a class="copiable-link" href='#index-bfd_005fget_005fnext_005fmapent-1'> &para;</a></span></dt>
<dd><p>Step through archive <var class="var">abfd</var>&rsquo;s symbol table (if it
has one).  Successively update <var class="var">sym</var> with the next symbol&rsquo;s
information, returning that symbol&rsquo;s (internal) index into the
symbol table.
</p>
<p>Supply <code class="code">BFD_NO_MORE_SYMBOLS</code> as the <var class="var">previous</var> entry to get
the first one; returns <code class="code">BFD_NO_MORE_SYMBOLS</code> when you&rsquo;ve already
got the last one.
</p>
<p>A <code class="code">carsym</code> is a canonical archive symbol.  The only
user-visible element is its name, a null-terminated string.
</p>
</dd></dl>
<a class="index-entry-id" id="index-bfd_005fset_005farchive_005fhead"></a>
</div>
<div class="subsubsection-level-extent" id="bfd_005fset_005farchive_005fhead">
<h4 class="subsubsection">2.8.1.2 <code class="code">bfd_set_archive_head</code></h4>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-bfd_005fset_005farchive_005fhead-1"><span class="category-def">Function: </span><span><code class="def-type">bool</code> <strong class="def-name">bfd_set_archive_head</strong> <code class="def-code-arguments">(bfd *output, bfd *new_head);</code><a class="copiable-link" href='#index-bfd_005fset_005farchive_005fhead-1'> &para;</a></span></dt>
<dd><p>Set the head of the chain of
BFDs contained in the archive <var class="var">output</var> to <var class="var">new_head</var>.
</p>
</dd></dl>
<a class="index-entry-id" id="index-bfd_005fopenr_005fnext_005farchived_005ffile"></a>
</div>
<div class="subsubsection-level-extent" id="bfd_005fopenr_005fnext_005farchived_005ffile">
<h4 class="subsubsection">2.8.1.3 <code class="code">bfd_openr_next_archived_file</code></h4>
<dl class="first-deftypefn">
<dt class="deftypefn" id="index-_002abfd_005fopenr_005fnext_005farchived_005ffile"><span class="category-def">Function: </span><span><code class="def-type">bfd</code> <strong class="def-name">*bfd_openr_next_archived_file</strong> <code class="def-code-arguments">(bfd *archive, bfd *previous);</code><a class="copiable-link" href='#index-_002abfd_005fopenr_005fnext_005farchived_005ffile'> &para;</a></span></dt>
<dd><p>Provided a BFD, <var class="var">archive</var>, containing an archive and NULL, open
an input BFD on the first contained element and returns that.
Subsequent calls should pass the archive and the previous return
value to return a created BFD to the next contained element.  NULL
is returned when there are no more.
Note - if you want to process the bfd returned by this call be
sure to call bfd_check_format() on it first.
</p>
</dd></dl>

</div>
</div>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Formats.html">File formats</a>, Previous: <a href="Symbols.html">Symbols</a>, Up: <a href="BFD-front-end.html">BFD Front End</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="BFD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
