<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.41.

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Builtin Functions (LD)</title>

<meta name="description" content="Builtin Functions (LD)">
<meta name="keywords" content="Builtin Functions (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Expressions.html" rel="up" title="Expressions">
<link href="Expression-Section.html" rel="prev" title="Expression Section">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Builtin-Functions">
<div class="nav-panel">
<p>
Previous: <a href="Expression-Section.html" accesskey="p" rel="prev">The Section of an Expression</a>, Up: <a href="Expressions.html" accesskey="u" rel="up">Expressions in Linker Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Builtin-Functions-1">3.10.9 Builtin Functions</h4>
<a class="index-entry-id" id="index-functions-in-expressions"></a>
<p>The linker script language includes a number of builtin functions for
use in linker script expressions.
</p>
<dl class="table">
<dt id='index-ABSOLUTE_0028exp_0029'><span><code class="code">ABSOLUTE(<var class="var">exp</var>)</code><a class="copiable-link" href='#index-ABSOLUTE_0028exp_0029'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-expression_002c-absolute"></a>
<p>Return the absolute (non-relocatable, as opposed to non-negative) value
of the expression <var class="var">exp</var>.  Primarily useful to assign an absolute
value to a symbol within a section definition, where symbol values are
normally section relative.  See <a class="xref" href="Expression-Section.html">The Section of an Expression</a>.
</p>
</dd>
<dt id='index-ADDR_0028section_0029'><span><code class="code">ADDR(<var class="var">section</var>)</code><a class="copiable-link" href='#index-ADDR_0028section_0029'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-section-address-in-expression"></a>
<p>Return the address (VMA) of the named <var class="var">section</var>.  Your
script must previously have defined the location of that section.  In
the following example, <code class="code">start_of_output_1</code>, <code class="code">symbol_1</code> and
<code class="code">symbol_2</code> are assigned equivalent values, except that
<code class="code">symbol_1</code> will be relative to the <code class="code">.output1</code> section while
the other two will be absolute:
</p><div class="example smallexample">
<div class="group"><pre class="example-preformatted">SECTIONS { &hellip;
  .output1 :
    {
    start_of_output_1 = ABSOLUTE(.);
    &hellip;
    }
  .output :
    {
    symbol_1 = ADDR(.output1);
    symbol_2 = start_of_output_1;
    }
&hellip; }
</pre></div></div>

</dd>
<dt id='index-ALIGN_0028align_0029'><span><code class="code">ALIGN(<var class="var">align</var>)</code><a class="copiable-link" href='#index-ALIGN_0028align_0029'> &para;</a></span></dt>
<dt><code class="code">ALIGN(<var class="var">exp</var>,<var class="var">align</var>)</code></dt>
<dd><a class="index-entry-id" id="index-ALIGN_0028exp_002calign_0029"></a>
<a class="index-entry-id" id="index-round-up-location-counter"></a>
<a class="index-entry-id" id="index-align-location-counter"></a>
<a class="index-entry-id" id="index-round-up-expression"></a>
<a class="index-entry-id" id="index-align-expression"></a>
<p>Return the location counter (<code class="code">.</code>) or arbitrary expression aligned
to the next <var class="var">align</var> boundary.  The single operand <code class="code">ALIGN</code>
doesn&rsquo;t change the value of the location counter&mdash;it just does
arithmetic on it.  The two operand <code class="code">ALIGN</code> allows an arbitrary
expression to be aligned upwards (<code class="code">ALIGN(<var class="var">align</var>)</code> is
equivalent to <code class="code">ALIGN(ABSOLUTE(.), <var class="var">align</var>)</code>).
</p>
<p>Here is an example which aligns the output <code class="code">.data</code> section to the
next <code class="code">0x2000</code> byte boundary after the preceding section and sets a
variable within the section to the next <code class="code">0x8000</code> boundary after the
input sections:
</p><div class="example smallexample">
<div class="group"><pre class="example-preformatted">SECTIONS { &hellip;
  .data ALIGN(0x2000): {
    *(.data)
    variable = ALIGN(0x8000);
  }
&hellip; }
</pre></div></div>
<p>The first use of <code class="code">ALIGN</code> in this example specifies the location of
a section because it is used as the optional <var class="var">address</var> attribute of
a section definition (see <a class="pxref" href="Output-Section-Address.html">Output Section Address</a>).  The second use
of <code class="code">ALIGN</code> is used to defines the value of a symbol.
</p>
<p>The builtin function <code class="code">NEXT</code> is closely related to <code class="code">ALIGN</code>.
</p>
</dd>
<dt id='index-ALIGNOF_0028section_0029'><span><code class="code">ALIGNOF(<var class="var">section</var>)</code><a class="copiable-link" href='#index-ALIGNOF_0028section_0029'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-section-alignment"></a>
<p>Return the alignment in bytes of the named <var class="var">section</var>, if that section has
been allocated.  If the section has not been allocated when this is
evaluated, the linker will report an error. In the following example,
the alignment of the <code class="code">.output</code> section is stored as the first
value in that section.
</p><div class="example smallexample">
<div class="group"><pre class="example-preformatted">SECTIONS{ &hellip;
  .output {
    LONG (ALIGNOF (.output))
    &hellip;
    }
&hellip; }
</pre></div></div>

</dd>
<dt id='index-BLOCK_0028exp_0029'><span><code class="code">BLOCK(<var class="var">exp</var>)</code><a class="copiable-link" href='#index-BLOCK_0028exp_0029'> &para;</a></span></dt>
<dd><p>This is a synonym for <code class="code">ALIGN</code>, for compatibility with older linker
scripts.  It is most often seen when setting the address of an output
section.
</p>
</dd>
<dt id='index-DATA_005fSEGMENT_005fALIGN_0028maxpagesize_002c-commonpagesize_0029'><span><code class="code">DATA_SEGMENT_ALIGN(<var class="var">maxpagesize</var>, <var class="var">commonpagesize</var>)</code><a class="copiable-link" href='#index-DATA_005fSEGMENT_005fALIGN_0028maxpagesize_002c-commonpagesize_0029'> &para;</a></span></dt>
<dd><p>This is equivalent to either
</p><div class="example smallexample">
<pre class="example-preformatted">(ALIGN(<var class="var">maxpagesize</var>) + (. &amp; (<var class="var">maxpagesize</var> - 1)))
</pre></div>
<p>or
</p><div class="example smallexample">
<pre class="example-preformatted">(ALIGN(<var class="var">maxpagesize</var>)
 + ((. + <var class="var">commonpagesize</var> - 1) &amp; (<var class="var">maxpagesize</var> - <var class="var">commonpagesize</var>)))
</pre></div>
<p>depending on whether the latter uses fewer <var class="var">commonpagesize</var> sized pages
for the data segment (area between the result of this expression and
<code class="code">DATA_SEGMENT_END</code>) than the former or not.
If the latter form is used, it means <var class="var">commonpagesize</var> bytes of runtime
memory will be saved at the expense of up to <var class="var">commonpagesize</var> wasted
bytes in the on-disk file.
</p>
<p>This expression can only be used directly in <code class="code">SECTIONS</code> commands, not in
any output section descriptions and only once in the linker script.
<var class="var">commonpagesize</var> should be less or equal to <var class="var">maxpagesize</var> and should
be the system page size the object wants to be optimized for while still
running on system page sizes up to <var class="var">maxpagesize</var>.  Note however
that &lsquo;<samp class="samp">-z relro</samp>&rsquo; protection will not be effective if the system
page size is larger than <var class="var">commonpagesize</var>.
</p>
<p>Example:
</p><div class="example smallexample">
<pre class="example-preformatted">  . = DATA_SEGMENT_ALIGN(0x10000, 0x2000);
</pre></div>

</dd>
<dt id='index-DATA_005fSEGMENT_005fEND_0028exp_0029'><span><code class="code">DATA_SEGMENT_END(<var class="var">exp</var>)</code><a class="copiable-link" href='#index-DATA_005fSEGMENT_005fEND_0028exp_0029'> &para;</a></span></dt>
<dd><p>This defines the end of data segment for <code class="code">DATA_SEGMENT_ALIGN</code>
evaluation purposes.
</p>
<div class="example smallexample">
<pre class="example-preformatted">  . = DATA_SEGMENT_END(.);
</pre></div>

</dd>
<dt id='index-DATA_005fSEGMENT_005fRELRO_005fEND_0028offset_002c-exp_0029'><span><code class="code">DATA_SEGMENT_RELRO_END(<var class="var">offset</var>, <var class="var">exp</var>)</code><a class="copiable-link" href='#index-DATA_005fSEGMENT_005fRELRO_005fEND_0028offset_002c-exp_0029'> &para;</a></span></dt>
<dd><p>This defines the end of the <code class="code">PT_GNU_RELRO</code> segment when
&lsquo;<samp class="samp">-z relro</samp>&rsquo; option is used.
When &lsquo;<samp class="samp">-z relro</samp>&rsquo; option is not present, <code class="code">DATA_SEGMENT_RELRO_END</code>
does nothing, otherwise <code class="code">DATA_SEGMENT_ALIGN</code> is padded so that
<var class="var">exp</var> + <var class="var">offset</var> is aligned to the <var class="var">commonpagesize</var>
argument given to <code class="code">DATA_SEGMENT_ALIGN</code>.  If present in the linker
script, it must be placed between <code class="code">DATA_SEGMENT_ALIGN</code> and
<code class="code">DATA_SEGMENT_END</code>.  Evaluates to the second argument plus any
padding needed at the end of the <code class="code">PT_GNU_RELRO</code> segment due to
section alignment.
</p>
<div class="example smallexample">
<pre class="example-preformatted">  . = DATA_SEGMENT_RELRO_END(24, .);
</pre></div>

</dd>
<dt id='index-DEFINED_0028symbol_0029'><span><code class="code">DEFINED(<var class="var">symbol</var>)</code><a class="copiable-link" href='#index-DEFINED_0028symbol_0029'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-symbol-defaults"></a>
<p>Return 1 if <var class="var">symbol</var> is in the linker global symbol table and is
defined before the statement using DEFINED in the script, otherwise
return 0.  You can use this function to provide
default values for symbols.  For example, the following script fragment
shows how to set a global symbol &lsquo;<samp class="samp">begin</samp>&rsquo; to the first location in
the &lsquo;<samp class="samp">.text</samp>&rsquo; section&mdash;but if a symbol called &lsquo;<samp class="samp">begin</samp>&rsquo; already
existed, its value is preserved:
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">SECTIONS { &hellip;
  .text : {
    begin = DEFINED(begin) ? begin : . ;
    &hellip;
  }
  &hellip;
}
</pre></div></div>

</dd>
<dt id='index-LENGTH_0028memory_0029'><span><code class="code">LENGTH(<var class="var">memory</var>)</code><a class="copiable-link" href='#index-LENGTH_0028memory_0029'> &para;</a></span></dt>
<dd><p>Return the length of the memory region named <var class="var">memory</var>.
</p>
</dd>
<dt id='index-LOADADDR_0028section_0029'><span><code class="code">LOADADDR(<var class="var">section</var>)</code><a class="copiable-link" href='#index-LOADADDR_0028section_0029'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-section-load-address-in-expression"></a>
<p>Return the absolute LMA of the named <var class="var">section</var>.  (see <a class="pxref" href="Output-Section-LMA.html">Output Section LMA</a>).
</p>
</dd>
<dt id='index-LOG2CEIL_0028exp_0029'><span><code class="code">LOG2CEIL(<var class="var">exp</var>)</code><a class="copiable-link" href='#index-LOG2CEIL_0028exp_0029'> &para;</a></span></dt>
<dd><p>Return the binary logarithm of <var class="var">exp</var> rounded towards infinity.
<code class="code">LOG2CEIL(0)</code> returns 0.
</p>
<a class="index-entry-id" id="index-MAX"></a>
</dd>
<dt><code class="code">MAX(<var class="var">exp1</var>, <var class="var">exp2</var>)</code></dt>
<dd><p>Returns the maximum of <var class="var">exp1</var> and <var class="var">exp2</var>.
</p>
<a class="index-entry-id" id="index-MIN"></a>
</dd>
<dt><code class="code">MIN(<var class="var">exp1</var>, <var class="var">exp2</var>)</code></dt>
<dd><p>Returns the minimum of <var class="var">exp1</var> and <var class="var">exp2</var>.
</p>
</dd>
<dt id='index-NEXT_0028exp_0029'><span><code class="code">NEXT(<var class="var">exp</var>)</code><a class="copiable-link" href='#index-NEXT_0028exp_0029'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-unallocated-address_002c-next"></a>
<p>Return the next unallocated address that is a multiple of <var class="var">exp</var>.
This function is closely related to <code class="code">ALIGN(<var class="var">exp</var>)</code>; unless you
use the <code class="code">MEMORY</code> command to define discontinuous memory for the
output file, the two functions are equivalent.
</p>
</dd>
<dt id='index-ORIGIN_0028memory_0029'><span><code class="code">ORIGIN(<var class="var">memory</var>)</code><a class="copiable-link" href='#index-ORIGIN_0028memory_0029'> &para;</a></span></dt>
<dd><p>Return the origin of the memory region named <var class="var">memory</var>.
</p>
</dd>
<dt id='index-SEGMENT_005fSTART_0028segment_002c-default_0029'><span><code class="code">SEGMENT_START(<var class="var">segment</var>, <var class="var">default</var>)</code><a class="copiable-link" href='#index-SEGMENT_005fSTART_0028segment_002c-default_0029'> &para;</a></span></dt>
<dd><p>Return the base address of the named <var class="var">segment</var>.  If an explicit
value has already been given for this segment (with a command-line
&lsquo;<samp class="samp">-T</samp>&rsquo; option) then that value will be returned otherwise the value
will be <var class="var">default</var>.  At present, the &lsquo;<samp class="samp">-T</samp>&rsquo; command-line option
can only be used to set the base address for the &ldquo;text&rdquo;, &ldquo;data&rdquo;, and
&ldquo;bss&rdquo; sections, but you can use <code class="code">SEGMENT_START</code> with any segment
name.
</p>
</dd>
<dt id='index-SIZEOF_0028section_0029'><span><code class="code">SIZEOF(<var class="var">section</var>)</code><a class="copiable-link" href='#index-SIZEOF_0028section_0029'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-section-size"></a>
<p>Return the size in bytes of the named <var class="var">section</var>, if that section has
been allocated.  If the section has not been allocated when this is
evaluated, the linker will report an error.  In the following example,
<code class="code">symbol_1</code> and <code class="code">symbol_2</code> are assigned identical values:
</p><div class="example smallexample">
<div class="group"><pre class="example-preformatted">SECTIONS{ &hellip;
  .output {
    .start = . ;
    &hellip;
    .end = . ;
    }
  symbol_1 = .end - .start ;
  symbol_2 = SIZEOF(.output);
&hellip; }
</pre></div></div>

</dd>
<dt id='index-SIZEOF_005fHEADERS'><span><code class="code">SIZEOF_HEADERS</code><a class="copiable-link" href='#index-SIZEOF_005fHEADERS'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-header-size"></a>
<p>Return the size in bytes of the output file&rsquo;s headers.  This is
information which appears at the start of the output file.  You can use
this number when setting the start address of the first section, if you
choose, to facilitate paging.
</p>
<a class="index-entry-id" id="index-not-enough-room-for-program-headers"></a>
<a class="index-entry-id" id="index-program-headers_002c-not-enough-room"></a>
<p>When producing an ELF output file, if the linker script uses the
<code class="code">SIZEOF_HEADERS</code> builtin function, the linker must compute the
number of program headers before it has determined all the section
addresses and sizes.  If the linker later discovers that it needs
additional program headers, it will report an error &lsquo;<samp class="samp">not enough
room for program headers</samp>&rsquo;.  To avoid this error, you must avoid using
the <code class="code">SIZEOF_HEADERS</code> function, or you must rework your linker
script to avoid forcing the linker to use additional program headers, or
you must define the program headers yourself using the <code class="code">PHDRS</code>
command (see <a class="pxref" href="PHDRS.html">PHDRS Command</a>).
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Expression-Section.html">The Section of an Expression</a>, Up: <a href="Expressions.html">Expressions in Linker Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
