<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.41.

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>PHDRS (LD)</title>

<meta name="description" content="PHDRS (LD)">
<meta name="keywords" content="PHDRS (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Scripts.html" rel="up" title="Scripts">
<link href="VERSION.html" rel="next" title="VERSION">
<link href="MEMORY.html" rel="prev" title="MEMORY">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="PHDRS">
<div class="nav-panel">
<p>
Next: <a href="VERSION.html" accesskey="n" rel="next">VERSION Command</a>, Previous: <a href="MEMORY.html" accesskey="p" rel="prev">MEMORY Command</a>, Up: <a href="Scripts.html" accesskey="u" rel="up">Linker Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="PHDRS-Command">3.8 PHDRS Command</h3>
<a class="index-entry-id" id="index-PHDRS"></a>
<a class="index-entry-id" id="index-program-headers"></a>
<a class="index-entry-id" id="index-ELF-program-headers"></a>
<a class="index-entry-id" id="index-program-segments"></a>
<a class="index-entry-id" id="index-segments_002c-ELF"></a>
<p>The ELF object file format uses <em class="dfn">program headers</em>, also knows as
<em class="dfn">segments</em>.  The program headers describe how the program should be
loaded into memory.  You can print them out by using the <code class="code">objdump</code>
program with the &lsquo;<samp class="samp">-p</samp>&rsquo; option.
</p>
<p>When you run an ELF program on a native ELF system, the system loader
reads the program headers in order to figure out how to load the
program.  This will only work if the program headers are set correctly.
This manual does not describe the details of how the system loader
interprets program headers; for more information, see the ELF ABI.
</p>
<p>The linker will create reasonable program headers by default.  However,
in some cases, you may need to specify the program headers more
precisely.  You may use the <code class="code">PHDRS</code> command for this purpose.  When
the linker sees the <code class="code">PHDRS</code> command in the linker script, it will
not create any program headers other than the ones specified.
</p>
<p>The linker only pays attention to the <code class="code">PHDRS</code> command when
generating an ELF output file.  In other cases, the linker will simply
ignore <code class="code">PHDRS</code>.
</p>
<p>This is the syntax of the <code class="code">PHDRS</code> command.  The words <code class="code">PHDRS</code>,
<code class="code">FILEHDR</code>, <code class="code">AT</code>, and <code class="code">FLAGS</code> are keywords.
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">PHDRS
{
  <var class="var">name</var> <var class="var">type</var> [ FILEHDR ] [ PHDRS ] [ AT ( <var class="var">address</var> ) ]
        [ FLAGS ( <var class="var">flags</var> ) ] ;
}
</pre></div></div>

<p>The <var class="var">name</var> is used only for reference in the <code class="code">SECTIONS</code> command
of the linker script.  It is not put into the output file.  Program
header names are stored in a separate name space, and will not conflict
with symbol names, file names, or section names.  Each program header
must have a distinct name.  The headers are processed in order and it
is usual for them to map to sections in ascending load address order.
</p>
<p>Certain program header types describe segments of memory which the
system loader will load from the file.  In the linker script, you
specify the contents of these segments by placing allocatable output
sections in the segments.  You use the &lsquo;<samp class="samp">:<var class="var">phdr</var></samp>&rsquo; output section
attribute to place a section in a particular segment.  See <a class="xref" href="Output-Section-Phdr.html">Output Section Phdr</a>.
</p>
<p>It is normal to put certain sections in more than one segment.  This
merely implies that one segment of memory contains another.  You may
repeat &lsquo;<samp class="samp">:<var class="var">phdr</var></samp>&rsquo;, using it once for each segment which should
contain the section.
</p>
<p>If you place a section in one or more segments using &lsquo;<samp class="samp">:<var class="var">phdr</var></samp>&rsquo;,
then the linker will place all subsequent allocatable sections which do
not specify &lsquo;<samp class="samp">:<var class="var">phdr</var></samp>&rsquo; in the same segments.  This is for
convenience, since generally a whole set of contiguous sections will be
placed in a single segment.  You can use <code class="code">:NONE</code> to override the
default segment and tell the linker to not put the section in any
segment at all.
</p>
<a class="index-entry-id" id="index-FILEHDR"></a>
<a class="index-entry-id" id="index-PHDRS-1"></a>
<p>You may use the <code class="code">FILEHDR</code> and <code class="code">PHDRS</code> keywords after
the program header type to further describe the contents of the segment.
The <code class="code">FILEHDR</code> keyword means that the segment should include the ELF
file header.  The <code class="code">PHDRS</code> keyword means that the segment should
include the ELF program headers themselves.  If applied to a loadable
segment (<code class="code">PT_LOAD</code>), all prior loadable segments must have one of
these keywords.
</p>
<p>The <var class="var">type</var> may be one of the following.  The numbers indicate the
value of the keyword.
</p>
<dl class="table">
<dt><code class="code">PT_NULL</code> (0)</dt>
<dd><p>Indicates an unused program header.
</p>
</dd>
<dt><code class="code">PT_LOAD</code> (1)</dt>
<dd><p>Indicates that this program header describes a segment to be loaded from
the file.
</p>
</dd>
<dt><code class="code">PT_DYNAMIC</code> (2)</dt>
<dd><p>Indicates a segment where dynamic linking information can be found.
</p>
</dd>
<dt><code class="code">PT_INTERP</code> (3)</dt>
<dd><p>Indicates a segment where the name of the program interpreter may be
found.
</p>
</dd>
<dt><code class="code">PT_NOTE</code> (4)</dt>
<dd><p>Indicates a segment holding note information.
</p>
</dd>
<dt><code class="code">PT_SHLIB</code> (5)</dt>
<dd><p>A reserved program header type, defined but not specified by the ELF
ABI.
</p>
</dd>
<dt><code class="code">PT_PHDR</code> (6)</dt>
<dd><p>Indicates a segment where the program headers may be found.
</p>
</dd>
<dt><code class="code">PT_TLS</code> (7)</dt>
<dd><p>Indicates a segment containing thread local storage.
</p>
</dd>
<dt><var class="var">expression</var></dt>
<dd><p>An expression giving the numeric type of the program header.  This may
be used for types not defined above.
</p></dd>
</dl>

<p>You can specify that a segment should be loaded at a particular address
in memory by using an <code class="code">AT</code> expression.  This is identical to the
<code class="code">AT</code> command used as an output section attribute (see <a class="pxref" href="Output-Section-LMA.html">Output Section LMA</a>).  The <code class="code">AT</code> command for a program header overrides the
output section attribute.
</p>
<p>The linker will normally set the segment flags based on the sections
which comprise the segment.  You may use the <code class="code">FLAGS</code> keyword to
explicitly specify the segment flags.  The value of <var class="var">flags</var> must be
an integer.  It is used to set the <code class="code">p_flags</code> field of the program
header.
</p>
<p>Here is an example of <code class="code">PHDRS</code>.  This shows a typical set of program
headers used on a native ELF system.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">PHDRS
{
  headers PT_PHDR PHDRS ;
  interp PT_INTERP ;
  text PT_LOAD FILEHDR PHDRS ;
  data PT_LOAD ;
  dynamic PT_DYNAMIC ;
}

SECTIONS
{
  . = SIZEOF_HEADERS;
  .interp : { *(.interp) } :text :interp
  .text : { *(.text) } :text
  .rodata : { *(.rodata) } /* defaults to :text */
  &hellip;
  . = . + 0x1000; /* move to a new page in memory */
  .data : { *(.data) } :data
  .dynamic : { *(.dynamic) } :data :dynamic
  &hellip;
}
</pre></div></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="VERSION.html">VERSION Command</a>, Previous: <a href="MEMORY.html">MEMORY Command</a>, Up: <a href="Scripts.html">Linker Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
