<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.41.

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Output Section Keywords (LD)</title>

<meta name="description" content="Output Section Keywords (LD)">
<meta name="keywords" content="Output Section Keywords (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="SECTIONS.html" rel="up" title="SECTIONS">
<link href="Output-Section-Discarding.html" rel="next" title="Output Section Discarding">
<link href="Output-Section-Data.html" rel="prev" title="Output Section Data">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
span.w-nolinebreak-text {white-space: nowrap}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="Output-Section-Keywords">
<div class="nav-panel">
<p>
Next: <a href="Output-Section-Discarding.html" accesskey="n" rel="next">Output Section Discarding</a>, Previous: <a href="Output-Section-Data.html" accesskey="p" rel="prev">Output Section Data</a>, Up: <a href="SECTIONS.html" accesskey="u" rel="up">SECTIONS Command</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Output-Section-Keywords-1">3.6.6 Output Section Keywords</h4>
<p>There are a couple of keywords which can appear as output section
commands.
</p>
<dl class="table">
<dd><a class="index-entry-id" id="index-CREATE_005fOBJECT_005fSYMBOLS"></a>
<a class="index-entry-id" id="index-input-filename-symbols"></a>
<a class="index-entry-id" id="index-filename-symbols"></a>
</dd>
<dt><code class="code">CREATE_OBJECT_SYMBOLS</code></dt>
<dd><p>The command tells the linker to create a symbol for each input file.
The name of each symbol will be the name of the corresponding input
file.  The section of each symbol will be the output section in which
the <code class="code">CREATE_OBJECT_SYMBOLS</code> command appears.
</p>
<p>This is conventional for the a.out object file format.  It is not
normally used for any other object file format.
</p>
<a class="index-entry-id" id="index-CONSTRUCTORS"></a>
<a class="index-entry-id" id="index-C_002b_002b-constructors_002c-arranging-in-link"></a>
<a class="index-entry-id" id="index-constructors_002c-arranging-in-link"></a>
</dd>
<dt><code class="code">CONSTRUCTORS</code></dt>
<dd><p>When linking using the a.out object file format, the linker uses an
unusual set construct to support C++ global constructors and
destructors.  When linking object file formats which do not support
arbitrary sections, such as ECOFF and XCOFF, the linker will
automatically recognize C++ global constructors and destructors by name.
For these object file formats, the <code class="code">CONSTRUCTORS</code> command tells the
linker to place constructor information in the output section where the
<code class="code">CONSTRUCTORS</code> command appears.  The <code class="code">CONSTRUCTORS</code> command is
ignored for other object file formats.
</p>
<p>The symbol <code class="code"><span class="w-nolinebreak-text">__CTOR_LIST__</span></code><!-- /@w --> marks the start of the global
constructors, and the symbol <code class="code"><span class="w-nolinebreak-text">__CTOR_END__</span></code><!-- /@w --> marks the end.
Similarly, <code class="code"><span class="w-nolinebreak-text">__DTOR_LIST__</span></code><!-- /@w --> and <code class="code"><span class="w-nolinebreak-text">__DTOR_END__</span></code><!-- /@w --> mark
the start and end of the global destructors.  The
first word in the list is the number of entries, followed by the address
of each constructor or destructor, followed by a zero word.  The
compiler must arrange to actually run the code.  For these object file
formats <small class="sc">GNU</small> C++ normally calls constructors from a subroutine
<code class="code">__main</code>; a call to <code class="code">__main</code> is automatically inserted into
the startup code for <code class="code">main</code>.  <small class="sc">GNU</small> C++ normally runs
destructors either by using <code class="code">atexit</code>, or directly from the function
<code class="code">exit</code>.
</p>
<p>For object file formats such as <code class="code">COFF</code> or <code class="code">ELF</code> which support
arbitrary section names, <small class="sc">GNU</small> C++ will normally arrange to put the
addresses of global constructors and destructors into the <code class="code">.ctors</code>
and <code class="code">.dtors</code> sections.  Placing the following sequence into your
linker script will build the sort of table which the <small class="sc">GNU</small> C++
runtime code expects to see.
</p>
<div class="example smallexample">
<pre class="example-preformatted">      __CTOR_LIST__ = .;
      LONG((__CTOR_END__ - __CTOR_LIST__) / 4 - 2)
      *(.ctors)
      LONG(0)
      __CTOR_END__ = .;
      __DTOR_LIST__ = .;
      LONG((__DTOR_END__ - __DTOR_LIST__) / 4 - 2)
      *(.dtors)
      LONG(0)
      __DTOR_END__ = .;
</pre></div>

<p>If you are using the <small class="sc">GNU</small> C++ support for initialization priority,
which provides some control over the order in which global constructors
are run, you must sort the constructors at link time to ensure that they
are executed in the correct order.  When using the <code class="code">CONSTRUCTORS</code>
command, use &lsquo;<samp class="samp">SORT_BY_NAME(CONSTRUCTORS)</samp>&rsquo; instead.  When using the
<code class="code">.ctors</code> and <code class="code">.dtors</code> sections, use &lsquo;<samp class="samp">*(SORT_BY_NAME(.ctors))</samp>&rsquo; and
&lsquo;<samp class="samp">*(SORT_BY_NAME(.dtors))</samp>&rsquo; instead of just &lsquo;<samp class="samp">*(.ctors)</samp>&rsquo; and
&lsquo;<samp class="samp">*(.dtors)</samp>&rsquo;.
</p>
<p>Normally the compiler and linker will handle these issues automatically,
and you will not need to concern yourself with them.  However, you may
need to consider this if you are using C++ and writing your own linker
scripts.
</p>
</dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Output-Section-Discarding.html">Output Section Discarding</a>, Previous: <a href="Output-Section-Data.html">Output Section Data</a>, Up: <a href="SECTIONS.html">SECTIONS Command</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
