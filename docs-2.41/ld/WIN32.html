<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.41.

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>WIN32 (LD)</title>

<meta name="description" content="WIN32 (LD)">
<meta name="keywords" content="WIN32 (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Machine-Dependent.html" rel="up" title="Machine Dependent">
<link href="Xtensa.html" rel="next" title="Xtensa">
<link href="TI-COFF.html" rel="prev" title="TI COFF">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="WIN32">
<div class="nav-panel">
<p>
Next: <a href="Xtensa.html" accesskey="n" rel="next"><code class="code">ld</code> and Xtensa Processors</a>, Previous: <a href="TI-COFF.html" accesskey="p" rel="prev"><code class="command">ld</code>&rsquo;s Support for Various TI COFF Versions</a>, Up: <a href="Machine-Dependent.html" accesskey="u" rel="up">Machine Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="ld-and-WIN32-_0028cygwin_002fmingw_0029">5.16 <code class="command">ld</code> and WIN32 (cygwin/mingw)</h3>

<p>This section describes some of the win32 specific <code class="command">ld</code> issues.
See <a class="ref" href="Options.html">Command-line Options</a> for detailed description of the
command-line options mentioned here.
</p>
<dl class="table">
<dd><a class="index-entry-id" id="index-import-libraries"></a>
</dd>
<dt><em class="emph">import libraries</em></dt>
<dd><p>The standard Windows linker creates and uses so-called import
libraries, which contains information for linking to dll&rsquo;s.  They are
regular static archives and are handled as any other static
archive.  The cygwin and mingw ports of <code class="command">ld</code> have specific
support for creating such libraries provided with the
&lsquo;<samp class="samp">--out-implib</samp>&rsquo; command-line option.
</p>
</dd>
<dt><em class="emph">Resource only DLLs</em></dt>
<dd><p>It is possible to create a DLL that only contains resources, ie just a
&lsquo;<samp class="samp">.rsrc</samp>&rsquo; section, but in order to do so a custom linker script
must be used.  This is because the built-in default linker scripts
will always create &lsquo;<samp class="samp">.text</samp>&rsquo; and &lsquo;<samp class="samp">.idata</samp>&rsquo; sections, even if
there is no input to go into them.
</p>
<p>The script should look like this, although the <code class="code">OUTPUT_FORMAT</code>
should be changed to match the desired format.
</p>
<div class="example">
<pre class="example-preformatted">OUTPUT_FORMAT(pei-i386)
SECTIONS
{
  . = SIZEOF_HEADERS;
  . = ALIGN(__section_alignment__);
  .rsrc __image_base__ + __section_alignment__ : ALIGN(4)
  {
    KEEP (*(.rsrc))
    KEEP (*(.rsrc$*))
  }
  /DISCARD/ : { *(*) }
}
</pre></div>

<p>With this script saved to a file called, eg <samp class="file">rsrc.ld</samp>, a command
line like this can be used to create the resource only DLL
<samp class="file">rsrc.dll</samp> from an input file called <samp class="file">rsrc.o</samp>:
</p>
<div class="example smallexample">
<pre class="example-preformatted">ld -dll --subsystem windows -e 0 -s rsrc.o -o rsrc.dll -T rsrc.ld
</pre></div>

</dd>
<dt id='index-exporting-DLL-symbols'><span><em class="emph">exporting DLL symbols</em><a class="copiable-link" href='#index-exporting-DLL-symbols'> &para;</a></span></dt>
<dd><p>The cygwin/mingw <code class="command">ld</code> has several ways to export symbols for dll&rsquo;s.
</p>
<dl class="table">
<dt id='index-using-auto_002dexport-functionality'><span><em class="emph">using auto-export functionality</em><a class="copiable-link" href='#index-using-auto_002dexport-functionality'> &para;</a></span></dt>
<dd><p>By default <code class="command">ld</code> exports symbols with the auto-export functionality,
which is controlled by the following command-line options:
</p>
<ul class="itemize mark-bullet">
<li>&ndash;export-all-symbols   [This is the default]
</li><li>&ndash;exclude-symbols
</li><li>&ndash;exclude-libs
</li><li>&ndash;exclude-modules-for-implib
</li><li>&ndash;version-script
</li></ul>

<p>When auto-export is in operation, <code class="command">ld</code> will export all the non-local
(global and common) symbols it finds in a DLL, with the exception of a few
symbols known to belong to the system&rsquo;s runtime and libraries.  As it will
often not be desirable to export all of a DLL&rsquo;s symbols, which may include
private functions that are not part of any public interface, the command-line
options listed above may be used to filter symbols out from the list for
exporting.  The &lsquo;<samp class="samp">--output-def</samp>&rsquo; option can be used in order to see the
final list of exported symbols with all exclusions taken into effect.
</p>
<p>If &lsquo;<samp class="samp">--export-all-symbols</samp>&rsquo; is not given explicitly on the
command line, then the default auto-export behavior will be <em class="emph">disabled</em>
if either of the following are true:
</p>
<ul class="itemize mark-bullet">
<li>A DEF file is used.
</li><li>Any symbol in any object file was marked with the __declspec(dllexport) attribute.
</li></ul>

</dd>
<dt id='index-using-a-DEF-file'><span><em class="emph">using a DEF file</em><a class="copiable-link" href='#index-using-a-DEF-file'> &para;</a></span></dt>
<dd><p>Another way of exporting symbols is using a DEF file.  A DEF file is
an ASCII file containing definitions of symbols which should be
exported when a dll is created.  Usually it is named &lsquo;<samp class="samp">&lt;dll
name&gt;.def</samp>&rsquo; and is added as any other object file to the linker&rsquo;s
command line.  The file&rsquo;s name must end in &lsquo;<samp class="samp">.def</samp>&rsquo; or &lsquo;<samp class="samp">.DEF</samp>&rsquo;.
</p>
<div class="example">
<pre class="example-preformatted">gcc -o &lt;output&gt; &lt;objectfiles&gt; &lt;dll name&gt;.def
</pre></div>

<p>Using a DEF file turns off the normal auto-export behavior, unless the
&lsquo;<samp class="samp">--export-all-symbols</samp>&rsquo; option is also used.
</p>
<p>Here is an example of a DEF file for a shared library called &lsquo;<samp class="samp">xyz.dll</samp>&rsquo;:
</p>
<div class="example">
<pre class="example-preformatted">LIBRARY &quot;xyz.dll&quot; BASE=0x20000000

EXPORTS
foo
bar
_bar = bar
another_foo = abc.dll.afoo
var1 DATA
doo = foo == foo2
eoo DATA == var1
</pre></div>

<p>This example defines a DLL with a non-default base address and seven
symbols in the export table. The third exported symbol <code class="code">_bar</code> is an
alias for the second. The fourth symbol, <code class="code">another_foo</code> is resolved
by &quot;forwarding&quot; to another module and treating it as an alias for
<code class="code">afoo</code> exported from the DLL &lsquo;<samp class="samp">abc.dll</samp>&rsquo;. The final symbol
<code class="code">var1</code> is declared to be a data object. The &lsquo;<samp class="samp">doo</samp>&rsquo; symbol in
export library is an alias of &lsquo;<samp class="samp">foo</samp>&rsquo;, which gets the string name
in export table &lsquo;<samp class="samp">foo2</samp>&rsquo;. The &lsquo;<samp class="samp">eoo</samp>&rsquo; symbol is an data export
symbol, which gets in export table the name &lsquo;<samp class="samp">var1</samp>&rsquo;.
</p>
<p>The optional <code class="code">LIBRARY &lt;name&gt;</code> command indicates the <em class="emph">internal</em>
name of the output DLL. If &lsquo;<samp class="samp">&lt;name&gt;</samp>&rsquo; does not include a suffix,
the default library suffix, &lsquo;<samp class="samp">.DLL</samp>&rsquo; is appended.
</p>
<p>When the .DEF file is used to build an application, rather than a
library, the <code class="code">NAME &lt;name&gt;</code> command should be used instead of
<code class="code">LIBRARY</code>. If &lsquo;<samp class="samp">&lt;name&gt;</samp>&rsquo; does not include a suffix, the default
executable suffix, &lsquo;<samp class="samp">.EXE</samp>&rsquo; is appended.
</p>
<p>With either <code class="code">LIBRARY &lt;name&gt;</code> or <code class="code">NAME &lt;name&gt;</code> the optional
specification <code class="code">BASE = &lt;number&gt;</code> may be used to specify a
non-default base address for the image.
</p>
<p>If neither <code class="code">LIBRARY &lt;name&gt;</code> nor  <code class="code">NAME &lt;name&gt;</code> is specified,
or they specify an empty string, the internal name is the same as the
filename specified on the command line.
</p>
<p>The complete specification of an export symbol is:
</p>
<div class="example">
<pre class="example-preformatted">EXPORTS
  ( (  ( &lt;name1&gt; [ = &lt;name2&gt; ] )
     | ( &lt;name1&gt; = &lt;module-name&gt; . &lt;external-name&gt;))
  [ @ &lt;integer&gt; ] [NONAME] [DATA] [CONSTANT] [PRIVATE] [== &lt;name3&gt;] ) *
</pre></div>

<p>Declares &lsquo;<samp class="samp">&lt;name1&gt;</samp>&rsquo; as an exported symbol from the DLL, or declares
&lsquo;<samp class="samp">&lt;name1&gt;</samp>&rsquo; as an exported alias for &lsquo;<samp class="samp">&lt;name2&gt;</samp>&rsquo;; or declares
&lsquo;<samp class="samp">&lt;name1&gt;</samp>&rsquo; as a &quot;forward&quot; alias for the symbol
&lsquo;<samp class="samp">&lt;external-name&gt;</samp>&rsquo; in the DLL &lsquo;<samp class="samp">&lt;module-name&gt;</samp>&rsquo;.
Optionally, the symbol may be exported by the specified ordinal
&lsquo;<samp class="samp">&lt;integer&gt;</samp>&rsquo; alias. The optional &lsquo;<samp class="samp">&lt;name3&gt;</samp>&rsquo; is the to be used
string in import/export table for the symbol.
</p>
<p>The optional keywords that follow the declaration indicate:
</p>
<p><code class="code">NONAME</code>: Do not put the symbol name in the DLL&rsquo;s export table.  It
will still be exported by its ordinal alias (either the value specified
by the .def specification or, otherwise, the value assigned by the
linker). The symbol name, however, does remain visible in the import
library (if any), unless <code class="code">PRIVATE</code> is also specified.
</p>
<p><code class="code">DATA</code>: The symbol is a variable or object, rather than a function.
The import lib will export only an indirect reference to <code class="code">foo</code> as
the symbol <code class="code">_imp__foo</code> (ie, <code class="code">foo</code> must be resolved as
<code class="code">*_imp__foo</code>).
</p>
<p><code class="code">CONSTANT</code>: Like <code class="code">DATA</code>, but put the undecorated <code class="code">foo</code> as
well as <code class="code">_imp__foo</code> into the import library. Both refer to the
read-only import address table&rsquo;s pointer to the variable, not to the
variable itself. This can be dangerous. If the user code fails to add
the <code class="code">dllimport</code> attribute and also fails to explicitly add the
extra indirection that the use of the attribute enforces, the
application will behave unexpectedly.
</p>
<p><code class="code">PRIVATE</code>: Put the symbol in the DLL&rsquo;s export table, but do not put
it into the static import library used to resolve imports at link time. The
symbol can still be imported using the <code class="code">LoadLibrary/GetProcAddress</code>
API at runtime or by using the GNU ld extension of linking directly to
the DLL without an import library.
</p>
<p>See ld/deffilep.y in the binutils sources for the full specification of
other DEF file statements
</p>
<a class="index-entry-id" id="index-creating-a-DEF-file"></a>
<p>While linking a shared dll, <code class="command">ld</code> is able to create a DEF file
with the &lsquo;<samp class="samp">--output-def &lt;file&gt;</samp>&rsquo; command-line option.
</p>
</dd>
<dt id='index-Using-decorations'><span><em class="emph">Using decorations</em><a class="copiable-link" href='#index-Using-decorations'> &para;</a></span></dt>
<dd><p>Another way of marking symbols for export is to modify the source code
itself, so that when building the DLL each symbol to be exported is
declared as:
</p>
<div class="example">
<pre class="example-preformatted">__declspec(dllexport) int a_variable
__declspec(dllexport) void a_function(int with_args)
</pre></div>

<p>All such symbols will be exported from the DLL.  If, however,
any of the object files in the DLL contain symbols decorated in
this way, then the normal auto-export behavior is disabled, unless
the &lsquo;<samp class="samp">--export-all-symbols</samp>&rsquo; option is also used.
</p>
<p>Note that object files that wish to access these symbols must <em class="emph">not</em>
decorate them with dllexport.  Instead, they should use dllimport,
instead:
</p>
<div class="example">
<pre class="example-preformatted">__declspec(dllimport) int a_variable
__declspec(dllimport) void a_function(int with_args)
</pre></div>

<p>This complicates the structure of library header files, because
when included by the library itself the header must declare the
variables and functions as dllexport, but when included by client
code the header must declare them as dllimport.  There are a number
of idioms that are typically used to do this; often client code can
omit the __declspec() declaration completely.  See
&lsquo;<samp class="samp">--enable-auto-import</samp>&rsquo; and &lsquo;<samp class="samp">automatic data imports</samp>&rsquo; for more
information.
</p></dd>
</dl>

<a class="index-entry-id" id="index-automatic-data-imports"></a>
</dd>
<dt><em class="emph">automatic data imports</em></dt>
<dd><p>The standard Windows dll format supports data imports from dlls only
by adding special decorations (dllimport/dllexport), which let the
compiler produce specific assembler instructions to deal with this
issue.  This increases the effort necessary to port existing Un*x
code to these platforms, especially for large
c++ libraries and applications.  The auto-import feature, which was
initially provided by Paul Sokolovsky, allows one to omit the
decorations to achieve a behavior that conforms to that on POSIX/Un*x
platforms. This feature is enabled with the &lsquo;<samp class="samp">--enable-auto-import</samp>&rsquo;
command-line option, although it is enabled by default on cygwin/mingw.
The &lsquo;<samp class="samp">--enable-auto-import</samp>&rsquo; option itself now serves mainly to
suppress any warnings that are ordinarily emitted when linked objects
trigger the feature&rsquo;s use.
</p>
<p>auto-import of variables does not always work flawlessly without
additional assistance.  Sometimes, you will see this message
</p>
<p>&quot;variable &rsquo;&lt;var&gt;&rsquo; can&rsquo;t be auto-imported. Please read the
documentation for ld&rsquo;s <code class="code">--enable-auto-import</code> for details.&quot;
</p>
<p>The &lsquo;<samp class="samp">--enable-auto-import</samp>&rsquo; documentation explains why this error
occurs, and several methods that can be used to overcome this difficulty.
One of these methods is the <em class="emph">runtime pseudo-relocs</em> feature, described
below.
</p>
<a class="index-entry-id" id="index-runtime-pseudo_002drelocation"></a>
<p>For complex variables imported from DLLs (such as structs or classes),
object files typically contain a base address for the variable and an
offset (<em class="emph">addend</em>) within the variable&ndash;to specify a particular
field or public member, for instance.  Unfortunately, the runtime loader used
in win32 environments is incapable of fixing these references at runtime
without the additional information supplied by dllimport/dllexport decorations.
The standard auto-import feature described above is unable to resolve these
references.
</p>
<p>The &lsquo;<samp class="samp">--enable-runtime-pseudo-relocs</samp>&rsquo; switch allows these references to
be resolved without error, while leaving the task of adjusting the references
themselves (with their non-zero addends) to specialized code provided by the
runtime environment.  Recent versions of the cygwin and mingw environments and
compilers provide this runtime support; older versions do not.  However, the
support is only necessary on the developer&rsquo;s platform; the compiled result will
run without error on an older system.
</p>
<p>&lsquo;<samp class="samp">--enable-runtime-pseudo-relocs</samp>&rsquo; is not the default; it must be explicitly
enabled as needed.
</p>
<a class="index-entry-id" id="index-direct-linking-to-a-dll"></a>
</dd>
<dt><em class="emph">direct linking to a dll</em></dt>
<dd><p>The cygwin/mingw ports of <code class="command">ld</code> support the direct linking,
including data symbols, to a dll without the usage of any import
libraries.  This is much faster and uses much less memory than does the
traditional import library method, especially when linking large
libraries or applications.  When <code class="command">ld</code> creates an import lib, each
function or variable exported from the dll is stored in its own bfd, even
though a single bfd could contain many exports.  The overhead involved in
storing, loading, and processing so many bfd&rsquo;s is quite large, and explains the
tremendous time, memory, and storage needed to link against particularly
large or complex libraries when using import libs.
</p>
<p>Linking directly to a dll uses no extra command-line switches other than
&lsquo;<samp class="samp">-L</samp>&rsquo; and &lsquo;<samp class="samp">-l</samp>&rsquo;, because <code class="command">ld</code> already searches for a number
of names to match each library.  All that is needed from the developer&rsquo;s
perspective is an understanding of this search, in order to force ld to
select the dll instead of an import library.
</p>

<p>For instance, when ld is called with the argument &lsquo;<samp class="samp">-lxxx</samp>&rsquo; it will attempt
to find, in the first directory of its search path,
</p>
<div class="example">
<pre class="example-preformatted">libxxx.dll.a
xxx.dll.a
libxxx.a
xxx.lib
libxxx.lib
cygxxx.dll (*)
libxxx.dll
xxx.dll
</pre></div>

<p>before moving on to the next directory in the search path.
</p>
<p>(*) Actually, this is not &lsquo;<samp class="samp">cygxxx.dll</samp>&rsquo; but in fact is &lsquo;<samp class="samp">&lt;prefix&gt;xxx.dll</samp>&rsquo;,
where &lsquo;<samp class="samp">&lt;prefix&gt;</samp>&rsquo; is set by the <code class="command">ld</code> option
&lsquo;<samp class="samp">--dll-search-prefix=&lt;prefix&gt;</samp>&rsquo;. In the case of cygwin, the standard gcc spec
file includes &lsquo;<samp class="samp">--dll-search-prefix=cyg</samp>&rsquo;, so in effect we actually search for
&lsquo;<samp class="samp">cygxxx.dll</samp>&rsquo;.
</p>
<p>Other win32-based unix environments, such as mingw or pw32, may use other
&lsquo;<samp class="samp">&lt;prefix&gt;</samp>&rsquo;es, although at present only cygwin makes use of this feature.  It
was originally intended to help avoid name conflicts among dll&rsquo;s built for the
various win32/un*x environments, so that (for example) two versions of a zlib dll
could coexist on the same machine.
</p>
<p>The generic cygwin/mingw path layout uses a &lsquo;<samp class="samp">bin</samp>&rsquo; directory for
applications and dll&rsquo;s and a &lsquo;<samp class="samp">lib</samp>&rsquo; directory for the import
libraries (using cygwin nomenclature):
</p>
<div class="example">
<pre class="example-preformatted">bin/
	cygxxx.dll
lib/
	libxxx.dll.a   (in case of dll's)
	libxxx.a       (in case of static archive)
</pre></div>

<p>Linking directly to a dll without using the import library can be
done two ways:
</p>
<p>1. Use the dll directly by adding the &lsquo;<samp class="samp">bin</samp>&rsquo; path to the link line
</p><div class="example">
<pre class="example-preformatted">gcc -Wl,-verbose  -o a.exe -L../bin/ -lxxx
</pre></div>

<p>However, as the dll&rsquo;s often have version numbers appended to their names
(&lsquo;<samp class="samp">cygncurses-5.dll</samp>&rsquo;) this will often fail, unless one specifies
&lsquo;<samp class="samp">-L../bin -lncurses-5</samp>&rsquo; to include the version.  Import libs are generally
not versioned, and do not have this difficulty.
</p>
<p>2. Create a symbolic link from the dll to a file in the &lsquo;<samp class="samp">lib</samp>&rsquo;
directory according to the above mentioned search pattern.  This
should be used to avoid unwanted changes in the tools needed for
making the app/dll.
</p>
<div class="example">
<pre class="example-preformatted">ln -s bin/cygxxx.dll lib/[cyg|lib|]xxx.dll[.a]
</pre></div>

<p>Then you can link without any make environment changes.
</p>
<div class="example">
<pre class="example-preformatted">gcc -Wl,-verbose  -o a.exe -L../lib/ -lxxx
</pre></div>

<p>This technique also avoids the version number problems, because the following is
perfectly legal
</p>
<div class="example">
<pre class="example-preformatted">bin/
	cygxxx-5.dll
lib/
	libxxx.dll.a -&gt; ../bin/cygxxx-5.dll
</pre></div>

<p>Linking directly to a dll without using an import lib will work
even when auto-import features are exercised, and even when
&lsquo;<samp class="samp">--enable-runtime-pseudo-relocs</samp>&rsquo; is used.
</p>
<p>Given the improvements in speed and memory usage, one might justifiably
wonder why import libraries are used at all.  There are three reasons:
</p>
<p>1. Until recently, the link-directly-to-dll functionality did <em class="emph">not</em>
work with auto-imported data.
</p>
<p>2. Sometimes it is necessary to include pure static objects within the
import library (which otherwise contains only bfd&rsquo;s for indirection
symbols that point to the exports of a dll).  Again, the import lib
for the cygwin kernel makes use of this ability, and it is not
possible to do this without an import lib.
</p>
<p>3. Symbol aliases can only be resolved using an import lib.  This is
critical when linking against OS-supplied dll&rsquo;s (eg, the win32 API)
in which symbols are usually exported as undecorated aliases of their
stdcall-decorated assembly names.
</p>
<p>So, import libs are not going away.  But the ability to replace
true import libs with a simple symbolic link to (or a copy of)
a dll, in many cases, is a useful addition to the suite of tools
binutils makes available to the win32 developer.  Given the
massive improvements in memory requirements during linking, storage
requirements, and linking speed, we expect that many developers
will soon begin to use this feature whenever possible.
</p>
</dd>
<dt><em class="emph">symbol aliasing</em></dt>
<dd><dl class="table">
<dt><em class="emph">adding additional names</em></dt>
<dd><p>Sometimes, it is useful to export symbols with additional names.
A symbol &lsquo;<samp class="samp">foo</samp>&rsquo; will be exported as &lsquo;<samp class="samp">foo</samp>&rsquo;, but it can also be
exported as &lsquo;<samp class="samp">_foo</samp>&rsquo; by using special directives in the DEF file
when creating the dll.  This will affect also the optional created
import library.  Consider the following DEF file:
</p>
<div class="example">
<pre class="example-preformatted">LIBRARY &quot;xyz.dll&quot; BASE=0x61000000

EXPORTS
foo
_foo = foo
</pre></div>

<p>The line &lsquo;<samp class="samp">_foo = foo</samp>&rsquo; maps the symbol &lsquo;<samp class="samp">foo</samp>&rsquo; to &lsquo;<samp class="samp">_foo</samp>&rsquo;.
</p>
<p>Another method for creating a symbol alias is to create it in the
source code using the &quot;weak&quot; attribute:
</p>
<div class="example">
<pre class="example-preformatted">void foo () { /* Do something.  */; }
void _foo () __attribute__ ((weak, alias (&quot;foo&quot;)));
</pre></div>

<p>See the gcc manual for more information about attributes and weak
symbols.
</p>
</dd>
<dt><em class="emph">renaming symbols</em></dt>
<dd><p>Sometimes it is useful to rename exports.  For instance, the cygwin
kernel does this regularly.  A symbol &lsquo;<samp class="samp">_foo</samp>&rsquo; can be exported as
&lsquo;<samp class="samp">foo</samp>&rsquo; but not as &lsquo;<samp class="samp">_foo</samp>&rsquo; by using special directives in the
DEF file. (This will also affect the import library, if it is
created).  In the following example:
</p>
<div class="example">
<pre class="example-preformatted">LIBRARY &quot;xyz.dll&quot; BASE=0x61000000

EXPORTS
_foo = foo
</pre></div>

<p>The line &lsquo;<samp class="samp">_foo = foo</samp>&rsquo; maps the exported symbol &lsquo;<samp class="samp">foo</samp>&rsquo; to
&lsquo;<samp class="samp">_foo</samp>&rsquo;.
</p></dd>
</dl>

<p>Note: using a DEF file disables the default auto-export behavior,
unless the &lsquo;<samp class="samp">--export-all-symbols</samp>&rsquo; command-line option is used.
If, however, you are trying to rename symbols, then you should list
<em class="emph">all</em> desired exports in the DEF file, including the symbols
that are not being renamed, and do <em class="emph">not</em> use the
&lsquo;<samp class="samp">--export-all-symbols</samp>&rsquo; option.  If you list only the
renamed symbols in the DEF file, and use &lsquo;<samp class="samp">--export-all-symbols</samp>&rsquo;
to handle the other symbols, then the both the new names <em class="emph">and</em>
the original names for the renamed symbols will be exported.
In effect, you&rsquo;d be aliasing those symbols, not renaming them,
which is probably not what you wanted.
</p>
<a class="index-entry-id" id="index-weak-externals"></a>
</dd>
<dt><em class="emph">weak externals</em></dt>
<dd><p>The Windows object format, PE, specifies a form of weak symbols called
weak externals.  When a weak symbol is linked and the symbol is not
defined, the weak symbol becomes an alias for some other symbol.  There
are three variants of weak externals:
</p><ul class="itemize mark-bullet">
<li>Definition is searched for in objects and libraries, historically
called lazy externals.
</li><li>Definition is searched for only in other objects, not in libraries.
This form is not presently implemented.
</li><li>No search; the symbol is an alias.  This form is not presently
implemented.
</li></ul>
<p>As a GNU extension, weak symbols that do not specify an alternate symbol
are supported.  If the symbol is undefined when linking, the symbol
uses a default value.
</p>
<a class="index-entry-id" id="index-aligned-common-symbols"></a>
</dd>
<dt><em class="emph">aligned common symbols</em></dt>
<dd><p>As a GNU extension to the PE file format, it is possible to specify the
desired alignment for a common symbol.  This information is conveyed from
the assembler or compiler to the linker by means of GNU-specific commands
carried in the object file&rsquo;s &lsquo;<samp class="samp">.drectve</samp>&rsquo; section, which are recognized
by <code class="command">ld</code> and respected when laying out the common symbols.  Native
tools will be able to process object files employing this GNU extension,
but will fail to respect the alignment instructions, and may issue noisy
warnings about unknown linker directives.
</p>
</dd>
</dl>



</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Xtensa.html"><code class="code">ld</code> and Xtensa Processors</a>, Previous: <a href="TI-COFF.html"><code class="command">ld</code>&rsquo;s Support for Various TI COFF Versions</a>, Up: <a href="Machine-Dependent.html">Machine Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
