<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.41.

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>Output Section LMA (LD)</title>

<meta name="description" content="Output Section LMA (LD)">
<meta name="keywords" content="Output Section LMA (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Output-Section-Attributes.html" rel="up" title="Output Section Attributes">
<link href="Forced-Output-Alignment.html" rel="next" title="Forced Output Alignment">
<link href="Output-Section-Type.html" rel="prev" title="Output Section Type">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
ul.mark-bullet {list-style-type: disc}
-->
</style>


</head>

<body lang="en">
<div class="subsubsection-level-extent" id="Output-Section-LMA">
<div class="nav-panel">
<p>
Next: <a href="Forced-Output-Alignment.html" accesskey="n" rel="next">Forced Output Alignment</a>, Previous: <a href="Output-Section-Type.html" accesskey="p" rel="prev">Output Section Type</a>, Up: <a href="Output-Section-Attributes.html" accesskey="u" rel="up">Output Section Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsubsection" id="Output-Section-LMA-1">3.6.8.2 Output Section LMA</h4>
<a class="index-entry-id" id="index-AT_003elma_005fregion"></a>
<a class="index-entry-id" id="index-AT_0028lma_0029"></a>
<a class="index-entry-id" id="index-load-address"></a>
<a class="index-entry-id" id="index-section-load-address"></a>
<p>Every section has a virtual address (VMA) and a load address (LMA); see
<a class="ref" href="Basic-Script-Concepts.html">Basic Linker Script Concepts</a>.  The virtual address is specified by the
see <a class="pxref" href="Output-Section-Address.html">Output Section Address</a> described earlier.  The load address is
specified by the <code class="code">AT</code> or <code class="code">AT&gt;</code> keywords.  Specifying a load
address is optional.
</p>
<p>The <code class="code">AT</code> keyword takes an expression as an argument.  This
specifies the exact load address of the section.  The <code class="code">AT&gt;</code> keyword
takes the name of a memory region as an argument.  See <a class="xref" href="MEMORY.html">MEMORY Command</a>.  The
load address of the section is set to the next free address in the
region, aligned to the section&rsquo;s alignment requirements.
</p>
<p>If neither <code class="code">AT</code> nor <code class="code">AT&gt;</code> is specified for an allocatable
section, the linker will use the following heuristic to determine the
load address:
</p>
<ul class="itemize mark-bullet">
<li>If the section has a specific VMA address, then this is used as
the LMA address as well.

</li><li>If the section is not allocatable then its LMA is set to its VMA.

</li><li>Otherwise if a memory region can be found that is compatible
with the current section, and this region contains at least one
section, then the LMA is set so the difference between the
VMA and LMA is the same as the difference between the VMA and LMA of
the last section in the located region.

</li><li>If no memory regions have been declared then a default region
that covers the entire address space is used in the previous step.

</li><li>If no suitable region could be found, or there was no previous
section then the LMA is set equal to the VMA.
</li></ul>

<a class="index-entry-id" id="index-ROM-initialized-data"></a>
<a class="index-entry-id" id="index-initialized-data-in-ROM"></a>
<p>This feature is designed to make it easy to build a ROM image.  For
example, the following linker script creates three output sections: one
called &lsquo;<samp class="samp">.text</samp>&rsquo;, which starts at <code class="code">0x1000</code>, one called
&lsquo;<samp class="samp">.mdata</samp>&rsquo;, which is loaded at the end of the &lsquo;<samp class="samp">.text</samp>&rsquo; section
even though its VMA is <code class="code">0x2000</code>, and one called &lsquo;<samp class="samp">.bss</samp>&rsquo; to hold
uninitialized data at address <code class="code">0x3000</code>.  The symbol <code class="code">_data</code> is
defined with the value <code class="code">0x2000</code>, which shows that the location
counter holds the VMA value, not the LMA value.
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">SECTIONS
  {
  .text 0x1000 : { *(.text) _etext = . ; }
  .mdata 0x2000 :
    AT ( ADDR (.text) + SIZEOF (.text) )
    { _data = . ; *(.data); _edata = . ;  }
  .bss 0x3000 :
    { _bstart = . ;  *(.bss) *(COMMON) ; _bend = . ;}
}
</pre></div></div>

<p>The run-time initialization code for use with a program generated with
this linker script would include something like the following, to copy
the initialized data from the ROM image to its runtime address.  Notice
how this code takes advantage of the symbols defined by the linker
script.
</p>
<div class="example smallexample">
<div class="group"><pre class="example-preformatted">extern char _etext, _data, _edata, _bstart, _bend;
char *src = &amp;_etext;
char *dst = &amp;_data;

/* ROM has data at end of text; copy it.  */
while (dst &lt; &amp;_edata)
  *dst++ = *src++;

/* Zero bss.  */
for (dst = &amp;_bstart; dst&lt; &amp;_bend; dst++)
  *dst = 0;
</pre></div></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Forced-Output-Alignment.html">Forced Output Alignment</a>, Previous: <a href="Output-Section-Type.html">Output Section Type</a>, Up: <a href="Output-Section-Attributes.html">Output Section Attributes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
