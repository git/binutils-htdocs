<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU linker LD
(GNU Binutils)
version 2.41.

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License". -->
<title>File Commands (LD)</title>

<meta name="description" content="File Commands (LD)">
<meta name="keywords" content="File Commands (LD)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="LD-Index.html" rel="index" title="LD Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Simple-Commands.html" rel="up" title="Simple Commands">
<link href="Format-Commands.html" rel="next" title="Format Commands">
<link href="Entry-Point.html" rel="prev" title="Entry Point">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="File-Commands">
<div class="nav-panel">
<p>
Next: <a href="Format-Commands.html" accesskey="n" rel="next">Commands Dealing with Object File Formats</a>, Previous: <a href="Entry-Point.html" accesskey="p" rel="prev">Setting the Entry Point</a>, Up: <a href="Simple-Commands.html" accesskey="u" rel="up">Simple Linker Script Commands</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Commands-Dealing-with-Files">3.4.2 Commands Dealing with Files</h4>
<a class="index-entry-id" id="index-linker-script-file-commands"></a>
<p>Several linker script commands deal with files.
</p>
<dl class="table">
<dt id='index-INCLUDE-filename'><span><code class="code">INCLUDE <var class="var">filename</var></code><a class="copiable-link" href='#index-INCLUDE-filename'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-including-a-linker-script"></a>
<p>Include the linker script <var class="var">filename</var> at this point.  The file will
be searched for in the current directory, and in any directory specified
with the <samp class="option">-L</samp> option.  You can nest calls to <code class="code">INCLUDE</code> up to
10 levels deep.
</p>
<p>You can place <code class="code">INCLUDE</code> directives at the top level, in <code class="code">MEMORY</code> or
<code class="code">SECTIONS</code> commands, or in output section descriptions.
</p>
</dd>
<dt id='index-INPUT_0028files_0029'><span><code class="code">INPUT(<var class="var">file</var>, <var class="var">file</var>, &hellip;)</code><a class="copiable-link" href='#index-INPUT_0028files_0029'> &para;</a></span></dt>
<dt><code class="code">INPUT(<var class="var">file</var> <var class="var">file</var> &hellip;)</code></dt>
<dd><a class="index-entry-id" id="index-input-files-in-linker-scripts"></a>
<a class="index-entry-id" id="index-input-object-files-in-linker-scripts"></a>
<a class="index-entry-id" id="index-linker-script-input-object-files"></a>
<p>The <code class="code">INPUT</code> command directs the linker to include the named files
in the link, as though they were named on the command line.
</p>
<p>For example, if you always want to include <samp class="file">subr.o</samp> any time you do
a link, but you can&rsquo;t be bothered to put it on every link command line,
then you can put &lsquo;<samp class="samp">INPUT (subr.o)</samp>&rsquo; in your linker script.
</p>
<p>In fact, if you like, you can list all of your input files in the linker
script, and then invoke the linker with nothing but a &lsquo;<samp class="samp">-T</samp>&rsquo; option.
</p>
<p>In case a <em class="dfn">sysroot prefix</em> is configured, and the filename starts
with the &lsquo;<samp class="samp">/</samp>&rsquo; character, and the script being processed was
located inside the <em class="dfn">sysroot prefix</em>, the filename will be looked
for in the <em class="dfn">sysroot prefix</em>.  The <em class="dfn">sysroot prefix</em> can also be forced by specifying
<code class="code">=</code> as the first character in the filename path, or prefixing the
filename path with <code class="code">$SYSROOT</code>. See also the description of
&lsquo;<samp class="samp">-L</samp>&rsquo; in <a class="ref" href="Options.html">Command-line Options</a>.
</p>
<p>If a <em class="dfn">sysroot prefix</em> is not used then the linker will try to open
the file in the directory containing the linker script.  If it is not
found the linker will then search the current directory.  If it is still
not found the linker will search through the archive library search
path.
</p>
<p>If you use &lsquo;<samp class="samp">INPUT (-l<var class="var">file</var>)</samp>&rsquo;, <code class="command">ld</code> will transform the
name to <code class="code">lib<var class="var">file</var>.a</code>, as with the command-line argument
&lsquo;<samp class="samp">-l</samp>&rsquo;.
</p>
<p>When you use the <code class="code">INPUT</code> command in an implicit linker script, the
files will be included in the link at the point at which the linker
script file is included.  This can affect archive searching.
</p>
</dd>
<dt id='index-GROUP_0028files_0029'><span><code class="code">GROUP(<var class="var">file</var>, <var class="var">file</var>, &hellip;)</code><a class="copiable-link" href='#index-GROUP_0028files_0029'> &para;</a></span></dt>
<dt><code class="code">GROUP(<var class="var">file</var> <var class="var">file</var> &hellip;)</code></dt>
<dd><a class="index-entry-id" id="index-grouping-input-files"></a>
<p>The <code class="code">GROUP</code> command is like <code class="code">INPUT</code>, except that the named
files should all be archives, and they are searched repeatedly until no
new undefined references are created.  See the description of &lsquo;<samp class="samp">-(</samp>&rsquo;
in <a class="ref" href="Options.html">Command-line Options</a>.
</p>
</dd>
<dt id='index-AS_005fNEEDED_0028files_0029'><span><code class="code">AS_NEEDED(<var class="var">file</var>, <var class="var">file</var>, &hellip;)</code><a class="copiable-link" href='#index-AS_005fNEEDED_0028files_0029'> &para;</a></span></dt>
<dt><code class="code">AS_NEEDED(<var class="var">file</var> <var class="var">file</var> &hellip;)</code></dt>
<dd><p>This construct can appear only inside of the <code class="code">INPUT</code> or <code class="code">GROUP</code>
commands, among other filenames.  The files listed will be handled
as if they appear directly in the <code class="code">INPUT</code> or <code class="code">GROUP</code> commands,
with the exception of ELF shared libraries, that will be added only
when they are actually needed.  This construct essentially enables
<samp class="option">--as-needed</samp> option for all the files listed inside of it
and restores previous <samp class="option">--as-needed</samp> resp. <samp class="option">--no-as-needed</samp>
setting afterwards.
</p>
</dd>
<dt id='index-OUTPUT_0028filename_0029'><span><code class="code">OUTPUT(<var class="var">filename</var>)</code><a class="copiable-link" href='#index-OUTPUT_0028filename_0029'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-output-file-name-in-linker-script"></a>
<p>The <code class="code">OUTPUT</code> command names the output file.  Using
<code class="code">OUTPUT(<var class="var">filename</var>)</code> in the linker script is exactly like using
&lsquo;<samp class="samp">-o <var class="var">filename</var></samp>&rsquo; on the command line (see <a class="pxref" href="Options.html">Command
Line Options</a>).  If both are used, the command-line option takes
precedence.
</p>
<p>You can use the <code class="code">OUTPUT</code> command to define a default name for the
output file other than the usual default of <samp class="file">a.out</samp>.
</p>
</dd>
<dt id='index-SEARCH_005fDIR_0028path_0029'><span><code class="code">SEARCH_DIR(<var class="var">path</var>)</code><a class="copiable-link" href='#index-SEARCH_005fDIR_0028path_0029'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-library-search-path-in-linker-script"></a>
<a class="index-entry-id" id="index-archive-search-path-in-linker-script"></a>
<a class="index-entry-id" id="index-search-path-in-linker-script"></a>
<p>The <code class="code">SEARCH_DIR</code> command adds <var class="var">path</var> to the list of paths where
<code class="command">ld</code> looks for archive libraries.  Using
<code class="code">SEARCH_DIR(<var class="var">path</var>)</code> is exactly like using &lsquo;<samp class="samp">-L <var class="var">path</var></samp>&rsquo;
on the command line (see <a class="pxref" href="Options.html">Command-line Options</a>).  If both
are used, then the linker will search both paths.  Paths specified using
the command-line option are searched first.
</p>
</dd>
<dt id='index-STARTUP_0028filename_0029'><span><code class="code">STARTUP(<var class="var">filename</var>)</code><a class="copiable-link" href='#index-STARTUP_0028filename_0029'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-first-input-file"></a>
<p>The <code class="code">STARTUP</code> command is just like the <code class="code">INPUT</code> command, except
that <var class="var">filename</var> will become the first input file to be linked, as
though it were specified first on the command line.  This may be useful
when using a system in which the entry point is always the start of the
first file.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Format-Commands.html">Commands Dealing with Object File Formats</a>, Previous: <a href="Entry-Point.html">Setting the Entry Point</a>, Up: <a href="Simple-Commands.html">Simple Linker Script Commands</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="LD-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
