<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>M32R-Directives (Using as)</title>

<meta name="description" content="M32R-Directives (Using as)">
<meta name="keywords" content="M32R-Directives (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="M32R_002dDependent.html" rel="up" title="M32R-Dependent">
<link href="M32R_002dWarnings.html" rel="next" title="M32R-Warnings">
<link href="M32R_002dOpts.html" rel="prev" title="M32R-Opts">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="M32R_002dDirectives">
<div class="nav-panel">
<p>
Next: <a href="M32R_002dWarnings.html" accesskey="n" rel="next">M32R Warnings</a>, Previous: <a href="M32R_002dOpts.html" accesskey="p" rel="prev">M32R Options</a>, Up: <a href="M32R_002dDependent.html" accesskey="u" rel="up">M32R Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="M32R-Directives">9.21.2 M32R Directives</h4>
<a class="index-entry-id" id="index-directives_002c-M32R"></a>
<a class="index-entry-id" id="index-M32R-directives"></a>

<p>The Renesas M32R version of <code class="code">as</code> has a few architecture
specific directives:
</p>
<dl class="table">
<dd>
<a class="index-entry-id" id="index-low-directive_002c-M32R"></a>
</dd>
<dt><code class="code">low <var class="var">expression</var></code></dt>
<dd><p>The <code class="code">low</code> directive computes the value of its expression and
places the lower 16-bits of the result into the immediate-field of the
instruction.  For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">   or3   r0, r0, #low(0x12345678) ; compute r0 = r0 | 0x5678
   add3, r0, r0, #low(fred)   ; compute r0 = r0 + low 16-bits of address of fred
</pre></div>

</dd>
<dt id='index-high-directive_002c-M32R'><span><code class="code">high <var class="var">expression</var></code><a class="copiable-link" href='#index-high-directive_002c-M32R'> &para;</a></span></dt>
<dd><p>The <code class="code">high</code> directive computes the value of its expression and
places the upper 16-bits of the result into the immediate-field of the
instruction.  For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">   seth  r0, #high(0x12345678) ; compute r0 = 0x12340000
   seth, r0, #high(fred)       ; compute r0 = upper 16-bits of address of fred
</pre></div>

</dd>
<dt id='index-shigh-directive_002c-M32R'><span><code class="code">shigh <var class="var">expression</var></code><a class="copiable-link" href='#index-shigh-directive_002c-M32R'> &para;</a></span></dt>
<dd><p>The <code class="code">shigh</code> directive is very similar to the <code class="code">high</code>
directive.  It also computes the value of its expression and places
the upper 16-bits of the result into the immediate-field of the
instruction.  The difference is that <code class="code">shigh</code> also checks to see
if the lower 16-bits could be interpreted as a signed number, and if
so it assumes that a borrow will occur from the upper-16 bits.  To
compensate for this the <code class="code">shigh</code> directive pre-biases the upper
16 bit value by adding one to it.  For example:
</p>
<p>For example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">   seth  r0, #shigh(0x12345678) ; compute r0 = 0x12340000
   seth  r0, #shigh(0x00008000) ; compute r0 = 0x00010000
</pre></div>

<p>In the second example the lower 16-bits are 0x8000.  If these are
treated as a signed value and sign extended to 32-bits then the value
becomes 0xffff8000.  If this value is then added to 0x00010000 then
the result is 0x00008000.
</p>
<p>This behaviour is to allow for the different semantics of the
<code class="code">or3</code> and <code class="code">add3</code> instructions.  The <code class="code">or3</code> instruction
treats its 16-bit immediate argument as unsigned whereas the
<code class="code">add3</code> treats its 16-bit immediate as a signed value.  So for
example:
</p>
<div class="example smallexample">
<pre class="example-preformatted">   seth  r0, #shigh(0x00008000)
   add3  r0, r0, #low(0x00008000)
</pre></div>

<p>Produces the correct result in r0, whereas:
</p>
<div class="example smallexample">
<pre class="example-preformatted">   seth  r0, #shigh(0x00008000)
   or3   r0, r0, #low(0x00008000)
</pre></div>

<p>Stores 0xffff8000 into r0.
</p>
<p>Note - the <code class="code">shigh</code> directive does not know where in the assembly
source code the lower 16-bits of the value are going set, so it cannot
check to make sure that an <code class="code">or3</code> instruction is being used rather
than an <code class="code">add3</code> instruction.  It is up to the programmer to make
sure that correct directives are used.
</p>
<a class="index-entry-id" id="index-_002em32r-directive_002c-M32R"></a>
</dd>
<dt><code class="code">.m32r</code></dt>
<dd><p>The directive performs a similar thing as the <em class="emph">-m32r</em> command
line option.  It tells the assembler to only accept M32R instructions
from now on.  An instructions from later M32R architectures are
refused.
</p>
<a class="index-entry-id" id="index-_002em32rx-directive_002c-M32RX"></a>
</dd>
<dt><code class="code">.m32rx</code></dt>
<dd><p>The directive performs a similar thing as the <em class="emph">-m32rx</em> command
line option.  It tells the assembler to start accepting the extra
instructions in the M32RX ISA as well as the ordinary M32R ISA.
</p>
<a class="index-entry-id" id="index-_002em32r2-directive_002c-M32R2"></a>
</dd>
<dt><code class="code">.m32r2</code></dt>
<dd><p>The directive performs a similar thing as the <em class="emph">-m32r2</em> command
line option.  It tells the assembler to start accepting the extra
instructions in the M32R2 ISA as well as the ordinary M32R ISA.
</p>
<a class="index-entry-id" id="index-_002elittle-directive_002c-M32RX"></a>
</dd>
<dt><code class="code">.little</code></dt>
<dd><p>The directive performs a similar thing as the <em class="emph">-little</em> command
line option.  It tells the assembler to start producing little-endian
code and data.  This option should be used with care as producing
mixed-endian binary files is fraught with danger.
</p>
<a class="index-entry-id" id="index-_002ebig-directive_002c-M32RX"></a>
</dd>
<dt><code class="code">.big</code></dt>
<dd><p>The directive performs a similar thing as the <em class="emph">-big</em> command
line option.  It tells the assembler to start producing big-endian
code and data.  This option should be used with care as producing
mixed-endian binary files is fraught with danger.
</p>
</dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="M32R_002dWarnings.html">M32R Warnings</a>, Previous: <a href="M32R_002dOpts.html">M32R Options</a>, Up: <a href="M32R_002dDependent.html">M32R Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
