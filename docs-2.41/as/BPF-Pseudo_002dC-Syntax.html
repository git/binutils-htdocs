<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>BPF Pseudo-C Syntax (Using as)</title>

<meta name="description" content="BPF Pseudo-C Syntax (Using as)">
<meta name="keywords" content="BPF Pseudo-C Syntax (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="BPF_002dDependent.html" rel="up" title="BPF-Dependent">
<link href="BPF-Opcodes.html" rel="prev" title="BPF Opcodes">


</head>

<body lang="en">
<div class="subsection-level-extent" id="BPF-Pseudo_002dC-Syntax">
<div class="nav-panel">
<p>
Previous: <a href="BPF-Opcodes.html" accesskey="p" rel="prev">Opcodes</a>, Up: <a href="BPF_002dDependent.html" accesskey="u" rel="up">BPF Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="BPF-Pseudo_002dC-Syntax-1">9.7.5 BPF Pseudo-C Syntax</h4>

<p>This assembler supports another syntax to denote BPF instructions,
which is an alternative to the normal looking syntax documented above.
This alternatative syntax, which we call <em class="dfn">pseudo-C syntax</em>, is
supported by the LLVM/clang integrated assembler.
</p>
<p>This syntax is very unconventional, but we need to support it in order
to support inline assembly in existing BPF programs.
</p>
<p>Note that the assembler is able to parse sources in which both
syntaxes coexist: some instructions can use the usual assembly like
syntax, whereas some other instructions in the same file can use the
pseudo-C syntax.
</p>
<ul class="mini-toc">
<li><a href="#Pseudo_002dC-Register-Names" accesskey="1">Pseudo-C Register Names</a></li>
<li><a href="#Arithmetic-instructions-1" accesskey="2">Arithmetic instructions</a></li>
<li><a href="#Endianness-conversion-instructions-1" accesskey="3">Endianness conversion instructions</a></li>
<li><a href="#g_t64_002dbit-load-and-pseudo-maps-1" accesskey="4">64-bit load and pseudo maps</a></li>
<li><a href="#Load-instructions-for-socket-filters-1" accesskey="5">Load instructions for socket filters</a></li>
<li><a href="#Generic-load_002fstore-instructions-1" accesskey="6">Generic load/store instructions</a></li>
<li><a href="#Jump-instructions-1" accesskey="7">Jump instructions</a></li>
<li><a href="#Atomic-instructions-1" accesskey="8">Atomic instructions</a></li>
</ul>
<div class="subsubsection-level-extent" id="Pseudo_002dC-Register-Names">
<h4 class="subsubsection">9.7.5.1 Pseudo-C Register Names</h4>

<p>All BPF registers are 64-bit long.  However, in the Pseudo-C syntax
registers can be referred using different names, which actually
reflect the kind of instruction they appear on:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">r0..r9</samp>&rsquo;</dt>
<dd><p>General-purpose register in an instruction that operates on its value
as if it was a 64-bit value.
</p></dd>
<dt>&lsquo;<samp class="samp">w0..w9</samp>&rsquo;</dt>
<dd><p>General-purpose register in an instruction that operates on its value
as if it was a 32-bit value.
</p></dd>
</dl>

<p>Note that in the Pseudo-C syntax register names are not preceded by
<code class="code">%</code> characters.
</p>
</div>
<div class="subsubsection-level-extent" id="Arithmetic-instructions-1">
<h4 class="subsubsection">9.7.5.2 Arithmetic instructions</h4>

<p>In all the instructions below, the operations are 64-bit or 32-bit
depending on the names used to refer to the registers.  For example
<code class="code">r3 += r2</code> will perform 64-bit addition, whereas <code class="code">w3 += w2</code>
will perform 32-bit addition.  Mixing register prefixes is an error,
for example <code class="code">r3 += w2</code>.
</p>
<dl class="table">
<dt><code class="code">dst_reg += (imm32|src_reg)</code></dt>
<dd><p>Arithmetic addition.
</p></dd>
<dt><code class="code">dst_reg -= (imm32|src_reg)</code></dt>
<dd><p>Arithmetic subtraction.
</p></dd>
<dt><code class="code">dst_reg *= (imm32|src_reg)</code></dt>
<dd><p>Arithmetic multiplication.
</p></dd>
<dt><code class="code">dst_reg /= (imm32|src_reg)</code></dt>
<dd><p>Arithmetic integer unsigned division.
</p></dd>
<dt><code class="code">dst_reg %= (imm32|src_reg)</code></dt>
<dd><p>Arithmetic integer unsigned remainder.
</p></dd>
<dt><code class="code">dst_reg &amp;= (imm32|src_reg)</code></dt>
<dd><p>Bit-wise &ldquo;and&rdquo; operation.
</p></dd>
<dt><code class="code">dst_reg |= (imm32|src_reg)</code></dt>
<dd><p>Bit-wise &ldquo;or&rdquo; operation.
</p></dd>
<dt><code class="code">dst_reg ^= (imm32|src_reg)</code></dt>
<dd><p>Bit-wise exclusive-or operation.
</p></dd>
<dt><code class="code">dst_reg &lt;&lt;= (imm32|src_reg)</code></dt>
<dd><p>Left shift, by whatever specified number of bits.
</p></dd>
<dt><code class="code">dst_reg &gt;&gt;= (imm32|src_reg)</code></dt>
<dd><p>Right logical shift, by whatever specified number of bits.
</p></dd>
<dt><code class="code">dst_reg s&gt;&gt;= (imm32|src_reg)</code></dt>
<dd><p>Right arithmetic shift, by whatever specified number of bits.
</p></dd>
<dt><code class="code">dst_reg = (imm32|src_reg)</code></dt>
<dd><p>Move the value in <code class="code">imm32</code> or <code class="code">src_reg</code> in <code class="code">dst_reg</code>.
</p></dd>
<dt><code class="code">dst_reg = -dst_reg</code></dt>
<dd><p>Arithmetic negation.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="Endianness-conversion-instructions-1">
<h4 class="subsubsection">9.7.5.3 Endianness conversion instructions</h4>

<dl class="table">
<dt><code class="code">dst_reg = le16 src_reg</code></dt>
<dd><p>Convert the 16-bit value in <code class="code">src_reg</code> to little-endian.
</p></dd>
<dt><code class="code">dst_reg = le32 src_reg</code></dt>
<dd><p>Convert the 32-bit value in <code class="code">src_reg</code> to little-endian.
</p></dd>
<dt><code class="code">dst_reg = le64 src_reg</code></dt>
<dd><p>Convert the 64-bit value in <code class="code">src_reg</code> to little-endian.
</p></dd>
<dt><code class="code">dst_reg = be16 src_reg</code></dt>
<dd><p>Convert the 16-bit value in <code class="code">src_reg</code> to big-endian.
</p></dd>
<dt><code class="code">dst_reg = be32 src_reg</code></dt>
<dd><p>Convert the 32-bit value in <code class="code">src_reg</code> to big-endian.
</p></dd>
<dt><code class="code">dst_reg = be64 src_reg</code></dt>
<dd><p>Convert the 64-bit value in <code class="code">src_reg</code> to big-endian.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="g_t64_002dbit-load-and-pseudo-maps-1">
<h4 class="subsubsection">9.7.5.4 64-bit load and pseudo maps</h4>

<dl class="table">
<dt><code class="code">dst_reg = imm64 ll</code></dt>
<dd><p>Load the given signed 64-bit immediate, or pseudo map descriptor, to
the destination register <code class="code">dst_reg</code>.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="Load-instructions-for-socket-filters-1">
<h4 class="subsubsection">9.7.5.5 Load instructions for socket filters</h4>

<dl class="table">
<dt><code class="code">r0 = *(u8 *)skb[imm32]</code></dt>
<dd><p>Absolute 8-bit load.
</p></dd>
<dt><code class="code">r0 = *(u16 *)skb[imm32]</code></dt>
<dd><p>Absolute 16-bit load.
</p></dd>
<dt><code class="code">r0 = *(u32 *)skb[imm32]</code></dt>
<dd><p>Absolute 32-bit load.
</p></dd>
<dt><code class="code">r0 = *(u64 *)skb[imm32]</code></dt>
<dd><p>Absolute 64-bit load.
</p></dd>
<dt><code class="code">r0 = *(u8 *)skb[src_reg + imm32]</code></dt>
<dd><p>Indirect 8-bit load.
</p></dd>
<dt><code class="code">r0 = *(u16 *)skb[src_reg + imm32]</code></dt>
<dd><p>Indirect 16-bit load.
</p></dd>
<dt><code class="code">r0 = *(u32 *)skb[src_reg + imm32]</code></dt>
<dd><p>Indirect 32-bit load.
</p></dd>
<dt><code class="code">r0 = *(u64 *)skb[src_reg + imm32]</code></dt>
<dd><p>Indirect 64-bit load.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="Generic-load_002fstore-instructions-1">
<h4 class="subsubsection">9.7.5.6 Generic load/store instructions</h4>

<dl class="table">
<dt><code class="code">dst_reg = *(u8 *)(src_reg + offset16)</code></dt>
<dd><p>Generic 8-bit load.
</p></dd>
<dt><code class="code">dst_reg = *(u16 *)(src_reg + offset16)</code></dt>
<dd><p>Generic 16-bit load.
</p></dd>
<dt><code class="code">dst_reg = *(u32 *)(src_reg + offset16)</code></dt>
<dd><p>Generic 32-bit load.
</p></dd>
<dt><code class="code">dst_reg = *(u64 *)(src_reg + offset16)</code></dt>
<dd><p>Generic 64-bit load.
</p></dd>
<dt><code class="code">*(u8 *)(dst_reg + offset16) = src_reg</code></dt>
<dd><p>Generic 8-bit store.
</p></dd>
<dt><code class="code">*(u16 *)(dst_reg + offset16) = src_reg</code></dt>
<dd><p>Generic 16-bit store.
</p></dd>
<dt><code class="code">*(u32 *)(dst_reg + offset16) = src_reg</code></dt>
<dd><p>Generic 32-bit store.
</p></dd>
<dt><code class="code">*(u64 *)(dst_reg + offset16) = src_reg</code></dt>
<dd><p>Generic 64-bit store.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="Jump-instructions-1">
<h4 class="subsubsection">9.7.5.7 Jump instructions</h4>

<dl class="table">
<dt><code class="code">goto disp16</code></dt>
<dd><p>Jump-always.
</p></dd>
<dt><code class="code">if dst_reg == (imm32|src_reg) goto disp16</code></dt>
<dd><p>Jump if equal.
</p></dd>
<dt><code class="code">if dst_reg &amp; (imm32|src_reg) goto disp16</code></dt>
<dd><p>Jump if signed equal.
</p></dd>
<dt><code class="code">if dst_reg != (imm32|src_reg) goto disp16</code></dt>
<dd><p>Jump if not equal.
</p></dd>
<dt><code class="code">if dst_reg &gt; (imm32|src_reg) goto disp16</code></dt>
<dd><p>Jump if bigger, unsigned.
</p></dd>
<dt><code class="code">if dst_reg &lt; (imm32|src_reg) goto disp16</code></dt>
<dd><p>Jump if smaller, unsigned.
</p></dd>
<dt><code class="code">if dst_reg &gt;= (imm32|src_reg) goto disp16</code></dt>
<dd><p>Jump if bigger or equal, unsigned.
</p></dd>
<dt><code class="code">if dst_reg &lt;= (imm32|src_reg) goto disp16</code></dt>
<dd><p>Jump if smaller or equal, unsigned.
</p></dd>
<dt><code class="code">if dst_reg s&gt; (imm32|src_reg) goto disp16</code></dt>
<dd><p>Jump if bigger, signed.
</p></dd>
<dt><code class="code">if dst_reg s&lt; (imm32|src_reg) goto disp16</code></dt>
<dd><p>Jump if smaller, signed.
</p></dd>
<dt><code class="code">if dst_reg s&gt;= (imm32|src_reg) goto disp16</code></dt>
<dd><p>Jump if bigger or equal, signed.
</p></dd>
<dt><code class="code">if dst_reg s&lt;= (imm32|src_reg) goto disp16</code></dt>
<dd><p>Jump if smaller or equal, signed.
</p></dd>
<dt><code class="code">call imm32</code></dt>
<dd><p>Jump and link.
</p></dd>
<dt><code class="code">exit</code></dt>
<dd><p>Terminate the eBPF program.
</p></dd>
</dl>

</div>
<div class="subsubsection-level-extent" id="Atomic-instructions-1">
<h4 class="subsubsection">9.7.5.8 Atomic instructions</h4>

<dl class="table">
<dt><code class="code">lock *(u64 *)(dst_reg + offset16) += src_reg</code></dt>
<dd><p>Exchange-and-add a 64-bit value at the specified location.
</p></dd>
<dt><code class="code">lock *(u32 *)(dst_reg + offset16) += src_reg</code></dt>
<dd><p>Exchange-and-add a 32-bit value at the specified location.
</p></dd>
</dl>


</div>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="BPF-Opcodes.html">Opcodes</a>, Up: <a href="BPF_002dDependent.html">BPF Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
