<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Macro (Using as)</title>

<meta name="description" content="Macro (Using as)">
<meta name="keywords" content="Macro (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Pseudo-Ops.html" rel="up" title="Pseudo Ops">
<link href="MRI.html" rel="next" title="MRI">
<link href="Long.html" rel="prev" title="Long">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Macro">
<div class="nav-panel">
<p>
Next: <a href="MRI.html" accesskey="n" rel="next"><code class="code">.mri <var class="var">val</var></code></a>, Previous: <a href="Long.html" accesskey="p" rel="prev"><code class="code">.long <var class="var">expressions</var></code></a>, Up: <a href="Pseudo-Ops.html" accesskey="u" rel="up">Assembler Directives</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="g_t_002emacro">7.63 <code class="code">.macro</code></h3>

<a class="index-entry-id" id="index-macros"></a>
<p>The commands <code class="code">.macro</code> and <code class="code">.endm</code> allow you to define macros that
generate assembly output.  For example, this definition specifies a macro
<code class="code">sum</code> that puts a sequence of numbers into memory:
</p>
<div class="example">
<pre class="example-preformatted">        .macro  sum from=0, to=5
        .long   \from
        .if     \to-\from
        sum     &quot;(\from+1)&quot;,\to
        .endif
        .endm
</pre></div>

<p>With that definition, &lsquo;<samp class="samp">SUM 0,5</samp>&rsquo; is equivalent to this assembly input:
</p>
<div class="example">
<pre class="example-preformatted">        .long   0
        .long   1
        .long   2
        .long   3
        .long   4
        .long   5
</pre></div>

<dl class="ftable">
<dt id='index-_002emacro-macname'><span><code class="code">.macro <var class="var">macname</var></code><a class="copiable-link" href='#index-_002emacro-macname'> &para;</a></span></dt>
<dt id='index-_002emacro-macname-macargs-_2026'><span><code class="code">.macro <var class="var">macname</var> <var class="var">macargs</var> &hellip;</code><a class="copiable-link" href='#index-_002emacro-macname-macargs-_2026'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-macro-directive"></a>
<p>Begin the definition of a macro called <var class="var">macname</var>.  If your macro
definition requires arguments, specify their names after the macro name,
separated by commas or spaces.  You can qualify the macro argument to
indicate whether all invocations must specify a non-blank value (through
&lsquo;<samp class="samp">:<code class="code">req</code></samp>&rsquo;), or whether it takes all of the remaining arguments
(through &lsquo;<samp class="samp">:<code class="code">vararg</code></samp>&rsquo;).  You can supply a default value for any
macro argument by following the name with &lsquo;<samp class="samp">=<var class="var">deflt</var></samp>&rsquo;.  You
cannot define two macros with the same <var class="var">macname</var> unless it has been
subject to the <code class="code">.purgem</code> directive (see <a class="pxref" href="Purgem.html"><code class="code">.purgem <var class="var">name</var></code></a>) between the two
definitions.  For example, these are all valid <code class="code">.macro</code> statements:
</p>
<dl class="table">
<dt><code class="code">.macro comm</code></dt>
<dd><p>Begin the definition of a macro called <code class="code">comm</code>, which takes no
arguments.
</p>
</dd>
<dt><code class="code">.macro plus1 p, p1</code></dt>
<dt><code class="code">.macro plus1 p p1</code></dt>
<dd><p>Either statement begins the definition of a macro called <code class="code">plus1</code>,
which takes two arguments; within the macro definition, write
&lsquo;<samp class="samp">\p</samp>&rsquo; or &lsquo;<samp class="samp">\p1</samp>&rsquo; to evaluate the arguments.
</p>
</dd>
<dt><code class="code">.macro reserve_str p1=0 p2</code></dt>
<dd><p>Begin the definition of a macro called <code class="code">reserve_str</code>, with two
arguments.  The first argument has a default value, but not the second.
After the definition is complete, you can call the macro either as
&lsquo;<samp class="samp">reserve_str <var class="var">a</var>,<var class="var">b</var></samp>&rsquo; (with &lsquo;<samp class="samp">\p1</samp>&rsquo; evaluating to
<var class="var">a</var> and &lsquo;<samp class="samp">\p2</samp>&rsquo; evaluating to <var class="var">b</var>), or as &lsquo;<samp class="samp">reserve_str
,<var class="var">b</var></samp>&rsquo; (with &lsquo;<samp class="samp">\p1</samp>&rsquo; evaluating as the default, in this case
&lsquo;<samp class="samp">0</samp>&rsquo;, and &lsquo;<samp class="samp">\p2</samp>&rsquo; evaluating to <var class="var">b</var>).
</p>
</dd>
<dt><code class="code">.macro m p1:req, p2=0, p3:vararg</code></dt>
<dd><p>Begin the definition of a macro called <code class="code">m</code>, with at least three
arguments.  The first argument must always have a value specified, but
not the second, which instead has a default value. The third formal
will get assigned all remaining arguments specified at invocation time.
</p>
<p>When you call a macro, you can specify the argument values either by
position, or by keyword.  For example, &lsquo;<samp class="samp">sum 9,17</samp>&rsquo; is equivalent to
&lsquo;<samp class="samp">sum to=17, from=9</samp>&rsquo;.
</p>
</dd>
</dl>

<p>Note that since each of the <var class="var">macargs</var> can be an identifier exactly
as any other one permitted by the target architecture, there may be
occasional problems if the target hand-crafts special meanings to certain
characters when they occur in a special position.  For example, if the colon
(<code class="code">:</code>) is generally permitted to be part of a symbol name, but the
architecture specific code special-cases it when occurring as the final
character of a symbol (to denote a label), then the macro parameter
replacement code will have no way of knowing that and consider the whole
construct (including the colon) an identifier, and check only this
identifier for being the subject to parameter substitution.  So for example
this macro definition:
</p>
<div class="example">
<pre class="example-preformatted">	.macro label l
\l:
	.endm
</pre></div>

<p>might not work as expected.  Invoking &lsquo;<samp class="samp">label foo</samp>&rsquo; might not create a label
called &lsquo;<samp class="samp">foo</samp>&rsquo; but instead just insert the text &lsquo;<samp class="samp">\l:</samp>&rsquo; into the
assembler source, probably generating an error about an unrecognised
identifier.
</p>
<p>Similarly problems might occur with the period character (&lsquo;<samp class="samp">.</samp>&rsquo;)
which is often allowed inside opcode names (and hence identifier names).  So
for example constructing a macro to build an opcode from a base name and a
length specifier like this:
</p>
<div class="example">
<pre class="example-preformatted">	.macro opcode base length
        \base.\length
	.endm
</pre></div>

<p>and invoking it as &lsquo;<samp class="samp">opcode store l</samp>&rsquo; will not create a &lsquo;<samp class="samp">store.l</samp>&rsquo;
instruction but instead generate some kind of error as the assembler tries to
interpret the text &lsquo;<samp class="samp">\base.\length</samp>&rsquo;.
</p>
<p>There are several possible ways around this problem:
</p>
<dl class="table">
<dt><code class="code">Insert white space</code></dt>
<dd><p>If it is possible to use white space characters then this is the simplest
solution.  eg:
</p>
<div class="example">
<pre class="example-preformatted">	.macro label l
\l :
	.endm
</pre></div>

</dd>
<dt><code class="code">Use &lsquo;<samp class="samp">\()</samp>&rsquo;</code></dt>
<dd><p>The string &lsquo;<samp class="samp">\()</samp>&rsquo; can be used to separate the end of a macro argument from
the following text.  eg:
</p>
<div class="example">
<pre class="example-preformatted">	.macro opcode base length
        \base\().\length
	.endm
</pre></div>

</dd>
<dt><code class="code">Use the alternate macro syntax mode</code></dt>
<dd><p>In the alternative macro syntax mode the ampersand character (&lsquo;<samp class="samp">&amp;</samp>&rsquo;) can be
used as a separator.  eg:
</p>
<div class="example">
<pre class="example-preformatted">	.altmacro
	.macro label l
l&amp;:
	.endm
</pre></div>
</dd>
</dl>

<p>Note: this problem of correctly identifying string parameters to pseudo ops
also applies to the identifiers used in <code class="code">.irp</code> (see <a class="pxref" href="Irp.html"><code class="code">.irp <var class="var">symbol</var>,<var class="var">values</var></code>&hellip;</a>)
and <code class="code">.irpc</code> (see <a class="pxref" href="Irpc.html"><code class="code">.irpc <var class="var">symbol</var>,<var class="var">values</var></code>&hellip;</a>) as well.
</p>
<p>Another issue can occur with the actual arguments passed during macro
invocation: Multiple arguments can be separated by blanks or commas.  To have
arguments actually contain blanks or commas (or potentially other non-alpha-
numeric characters), individual arguments will need to be enclosed in either
parentheses <code class="code">()</code>, square brackets <code class="code">[]</code>, or double quote <code class="code">&quot;</code>
characters.  The latter may be the only viable option in certain situations,
as only double quotes are actually stripped while establishing arguments.  It
may be important to be aware of two escaping models used when processing such
quoted argument strings: For one two adjacent double quotes represent a single
double quote in the resulting argument, going along the lines of the stripping
of the enclosing quotes.  But then double quotes can also be escaped by a
backslash <code class="code">\</code>, but this backslash will not be retained in the resulting
actual argument as then seen / used while expanding the macro.
</p>
<p>As a consequence to the first of these escaping mechanisms two string literals
intended to be representing separate macro arguments need to be separated by
white space (or, better yet, by a comma).  To state it differently, such
adjacent string literals - even if separated only by a blank - will not be
concatenated when determining macro arguments, even if they&rsquo;re only separated
by white space.  This is unlike certain other pseudo ops, e.g. <code class="code">.ascii</code>.
</p>
</dd>
<dt id='index-_002eendm'><span><code class="code">.endm</code><a class="copiable-link" href='#index-_002eendm'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-endm-directive"></a>
<p>Mark the end of a macro definition.
</p>
</dd>
<dt id='index-_002eexitm'><span><code class="code">.exitm</code><a class="copiable-link" href='#index-_002eexitm'> &para;</a></span></dt>
<dd><a class="index-entry-id" id="index-exitm-directive"></a>
<p>Exit early from the current macro definition.
</p>
<a class="index-entry-id" id="index-number-of-macros-executed"></a>
<a class="index-entry-id" id="index-macros_002c-count-executed"></a>
</dd>
<dt id='index-_005c_0040'><span><code class="code">\@</code><a class="copiable-link" href='#index-_005c_0040'> &para;</a></span></dt>
<dd><p><code class="command">as</code> maintains a counter of how many macros it has
executed in this pseudo-variable; you can copy that number to your
output with &lsquo;<samp class="samp">\@</samp>&rsquo;, but <em class="emph">only within a macro definition</em>.
</p>
</dd>
<dt id='index-LOCAL-name-_005b-_002c-_2026-_005d-1'><span><code class="code">LOCAL <var class="var">name</var> [ , &hellip; ]</code><a class="copiable-link" href='#index-LOCAL-name-_005b-_002c-_2026-_005d-1'> &para;</a></span></dt>
<dd><p><em class="emph">Warning: <code class="code">LOCAL</code> is only available if you select &ldquo;alternate
macro syntax&rdquo; with &lsquo;<samp class="samp">--alternate</samp>&rsquo; or <code class="code">.altmacro</code>.</em>
See <a class="xref" href="Altmacro.html"><code class="code">.altmacro</code></a>.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="MRI.html"><code class="code">.mri <var class="var">val</var></code></a>, Previous: <a href="Long.html"><code class="code">.long <var class="var">expressions</var></code></a>, Up: <a href="Pseudo-Ops.html">Assembler Directives</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
