<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>CR16 Operand Qualifiers (Using as)</title>

<meta name="description" content="CR16 Operand Qualifiers (Using as)">
<meta name="keywords" content="CR16 Operand Qualifiers (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="CR16_002dDependent.html" rel="up" title="CR16-Dependent">
<link href="CR16-Syntax.html" rel="next" title="CR16 Syntax">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="CR16-Operand-Qualifiers">
<div class="nav-panel">
<p>
Next: <a href="CR16-Syntax.html" accesskey="n" rel="next">CR16 Syntax</a>, Up: <a href="CR16_002dDependent.html" accesskey="u" rel="up">CR16 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="CR16-Operand-Qualifiers-1">9.8.1 CR16 Operand Qualifiers</h4>
<a class="index-entry-id" id="index-CR16-Operand-Qualifiers"></a>

<p>The National Semiconductor CR16 target of <code class="code">as</code> has a few machine dependent operand qualifiers.
</p>
<p>Operand expression type qualifier is an optional field in the instruction operand, to determines the type of the expression field of an operand. The <code class="code">@</code> is required. CR16 architecture uses one of the following expression qualifiers:
</p>
<dl class="table">
<dt><code class="code">s</code></dt>
<dd><p>- <code class="code">Specifies expression operand type as small</code>
</p></dd>
<dt><code class="code">m</code></dt>
<dd><p>- <code class="code">Specifies expression operand type as medium</code>
</p></dd>
<dt><code class="code">l</code></dt>
<dd><p>- <code class="code">Specifies expression operand type as large</code>
</p></dd>
<dt><code class="code">c</code></dt>
<dd><p>- <code class="code">Specifies the CR16 Assembler generates a relocation entry for the operand, where pc has implied bit, the expression is adjusted accordingly. The linker uses the relocation entry to update the operand address at link time.</code>
</p></dd>
<dt><code class="code">got/GOT</code></dt>
<dd><p>- <code class="code">Specifies the CR16 Assembler generates a relocation entry for the operand, offset from Global Offset Table. The linker uses this relocation entry to update the operand address at link time</code>
</p></dd>
<dt><code class="code">cgot/cGOT</code></dt>
<dd><p>- <code class="code">Specifies the CompactRISC Assembler generates a relocation entry for the operand, where pc has implied bit, the expression is adjusted accordingly. The linker uses the relocation entry to update the operand address at link time.</code>
</p></dd>
</dl>

<p>CR16 target operand qualifiers and its size (in bits):
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">Immediate Operand: s</samp>&rsquo;</dt>
<dd><p>4 bits.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Immediate Operand: m</samp>&rsquo;</dt>
<dd><p>16 bits, for movb and movw instructions.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Immediate Operand: m</samp>&rsquo;</dt>
<dd><p>20 bits, movd instructions.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Immediate Operand: l</samp>&rsquo;</dt>
<dd><p>32 bits.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Absolute Operand: s</samp>&rsquo;</dt>
<dd><p>Illegal specifier for this operand.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Absolute Operand: m</samp>&rsquo;</dt>
<dd><p>20 bits, movd instructions.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Displacement Operand: s</samp>&rsquo;</dt>
<dd><p>8 bits.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Displacement Operand: m</samp>&rsquo;</dt>
<dd><p>16 bits.
</p>
</dd>
<dt>&lsquo;<samp class="samp">Displacement Operand: l</samp>&rsquo;</dt>
<dd><p>24 bits.
</p>
</dd>
</dl>

<p>For example:
</p><div class="example">
<pre class="example-preformatted">1   <code class="code">movw $_myfun@c,r1</code>

    This loads the address of _myfun, shifted right by 1, into r1.

2   <code class="code">movd $_myfun@c,(r2,r1)</code>

    This loads the address of _myfun, shifted right by 1, into register-pair r2-r1.

3   <code class="code">_myfun_ptr:</code>
    <code class="code">.long _myfun@c</code>
    <code class="code">loadd _myfun_ptr, (r1,r0)</code>
    <code class="code">jal (r1,r0)</code>

    This .long directive, the address of _myfunc, shifted right by 1 at link time.

4   <code class="code">loadd  _data1@GOT(r12), (r1,r0)</code>

    This loads the address of _data1, into global offset table (ie GOT) and its offset value from GOT loads into register-pair r2-r1.

5   <code class="code">loadd  _myfunc@cGOT(r12), (r1,r0)</code>

    This loads the address of _myfun, shifted right by 1, into global offset table (ie GOT) and its offset value from GOT loads into register-pair r1-r0.
</pre></div>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="CR16-Syntax.html">CR16 Syntax</a>, Up: <a href="CR16_002dDependent.html">CR16 Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
