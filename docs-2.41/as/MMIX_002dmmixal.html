<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>MMIX-mmixal (Using as)</title>

<meta name="description" content="MMIX-mmixal (Using as)">
<meta name="keywords" content="MMIX-mmixal (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="MMIX_002dDependent.html" rel="up" title="MMIX-Dependent">
<link href="MMIX_002dSyntax.html" rel="prev" title="MMIX-Syntax">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="subsection-level-extent" id="MMIX_002dmmixal">
<div class="nav-panel">
<p>
Previous: <a href="MMIX_002dSyntax.html" accesskey="p" rel="prev">Syntax</a>, Up: <a href="MMIX_002dDependent.html" accesskey="u" rel="up">MMIX Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection section-level-set-subsection" id="Differences-to-mmixal">9.28.4 Differences to <code class="code">mmixal</code></h4>
<a class="index-entry-id" id="index-mmixal-differences"></a>
<a class="index-entry-id" id="index-differences_002c-mmixal"></a>

<p>The binutils <code class="code">as</code> and <code class="code">ld</code> combination has a few
differences in function compared to <code class="code">mmixal</code> (see <a class="pxref" href="MMIX_002dSyntax.html#mmixsite">mmixsite</a>).
</p>
<p>The replacement of a symbol with a GREG-allocated register
(see <a class="pxref" href="MMIX_002dPseudos.html#GREG_002dbase">GREG-base</a>) is not handled the exactly same way in
<code class="code">as</code> as in <code class="code">mmixal</code>.  This is apparent in the
<code class="code">mmixal</code> example file <code class="code">inout.mms</code>, where different registers
with different offsets, eventually yielding the same address, are used in
the first instruction.  This type of difference should however not affect
the function of any program unless it has specific assumptions about the
allocated register number.
</p>
<p>Line numbers (in the &lsquo;<samp class="samp">mmo</samp>&rsquo; object format) are currently not
supported.
</p>
<p>Expression operator precedence is not that of mmixal: operator precedence
is that of the C programming language.  It&rsquo;s recommended to use
parentheses to explicitly specify wanted operator precedence whenever more
than one type of operators are used.
</p>
<p>The serialize unary operator <code class="code">&amp;</code>, the fractional division operator
&lsquo;<samp class="samp">//</samp>&rsquo;, the logical not operator <code class="code">!</code> and the modulus operator
&lsquo;<samp class="samp">%</samp>&rsquo; are not available.
</p>
<p>Symbols are not global by default, unless the option
&lsquo;<samp class="samp">--globalize-symbols</samp>&rsquo; is passed.  Use the &lsquo;<samp class="samp">.global</samp>&rsquo; directive to
globalize symbols (see <a class="pxref" href="Global.html"><code class="code">.global <var class="var">symbol</var></code>, <code class="code">.globl <var class="var">symbol</var></code></a>).
</p>
<p>Operand syntax is a bit stricter with <code class="code">as</code> than
<code class="code">mmixal</code>.  For example, you can&rsquo;t say <code class="code">addu 1,2,3</code>, instead you
must write <code class="code">addu $1,$2,3</code>.
</p>
<p>You can&rsquo;t LOC to a lower address than those already visited
(i.e., &ldquo;backwards&rdquo;).
</p>
<p>A LOC directive must come before any emitted code.
</p>
<p>Predefined symbols are visible as file-local symbols after use.  (In the
ELF file, that is&mdash;the linked mmo file has no notion of a file-local
symbol.)
</p>
<p>Some mapping of constant expressions to sections in LOC expressions is
attempted, but that functionality is easily confused and should be avoided
unless compatibility with <code class="code">mmixal</code> is required.  A LOC expression to
&lsquo;<samp class="samp">0x2000000000000000</samp>&rsquo; or higher, maps to the &lsquo;<samp class="samp">.data</samp>&rsquo; section and
lower addresses map to the &lsquo;<samp class="samp">.text</samp>&rsquo; section (see <a class="pxref" href="MMIX_002dPseudos.html#MMIX_002dloc">MMIX-loc</a>).
</p>
<p>The code and data areas are each contiguous.  Sparse programs with
far-away LOC directives will take up the same amount of space as a
contiguous program with zeros filled in the gaps between the LOC
directives.  If you need sparse programs, you might try and get the wanted
effect with a linker script and splitting up the code parts into sections
(see <a class="pxref" href="Section.html"><code class="code">.section <var class="var">name</var></code></a>).  Assembly code for this, to be compatible with
<code class="code">mmixal</code>, would look something like:
</p><div class="example smallexample">
<pre class="example-preformatted"> .if 0
 LOC away_expression
 .else
 .section away,&quot;ax&quot;
 .fi
</pre></div>
<p><code class="code">as</code> will not execute the LOC directive and <code class="code">mmixal</code>
ignores the lines with <code class="code">.</code>.  This construct can be used generally to
help compatibility.
</p>
<p>Symbols can&rsquo;t be defined twice&ndash;not even to the same value.
</p>
<p>Instruction mnemonics are recognized case-insensitive, though the
&lsquo;<samp class="samp">IS</samp>&rsquo; and &lsquo;<samp class="samp">GREG</samp>&rsquo; pseudo-operations must be specified in
upper-case characters.
</p>
<p>There&rsquo;s no unicode support.
</p>
<p>The following is a list of programs in &lsquo;<samp class="samp">mmix.tar.gz</samp>&rsquo;, available at
<a class="url" href="http://www-cs-faculty.stanford.edu/~knuth/mmix-news.html">http://www-cs-faculty.stanford.edu/~knuth/mmix-news.html</a>, last
checked with the version dated 2001-08-25 (md5sum
c393470cfc86fac040487d22d2bf0172) that assemble with <code class="code">mmixal</code> but do
not assemble with <code class="code">as</code>:
</p>
<dl class="table">
<dt><code class="code">silly.mms</code></dt>
<dd><p>LOC to a previous address.
</p></dd>
<dt><code class="code">sim.mms</code></dt>
<dd><p>Redefines symbol &lsquo;<samp class="samp">Done</samp>&rsquo;.
</p></dd>
<dt><code class="code">test.mms</code></dt>
<dd><p>Uses the serial operator &lsquo;<samp class="samp">&amp;</samp>&rsquo;.
</p></dd>
</dl>

</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="MMIX_002dSyntax.html">Syntax</a>, Up: <a href="MMIX_002dDependent.html">MMIX Dependent Features</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
