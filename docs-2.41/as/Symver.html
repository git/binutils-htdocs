<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- This file documents the GNU Assembler "as".

Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>Symver (Using as)</title>

<meta name="description" content="Symver (Using as)">
<meta name="keywords" content="Symver (Using as)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="AS-Index.html" rel="index" title="AS Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Pseudo-Ops.html" rel="up" title="Pseudo Ops">
<link href="Tag.html" rel="next" title="Tag">
<link href="SubSection.html" rel="prev" title="SubSection">
<style type="text/css">
<!--
div.example {margin-left: 3.2em}
-->
</style>


</head>

<body lang="en">
<div class="section-level-extent" id="Symver">
<div class="nav-panel">
<p>
Next: <a href="Tag.html" accesskey="n" rel="next"><code class="code">.tag <var class="var">structname</var></code></a>, Previous: <a href="SubSection.html" accesskey="p" rel="prev"><code class="code">.subsection <var class="var">name</var></code></a>, Up: <a href="Pseudo-Ops.html" accesskey="u" rel="up">Assembler Directives</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="g_t_002esymver">7.97 <code class="code">.symver</code></h3>
<a class="index-entry-id" id="index-symver-directive"></a>
<a class="index-entry-id" id="index-symbol-versioning"></a>
<a class="index-entry-id" id="index-versions-of-symbols"></a>
<p>Use the <code class="code">.symver</code> directive to bind symbols to specific version nodes
within a source file.  This is only supported on ELF platforms, and is
typically used when assembling files to be linked into a shared library.
There are cases where it may make sense to use this in objects to be bound
into an application itself so as to override a versioned symbol from a
shared library.
</p>
<p>For ELF targets, the <code class="code">.symver</code> directive can be used like this:
</p><div class="example smallexample">
<pre class="example-preformatted">.symver <var class="var">name</var>, <var class="var">name2@nodename</var>[ ,<var class="var">visibility</var>]
</pre></div>
<p>If the original symbol <var class="var">name</var> is defined within the file
being assembled, the <code class="code">.symver</code> directive effectively creates a symbol
alias with the name <var class="var">name2@nodename</var>, and in fact the main reason that we
just don&rsquo;t try and create a regular alias is that the <var class="var">@</var> character isn&rsquo;t
permitted in symbol names.  The <var class="var">name2</var> part of the name is the actual name
of the symbol by which it will be externally referenced.  The name <var class="var">name</var>
itself is merely a name of convenience that is used so that it is possible to
have definitions for multiple versions of a function within a single source
file, and so that the compiler can unambiguously know which version of a
function is being mentioned.  The <var class="var">nodename</var> portion of the alias should be
the name of a node specified in the version script supplied to the linker when
building a shared library.  If you are attempting to override a versioned
symbol from a shared library, then <var class="var">nodename</var> should correspond to the
nodename of the symbol you are trying to override.  The optional argument
<var class="var">visibility</var> updates the visibility of the original symbol.  The valid
visibilities are <code class="code">local</code>, <code class="code">hidden</code>, and <code class="code">remove</code>.  The
<code class="code">local</code> visibility makes the original symbol a local symbol
(see <a class="pxref" href="Local.html"><code class="code">.local <var class="var">names</var></code></a>).  The <code class="code">hidden</code> visibility sets the visibility of the
original symbol to <code class="code">hidden</code> (see <a class="pxref" href="Hidden.html"><code class="code">.hidden <var class="var">names</var></code></a>).  The <code class="code">remove</code>
visibility removes the original symbol from the symbol table.  If visibility
isn&rsquo;t specified, the original symbol is unchanged.
</p>
<p>If the symbol <var class="var">name</var> is not defined within the file being assembled, all
references to <var class="var">name</var> will be changed to <var class="var">name2@nodename</var>.  If no
reference to <var class="var">name</var> is made, <var class="var">name2@nodename</var> will be removed from the
symbol table.
</p>
<p>Another usage of the <code class="code">.symver</code> directive is:
</p><div class="example smallexample">
<pre class="example-preformatted">.symver <var class="var">name</var>, <var class="var">name2@@nodename</var>
</pre></div>
<p>In this case, the symbol <var class="var">name</var> must exist and be defined within
the file being assembled. It is similar to <var class="var">name2@nodename</var>. The
difference is <var class="var">name2@@nodename</var> will also be used to resolve
references to <var class="var">name2</var> by the linker.
</p>
<p>The third usage of the <code class="code">.symver</code> directive is:
</p><div class="example smallexample">
<pre class="example-preformatted">.symver <var class="var">name</var>, <var class="var">name2@@@nodename</var>
</pre></div>
<p>When <var class="var">name</var> is not defined within the
file being assembled, it is treated as <var class="var">name2@nodename</var>. When
<var class="var">name</var> is defined within the file being assembled, the symbol
name, <var class="var">name</var>, will be changed to <var class="var">name2@@nodename</var>.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Tag.html"><code class="code">.tag <var class="var">structname</var></code></a>, Previous: <a href="SubSection.html"><code class="code">.subsection <var class="var">name</var></code></a>, Up: <a href="Pseudo-Ops.html">Assembler Directives</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="AS-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
