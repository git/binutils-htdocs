<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.0.2, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Copyright © 1991-2023 Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no Front-Cover Texts, and with no
Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
 -->
<title>addr2line (GNU Binary Utilities)</title>

<meta name="description" content="addr2line (GNU Binary Utilities)">
<meta name="keywords" content="addr2line (GNU Binary Utilities)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Binutils-Index.html" rel="index" title="Binutils Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="windmc.html" rel="next" title="windmc">
<link href="c_002b_002bfilt.html" rel="prev" title="c++filt">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>


</head>

<body lang="en">
<div class="chapter-level-extent" id="addr2line">
<div class="nav-panel">
<p>
Next: <a href="windmc.html" accesskey="n" rel="next">windmc</a>, Previous: <a href="c_002b_002bfilt.html" accesskey="p" rel="prev">c++filt</a>, Up: <a href="index.html" accesskey="u" rel="up">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h2 class="chapter" id="addr2line-1">10 addr2line</h2>

<a class="index-entry-id" id="index-addr2line"></a>
<a class="index-entry-id" id="index-address-to-file-name-and-line-number"></a>


<div class="example smallexample">
<pre class="example-preformatted">addr2line [<samp class="option">-a</samp>|<samp class="option">--addresses</samp>]
          [<samp class="option">-b</samp> <var class="var">bfdname</var>|<samp class="option">--target=</samp><var class="var">bfdname</var>]
          [<samp class="option">-C</samp>|<samp class="option">--demangle</samp>[=<var class="var">style</var>]]
          [<samp class="option">-r</samp>|<samp class="option">--no-recurse-limit</samp>]
          [<samp class="option">-R</samp>|<samp class="option">--recurse-limit</samp>]
          [<samp class="option">-e</samp> <var class="var">filename</var>|<samp class="option">--exe=</samp><var class="var">filename</var>]
          [<samp class="option">-f</samp>|<samp class="option">--functions</samp>] [<samp class="option">-s</samp>|<samp class="option">--basename</samp>]
          [<samp class="option">-i</samp>|<samp class="option">--inlines</samp>]
          [<samp class="option">-p</samp>|<samp class="option">--pretty-print</samp>]
          [<samp class="option">-j</samp>|<samp class="option">--section=</samp><var class="var">name</var>]
          [<samp class="option">-H</samp>|<samp class="option">--help</samp>] [<samp class="option">-V</samp>|<samp class="option">--version</samp>]
          [addr addr &hellip;]
</pre></div>


<p><code class="command">addr2line</code> translates addresses or symbol+offset into file names and line numbers.
Given an address or symbol+offset in an executable or an offset in a section of a relocatable
object, it uses the debugging information to figure out which file name and
line number are associated with it.
</p>
<p>The executable or relocatable object to use is specified with the <samp class="option">-e</samp>
option.  The default is the file <samp class="file">a.out</samp>.  The section in the relocatable
object to use is specified with the <samp class="option">-j</samp> option.
</p>
<p><code class="command">addr2line</code> has two modes of operation.
</p>
<p>In the first, hexadecimal addresses or symbol+offset are specified on the command line,
and <code class="command">addr2line</code> displays the file name and line number for each
address.
</p>
<p>In the second, <code class="command">addr2line</code> reads hexadecimal addresses or symbol+offset from
standard input, and prints the file name and line number for each
address on standard output.  In this mode, <code class="command">addr2line</code> may be used
in a pipe to convert dynamically chosen addresses.
</p>
<p>The format of the output is &lsquo;<samp class="samp">FILENAME:LINENO</samp>&rsquo;.  By default
each input address generates one line of output.
</p>
<p>Two options can generate additional lines before each
&lsquo;<samp class="samp">FILENAME:LINENO</samp>&rsquo; line (in that order).
</p>
<p>If the <samp class="option">-a</samp> option is used then a line with the input address
is displayed.
</p>
<p>If the <samp class="option">-f</samp> option is used, then a line with the
&lsquo;<samp class="samp">FUNCTIONNAME</samp>&rsquo; is displayed.  This is the name of the function
containing the address.
</p>
<p>One option can generate additional lines after the
&lsquo;<samp class="samp">FILENAME:LINENO</samp>&rsquo; line.
</p>
<p>If the <samp class="option">-i</samp> option is used and the code at the given address is
present there because of inlining by the compiler then additional
lines are displayed afterwards.  One or two extra lines (if the
<samp class="option">-f</samp> option is used) are displayed for each inlined function.
</p>
<p>Alternatively if the <samp class="option">-p</samp> option is used then each input
address generates a single, long, output line containing the address,
the function name, the file name and the line number.  If the
<samp class="option">-i</samp> option has also been used then any inlined functions will
be displayed in the same manner, but on separate lines, and prefixed
by the text &lsquo;<samp class="samp">(inlined by)</samp>&rsquo;.
</p>
<p>If the file name or function name can not be determined,
<code class="command">addr2line</code> will print two question marks in their place.  If the
line number can not be determined, <code class="command">addr2line</code> will print 0.
</p>
<p>When symbol+offset is used, +offset is optional, except when the symbol
is ambigious with a hex number. The resolved symbols can be mangled
or unmangled, except unmangled symbols with + are not allowed.
</p>


<p>The long and short forms of options, shown here as alternatives, are
equivalent.
</p>
<dl class="table">
<dt><code class="env">-a</code></dt>
<dt><code class="env">--addresses</code></dt>
<dd><p>Display the address before the function name, file and line number
information.  The address is printed with a &lsquo;<samp class="samp">0x</samp>&rsquo; prefix to easily
identify it.
</p>
</dd>
<dt id='index-object-code-format-4'><span><code class="env">-b <var class="var">bfdname</var></code><a class="copiable-link" href='#index-object-code-format-4'> &para;</a></span></dt>
<dt><code class="env">--target=<var class="var">bfdname</var></code></dt>
<dd><p>Specify that the object-code format for the object files is
<var class="var">bfdname</var>.
</p>
</dd>
<dt id='index-demangling-in-objdump-1'><span><code class="env">-C</code><a class="copiable-link" href='#index-demangling-in-objdump-1'> &para;</a></span></dt>
<dt><code class="env">--demangle[=<var class="var">style</var>]</code></dt>
<dd><p>Decode (<em class="dfn">demangle</em>) low-level symbol names into user-level names.
Besides removing any initial underscore prepended by the system, this
makes C++ function names readable.  Different compilers have different
mangling styles. The optional demangling style argument can be used to
choose an appropriate demangling style for your compiler. See <a class="xref" href="c_002b_002bfilt.html">c++filt</a>,
for more information on demangling.
</p>
</dd>
<dt><code class="env">-e <var class="var">filename</var></code></dt>
<dt><code class="env">--exe=<var class="var">filename</var></code></dt>
<dd><p>Specify the name of the executable for which addresses should be
translated.  The default file is <samp class="file">a.out</samp>.
</p>
</dd>
<dt><code class="env">-f</code></dt>
<dt><code class="env">--functions</code></dt>
<dd><p>Display function names as well as file and line number information.
</p>
</dd>
<dt><code class="env">-s</code></dt>
<dt><code class="env">--basenames</code></dt>
<dd><p>Display only the base of each file name.
</p>
</dd>
<dt><code class="env">-i</code></dt>
<dt><code class="env">--inlines</code></dt>
<dd><p>If the address belongs to a function that was inlined, the source
information for all enclosing scopes back to the first non-inlined
function will also be printed.  For example, if <code class="code">main</code> inlines
<code class="code">callee1</code> which inlines <code class="code">callee2</code>, and address is from
<code class="code">callee2</code>, the source information for <code class="code">callee1</code> and <code class="code">main</code>
will also be printed.
</p>
</dd>
<dt><code class="env">-j</code></dt>
<dt><code class="env">--section</code></dt>
<dd><p>Read offsets relative to the specified section instead of absolute addresses.
</p>
</dd>
<dt><code class="env">-p</code></dt>
<dt><code class="env">--pretty-print</code></dt>
<dd><p>Make the output more human friendly: each location are printed on one line.
If option <samp class="option">-i</samp> is specified, lines for all enclosing scopes are
prefixed with &lsquo;<samp class="samp">(inlined by)</samp>&rsquo;.
</p>
</dd>
<dt><code class="env">-r</code></dt>
<dt><code class="env">-R</code></dt>
<dt><code class="env">--recurse-limit</code></dt>
<dt><code class="env">--no-recurse-limit</code></dt>
<dt><code class="env">--recursion-limit</code></dt>
<dt><code class="env">--no-recursion-limit</code></dt>
<dd><p>Enables or disables a limit on the amount of recursion performed
whilst demangling strings.  Since the name mangling formats allow for
an infinite level of recursion it is possible to create strings whose
decoding will exhaust the amount of stack space available on the host
machine, triggering a memory fault.  The limit tries to prevent this
from happening by restricting recursion to 2048 levels of nesting.
</p>
<p>The default is for this limit to be enabled, but disabling it may be
necessary in order to demangle truly complicated names.  Note however
that if the recursion limit is disabled then stack exhaustion is
possible and any bug reports about such an event will be rejected.
</p>
<p>The <samp class="option">-r</samp> option is a synonym for the
<samp class="option">--no-recurse-limit</samp> option.  The <samp class="option">-R</samp> option is a
synonym for the <samp class="option">--recurse-limit</samp> option.
</p>
<p>Note this option is only effective if the <samp class="option">-C</samp> or
<samp class="option">--demangle</samp> option has been enabled.
</p>
</dd>
</dl>



</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="windmc.html">windmc</a>, Previous: <a href="c_002b_002bfilt.html">c++filt</a>, Up: <a href="index.html">Introduction</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binutils-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
